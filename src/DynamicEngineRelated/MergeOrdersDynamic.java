package DynamicEngineRelated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import Data.Order;

//stops = Integer.parseInt(tokens[3]);
//sumAllOrder = stops + totalCar * 2 + 1;

public class MergeOrdersDynamic {
	int totalGroup;
	int totalCar;
	int stops;
	int points;
	int timeVioCost;

	public HashMap<Integer, List<Integer>> initialMap = new HashMap<Integer, List<Integer>>();
	public HashMap<Integer, Integer> reverseMap = new HashMap<Integer, Integer>();
	Order[] orders;
	Order[] newOrders;

	public HashMap<Integer, List<Order>> orderMap = new HashMap<Integer, List<Order>>();

	int newOrderSize = 0;
	int sumTrip = 0;
	ArrayList<Integer> newServiceTime = new ArrayList<Integer>();
	String fileName;
	int maxCapacity = 0;
	int[] fairPriceOrders;
	int globalStartTime = 0;
	List<RemainingTrips> remainingTripsList = new ArrayList<RemainingTrips>();

	public MergeOrdersDynamic(String fileName) throws IOException {
		this.fileName = fileName;
		File file = new File(fileName);

		int cnt = 0;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		int temp = 0;
		int cur = 0;
		double weight = 0;
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// String[] tokens =
				// line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				String[] tokens = line.split(",", -1);

				if (tokens.length == 2 && tokens[0].matches("StartTime")) {
					globalStartTime = Integer.parseInt(tokens[1]);
				}

				if (tokens.length == 5 && tokens[0].matches("overall")) {
					totalGroup = Integer.parseInt(tokens[4]);
					totalCar = Integer.parseInt(tokens[2]);
					stops = Integer.parseInt(tokens[3]);
					points = Integer.parseInt(tokens[4]);
					orders = new Order[stops + 1];
					fairPriceOrders = new int[stops + 1];
				}
				if (tokens.length >= 8 && tokens[0].matches("vehicle") && tokens[1].matches("[0-9]+")) {
					if (Double.parseDouble(tokens[4]) > maxCapacity)
						maxCapacity = (int) Double.parseDouble(tokens[4]);
				}
				if (tokens.length >= 14 && tokens[0].matches("order") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					weight = Double.parseDouble(tokens[3]);

					if (tokens[13].equals("Cancel"))
						continue;
					Order oder = new Order();

					String[] range = tokens[5].substring(1, tokens[5].length() - 1).split(":");
					int from = Math.max(globalStartTime, Integer.parseInt(range[0]));
					int to = Integer.parseInt(range[1]);

					oder.set(Integer.parseInt(tokens[1]), Double.parseDouble(tokens[4]), 0, 0, from, to,
							(int) Double.parseDouble(tokens[7]), Integer.parseInt(tokens[3]));

					oder.setVolume(Double.parseDouble(tokens[6]));

					oder.setOriginLSP(Integer.parseInt(tokens[8]));
					oder.setCustomerId(tokens[10]);
					oder.setCargoType(tokens[11]);
					oder.setDepotPickup(Integer.parseInt(tokens[12]));

					if (!tokens[2].equals("null"))
						oder.setPreDefVehicle(Integer.parseInt(tokens[2]));

					oder.setStatus(tokens[13]);
					oder.setSchedualFlag(tokens[14]);
					if (!orderMap.containsKey(10000 * oder.originLSP + oder.orderLocation)) {
						orderMap.put(10000 * oder.originLSP + oder.orderLocation, new ArrayList<Order>());
						orderMap.get(10000 * oder.originLSP + oder.orderLocation).add(oder);
					} else {
						orderMap.get(10000 * oder.originLSP + oder.orderLocation).add(oder);
					}
					orders[cur] = oder;
					fairPriceOrders[cur] = Integer.parseInt(tokens[9]);

				}
				if (tokens.length == 4 && tokens[0].matches("RemainingTrips")) {
					RemainingTrips rt = new RemainingTrips(Integer.parseInt(tokens[1]), tokens[2], tokens[3]);
					remainingTripsList.add(rt);
				}
			}

		} finally {
			LineIterator.closeQuietly(it);
		}

		cnt = 1;

		for (List<Order> group : orderMap.values()) {

			if (group.size() == 1) {
				initialMap.put(cnt, new ArrayList<Integer>());
				initialMap.get(cnt).add(group.get(0).id);
				cnt++;
			}
			if (group.size() == 2) {
				if (Math.min(group.get(0).upperTime, group.get(1).upperTime)
						- Math.max(group.get(0).lowerTime, group.get(1).lowerTime) > 100
						&& group.get(0).volume + group.get(1).volume < maxCapacity) {
					initialMap.put(cnt, new ArrayList<Integer>());
//					orders[group.get(0).id].serviceTime = Math.max(orders[group.get(0).id].serviceTime,
//							orders[group.get(1).id].serviceTime);
					orders[group.get(0).id].volume += orders[group.get(1).id].volume;
					initialMap.get(cnt).add(group.get(0).id);
					initialMap.get(cnt).add(group.get(1).id);
					cnt++;
				} else {
					initialMap.put(cnt, new ArrayList<Integer>());
					initialMap.get(cnt).add(group.get(0).id);
					cnt++;
					initialMap.put(cnt, new ArrayList<Integer>());
					initialMap.get(cnt).add(group.get(1).id);
					cnt++;
				}
			}
			if (group.size() > 2) {
				// System.err.println("ever happen\t");
				// Collections.sort(group, new Comparator<Order>() {
				// public int compare(Order o1, Order o2) {
				// if (o1.volume == o2.volume)
				// return 0;
				// return o1.volume < o2.volume ? -1 : 1;
				// }
				// });
				// for (Order node : group)
				// System.out.println(node.id);
				// if (Math.min(group.get(0).upperTime, group.get(1).upperTime)
				// - Math.max(group.get(0).lowerTime, group.get(1).lowerTime) > 240
				// && group.get(0).volume + group.get(1).volume < maxCapacity) {
				// initialMap.put(cnt, new ArrayList<Integer>());
				// orders[group.get(0).id-1].serviceTime+=orders[group.get(1).id-1].serviceTime;
				// orders[group.get(0).id-1].volume+=orders[group.get(1).id-1].volume;
				// initialMap.get(cnt).add(group.get(0).id);
				// }
				for (int i = 0; i < group.size(); i++) {
					initialMap.put(cnt, new ArrayList<Integer>());
					initialMap.get(cnt).add(group.get(i).id);
					cnt++;
				}
			}
			// cnt++;

		}

		outPut(file);
	}

	public void outPut(File file) throws IOException {
		String[] extensionRemoved = this.fileName.split("\\.");
		String newFile = extensionRemoved[0] + "new." + extensionRemoved[1];
		PrintWriter out = new PrintWriter(newFile);
		int cnt = 0;
		int newCnt = 1;
		int size = 0;
		int vehicleCnt = 1;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		newOrderSize = initialMap.size();

		System.err.println(initialMap);
		for (Integer u : initialMap.keySet()) {
			for (Integer v : initialMap.get(u))
				reverseMap.put(v, u);
		}
		System.err.println(reverseMap);
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// String[] tokens =
				// line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				String[] tokens = line.split(",", -1);
				if (tokens.length == 5 && tokens[0].matches("overall")) {
					out.println("overall," + tokens[1] + "," + totalCar + "," + newOrderSize + "," + tokens[4]);
				} else if (tokens.length >= 8 && tokens[0].matches("vehicle") && tokens[1].matches("[0-9]+")) {
					out.println(line);
				} else if ((!tokens[0].matches("trip")) && (!tokens[0].matches("order"))
						&& (!tokens[0].matches("vehicle") && !tokens[0].matches("RemainingTrips"))) {
					out.println(line);
				}
			}

			for (RemainingTrips rt : remainingTripsList) {
				String newString = "[";
				String[] parts = rt.path.split(";");
				String[] newPart = parts[0].split("\\[");
				Integer tempValue = 0;
				if (!newPart[1].contains("depLoc")) {
					tempValue = Integer.parseInt(newPart[1]);
					if (reverseMap.containsKey(tempValue)) {
						tempValue = reverseMap.get(tempValue);
						newString += tempValue;
					}
				} else {
					newString += newPart[1];
				}

				for (int i = 1; i < parts.length - 1; i++) {
					if (!parts[i].contains("depLoc")) {
						tempValue = Integer.parseInt(parts[i]);
						if (reverseMap.containsKey(tempValue)) {
							tempValue = reverseMap.get(tempValue);
							newString += ";" + tempValue;
						}
					} else {
						newString += ";" + parts[i];
					}
				}
				if (parts.length > 1) {
					newPart = parts[(parts.length - 1)].split("\\]");
					if (!newPart[0].contains("depLoc")) {
						tempValue = Integer.parseInt(newPart[0]);
						if (reverseMap.containsKey(tempValue)) {
							tempValue = reverseMap.get(tempValue);
							newString += ";" + tempValue;
						}
					} else {
						newString += ";" + newPart[0];
					}
				}
				newString += "]";
				out.println("RemainingTrips," + rt.vehicleId + "," + newString+","+rt.startLocation);
			}

		} finally {
			LineIterator.closeQuietly(it);
		}

		cnt = 0;
		int temp = 0;
		for (Integer node : initialMap.keySet()) {
			cnt++;
			temp = initialMap.get(node).get(0);
			out.println("order," + cnt + "," + orders[temp].specialVehicle + "," + orders[temp].orderLocation + "," + 0
					+ ",[" + orders[temp].lowerTime + ":" + orders[temp].upperTime + "]," + orders[temp].volume + ","
					+ orders[temp].serviceTime + "," + orders[temp].originLSP + "," + fairPriceOrders[temp] + ","
					+ orders[temp].customerId + "," + orders[temp].cargoType + "," + orders[temp].pickupDepotLocation
					+ "," + orders[initialMap.get(node).get(0)].deliveryStatus + ","
					+ orders[initialMap.get(node).get(0)].schedualFlag);
		}
		// System.err.println(initialMap);
		out.close();

	}
}
