package DynamicEngineRelated;

import java.util.Collections;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import Data.Order;
import Data.PathSolution;
import DynamicEngineRelated.DynamicBasicData;
import MasterModel.CommonFunction;
import MasterModel.LspMixedALNS;
import PreProcessor.LSP;

public class DynamicMain {
	public HashMap<Integer, Vector<Vector<Integer>>> avalibleTimeSlotsMall = new HashMap<Integer, Vector<Vector<Integer>>>();
	public HashMap<Vector<Integer>, Integer> initialRoutes = new HashMap<Vector<Integer>, Integer>();
	public ArrayList<ArrayList<Order>> bestUsedRoute = new ArrayList<ArrayList<Order>>();
	public Vector<Vector<Integer>> bestFeasiRoute = new Vector<Vector<Integer>>();

	public boolean check = true;
	public boolean shuffleBetweenRouteAllowed = false;
	public boolean timeVio = false;
	public int alnsSize = 3;
	public int[] arriveTime;
	public double[] alnsWeigt = new double[alnsSize];
	public int[] alnsScore = new int[alnsSize];
	public int[] cnt = new int[alnsSize];
	public int choose;
	public List<Integer> needUpdateNode = new ArrayList<Integer>();;
	public int[] checkArray = new int[3];
	ArrayList<ArrayList<Order>> bestRoute = new ArrayList<ArrayList<Order>>();
	public int totalDepot;
	public int insertChoose;
	public Random rand = new Random();
	public double T0;
	public int oldHash;
	public double bestCost;
	public double toursCost = -1;
	ArrayList<Integer> removeInx;
	public Vector<Integer> feaAndPos;
	public double totalTime = 1e6;
	public double travelTime = 0;
	double bestFeasiCost = 1e6;
	int upperBound;
	/***********************************************************************/
	ArrayList<ArrayList<Order>> allRoute = new ArrayList<ArrayList<Order>>();
	double totalCost = 0;
	Random r = new Random();
	List<Order> removeSet;
	List<Integer> unusedTruck;
	ArrayList<int[]> blockedMap;
	HashMap<Integer, Integer> totalCostList;

	public static HashMap<Integer, List<Integer>> tabuList = new HashMap<>();
	DynamicBasicData d;
	int globalSize = 0;
	ArrayList<ArrayList<Order>> route;
	ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
	LspMixedALNS alns;
	CommonFunction cf;

	public DynamicMain(DynamicBasicData d, double alpha)
			throws FileNotFoundException, InterruptedException, ExecutionException {
		this.d = d;
		allRoute = new ArrayList<ArrayList<Order>>();
		bestCost = Integer.MAX_VALUE;
		bestFeasiCost = Integer.MAX_VALUE;
		if (!d.con.slotsFalg) {
			d.con.setSlotsInterval(1);
		}
		cf = new CommonFunction();
		route = cf.cloneVector(d.initialRoute);
		alns = new LspMixedALNS(d);
		if (d.newOrders.size() < 8)
			optimize();
		else
			heuristic();

		removeDuplicateUnexsitReplacePickUp(bestRoute);
		removeDuplicateUnexsitReplacePickUp(bestRoute);

		if (d.reSequenceFlagOn) {
			route = cf.cloneVector(bestRoute);
			// can only reSequence same trip order, cause never know the load assignment for
			// pick-drop-pick-pick-drop kind of thing
			reSequence();
			bestRoute = cf.cloneVector(route);
			alns.calcuNewBenfitDynamic(bestRoute);
		}

		// Order tempNode = bestRoute.get(0).get(1);
		// bestRoute.get(0).remove(1);
		// bestRoute.get(0).add(3, tempNode);
		// outPut();
	}

	public void optimize() throws FileNotFoundException, InterruptedException, ExecutionException {
		globalSize = d.newOrders.size();
		int[] num = new int[globalSize];
		for (int i = 0; i < globalSize; i++) {
			num[i] = i + 1;
		}
		permute(num);

		bestRoute = new ArrayList<ArrayList<Order>>();
		double smallestObj = 1e10;
		double obj = 0;

		for (ArrayList<Integer> remove : result) {
			route = cf.cloneVector(d.initialRoute);
			removeSet = new ArrayList<Order>();
			for (Integer idx : remove)
				removeSet.add(d.orders.get(d.newOrdersId.get(idx - 1)));

			insertOneByOne(removeSet);
			alns.setGlobalCnt(1e8);
			obj = alns.calcuNewBenfitDynamic(route);
			if (obj < smallestObj) {
				smallestObj = obj;
				bestRoute = cf.cloneVector(route);
			}
		}

	}

	private void transferRemove(ArrayList<Integer> remove) {
		removeSet = new ArrayList<Order>();
		for (Integer index : remove)
			removeSet.add(d.orders.get(index));
	}


	public void heuristic() throws FileNotFoundException, InterruptedException, ExecutionException {
		globalSize = d.newOrdersId.size();
		Collections.shuffle(d.newOrdersId);

		bestRoute = new ArrayList<ArrayList<Order>>();
		double smallestObj = 1e10;
		double obj = 0;

		route = cf.cloneVector(d.initialRoute);
		insertOneByOne(d.newOrders);
		alns.setGlobalCnt(1e8);
		obj = alns.calcuNewBenfitDynamic(route);
		if (obj < smallestObj) {
			smallestObj = obj;
			bestRoute = cf.cloneVector(route);
		}

		ArrayList<ArrayList<Order>> recRouteall = new ArrayList<ArrayList<Order>>();
		double newbenfit = 0;
		double oldBenfit = alns.calcuNewBenfitDynamic(route);
		double bestBenifit = oldBenfit;
		bestUsedRoute = cf.cloneVector(route);

		for (int reopt = 0; reopt < 8000; reopt++) {
			remove(d.newOrders);

			insertOneByOne(removeSet);
			newbenfit = alns.calcuNewBenfitDynamic(route);

			if (newbenfit < bestBenifit && d.totalSize == alns.onelspServeAll(route)) {
				bestBenifit = newbenfit;
				removeDuplicateUnexsitReplacePickUp(route);
				bestUsedRoute = cf.cloneVector(route);
			}
			recRouteall = cf.cloneVector(route);
			if (newbenfit < oldBenfit) {
				oldBenfit = newbenfit;
			} else {
				// double randprob = rand.nextDouble();
				if (newbenfit > oldBenfit * 1.2)// randprob > 0.5 &&
				{
					route = cf.cloneVector(recRouteall);
					// System.err.println("ever happened");
				} else
					oldBenfit = newbenfit;
			}

		}
		route = cf.cloneVector(bestUsedRoute);

	}

	private void insertOneByOne(List<Order> remove) {
		int[] insertArray = new int[3];
		for (Order chooseNode : remove) {
			for (ArrayList<Order> path : route) {
				if (path.contains(chooseNode))
					{path.remove(chooseNode);
					path.remove(d.pairs.get(chooseNode.id).pickUp);
					}
			}
			insertArray[1] = -1;

			insertArray = alns.multiVehiclesInsertion(route, d.pairs.get(chooseNode.id).pickUp,
					d.pairs.get(chooseNode.id).dropOff);

			if (insertArray[1] > 0) {
				route.get(insertArray[0]).add(insertArray[1], d.pairs.get(chooseNode.id).pickUp);
				route.get(insertArray[0]).add(insertArray[2], d.pairs.get(chooseNode.id).dropOff);
			}
		}

	}

	private void removeDuplicateUnexsitReplacePickUp(ArrayList<ArrayList<Order>> route) {
		Order pre = null;
		boolean check = false;

		for (ArrayList<Order> path : route) {
			pre = null;
			Iterator<Order> i = path.iterator();
			while (i.hasNext()) {
				check = false;
				Order s = i.next();

				if (pre != null && s.id == pre.id)
					i.remove();

				if (s.orderLocation == d.locations.length - 1)
					i.remove();

				if (pre != null)
					check = (d.startEndNodes.contains(pre) && d.newOrdersCorresPickup.contains(s.id))
							|| (d.startEndNodes.contains(s) && d.newOrdersCorresPickup.contains(pre.id));

				// System.err.println(s.id+"\t"+check);
				if (check && pre.orderLocation == s.orderLocation)
					i.remove();
				pre = s;
			}

			if (path.size() >= 3 && path.get(path.size() - 1).equals(path.get(path.size() - 2)))
				path.remove(path.size() - 1);
		}

		for (ArrayList<Order> path : route)
			for (int i = 0; i < path.size(); i++) {
				if (d.newOrdersCorresPickup.contains(path.get(i).id))
					path.set(i, d.orders.get(d.pickupCorrsOrder.get(path.get(i).id)));
			}
	}

	private void reSequence() {
		int k = 0;
		System.err.println("route before resequence\t" + alns.human(route));
		// for (int reopt = 0; reopt < 300; reopt++) {
		// k = 0;
		// for (ArrayList<Order> path : route) {
		// // two opt will merge the route, even different trips, but may infeasible
		// cause
		// // the multi-depots
		// // cannot allowed: [7, 19, 11, 18, 1, 13, 19]=>[7, 13, 1, 18, 11, 19, 19]
		// ArrayList<Order> newPath = alns.twoOptAlternate(path, k);
		// route.set(k, newPath);
		// k++;
		// }
		// }
		// insert to same vehicle and same trip
		int cnt = 0;
		while (route != null && route.size() > 0 && cnt < 3) {
			for (int reopt = 0; reopt < 3000; reopt++) {
				k = 0;
				for (ArrayList<Order> path : route) {
					ArrayList<Order> newPath = alns.swap(path, k);
					route.set(k, newPath);
					k++;
				}
			}
			for (int reopt = 0; reopt < 3000; reopt++) {
				k = 0;
				for (ArrayList<Order> path : route) {
					ArrayList<Order> newPath = alns.removeEachOneInsertSameTrip(path, k);
					route.set(k, newPath);
					k++;
				}
			}

			if (shuffleBetweenRouteAllowed) {
				ArrayList<ArrayList<Order>> recRouteall = new ArrayList<ArrayList<Order>>();
				double newbenfit = 0;
				double oldBenfit = alns.calcuNewBenfitDynamic(route);
				double bestBenifit = oldBenfit;
				bestUsedRoute = cf.cloneVector(route);
				// reSequence between vehicles for unloaded job, for example, one vehicle
				// delayed too much, other early ended vehicles can help for delivery
				for (int reopt = 0; reopt < 8000; reopt++) {
					remove(partMergeAll(route));
					insert();
					newbenfit = alns.calcuNewBenfitDynamic(route);

					if (newbenfit < bestBenifit && d.totalSize == alns.onelspServeAll(route)) {
						bestBenifit = newbenfit;
						removeDuplicateUnexsitReplacePickUp(route);
						bestUsedRoute = cf.cloneVector(route);
						// System.err.println("ever happen\t" + bestBenifit + "\t" +
						// alns.human(bestUsedRoute));
					}
					recRouteall = cf.cloneVector(route);
					if (newbenfit < oldBenfit) {
						oldBenfit = newbenfit;
					} else {
						// double randprob = rand.nextDouble();
						if (newbenfit > oldBenfit * 1.2)// randprob > 0.5 &&
						{
							route = cf.cloneVector(recRouteall);
							// System.err.println("ever happened");
						} else
							oldBenfit = newbenfit;
					}

				}
				route = cf.cloneVector(bestUsedRoute);
			}
			cnt++;
		}
		System.err.println("route after resequence\t" + alns.human(route));
	}

	private void insert() {
		Order chooseNode = null;
		// can only insert to the start depot after position
		int[] insertArray = new int[2];

		for (int index = 0; index < removeSet.size(); index++) {
			ArrayList<ArrayList<Order>> recRouteall = cf.cloneVector(route);
			chooseNode = removeSet.get(index);
			if (d.startEndNodes.contains(chooseNode))
				continue;
			ArrayList<Order> temp = new ArrayList<Order>();
			temp.add(chooseNode);
			if(d.newOrders.contains(chooseNode))
				temp.add(d.pairs.get(chooseNode.id).pickUp);

			for (ArrayList<Order> path : route)
				path.removeAll(temp);

			insertArray = multiVehiclesInsertion(route, chooseNode);

			if (insertArray[1] > 0)
				route.get(insertArray[0]).add(insertArray[1], chooseNode);
			else
				route = cf.cloneVector(recRouteall);
		}
	}

	public int[] multiVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		int[] minarray = new int[2];
		minarray[1] = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		int indexNextDepot = 0;
		int pos = 0;
		int v = 0;
		for (int i = 0; i < route.size(); i++) {
			if (chooseNode.specialVehicle > -1 && Math.abs(chooseNode.specialVehicle - i) > 1e-3)
				continue;
			increa_comp = alns.calcuResult(new PathSolution(route.get(i), i), d.con.yita2, 0, 0, 0);
			globalCost = 0;
			pos = 0;
			v = 0;
			for (Order u : route.get(i)) {
				if (d.startEndNodes.contains(u)) {
					pos = v;
					break;
				}
				v++;
			}

			for (int m = v; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;
				if (alns.tripSize(route.get(i), m + 1) > d.vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}
				route.get(i).add(m + 1, chooseNode);
				globalCost = alns.calcuResult(new PathSolution(route.get(i), i), d.con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp;

				if (benefi < minBeneif) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;
				}

				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	private void remove(List<Order> allChoosedNode) {
		removeSet = new ArrayList<Order>();
		Collections.shuffle(allChoosedNode);
		if (allChoosedNode.size() >= 1)
			removeSet.addAll(
					allChoosedNode.subList(0, (int) Math.max(0.15 * allChoosedNode.size(), 1)));
	}

	public List<Order> partMergeAll(ArrayList<ArrayList<Order>> route) {
		HashSet<Order> mergeAllNode = new HashSet<Order>();
		int pos = 0;
		int v = 0;
		for (ArrayList<Order> path : route) {
			if (path.size() < 2)
				continue;
			v = 0;
			for (Order u : path) {
				if (d.startEndNodes.contains(u)) {
					pos = v;
					break;
				}
				v++;
			}
			if (pos + 1 < path.size() - 1)
				mergeAllNode.addAll(path.subList(pos + 1, path.size() - 1));
		}
		List<Order> updated = deepCopySet(mergeAllNode);
		return updated;
	}

	private List<Order> deepCopySet(HashSet<Order> mergeAllNode) {
		// TODO Auto-generated method stub
		List<Order> updated = new ArrayList<Order>();
		for (Order u : mergeAllNode)
			updated.add(u);
		return updated;
	}

	public void permute(int[] num) {
		result = new ArrayList<ArrayList<Integer>>();
		// start from an empty list
		result.add(new ArrayList<Integer>());
		for (int i = 0; i < num.length; i++) {
			// list of list in current iteration of the array num
			ArrayList<ArrayList<Integer>> current = new ArrayList<ArrayList<Integer>>();

			for (ArrayList<Integer> l : result) {
				// # of locations to insert is largest index + 1
				for (int j = 0; j < l.size() + 1; j++) {
					// + add num[i] to different locations
					l.add(j, num[i]);

					ArrayList<Integer> temp = new ArrayList<Integer>(l);
					current.add(temp);
					l.remove(j);
				}
			}

			result = new ArrayList<ArrayList<Integer>>(current);
		}

	}

}
