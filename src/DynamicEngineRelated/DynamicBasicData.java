package DynamicEngineRelated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import Data.AvilDock;
import Data.BasicData;
import Data.Constants;
import Data.DummyOrder;
import Data.Location;
import Data.Lsp;
import Data.Order;
import Data.Parameter;
import Data.Trip;
import Data.Vehicle;
import MasterModel.CommonFunction;

//stops = Integer.parseInt(tokens[3]);
//sumAllOrder = stops + totalCar * 2 + 1;

public class DynamicBasicData extends BasicData {
	public boolean dynamicFlag = true;
	boolean reSequenceFlagOn = false;

	public Set<Order> startEndNodes = new HashSet<Order>();
	public List<Integer> newOrdersId = new ArrayList<Integer>();
	public List<Integer> newOrdersCorresPickup = new ArrayList<Integer>();

	public ArrayList<ArrayList<Order>> initialRoute = new ArrayList<ArrayList<Order>>();
	public HashMap<Integer, Order> locationOrders = new HashMap<Integer, Order>();
	public List<Integer> singlePath = new ArrayList<Integer>();
	public ArrayList<Vehicle> vehicles = new ArrayList<>();
	public ArrayList<Lsp> lsps = new ArrayList<Lsp>();
	public ArrayList<Order> orders = new ArrayList<Order>();
	public Location[] locations;
	public Constants con = new Constants();
	public HashMap<Integer, Parameter> parameterMatrix;
	/**************************************************************/
	public File file;
	public String fileName;
	public Scanner st;

	/**************************************************************/
	public int totalCar = 0;
	int setSumAllOrder = 0;

	public int minOrderTime = (int) 10e8;
	public int stops = 0;
	int points = 0;
	int imdTotalGroup = 0;
	public int[] Ucapacity;

	int timeLimit = 20;
	public int iterations = 25000;
	int objFlag = 0;
	double alpha = 0;

	double timeVioCost = 500;
	double travelWeight = 10;// 10
	double waitWeight = 0;// 1
	double maxSpanWeight = 20;// 20
	double loadBalanceWeight = 0;// 10
	List<AvilDock> avaiDocking = new ArrayList<>();
	List<DummyOrder> dummyOrders = new ArrayList<>();
	HashSet<Integer> fairPriceOrders = new HashSet<>();
	public HashSet<Integer> startNodes = new HashSet<>();
	public HashSet<Integer> endNodes = new HashSet<>();

	HashMap<Integer, Integer> normalVehicle;
	HashMap<Integer, Integer> rank = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> carSetGroup = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> nodeSetGroup = new HashMap<Integer, Integer>();
	List<Integer> depotSet;
	public List<Integer> shuffledCarSet;
	ArrayList<Integer> allNodes;
	int totalGroup = 0;
	int deleteDepotCnt = 0;
	boolean finalCheck;
	boolean needAddExtra = false;
	private int waitTime;
	HashMap<Integer, Integer> depotTimeMap = new HashMap<Integer, Integer>();
	int[][] oriMatrix = null;
	double[][] disMatrix = null;
	public ArrayList<Integer> sameGroup = new ArrayList<Integer>();
	ArrayList<Integer> imdSameGroup = new ArrayList<Integer>();
	// HashMap<Integer, ArrayList<Integer>> orderInGroup = new HashMap<Integer,
	// ArrayList<Integer>>();
	public HashMap<Integer, ArrayList<Integer>> imdOrderInGroup;
	ArrayList<Integer> imdOrders = new ArrayList<Integer>();
	ArrayList<Integer> corrCar = new ArrayList<Integer>();
	HashMap<Integer, Boolean> nonIMDMap = new HashMap<Integer, Boolean>();
	HashSet<Integer> mallSet = new HashSet<Integer>();
	ArrayList<ArrayList<Double>> finalTimeHash = new ArrayList<ArrayList<Double>>();

	boolean dockingVehicelType;
	int sumAllSameGroupOrder = 0;
	List<RemainingTrips> remainingTripsList = new ArrayList<RemainingTrips>();
	int posCnt = 0;
	int negCnt = 0;
	int cnt = 0;
	int posAveWeight = 0;
	int negAveWeight = 0;
	int aveWeight = 0;
	int numLSP = 0;
	public int tripsSize = 0;
	public int maxTime = 0;
	public int maxOrderUpperTime = 0;
	public ConcurrentHashMap<Integer, Trip> trips;
	public Set<List<Order>> staticPath = new HashSet<List<Order>>();
	public List<Integer> vehiclesOrderSize;
	HashMap<Integer, List<Integer>> initialMap;
	int maxId = 0;
	String outPut;
	public boolean allTrips;
	public List<ArrayList<Integer>> timeWindowGroup;
	public double totalTimeSpan;
	public CommonFunction cf;
	public int maxServiceTime = 0;
	int globalStartTime = 0;

	HashMap<Integer, PickDropPair> pairs = new HashMap<Integer, PickDropPair>();
	HashMap<Integer, Integer> pickupCorrsOrder = new HashMap<Integer, Integer>();
	public int totalSize;

	public DynamicBasicData(String fileName, String outPut, HashMap<Integer, List<Integer>> initialMap)
			throws IOException {
		super();
		this.outPut = outPut;
		file = new File(fileName);
		this.initialMap = initialMap;
		// this.initialVehicleMap = initialVehicleMap;
		readFile(file);
	}

	private void readFile(File file) throws IOException {
		int cur = 0;
		CommonFunction cf = new CommonFunction();
		cnt = 0;
		allTrips = false;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		int orderCnt = 0;
		int vehicleCnt = 0;
		HashSet<Integer> tripVehicles = new HashSet<Integer>();
		int locationsCnt = 0;
		int travelTimeCnt = 0;
		double totalOrderLoad = 0;
		double vehicleTotalCapacity = 0;
		parameterMatrix = new HashMap<Integer, Parameter>();
		locationOrders = new HashMap<Integer, Order>();
		Parameter hardCodeValue = new Parameter(1, 1, 10, 1, 0);// max-smallest
		parameterMatrix.put(0, hardCodeValue);
		// Pattern mypattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
		// Matcher mymatcher= mypattern.matcher(mystring);
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				String[] tokens = line.split(",", -1);

				if (tokens.length == 2 && tokens[0].matches("StartTime")) {
					globalStartTime = Integer.parseInt(tokens[1]);
				}
				if (tokens.length == 2 && tokens[0].matches("WithInTripResequenceAllowed")) {
					if (tokens[1].matches("TRUE") || tokens[1].matches("True") || tokens[1].matches("true"))
						this.reSequenceFlagOn = true;
				}
				if (tokens.length == 5 && tokens[0].matches("overall")) {
					for (int i = 0; i < Integer.parseInt(tokens[1]); i++)
						lsps.add(new Lsp(new HashSet<Order>(), new Vector<Integer>()));

					totalGroup = Integer.parseInt(tokens[4]);
					totalCar = Integer.parseInt(tokens[2]);
					stops = Integer.parseInt(tokens[3]);
					points = Integer.parseInt(tokens[4]);

					totalSize = stops;
					imdTotalGroup = totalGroup;
					orders = new ArrayList<Order>();
					locations = new Location[points + 1];
					oriMatrix = new int[points + 1][points + 1];
					disMatrix = new double[points + 1][points + 1];
					sameGroup = new ArrayList<Integer>();
					imdSameGroup = new ArrayList<Integer>();
					for (int i = 0; i < stops + 1; i++) {
						sameGroup.add(-1);
						imdSameGroup.add(-1);
						orders.add(null);
					}

					for (int i = 0; i < totalCar; i++)
						vehicles.add(null);

					trips = new ConcurrentHashMap<Integer, Trip>();
					Ucapacity = new int[points];
				}

				if (tokens.length == 3 && tokens[0].matches("lsp") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					rank.put(cur, Integer.parseInt(tokens[2]));
				}

				if (tokens.length >= 5 && tokens[0].matches("Location") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					Ucapacity[cur] = Integer.parseInt(tokens[2]);
					Location temp = new Location(true, Ucapacity[cur]);
					locations[cur] = temp;
					if (Ucapacity[cur] < 100)
						mallSet.add(cur);
					else if (Ucapacity[cur] < 1)
						Ucapacity[cur] = 100;

					locations[cur].setStart(Math.max(globalStartTime, Integer.parseInt(tokens[3])));
					locations[cur].setEnd(Integer.parseInt(tokens[4]));
					locations[cur].setId(cur);
					// System.err.println(cur+"\t"+locations[cur].open+"\t"+locations[cur].close);
					if (tokens.length == 6) {
						locations[cur].setType(tokens[5]);
					} else if (tokens.length == 7) {
						String[] parts = tokens[5].split(":");
						String[] newPart = parts[0].split("\\[");
						locations[cur].setLunchBreakStartTime(Integer.parseInt(newPart[1]));
						newPart = parts[(parts.length - 1)].split("\\]");
						locations[cur].setLunchBreakEndTime(Integer.parseInt(newPart[0]));
						locations[cur].setType(tokens[6]);
					}
					locationsCnt++;
				}

				if (tokens.length >= 8 && tokens[0].matches("vehicle") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					int truckType = Integer.parseInt(tokens[7]);

					Vehicle newVec = new Vehicle((int) Double.parseDouble(tokens[4]),
							Math.max(globalStartTime, Integer.parseInt(tokens[2])), (int) Double.parseDouble(tokens[3]),
							truckType);

					String[] parts = tokens[8].split(";");
					String[] newPart = parts[0].split("\\[");
					if (newPart[1].matches("^-?\\d+$")) {
						newVec.setStartLocation(Integer.parseInt(newPart[1]));
					}
					newPart = parts[1].split("\\]");
					newVec.setEndLocation(Integer.parseInt(newPart[0]));
					newVec.setDepotLoadTime(Integer.parseInt(tokens[9]));
					newVec.setOriginLSP(Integer.parseInt(tokens[6]));
					newVec.setVolume(Double.parseDouble(tokens[4]));
					vehicles.set(cur - 1, newVec);

					carSetGroup.put(cur, Integer.parseInt(tokens[6]));
					if (numLSP < Integer.parseInt(tokens[6]))
						numLSP = Integer.parseInt(tokens[6]);

					vehicleCnt++;
				}

				if (tokens.length >= 14 && tokens[0].matches("order") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					double posWeight = 0;
					double negWeight = 0;

					double weight = Double.parseDouble(tokens[4]);
					int temp = 0;
					// all non imd use -1
					if (weight > 0)
						posWeight = weight;
					if (weight < 0)
						negWeight = weight;

					temp = Integer.parseInt(tokens[3]);
					sameGroup.set(cur, temp);

					if (orders.get(cur) == null)
						orders.set(cur, new Order());
					String[] range = tokens[5].substring(1, tokens[5].length() - 1).split(":");
					int from = Math.max(globalStartTime, Integer.parseInt(range[0]));
					int to = Integer.parseInt(range[1]);

					orders.get(cur).set(cur, weight, posWeight, negWeight, Math.max(globalStartTime, from), to,
							(int) Double.parseDouble(tokens[7]), Integer.parseInt(tokens[3]));

					orders.get(cur).setCustomerId(tokens[10]);
					orders.get(cur).setCargoType(tokens[11]);
					orders.get(cur).setDepotPickup(Integer.parseInt(tokens[12]));
					orders.get(cur).setStatus(tokens[13]);
					if (tokens[13].equals("New")) {
						newOrdersId.add(cur);
						if (!tokens[2].equals("null") && !tokens[2].equals("-1")) {
							orders.get(cur).setPreDefVehicle(Integer.parseInt(tokens[2]) - 1);
							// orders.get(cur).setVehiclesIndex(temp);
						}
					}
					if (minOrderTime > Math.max(globalStartTime, from))
						minOrderTime = Math.max(globalStartTime, from);

					if (maxOrderUpperTime < to)
						maxOrderUpperTime = to;
					if (maxServiceTime < (int) Double.parseDouble(tokens[7]))
						maxServiceTime = (int) Double.parseDouble(tokens[7]);

					orders.get(cur).setVolume(Double.parseDouble(tokens[6]));

					imdSameGroup.set(cur, Integer.parseInt(tokens[3]));
					if (!nodeSetGroup.containsKey((Integer) cur))
						nodeSetGroup.put(cur, Integer.parseInt(tokens[8]));

					if (Integer.parseInt(tokens[9]) > 0) {
						fairPriceOrders.add(cur);
					}
					orders.get(cur).setOriginLSP(Integer.parseInt(tokens[8]));
					orders.get(cur).setSchedualFlag(tokens[14]);
					orderCnt++;
				}

				if (tokens.length == 5 && tokens[0].matches("slot") && tokens[1].matches("[0-9]+")) {
					cur = Integer.parseInt(tokens[1]);
					AvilDock temp = new AvilDock(cur, Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]),
							Integer.parseInt(tokens[4]));
					avaiDocking.add(temp);
					if (Integer.parseInt(tokens[4]) < 1) {
						System.err.println("number of slots cannot be 0");
						return;
					}
				}
				if (tokens.length == 2 && tokens[0].matches("TimeLimit")) {
					timeLimit = Integer.parseInt(tokens[1]);
				}

				if (tokens.length == 2 && tokens[0].matches("Iterations")) {
					iterations = Integer.parseInt(tokens[1]);
				}

				if (tokens.length == 2 && tokens[0].matches("alpha")) {
					alpha = Double.parseDouble(tokens[1]);
				}

				if (tokens.length == 2 && tokens[0].matches("objFlag")) {
					objFlag = Integer.parseInt(tokens[1]);
				}

				int temp = 0;
				if ((tokens.length == 5) && (tokens[0].matches("[0-9]+"))) {
					String[] parts = tokens[1].split(";");
					String[] newPart = parts[0].split("\\[");

					if (newPart[1].matches("^-?\\d+$")) {
						travelWeight = Double.parseDouble(newPart[1]);
					}
					waitWeight = Double.parseDouble(parts[1]);// 1
					timeVioCost = Double.parseDouble(parts[2]);

					maxSpanWeight = Double.parseDouble(parts[3]);// 20
					newPart = parts[4].split("\\]");
					loadBalanceWeight = Double.parseDouble(newPart[0]);// 10
					if (travelWeight > 9)
						lsps.get(Integer.parseInt(tokens[0])).setParameter(new Parameter(travelWeight / 10.0,
								waitWeight / 10.0, timeVioCost / 10.0, maxSpanWeight / 10.0, loadBalanceWeight / 10.0));
					else
						lsps.get(Integer.parseInt(tokens[0])).setParameter(
								new Parameter(travelWeight, waitWeight, timeVioCost, maxSpanWeight, loadBalanceWeight));

					lsps.get(Integer.parseInt(tokens[0])).setExtra(Integer.parseInt(tokens[2]),
							Double.parseDouble(tokens[3]), Double.parseDouble(tokens[4]));
				}

				if ((tokens.length == 4) && (tokens[0].matches("RemainingTrips"))) {
					String[] parts = tokens[2].split(";");
					String[] newPart = parts[0].split("\\[");

					temp = Integer.parseInt(tokens[1]);
					tripVehicles.add(temp);

					singlePath = new ArrayList<Integer>();
					if (newPart[1].matches("^-?\\d+$")) {
						singlePath.add(Integer.parseInt(newPart[1]));
					} else {
						addOrder(newPart[1], temp);
					}
					for (int i = 1; i < parts.length - 1; i++) {
						if (parts[i].matches("^-?\\d+$")) {
							if (!singlePath.contains(Integer.parseInt(parts[i])))
								singlePath.add(Integer.parseInt(parts[i]));
						} else {
							addOrder(parts[i], temp);
						}
					}
					if (parts.length > 1) {
						newPart = parts[(parts.length - 1)].split("\\]");
						if (newPart[0].matches("^-?\\d+$")) {
							if (!singlePath.contains(Integer.parseInt(newPart[0])))
								singlePath.add(Integer.parseInt(newPart[0]));
						} else {
							addOrder(newPart[0], temp);
						}

					} else {
						String[] cPart = newPart[1].split("\\]");
						if (cPart[0].matches("^-?\\d+$")) {
							orders.get(Integer.parseInt(cPart[0])).setVehiclesIndex(temp);
							if (!singlePath.contains(Integer.parseInt(cPart[0])))
								singlePath.add(Integer.parseInt(cPart[0]));
						} else {
							addOrder(cPart[0], temp);
						}
					}

					// addStartEnd(locationStart, temp);

					Trip tempTrip = new Trip(temp, singlePath);

					tempTrip.setLocationStartId(Integer.parseInt(tokens[3]));
					// locationOrders.get(locationEnd));
					trips.put(temp, tempTrip);

				}

				// come to here
				if (tokens.length == 4 && isInteger(tokens[0])) {
					oriMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = Integer.parseInt(tokens[2]);
					if (tokens.length > 3)
						disMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = Double
								.parseDouble(tokens[3]);

					if (Integer.parseInt(tokens[2]) > maxTime)
						maxTime = Integer.parseInt(tokens[2]);
					travelTimeCnt++;
				}

				if (tokens.length == 1 && isInteger(tokens[0])) {
					timeLimit = Integer.parseInt(tokens[0]);
				}

			}

			int lowerTime = 0;
			cnt = 0;

			int cnt = 0;

			for (int i = 0; i < locations.length - 1; i++)
				locations[i].setUcapacity(Ucapacity[i]);
			int small = (int) 1e10;
			for (AvilDock u : avaiDocking) {
				small = (int) 1e10;
				if (u.aviDock > Ucapacity[u.mallId]) {
					Ucapacity[u.mallId] = u.aviDock;
					locations[u.mallId].setUcapacity(u.aviDock);
					if (u.aviDock < small)
						small = u.aviDock;

				}
				locations[u.mallId].setMinCapacity(small);
			}
			addDummy();
			imdOrderInGroup = new HashMap<>();
			for (int i = 0; i < imdTotalGroup; i++)
				imdOrderInGroup.put(i, new ArrayList<Integer>());

			cnt = 0;
			for (Integer orderLocation : sameGroup) {
				if (orderLocation > -1 && mallSet.contains(orderLocation)) {
					nonIMDMap.put(cnt, false);
					// sumDockingGroup++;
				} else
					nonIMDMap.put(cnt, true);
				cnt++;
			}

			cnt = 0;

			for (Integer order : imdSameGroup) {
				if (order > 0 && locations[orders.get(cnt).orderLocation].type == "IMD") {
					imdOrderInGroup.get(order).add(cnt);
					imdOrders.add(cnt);
					// sumDockingGroup++;
				}
				cnt++;
			}

			Iterator<Entry<Integer, ArrayList<Integer>>> iter = imdOrderInGroup.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<Integer, ArrayList<Integer>> entry = iter.next();
				if (entry.getValue() == null || entry.getValue().size() == 0) {
					iter.remove();
				}
			}

			for (Integer o : imdOrderInGroup.keySet()) {
				sumAllSameGroupOrder += imdOrderInGroup.get(o).size() * imdOrderInGroup.get(o).size();
			}

			for (Integer key : imdOrderInGroup.keySet()) {
				ArrayList<Integer> pos = new ArrayList<Integer>();
				int cntT = 0;
				for (int i = 0; i < imdOrderInGroup.get(key).size(); i++) {
					if (fairPriceOrders.contains(imdOrderInGroup.get(key).get(i)))
						pos.add(i);
				}
				for (Integer posId : pos) {
					imdOrderInGroup.get(key).remove(posId - cntT);
					cntT++;
				}
			}
			if (trips.keySet().size() == totalCar)
				allTrips = true;
			// recheck again
			iter = imdOrderInGroup.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<Integer, ArrayList<Integer>> entry = iter.next();
				if (entry.getValue() == null || entry.getValue().size() == 0) {
					iter.remove();
				}
			}

			allNodes = new ArrayList<Integer>();
			for (int i = 1; i < orders.size(); i++) {
				if (orders.get(i).weight() > 0) {
					posAveWeight += orders.get(i).weight();
					posCnt++;
				}
				if (orders.get(i).weight() < 0) {
					negAveWeight += -orders.get(i).weight();
					negCnt++;
				}
				aveWeight += orders.get(i).weight();
			}
			aveWeight /= orders.size();

			if (posCnt > 0)
				posAveWeight /= posCnt;
			if (negCnt > 0)
				negAveWeight /= negCnt;

		} finally {
			LineIterator.closeQuietly(it);
		}

		if (orderCnt < stops) {
			System.err.println("number of orders is smaller than " + (stops));
			return;
		}

		if (vehicleCnt < totalCar) {
			System.err.println("number of vehicles is smaller than " + totalCar);
			return;
		}
		if (locationsCnt < points) {
			System.err.println("number of vehicles is smaller than " + totalCar);
			return;
		}
		// if (consolatedCnt < stops) {
		// System.err.println("No or only partial pre-consolidate trip
		// provided");
		// return;
		// }
		if (travelTimeCnt < points * (points - 1)) {
			System.err.println("No or only partial travel time between locations been provided");
			return;
		}

		HashSet<ArrayList<Integer>> timeWindowIden = new HashSet<ArrayList<Integer>>();
		for (Order od : orders) {
			if (od == null)
				continue;
			if (startEndNodes.contains(od))
				continue;
			ArrayList<Integer> timeWindowList = new ArrayList<Integer>();
			timeWindowList.add(od.lowerTime);
			timeWindowList.add(od.upperTime);
			timeWindowIden.add(timeWindowList);
		}
		ArrayList<Integer> timeWindowList = new ArrayList<Integer>();
		ArrayList<Integer> timeWindowSingleGroup = new ArrayList<Integer>();
		timeWindowGroup = new ArrayList<ArrayList<Integer>>();
		cnt = 0;
		for (ArrayList<Integer> timewindow : timeWindowIden) {
			timeWindowSingleGroup.clear();
			for (Order od : orders) {
				if (od == null)
					continue;
				if (startEndNodes.contains(od))
					continue;

				if ((Math.abs(od.upperTime - timewindow.get(1)) <= 60)
						&& (Math.abs(od.lowerTime - timewindow.get(0)) <= 60))
					timeWindowSingleGroup.add(od.id);
			}
			timeWindowGroup.add(cf.singleClone(timeWindowSingleGroup));
			cnt++;
		}
		initializeRoute();
		updateTripsWithMoreOrders();
		// stops and sumAllOrder change, put in between

		depotSet = new ArrayList<Integer>();

		shuffledCarSet = new ArrayList<Integer>();
		for (int i = 1; i <= totalCar; i++) {
			depotSet.add(i);
			shuffledCarSet.add(i - 1);
		}

		con.setInitial(timeLimit, sumAllSameGroupOrder, imdTotalGroup, totalCar, stops, points, oriMatrix, disMatrix,
				totalGroup, deleteDepotCnt, shuffledCarSet, collectionOrder);

		con.setAveWeight(aveWeight);
		con.setPosAveWeight(posAveWeight);
		con.setNegAveWeight(negAveWeight);
		con.setAllStartEnd(startEndNodes);
		con.setAllSet(depotSet, imdOrders, imdOrderInGroup, allNodes, sameGroup, nodeSetGroup, imdSameGroup, nonIMDMap,
				carSetGroup, lsps, dummyOrders, initialMap, objFlag);
		con.setAlpha(alpha, outPut);
		con.setMaxId(maxId);
		con.initializeX();
		update(nodeSetGroup, carSetGroup);

		HashSet<Order> doubleCheckOrder = new HashSet<Order>();
		for (Order o : orders)
			doubleCheckOrder.add(o);
		orders.clear();
		for (Order o : doubleCheckOrder)
			orders.add(o);

		totalTimeSpan = 0;// sumTimeSpan()*0.5;
		totalOrderLoad = 0;
		int locationId = 0;
		maxOrderUpperTime = 0;
		for (int i = 1; i < orders.size(); i++) {
			totalOrderLoad += orders.get(i).volume;
			locationId = orders.get(i).orderLocation;
			orders.get(i).setLocation(locations[locationId]);
			if (con.slotsFalg && !locations[locationId].type.equals("IMD"))
				noNeedFifteen.add(orders.get(i));

			if (orders.get(i).lowerTime < locations[locationId].open)
				orders.get(i).setLowerTime(locations[locationId].open);
			if (orders.get(i).upperTime > locations[locationId].close)
				orders.get(i).setUpperTime(locations[locationId].close);
			if (maxOrderUpperTime < orders.get(i).upperTime())
				maxOrderUpperTime = orders.get(i).upperTime();
			// System.err.println(orders.get(i).lowerTime()+"\t"+orders.get(i).upperTime()+"\t"+locations[locationId].open);
		}

		con.setNoNeedFifteen(noNeedFifteen);
		// System.err.println(totalOrderLoad + "\t" + 2 * vehicleTotalCapacity);
		for (Vehicle k : vehicles) {
			k.setlSP(lsps.get(k.originLSP));
			// k.setTruckLowerTime(Math.max(k.truckLowerTime, k.depot.location.open));
			// k.setTruckUpperTime(Math.min(k.truckUpperTime, k.depot.location.close));
			k.setInitialUpper(k.truckUpperTime);
			k.setInitialLower(k.truckLowerTime);
		}
		cnt = 0;
		for (Lsp u : lsps) {
			u.setId(cnt);
			cnt++;
		}
		if (totalOrderLoad > 2 * vehicleTotalCapacity * 0.8) {
			for (Vehicle k : vehicles) {
				k.updateVolume(0.1);
			}
		}

		pairs = new HashMap<Integer, PickDropPair>();

		for (Integer o : newOrdersId) {
			Order newPickupOrder = new Order();
			newPickupOrder.set(orders.size(), -orders.get(o).volume(), 0, 0, globalStartTime, globalStartTime + 1200,
					20, orders.get(o).pickupDepotLocation);
			newPickupOrder.setPreDefVehicle(orders.get(o).specialVehicle - 1);
			newPickupOrder.setNewOrderFlag(true);
			orders.add(newPickupOrder);
			newOrdersCorresPickup.add(newPickupOrder.id);
			PickDropPair p = new PickDropPair(orders.get(o), newPickupOrder);
			pairs.put(o, p);
		}

		for (Integer nodeIndex : newOrdersCorresPickup) {
			for (Order o : startEndNodes) {
				if (orders.get(nodeIndex).orderLocation == o.orderLocation) {
					pickupCorrsOrder.put(nodeIndex, o.id);
					break;
				}
			}
		}
		newOrders.clear();
		for (Integer idx : newOrdersId)
			newOrders.add(orders.get(idx));
		super.initilize(startEndNodes, vehicles, lsps, orders, locations, con, totalCar, stops, Ucapacity,
				fairPriceOrders);
		super.setDynamicFlag(dynamicFlag);
		super.setNewOrders(newOrdersId, newOrders, newOrdersCorresPickup);
		super.setReSequenceFlagOn(reSequenceFlagOn);
	}

	private void addStartEnd(int locationStart, int vehicleId) {
		if (!locationOrders.keySet().contains(locationStart)) {
			Order newOrder = new Order();
			newOrder.set(orders.size(), 0, 0, 0, Math.max(globalStartTime, locations[locationStart].open),
					locations[locationStart].close, vehicles.get(vehicleId - 1).depotLoadTime, locationStart);
			orders.add(newOrder);
			locationOrders.put(locationStart, newOrder);
			startEndNodes.add(newOrder);
		}
	}

	private void initializeRoute() {
		Location temp = new Location(false, 10000);
		locations[locations.length - 1] = temp;
		locations[locations.length - 1].setStart(globalStartTime);
		locations[locations.length - 1].setEnd(globalStartTime + 1000);

		for (int i = 0; i < totalCar; i++)
			initialRoute.add(new ArrayList<Order>());
		for (Integer key : trips.keySet()) {
			ArrayList<Order> initialPath = new ArrayList<Order>();
			// if (!startEndNodes.contains(orders.get(trips.get(key).path.get(0)))) {
			Order dummyOrder = new Order();
			dummyOrder.set(orders.size(), 0, 0, 0, globalStartTime, globalStartTime + 1000, 0,
					trips.get(key).startLocationId);

			initialPath.add(dummyOrder);
			if (!startEndNodes.contains(dummyOrder)) {
				startEndNodes.add(dummyOrder);
				orders.add(dummyOrder);
			}
			// }
			for (Integer nodeIdx : trips.get(key).path)
				initialPath.add(orders.get(nodeIdx));
			initialRoute.set(key - 1, initialPath);
		}
		// Iterator<ArrayList<Order>> i = initialRoute.iterator();
		// while (i.hasNext()) {
		// ArrayList<Order> s = i.next(); // must be called before you can call
		// i.remove()
		// if(s.size()<1)
		// i.remove();
		// }
	}

	private void addOrder(String newPart, int vehicleId) {
		String[] range = newPart.split("\\(")[1].split("\\)");
		Integer locationId = Integer.parseInt(range[0]);
		addStartEnd(locationId, vehicleId);
		singlePath.add(locationOrders.get(locationId).id);
	}

	private double sumTimeSpan() {
		double sum = 0;
		for (int k = 0; k < totalCar; k++)
			sum += vehicles.get(k).truckUpperTime() - vehicles.get(k).truckLowerTime();
		return sum;
	}

	private void updateTripsWithMoreOrders() {
		cnt = 1;
		int orderCnt = 0;
		vehiclesOrderSize = new ArrayList<Integer>();
		int tempVal = orders.size() - 1;
		for (int i = 0; i < totalCar; i++)
			vehiclesOrderSize.add(0);
	}

	private void addDummy() {
		for (AvilDock u : avaiDocking) {
			if (u.aviDock < Ucapacity[u.mallId]) {
				DummyOrder dummyOrder = new DummyOrder(u.mallId, u.start, u.end, Ucapacity[u.mallId] - u.aviDock);
				dummyOrders.add(dummyOrder);
			}
		}

	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	private void update(HashMap<Integer, Integer> nodeSetGroup, HashMap<Integer, Integer> carSetGroup) {
		int lspId = 0;

		for (Integer key : carSetGroup.keySet()) {
			lspId = carSetGroup.get(key);
			lsps.get(lspId).addVechicle(key);
		}
		for (Integer lspID : carSetGroup.values()) {
			for (Integer key : nodeSetGroup.keySet()) {
				lspId = nodeSetGroup.get(key);
				if (Math.abs(lspID - lspId) < 1e-3) {
					lsps.get(lspId).addRealOrders(orders.get(key));
					lsps.get(lspId).addLocations(orders.get(key).orderLocation);

				}
			}
		}

		for (Lsp u : lsps) {
			for (Integer truckID : trips.keySet())
				u.lspVehicles.remove(truckID);
		}
		int minTime = (int) 1e10;
		int maxTime = 0;
		int cnt = 0;
		for (Order order : orders) {
			if (order == null)
				continue;

			if (order.vehiclesIndex == null || order.vehiclesIndex.size() < 1) {
				order.setVehicles(lsps.get(order.originLSP).lspVehicles);
			}

			if (cnt > totalCar && cnt <= stops + totalCar) {
				if (order.lowerTime < minTime)
					minTime = order.lowerTime;
				if (order.upperTime > maxTime)
					maxTime = order.upperTime;
			}
			cnt++;
		}

		for (Vehicle vehicle : vehicles) {
			for (Integer id : rank.keySet()) {
				if (vehicle.originLSP == id)
					vehicle.setAddRank(rank.get(id));
			}
		}

		for (Order order : orders) {
			if (order == null)
				continue;

			if (locations[order.orderLocation].type != null && locations[order.orderLocation].type.equals("IMD"))
				order.setIsMall();
		}

	}

	public Vector<Integer> convertListToVector(List<Integer> path) {
		Vector<Integer> vec = new Vector<Integer>();
		for (Integer node : path)
			vec.add(node);
		return vec;
	}

	public boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	public ArrayList<Vehicle> vehicles() {
		return this.vehicles;
	}

	public ArrayList<Order> orders() {
		return this.orders;
	}

	public Location[] locations() {
		// TODO Auto-generated method stub
		return this.locations;
	}

	public ArrayList<Lsp> lsps() {
		return this.lsps;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

	public int totalSize(ArrayList<ArrayList<Order>> allRoute) {
		int size = 0;
		for (ArrayList route : allRoute) {
			size = route.size() - 2;
		}
		return size;
	}

}
