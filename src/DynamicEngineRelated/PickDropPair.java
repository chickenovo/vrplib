package DynamicEngineRelated;

import Data.Order;

public class PickDropPair {
	Order pickUp;
	Order dropOff;
	Order pickupOrginalOrder;

	public PickDropPair(Order order, Order newPickupOrder) {
		this.dropOff = order;
		this.pickUp = newPickupOrder;
	}
}
