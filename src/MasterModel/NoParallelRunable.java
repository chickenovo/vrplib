package MasterModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import DynamicEngineRelated.DynamicBasicData;
import DynamicEngineRelated.DynamicMain;
import DynamicPreProcessor.DynamicPreProcessorMainClass;


public class NoParallelRunable {

	public static void main(String[] args) throws Exception {
		String intputFileName = args[0];
		//Input_dynamic_sample2_resequence  Input_dynamic_sample1 strange_Input.txt
		String outputFileName = args[1];
		
		String[] extensionRemoved = intputFileName.split("\\.");
		BufferedReader branchIdentify = new BufferedReader(new FileReader(new File(intputFileName)));
        System.out.println("Start reading file");
		if (branchIdentify.readLine().split(",").length < 4) {
			HashMap<Integer, List<Integer>> initialMap = new DynamicPreProcessorMainClass().RunPreProcessor(intputFileName,
					extensionRemoved[0] + "new." + extensionRemoved[1]);

			DynamicBasicData d = new DynamicBasicData(extensionRemoved[0] + "new." + extensionRemoved[1], outputFileName,
					initialMap);
			DynamicMain search = new DynamicMain(d, 0.5);
		} 
		branchIdentify.close();
	}
}




