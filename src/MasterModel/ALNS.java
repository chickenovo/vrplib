//package MasterModel;
//package MasterModel;
//
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.LinkedHashSet;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Queue;
//import java.util.Random;
//import java.util.Set;
//import java.util.Vector;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//
//import Data.ArrayIndexComparator;
//import Data.BasicData;
//import Data.Constants;
//import Data.CriticalFeature;
//import Data.DummyOrder;
//import Data.KMedoids;
//import Data.Location;
//import Data.Lsp;
//
//import Data.Order;
//import Data.PathSolution;
//import Data.TimeInterval;
//import Data.Trip;
//import Data.Vehicle;
//import ilog.concert.IloException;
//
//public class ALNS {
//	double fitnessParaThreshold = 1.1;
//	int travelTimeParaThreshold = 20;
//	double smoothThreshold = 6.0;
//	double similarityThreshold = 6.0;
//	double scalingFactor = 2.0;
//
//	int vioVolume = 0;
//	boolean globalInsertInfeasible = false;
//	boolean lunchFeasible = false;
//	boolean lunchFeasibleCheckNeed = true;
//	HashMap<Integer, Double> forWardVolume = new HashMap<Integer, Double>();
//	HashMap<Integer, int[]> timeRecord = new HashMap<Integer, int[]>();
//	public ArrayList<ArrayList<Order>> orginRoute;
//	boolean finalOptimizeFlag = false;
//	ConcurrentHashMap<Integer, Vector<TimeInterval>> timeIntervalsList = new ConcurrentHashMap<Integer, Vector<TimeInterval>>();
//	double threshold;
//	double timeUsed = 0;
//	long start = 0;
//	long globalStart = 0;
//	int alreadyRunned = 0;
//	int indexNextDepot = 0;
//	double maxWait = 0;
//	int maxWaitPos = 0;
//	int totalTrip = 0;
//	int nextTripSize = 0;
//	public int startTimeFirstOrder = 0;
//	CplexCPMostNoneIMD example;
//	public List<List<Order>> allPaths = new ArrayList<List<Order>>();
//	public HashMap<Integer, Vector<Vector<Integer>>> completed = new HashMap<Integer, Vector<Vector<Integer>>>();
//	public HashMap<Vector<Integer>, Integer> initialRoutes = new HashMap<Vector<Integer>, Integer>();
//	public ArrayList<ArrayList<Order>> bestUsedRoute = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> feasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> noTimeVioRoute = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> allBestfeasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> allBestnoTimeVioRoute = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> allBestbestUsedRoute = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<Order>> overallMergedRoute = new ArrayList<ArrayList<Order>>();
//	public ArrayList<ArrayList<ArrayList<Order>>> lspRoute = new ArrayList<ArrayList<ArrayList<Order>>>();
//
//	ArrayList<Trip> currentSolutionMultiTrips = new ArrayList<Trip>();
//	// HashSet<Integer> notAllowedVehicle = new HashSet<Integer>();
//	public int[] startServTime;
//	public boolean check = true;
//	public int[] arriveTime;
//	public int tabuCnt;
//	public int removeSize = 6;
//	public int insertSize = 3;
//	public boolean noNeedMore = false;
//	public int loopCnt = 1;
//	public double[] cntRemove;
//	public double[] scoreRemove;
//	public double[] removeWeigt;
//	public int globalCnt = 0;
//	public double[] cntInsert;
//	public double[] scoreInsert;
//	public double[] insertWeigt;
//	public int totalDepot;
//	public int insertChoose;
//	public Random rand = new Random();
//	public double T0;
//	public int oldHash;
//	public double bestBenifit;
//	public double noTimeVioBenifit = 1e8;
//	public HashSet<Integer> noNeedCheckMall;
//	public double toursCost = -1;
//	public boolean serveAll;
//	public boolean volumeFeasible;
//	public Vector<Integer> feaAndPos;
//
//	public ArrayList<Vehicle> vehicles = new ArrayList<>();
//	public LinkedHashSet<Integer> tabuList = new LinkedHashSet<>();
//	public ArrayList<Order> orders;
//	public Location[] locations;
//	public double totalTime = 1e6;
//	public double travelTime = 0;
//	public double[] postPone;
//	public double[][] needMiddleAdd;
//	public double travelServe = 0;
//	CommonFunction cf;
//	Random r = new Random();
//	double feasibenefitWhole = 1e6;
//	int cntCpOpt = 20;
//	boolean canCallMIP = true;
//	double[] tempTime = new double[2];
//	HashMap<Integer, Integer> carIndex = new HashMap<Integer, Integer>();
//	ArrayList<Lsp> lsps;
//	BasicData d;
//	HashMap<Integer, double[]> StoredCriticalPoint = new HashMap<Integer, double[]>();
//	HashMap<Integer, CriticalFeature> StoredMultiCriticalPoint = new HashMap<Integer, CriticalFeature>();
//	ArrayList<Integer> usedVehicle = new ArrayList<Integer>();
//	HashSet<Order> infeasibleOrder = new HashSet<Order>();
//	double allBestfeasibenefitWhole = 9e10;
//	double allBestNoTimeVioBenifit = 9e10;
//	public double allBestbestBenifit = 9e10;
//	public double maxUtilize = 0;
//	public double utilize = 0;
//	public double globalEndTime = 0;
//	public double maxTime = 0;
//	public double totalTravelDis = 0;
//	public double maxTripDis = 0;
//	public int maxCnt = 0;
//	public static Lsp lspCurrent;
//	double travelDistance = 0;
//
//	boolean needSmooth = false;
//
//	public int k = 0;
//	public int MAX_ORDERS_PER_VEHICLE = 7;
//	public double HARD_CODED_FACTOR_TO_DECIDE_HALF_TRIP_USED = 0.8;
//
//	public ALNS(BasicData d) throws FileNotFoundException, InterruptedException, ExecutionException, IloException {
//		this.d = d;
//		this.vehicles = d.vehicles;
//		this.lsps = d.lsps;
//		this.orders = d.orders;
//		this.locations = d.locations;
//		needMiddleAdd = new double[vehicles.size()][2];
//		// this.k = kValue();
//		cf = new CommonFunction();
//	}
//
//	public ALNS(BasicData d, ArrayList<ArrayList<Order>> allRoute)
//			throws FileNotFoundException, InterruptedException, ExecutionException, IloException {
//		this.d = d;
//		this.vehicles = d.vehicles;
//		this.lsps = d.lsps;
//		this.orders = d.orders;
//		this.locations = d.locations;
//		int cnt = 1;
//		for (Lsp lsp : d.lsps) {
//			lspCurrent = lsp;
//			for (int i = 0; i < lspCurrent.totalCar; i++) {
//				d.con.lspVechiclesHiddenMap.put(lspCurrent.lspVehiclesObject.get(i).depot.id, cnt);
//				cnt++;
//			}
//		}
//		needMiddleAdd = new double[vehicles.size()][2];
//		cf = new CommonFunction();
//		ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>(allRoute);
//		initialStatic();
//		noNeedMore = false;
//		tabuCnt = (int) (7.5 * Math.log(d.con.totalCar()));
//
//		// @used for multi-lsps, run independent for each lsp, once a solution for a
//		// single lsp been found, can add to this arraylist
//
//		ArrayList<ArrayList<Order>> overallBestRoute = new ArrayList<ArrayList<Order>>();
//		double bestObj = 9e10;
//		globalStart = System.currentTimeMillis();
//
//		for (int i = 0; i < d.con.paramALNSItr; i++) {
//			overallMergedRoute = new ArrayList<ArrayList<Order>>();
//			allBestfeasibenefitWhole = 9e10;
//			allBestNoTimeVioBenifit = 9e10;
//			allBestbestBenifit = 9e10;
//			Collections.shuffle(d.lsps);
//
//			d.con.dummyGroup = new HashMap<Integer, ArrayList<DummyOrder>>();
//			for (Lsp lsp : d.lsps) {
//				lspCurrent = lsp;
//				// this.k = kValue();
//				start = System.currentTimeMillis();
//				timeUsed = 0;
//				allBestnoTimeVioRoute = new ArrayList<ArrayList<Order>>();
//				allBestbestUsedRoute = new ArrayList<ArrayList<Order>>();
//				allBestfeasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
//				allBestfeasibenefitWhole = 9e10;
//				allBestNoTimeVioBenifit = 9e10;
//				allBestbestBenifit = 9e10;
//
//				// System.out.println("iteration "+i);
//				route = new ArrayList<ArrayList<Order>>();
//
//				if (d.totalSize(route) < 1) {
//					if (i == d.con.paramALNSItr % 2
//							&& (allBestfeasiusedRouteWhole == null || allBestfeasiusedRouteWhole.size() < 1)) {
//						lsp.setMaxDrop((int) (lsp.maxDrop + Math.ceil(0.1 * lsp.maxDrop)));
//						lsp.setIncreasedDrops(true);
//					}
//
//					// System.out.println("before initialize");
//					initilize(route);
//					bestUsedRoute = new ArrayList<ArrayList<Order>>();
//					feasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
//					noTimeVioRoute = new ArrayList<ArrayList<Order>>();
//					feasibenefitWhole = 1e9;
//					noTimeVioBenifit = 1e9;
//					bestBenifit = 1e9;
//					//System.out.println("after initialize\t" + human(route));
//
//					initial_2cnt();
//					setWeight();
//					d.con.initial_yita();
//					threshold = 0.501;
//					double newbenfit = calcuNewBenfit(route);
//					feasiusedRouteWhole.clear();
//
//					if (lspCurrent.totalSize == serveAll(route) && newbenfit < bestBenifit) {
//						bestUsedRoute = cf.cloneVector(route);
//						bestBenifit = newbenfit;
//					}
//					alnsSearch(route, i);
//				}
//				finalAllBest();
//
//			}
//			//System.out.println("calling CP internal");
//			example = new CplexCPMostNoneIMD();
//			example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//			example.dynamicSet(overallMergedRoute, false);
//
//			double newbenfit = example.objective + example.totalTraPlusServ
//					+ (lspCurrent.totalSize - serveAll(overallMergedRoute)) * d.con.price;
//
//			if (newbenfit < bestObj) {
//				overallBestRoute = cf.cloneVector(overallMergedRoute);
//				bestObj = newbenfit;
//			}
//
//		}
//		overallBestRoute = cf.cloneVector(overallBestRoute);
//		d.con.dummyGroup = new HashMap<Integer, ArrayList<DummyOrder>>();
//		if (d.withMixLSPCPOptimize)
//			callCP();
//
//		setLspRoute();
//		//System.out.println("calling CP second time");
//		example = new CplexCPMostNoneIMD();
//		example.set(false, 0, vehicles, orders, locations, d.con, d.lspDocks);
//		// example.dynamicSet(overallMergedRoute, false);
//		example.setRoute(lspRoute, overallMergedRoute, travelTime, totalTravelDis, postPone, needMiddleAdd);
//		// example.setObjective(1e8);
//
//	}
//
//	//
//	// @try to improve the solution by adding depot, been called for initial
//	// solution and every 280 iterations
//	private void addDepot(ArrayList<ArrayList<Order>> route) {
//		boolean feasible = true;
//		boolean allFeasible = false;
//		int cnt = 0;
//		double orginalResult = 1e10;
//		double bestResult = 1e10;
//		Order insertNode = null;
//		int bestVioVolume = 0;
//		int insertPos = 0;
//		int realK = 0;
//		while (!allFeasible && cnt < 2) {
//			allFeasible = true;
//			for (int k = 0; k < route.size(); k++) {
//				realK = d.con.lspVechiclesHiddenMap.get(route.get(k).get(0).id) - 1;
//
//				if (calculateNumberTrips(route.get(k)) >= 4)
//					continue;
//
//				feasible = calculateMultiDepotCapacityConstraint(new PathSolution(route.get(k), realK), 0);
//				if (!feasible) {
//					allFeasible = false;
//					bestResult = calcuResult(new PathSolution(route.get(k), realK), d.con.yita2, 0, 0, 0);
//					bestVioVolume = vioVolume;
//					insertNode = null;
//					insertPos = -1;
//					for (Order node : vehicles.get(realK).vehicleStartMiddleOrders) {
//						// System.err.println("addDepot\t" + node.id);
//						for (int j = 0; j < route.get(k).size() - 2; j++) {
//							if (calculateNumberTrips(route.get(k)) >= 4)
//								break;
//							if (node.id == route.get(k).get(0).id
//									&& (route.get(k).get(0).serviceTime < 2 || route.get(k).get(1).serviceTime < 2))
//								continue;
//							ArrayList<Order> newPath = cf.arraylistCloneObject(route.get(k));
//							newPath.add(j + 1, node);
//							orginalResult = calcuResult(new PathSolution(newPath, realK), d.con.yita2, 0, 0, 0);
//							bestVioVolume = vioVolume;
//							// if ((vioVolume==0&&orginalResult <
//							// bestResult)||vioVolume<bestVioVolume) {
//							if (orginalResult < bestResult) {
//								bestResult = orginalResult;
//								insertNode = node;
//								insertPos = j + 1;
//							}
//						}
//					}
//					// System.out.println("insertNode\t"+insertNode);
//					if (insertNode != null)
//						route.get(k).add(insertPos, insertNode);
//				}
//			}
//			cnt++;
//		}
//	}
//
//	private int calculateNumberTrips(ArrayList<Order> path) {
//		int numberTrips = 0;
//		boolean before = true;
//		boolean after = true;
//		Order preNode = null;
//		for (int i = 0; i < path.size() - 1; i++) {
//			before = true;
//			after = true;
//			if (d.startEndNodes.contains(path.get(i)))
//				before = false;
//			if (d.startEndNodes.contains(path.get(i + 1)))
//				after = false;
//			if (before != after)
//				numberTrips += 1;
//			else if (path.get(i).id == path.get(i + 1).id)
//				numberTrips += 2;
//		}
//		return numberTrips / 2;
//	}
//
//	private boolean calculateMultiDepotCapacityConstraint(PathSolution pathSolution, int pos) {
//		boolean feasible = true;
//		int size = pathSolution.getPath().size();
//		if (size < 2)
//			return false;
//
//		int k = d.con.lspVechiclesHiddenMap.get(pathSolution.getPath().get(0).id) - 1;
//		if (size == 3 && Math.abs(pathSolution.getPath().get(1).volume) > vehicles.get(k).capacity)
//			return false;
//		int contains = 0;
//		int index = 0;
//		int cnt = 0;
//		// feasible = simpleCheck(pathSolution, pos);
//		if (d.containsCollectionOrderFlag) {
//			Set<Integer> newSet = d.con.cloneSet(d.collectionOrder.keySet());
//
//			size = newSet.size();
//			ArrayList<Integer> newPath = d.con.humanU(pathSolution.getPath());
//			newSet.removeAll(newPath);
//			index = 0;
//			cnt = 0;
//
//			contains = size - newSet.size();
//
//			if (contains > 0) {
//				for (Integer node : d.collectionOrder.keySet())
//					orders.get(node).setVolume(d.collectionOrder.get(node)[0]);
//
//				Set<Integer> nnSet = d.con.cloneSet(d.collectionOrder.keySet());
//				nnSet.removeAll(newSet);
//				ArrayList<Order> path = new ArrayList<Order>(d.con.cloneSingleVector(pathSolution.getPath()));
//				ArrayList<Order> oldPath = new ArrayList<Order>(d.con.cloneSingleVector(pathSolution.getPath()));
//				cnt = 1;
//				for (Integer nodeIdx : nnSet) {
//					index = newPath.indexOf(nodeIdx);
//					path.add(index + cnt, d.dummyOrderMap.get(nodeIdx));
//					cnt++;
//				}
//				pathSolution.setPath(path);
//				feasible = deCoupleCheck(pathSolution, pos);
//				pathSolution.setPath(d.con.cloneSingleVector(oldPath));
//
//			}
//		}
//		if (contains < 1)
//			feasible = deCoupleCheck(pathSolution, pos);
//
//		for (Integer node : d.collectionOrder.keySet())
//			orders.get(node).setVolume(d.collectionOrder.get(node)[0] + d.collectionOrder.get(node)[1]);
//		return feasible;
//	}
//
//	private boolean simpleCheck(PathSolution pathSolution, int pos) {
//		boolean feasible = true;
//		ArrayList<Order> newPath = cf.arraylistCloneObject(pathSolution.getPath());
//		int size = newPath.size();
//		int k = pathSolution.getVehicleID();
//		double totalVolume = 0;
//		double posVolume = 0;
//		double negVolume = 0;
//		for (int i = 1; i < size - 1; i++) {
//			if (d.collectionOrder.containsKey(newPath.get(i).id)) {
//				posVolume += d.collectionOrder.get(newPath.get(i).id)[0];
//				negVolume += d.collectionOrder.get(newPath.get(i).id)[1];
//				// System.err.println(newPath.get(i).id+"\t"+
//				// d.con.initialMap.get(newPath.get(i).id)+"\t"+newPath.get(i).volume()+"\t"+d.collectionOrder.get(newPath.get(i).id)[0]+"\t"+d.collectionOrder.get(newPath.get(i).id)[1]);
//			}
//
//			totalVolume += newPath.get(i).volume();
//			if (d.startEndNodes.contains(newPath.get(i))) {
//				totalVolume = 0;
//				posVolume = 0;
//				negVolume = 0;
//			}
//			if (Math.abs(totalVolume) > vehicles.get(k).capacity || posVolume > vehicles.get(k).capacity
//					|| Math.abs(negVolume) > vehicles.get(k).capacity) {
//				feasible = false;
//				break;
//			}
//
//		}
//		return feasible;
//	}
//
//	private boolean deCoupleCheck(PathSolution pathSolution, int pos) {
//		boolean feasible = true;
//		int k = pathSolution.getVehicleID();
//		vioVolume = 0;
//		deductLoad(pathSolution, pos);
//		double totalVolume = 0;
//		int size = pathSolution.getPath().size();
//		// add dummy order for collection order
//		for (int i = pos; i < size - 1; i++) {
//			totalVolume += forWardVolume.get(i);
//			if (totalVolume > vehicles.get(k).capacity || totalVolume < -1e-3) {
//				feasible = false;
//				vioVolume += Math.max(0, (totalVolume - vehicles.get(k).capacity)) + Math.max(0, -totalVolume);
//				break;
//			}
//		}
//		return feasible;
//	}
//
//	private void deductLoad(PathSolution pathSolution, int pos) {
//		int size = pathSolution.getPath().size();
//		Order currentNode = pathSolution.getPath().get(pos);
//		Order nextNode = null;
//		vioVolume = 0;
//		forWardVolume = new HashMap<Integer, Double>();
//
//		for (int i = pos; i < size - 1; i++) {
//			forWardVolume.put(i, -pathSolution.getPath().get(i).volume());
//		}
//
//		for (int i = pos; i < size - 1; i++) {
//			nextNode = pathSolution.getPath().get(i);
//
//			if (d.startEndNodes.contains(nextNode)) {
//				for (int j = i + 1; j < size - 1; j++) {
//					currentNode = pathSolution.getPath().get(j);
//					if (currentNode.newAddedOrderFlag)
//						continue;
//					if (d.startEndNodes.contains(currentNode)) {
//						if (Math.abs(currentNode.orderLocation - nextNode.orderLocation) < 1e-3) {
//							break;
//						} else
//							continue;
//					} else if (currentNode.volume() > 0
//							&& Math.abs(currentNode.pickupDepotLocation - nextNode.orderLocation) < 1e-3) {
//						forWardVolume.put(i, forWardVolume.get(i) + currentNode.volume());
//					}
//				}
//			}
//		}
//		// System.out.println(d.con.humanU(pathSolution.getPath()));
//	}
//
//	// @no need currently, mainly used for trips provided beforehand
//	private void initialStatic() {
//		d.staticNodes = new ArrayList<Order>();
//	}
//
//	// @main code
//	public void alnsSearch(ArrayList<ArrayList<Order>> route, int iter)
//			throws FileNotFoundException, InterruptedException, ExecutionException, IloException {
//		/* not affected */
//		ArrayList<ArrayList<Order>> recRouteall = new ArrayList<ArrayList<Order>>();
//		List<Integer> vehicleSet = new ArrayList<Integer>();
//		for (int i = 0; i < lspCurrent.totalCar; i++) {
//			vehicleSet.add(i);
//		}
//
//		Random rand = new Random();
//		int stop = 0;
//		double newbenfit = 0;
//		double newFeaBenfit = 0;
//		/* not affected */
//
//		maxCnt = d.iterations / cntCpOpt;
//		HashSet<Integer> storedHashSet = new HashSet<Integer>();
//		d.con.setInitialYita1(1);
//		d.con.setInitialYita2(1);
//		d.con.setInitialYita3(1);
//		newbenfit = calcuNewBenfit(route);
//		boolean needCheckCP = true;
//		if (check && (lspCurrent.totalSize == serveAll(route))) {
//			needCheckCP = needCheckCPForWaitingTime(route);
//			if (!needCheckCP) {
//				feasibenefitWhole = newbenfit + (lspCurrent.totalSize - serveAll(route)) * d.con.price;
//				feasiusedRouteWhole = cf.cloneVector(route);
//			} else {
//				example = new CplexCPMostNoneIMD();
//				example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//				try {
//					example.dynamicSet(route, false);
//				} catch (IloException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				newbenfit = example.objective
//						+ example.totalTraPlusServ * +(lspCurrent.totalSize - serveAll(route)) * d.con.price;
//				if (example.totalWaitTime < 10e5 && newbenfit < feasibenefitWhole) {
//					feasibenefitWhole = newbenfit;
//					feasiusedRouteWhole = cf.cloneVector(route);
//				}
//			}
//		}
//
//		int removeChoose = 0;
//		int k = 0;
//		double oldBenfit = feasibenefitWhole;
//		double probability = 0.5;
//		T0 = -Math.abs(oldBenfit) / Math.log(probability);
//		oldHash = 0;
//		// recRouteall = SortCopy.cloneVector(route);
//		startServTime = new int[d.con.stops() + d.con.totalCar() * 2 + 1];
//		double preTimeUsed = 0;
//
//		long last = System.currentTimeMillis();// maxCnt
//
//		// System.out.println(d.con.stops);
//		oldBenfit = 1e10;
//		globalCnt = 0;
//
//		while (globalCnt < maxCnt && timeUsed < lspCurrent.timeLimit / d.con.paramALNSItr) {
//			// @first half iterations mainly focus on minimize the travel time, after that
//			// will focus on whole objective value
//			if (globalCnt == maxCnt / 2) {
//				// d.con.setObjFlag(initialObjFlag);
//				insertSize = 4;
//				initial_2cnt();
//				setWeight();
//				insertWeigt[0] = 0.1;
//				insertWeigt[1] = 0.1;
//				insertWeigt[2] = 0.4;
//				insertWeigt[3] = 0.4;
//
//				if (feasiusedRouteWhole != null)
//					feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
//				if (bestUsedRoute != null)
//					bestBenifit = calcuNewBenfit(bestUsedRoute);
//				if (noTimeVioRoute != null)
//					noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
//				newbenfit = calcuNewBenfit(route);
//			}
//
//			needCheckCP = true;
//			globalCnt++;
//			preTimeUsed = timeUsed;
//			removeChoose = cf.removeRoulwheel(removeWeigt);
//			List<Order> removeSet = remove(route, removeChoose);
//
//			cntRemove[removeChoose] += 1.0D;
//			if (serveAll(feasiusedRouteWhole) < lspCurrent.totalSize) {
//				feasiusedRouteWhole.clear();
//			}
//			insertChoose = cf.removeRoulwheel(insertWeigt);
//			if (removeSet.size() < 1)
//				continue;
//			cntInsert[insertChoose] += 1.0D;
//			insert(route, removeSet);
//
//			for (ArrayList<Order> path : route) {
//				if (path.size() < 2)
//					break;
//			}
//			// fillLeftOver(route);
//			// removeDuplicate(route);
//			last = System.currentTimeMillis();
//			timeUsed = TimeUnit.MILLISECONDS.toSeconds(last - start) / 60.0;
//			check = true;
//			newbenfit = calcuNewBenfit(route);
//
//			if (newbenfit > 0 && newbenfit < bestBenifit && lspCurrent.totalSize == serveAll(route)) {
//				bestBenifit = newbenfit;
//				bestUsedRoute = cf.cloneVector(route);
//			}
//
//			if (check && newbenfit < noTimeVioBenifit) {
//				scoreRemove[removeChoose] += d.con.x()[5] * 0.8;
//				scoreInsert[insertChoose] += d.con.x()[5] * 0.8;
//
//				noTimeVioBenifit = newbenfit;
//				noTimeVioRoute = cf.cloneVector(route);
//			}
//
//			// &&continueCnt<2
//			if (check) {
//				scoreRemove[removeChoose] += d.con.x()[5] * 1.0;
//				scoreInsert[insertChoose] += d.con.x()[5] * 1.0;
//				// System.out.println("route\t" + route);
//				needCheckCP = needCheckCPForWaitingTime(route);
//				// System.err.println("ever come 0\t"+lspCurrent.totalSize
//				// +"\t"+serveAll(route)+newbenfit +"\t"+ feasibenefitWhole);
//				if (needCheckCP == false && (lspCurrent.totalSize == serveAll(route))
//						&& newbenfit < feasibenefitWhole) {
//					feasibenefitWhole = newbenfit;
//					feasiusedRouteWhole = cf.cloneVector(route);
//				} else if (needCheckCP && !storedHashSet.contains(route.hashCode())) {
//					noNeedMore = true;
//					// maxCnt = 15000;
//					d.con.setVioCheck1(true);
//					d.con.setVioCheck2(true);
//
//					example = new CplexCPMostNoneIMD();
//					example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//					example.dynamicSet(route, false);
//
//					if (example.totalWaitTime < 10e5) {
//						newFeaBenfit = example.objValue + (lspCurrent.totalSize - serveAll(route)) * d.con.price;
//
//						if (newFeaBenfit < feasibenefitWhole) {
//							feasibenefitWhole = newFeaBenfit;
//							feasiusedRouteWhole = cf.cloneVector(route);
//							d.con.decreaseYita3();
//							scoreRemove[removeChoose] += d.con.x()[5] * 1.2;
//							scoreInsert[insertChoose] += d.con.x()[5] * 1.2;
//						}
//						if (newFeaBenfit < bestBenifit && (lspCurrent.totalSize == serveAll(route))) {
//							bestBenifit = newbenfit;
//							bestUsedRoute = cf.cloneVector(route);
//						}
//					}
//					if (example.totalWaitTime >= 10e5) {
//						d.con.increaseYita3();
//						check = false;
//					}
//					example.clear();
//					storedHashSet.add(feasiusedRouteWhole.hashCode());
//					if (storedHashSet.size() > 100)
//						storedHashSet = new HashSet<Integer>();
//				}
//			}
//			d.con.resetyita(d.con.vioCheck1(), d.con.vioCheck2());
//			recRouteall = cf.cloneVector(route);
//			if (newbenfit < oldBenfit) {
//				stop = 0;
//				scoreRemove[removeChoose] += d.con.x()[6] * 0.4;
//				scoreInsert[insertChoose] += d.con.x()[6] * 0.4;
//				oldBenfit = newbenfit;
//			} else {
//				stop++;
//				probability = calcuTemp(d.con.x()[4], newbenfit, oldBenfit);
//				double randprob = rand.nextDouble();
//
//				if (randprob <= probability) {
//					scoreRemove[removeChoose] += d.con.x()[7] * 0.2;
//					scoreInsert[insertChoose] += d.con.x()[7] * 0.2;
//					oldBenfit = newbenfit;
//				} else if (!check)
//					route = cf.cloneVector(recRouteall);
//			}
//
//			if ((globalCnt + 1) % 280 == 0) {
//				// postProcessMerge(route);this one
//				addDepot(route);
//				// too many depot, remove them
//				// removeInsertDepot(route, example);
//				for (int i = 0; i < removeSize; i++) {
//					removeWeigt[i] = (0.6 * removeWeigt[i] + 0.4 * scoreRemove[i] / cntRemove[i]);
//				}
//				for (int i = 0; i < insertSize; i++) {
//					insertWeigt[i] = (0.6 * insertWeigt[i] + 0.4 * scoreInsert[i] / cntInsert[i]);
//				}
//				scoreInsert = new double[insertSize];
//				scoreRemove = new double[removeSize];
//				cntRemove = new double[removeSize];
//				cntInsert = new double[insertSize];
//				initial_2cnt();
//			}
//			if ((globalCnt + 1) % 290 == 0) {
//				removeDepot(route, false);
//			}
//			tabuList.clear();
//			for (ArrayList<Order> path : route)
//				tabuList.add(path.hashCode());
//			updateTabuList();
//
//			if (globalCnt == 10000) {
//				d.con.setTimeVioCost();
//				newbenfit = calcuNewBenfit(route);
//				bestBenifit = calcuNewBenfit(bestUsedRoute);
//			}
//
//			if (globalCnt > maxCnt / 2 && stop > 5000)
//				break;
//		}
//		threshold = 0.1;
//		removeDepot(route, true);
//
//		// for (Vehicle car : vehicles) {
//		// car.setTruckUpperTime(car.initialUpper);
//		// car.setTruckLowerTime(car.initialLower);
//		// }
//
//		withSmoothAndWithoutSmooth(iter);
//
//		check = true;
//		calcuNewBenfit(feasiusedRouteWhole);
//
//		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
//			removeDepot(feasiusedRouteWhole, true);
//			removeDuplicate(feasiusedRouteWhole);
//			removePath(feasiusedRouteWhole);
//			feasibenefitWhole = calculateWithDockCapacityObj(feasiusedRouteWhole);
//			feasibenefitWhole += feasiusedRouteWhole.size() * d.con.price / 100;
//			// example.clear();
//			//System.err.println("feasiusedRouteWhole\t" + human(feasiusedRouteWhole) + "\t" + feasibenefitWhole);
//		}
//		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0
//				&& feasibenefitWhole < allBestfeasibenefitWhole) {
//			allBestfeasiusedRouteWhole = cf.cloneVector(feasiusedRouteWhole);
//			allBestfeasibenefitWhole = feasibenefitWhole;
//		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0 && noTimeVioBenifit
//				+ 1e3 * vioTime(noTimeVioRoute) < allBestNoTimeVioBenifit + 1e3 * vioTime(allBestnoTimeVioRoute)) {
//			removeDepot(noTimeVioRoute, true);
//			removeDuplicate(noTimeVioRoute);
//			removePath(noTimeVioRoute);
//			// noTimeVioBenifit = calculateWithDockCapacityObj(noTimeVioRoute);
//			// example.clear();
//			allBestnoTimeVioRoute = cf.cloneVector(noTimeVioRoute);
//			allBestNoTimeVioBenifit = noTimeVioBenifit;
//		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0 && bestBenifit < allBestbestBenifit) {
//			removeDepot(bestUsedRoute, true);
//			removeDuplicate(bestUsedRoute);
//			removePath(bestUsedRoute);
//			// bestBenifit = calculateWithDockCapacityObj(bestUsedRoute);
//			// example.clear();
//			allBestbestUsedRoute = cf.cloneVector(bestUsedRoute);
//			allBestbestBenifit = bestBenifit;
//			//System.err.println("allBestbestUsedRoute\t" + human(allBestbestUsedRoute) + "\t" + allBestbestBenifit);
//		}
//
//	}
//
//	private void removePath(ArrayList<ArrayList<Order>> feasiusedRouteWhole) {
//		Iterator<ArrayList<Order>> i = feasiusedRouteWhole.iterator();
//
//		while (i.hasNext()) {
//			if (i.next().size() <= 2)
//				i.remove();
//		}
//
//	}
//
//	private double calculateWithDockCapacityObj(ArrayList<ArrayList<Order>> route) {
//		if (route.get(0).get(0).serviceTime == 1) {
//			for (ArrayList<Order> path : route) {
//				if (path.size() < 3)
//					continue;
//				path.get(0).setServiceTime(45);
//				path.get(1).setServiceTime(1);
//			}
//		}
//		example = new CplexCPMostNoneIMD();
//		int u = lspCurrent.id;
//		example.set(false, u, vehicles, orders, locations, d.con, d.lspDocks);
//		try {
//			example.dynamicSet(route, false);
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IloException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		if (example.callCP)
//			return example.objective + example.totalTraPlusServ + example.totalTraPlusServ * 0.8;
//		else
//			return feasibenefitWhole;
//	}
//
//	private void doubleCheckDepot(ArrayList<ArrayList<Order>> route) {
//		for (int i = 0; i < route.size(); i++) {
//			if (route.get(i).get(0).id != d.vehicles.get(i).endDepot.id)
//				route.get(i).add(0, d.vehicles.get(i).endDepot);
//			else if (!d.startEndNodes.contains(route.get(i).get(1)))
//				route.get(i).add(0, d.vehicles.get(i).middleDepots.get(0));
//		}
//	}
//
//	/**
//	 * Find the lowerbound of vehicles = max[ (number of orders)/(2*max(orders per
//	 * vehicle)), (order capacity)/(2*max(cap per vehicle)) ] 2 because 2 trips per
//	 * day Number of orders: find sum of all merged orders max(orders per vehicles):
//	 * this is pre-defined and can be changed in FileManager when read from
//	 * BR1_*input.txt order capacity: sum of capacity of merged orders max(cap per
//	 * vehicle): maximum of capacity per vehicle
//	 * 
//	 * @return k
//	 */
//	private int kValue() {
//
//		// Find MAX_ORDERS_PER_VEHICLE
//		int MAX_ORDERS_PER_VEHICLE = this.lspCurrent.getMaxDrop();
//		System.out.println("max order: " + MAX_ORDERS_PER_VEHICLE);
//
//		// Find sum of all merged orders
//		int numOfOrders = this.lspCurrent.lspRealOrders.size();
//
//		// Find sum of capacity of merged orders
//		double totalCapacityOfMergedOrders = 0.0;
//		for (Order order : this.lspCurrent.lspRealOrders) {
//			if (order != null)
//				totalCapacityOfMergedOrders += order.volume;
//		}
//
//		// Find the maximum capacity of all vehicles
//		Comparator<Vehicle> cmpCap = new Comparator<Vehicle>() {
//			@Override
//			public int compare(Vehicle v1, Vehicle v2) {
//				return Double.valueOf(v1.capacity).compareTo(Double.valueOf(v2.capacity));
//			}
//		};
//		double maxCapPerVehicle = Collections.max(this.lspCurrent.lspVehiclesObject, cmpCap).capacity;
//
//		int metric1 = (int) Math.ceil((double) numOfOrders / (2 * MAX_ORDERS_PER_VEHICLE));
//		int metric2 = (int) Math.ceil((double) totalCapacityOfMergedOrders / (2 * maxCapPerVehicle));
//
//		return Math.min(Math.max(metric1, metric2), this.lspCurrent.lspVehiclesObject.size()); // min outside is to
//																								// protect if the max
//		// inside bigger than the number of vehicles
//		// LSP gives to us
//	}
//
//	// post process for reducing number of vehicles
//	// sort vehicle according to the number of orders
//	private void sortVehicles(ArrayList<ArrayList<Order>> route) {
//		// sort vehicle according to the capacity
//		Collections.sort(route, (Comparator<ArrayList<Order>>) (p1, p2) -> {
//			if (p1.size() == p2.size())
//				return 0;
//			return p1.size() < p2.size() ? -1 : 1;
//		});
//	}
//
//	private void withSmoothAndWithoutSmooth(int iter) {
//		needSmooth = false;
//		finalOptimizeFlag = true;
//		callPostProcess();// 5 minutes
//		needSmooth = true;
//		callPostProcessOnlyIJ(iter);
//	}
//
//	private void callPostProcessOnlyIJ(int iter) {
//		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
//			feasiusedRouteWhole = cf.cloneVector(reOptimizeSmallSet(feasiusedRouteWhole, iter));
//			feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
//		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
//			noTimeVioRoute = cf.cloneVector(reOptimizeSmallSet(noTimeVioRoute, iter));
//			noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
//		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
//			bestUsedRoute = cf.cloneVector(reOptimizeSmallSet(bestUsedRoute, iter));
//			bestBenifit = calcuNewBenfit(bestUsedRoute);
//		}
//	}
//
//	private ArrayList<ArrayList<Order>> reOptimizeSmallSet(ArrayList<ArrayList<Order>> route, int iter) {
//		//System.out.println("post process before " + human(route) + "\n");
//		if (route != null && route.size() > 0) {
//			route = smoothMultiVehicleInsertion(route);
//			route = smoothBetweenTrip(route);
//			route = smoothFinalSwap(route);
//			// Reducing Vehicles
//			// if (kValue() < route.size()) {
//			// // if(iter == 9) {
//			//
//			// route = mergeTwoPossibleVehicles(route);
//			//
//			// System.out.println("route size after checking k value " + route.size());
//			// System.out.println("before sorting " + human(route));
//			// sortVehicles(route);
//			// System.out.println("after sorting" + human(route));
//			//
//			// route = assignToDifferentVehicles(route);
//			// System.out.println("After reassigning " + human(route));
//			//
//			// }
//		}
//		//System.out.println("post process after " + route.size() + "\t"+human(route)+"\n");
//		return route;
//	}
//
//	// this problem
//	private ArrayList<ArrayList<Order>> addSolution(ArrayList<ArrayList<Order>> solution) {
//		ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>();
//		for (int i = 0; i < lspCurrent.totalCar; i++) {
//			ArrayList<Order> singPath = new ArrayList<Order>();
//			int multiTripCnt = Math.max(lspCurrent.multiTripCnt, 2);
//
//			if (d.lspMiddleDepotOrder.get(lspCurrent.id).size() == 0) {
//				while (multiTripCnt > 0) {
//					singPath.add(lspCurrent.lspVehiclesObject.get(i).depot);
//					multiTripCnt--;
//				}
//			} else {
//				singPath.add(lspCurrent.lspVehiclesObject.get(i).depot);
//				// singPath.add(d.vehicles.get(i).depot);
//				// multiTripCnt -= 1;
//				while (multiTripCnt > 0) {
//					for (Order u : d.lspMiddleDepotOrder.get(lspCurrent.id)) {
//						singPath.add(u);
//						multiTripCnt -= 1;
//					}
//				}
//			}
//			singPath.add(lspCurrent.lspVehiclesObject.get(i).endDepot);
//			route.add(singPath);
//		}
//		if (solution.size() < 1)
//			return cf.cloneVector(route);
//		for (int i = 0; i < route.size(); i++) {
//			if (solution.size() - 1 < i)
//				break;
//			else if (solution.get(i).get(0).id != route.get(i).get(0).id)
//				solution.add(i, cf.singleCloneOrder(route.get(i)));
//			else if (solution.get(i).size() < 3)
//				solution.set(i, cf.singleCloneOrder(route.get(i)));
//		}
//		// solution.add(cf.singleCloneOrder(route.get(i)));
//		return cf.cloneVector(solution);
//	}
//
//	private int[] multiVehiclesInsertionVersion2(ArrayList<ArrayList<Order>> newRoute, Order chooseNode,
//			ArrayList<int[]> trackInsertArray) {
//		int[] minarray = new int[2];
//		minarray[1] = -1;
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		boolean flag;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		needMiddleAdd = new double[vehicles.size()][2];
//		postPone = new double[vehicles.size()];
//		int realK = 0;
//		for (int i = 0; i < route.size(); i++) {
//			realK = lspCurrent.vehicleMap.get((Integer) (i));
//
//			increa_comp = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//			globalCost = 0;
//			// System.out.println(human(route));
//			for (int m = 0; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//
//				if (tripSize(route.get(i), m + 1) > lspCurrent.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//				benefi = globalCost - increa_comp + r.nextDouble() * 1;
//				if (finalOptimizeFlag) {
//					flag = false;
//					if (benefi < minBeneif && volumeFeasible) {
//						for (int[] insertArray : trackInsertArray) {
//							if (insertArray[0] == i && insertArray[1] == (m + 1)) {
//								flag = true;
//								break;
//							}
//						}
//						if (flag == false) {
//							minBeneif = benefi;
//							minarray[0] = i;
//							minarray[1] = m + 1;
//						}
//					}
//				} else {
//					flag = false;
//					if (benefi < minBeneif) {
//						for (int[] insertArray : trackInsertArray) {
//							if (insertArray[0] == i && insertArray[1] == (m + 1)) {
//								flag = true;
//								break;
//							}
//						}
//						if (flag == false) {
//							minBeneif = benefi;
//							minarray[0] = i;
//							minarray[1] = m + 1;
//						}
//					}
//				}
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	public static ArrayList<Integer> human2(ArrayList<Order> path) {
//		ArrayList<Integer> humanReadRoute = new ArrayList<Integer>();
//		for (Order node : path) {
//			humanReadRoute.add(node.id);
//		}
//		return humanReadRoute;
//	}
//
//	private ArrayList<ArrayList<Order>> smoothBetweenTrip(ArrayList<ArrayList<Order>> route) {
//		double oldCost = calcuNewBenfit(route);
//		double oldTravelTime = travelTime;
//		double newCost = 0;
//		int cnt = 0;
//		Order startDepot1 = null;
//		Order startDepot2 = null;
//		for (int loop = 0; loop < 2; loop++) {
//			HashMap<Integer, ArrayList<ArrayList<Order>>> routeTrips = splitRouteToTrips(route);
//
//			ArrayList<Order> tempTrip1 = new ArrayList<Order>();
//			ArrayList<Order> tempTrip2 = new ArrayList<Order>();
//			cnt = 0;
//
//			for (int i = 0; i < route.size(); i++) {
//				for (int j = i + 1; j < route.size(); j++) {
//					// if (vehicles.get(i).vehicleStartMiddleLocations.size() >1||
//					// vehicles.get(j).vehicleStartMiddleLocations.size()>1||
//					// vehicles.get(i).vehicleStartMiddleLocations.hashCode()!=vehicles.get(j).vehicleStartMiddleLocations.hashCode())
//					//// vehicles.get(i).vehicleStartMiddleLocations
//					//// .hashCode() != vehicles.get(j).vehicleStartMiddleLocations.hashCode())
//					// continue;
//					if (routeTrips.get((Integer) i).size() <= 1 && routeTrips.get((Integer) j).size() <= 1)
//						continue;
//					orginRoute = cf.cloneVector(route);
//					startDepot1 = route.get(i).get(0);
//					startDepot2 = route.get(j).get(0);
//					oldCost = calcuNewBenfit(route);
//					int[] swapArray = swapTrips(i, routeTrips.get(i), startDepot1, j, routeTrips.get(j), startDepot2);
//					if (swapArray[0] > -2) {
//						if (routeTrips.get(i).size() > 1 && routeTrips.get(j).size() > 1) {
//							tempTrip1 = routeTrips.get(i).get(swapArray[0]);
//							tempTrip2 = routeTrips.get(j).get(swapArray[1]);
//							routeTrips.get(i).set(swapArray[0], tempTrip2);
//							routeTrips.get(j).set(swapArray[1], tempTrip1);
//						} else if ((routeTrips.get(i).size() <= 1 && routeTrips.get(j).size() > 1)) {
//							routeTrips.get(i).add(routeTrips.get(j).get(swapArray[1]));
//							routeTrips.get(j).remove(swapArray[1]);
//						} else if ((routeTrips.get(i).size() > 1 && routeTrips.get(j).size() <= 1)) {
//							routeTrips.get(j).add(routeTrips.get(i).get(swapArray[1]));
//							routeTrips.get(i).remove(swapArray[1]);
//						}
//						route.set(i, mergeSeperateTrip(startDepot1, routeTrips.get(i)));
//						route.set(j, mergeSeperateTrip(startDepot2, routeTrips.get(j)));
//						cnt++;
//						// System.err.println("ever happen");
//						// System.out.println("origin\t" + human(orginRoute));
//						// System.out.println("final\t" + human(route));
//						newCost = calcuNewBenfit(route);
//						if ((newCost >= fitnessParaThreshold * oldCost)
//								|| (cnt > 1 && travelTime >= oldTravelTime + travelTimeParaThreshold)) {
//							route = cf.cloneVector(orginRoute);
//							break;
//						}
//					}
//				}
//			}
//		}
//		return route;
//	}
//
//	HashMap<Integer, ArrayList<ArrayList<Order>>> splitRouteToTrips(ArrayList<ArrayList<Order>> route) {
//		HashMap<Integer, ArrayList<ArrayList<Order>>> routeTrips = new HashMap<Integer, ArrayList<ArrayList<Order>>>();
//
//		for (int key = 0; key < route.size(); key++) {
//			ArrayList<ArrayList<Order>> splitTrips = new ArrayList<ArrayList<Order>>();
//
//			ArrayList<Order> trip = new ArrayList<Order>();
//			for (int i = 0; i < route.get(key).size() - 1; i++) {
//				if (i > 0 && d.startEndNodes.contains(route.get(key).get(i))) {
//					trip.add(route.get(key).get(i));
//					if (trip.size() > 2)
//						splitTrips.add(trip);
//					trip = new ArrayList<Order>();
//					trip.add(route.get(key).get(i));
//				} else
//					trip.add(route.get(key).get(i));
//			}
//			trip.add(route.get(key).get(route.get(key).size() - 1));
//			if (trip.size() > 2)
//				splitTrips.add(trip);
//
//			routeTrips.put(key, splitTrips);
//		}
//		return routeTrips;
//	}
//
//	ArrayList<Order> mergeSeperateTrip(Order startDepot, ArrayList<ArrayList<Order>> vehicleTrip) {
//		ArrayList<Order> mergeTrip = new ArrayList<Order>();
//		if (vehicleTrip.size() < 1 || vehicleTrip == null)
//			return mergeTrip;
//		mergeTrip.add(startDepot);
//		mergeTrip.addAll(vehicleTrip.get(0));
//		for (int i = 1; i < vehicleTrip.size(); i++) {
//			if (vehicleTrip.get(i).get(0).orderLocation == vehicleTrip.get(i - 1)
//					.get(vehicleTrip.get(i - 1).size() - 1).orderLocation)
//				mergeTrip.addAll(vehicleTrip.get(i).subList(1, vehicleTrip.get(i).size()));
//			else
//				mergeTrip.addAll(vehicleTrip.get(i));
//		}
//		return mergeTrip;
//	}
//
//	private int[] swapTrips(int vehicle1, ArrayList<ArrayList<Order>> vehicleTrip1, Order startDepot1, int vehicle2,
//			ArrayList<ArrayList<Order>> vehicleTrip2, Order startDepot2) {
//		int realK1 = lspCurrent.vehicleMap.get((Integer) (vehicle1));
//		int realK2 = lspCurrent.vehicleMap.get((Integer) (vehicle2));
//
//		int initialSimilary = calculateSimilarity(mergeSeperateTrip(startDepot1, vehicleTrip1),
//				mergeSeperateTrip(startDepot2, vehicleTrip2));
//		double oldCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, vehicleTrip1), realK1),
//				d.con.yita2, 0, 0, 0)
//				+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, vehicleTrip2), realK2), d.con.yita2, 0, 0,
//						0);
//		double oldTravelTime = calcuAdjustTravelTime(vehicleTrip1) + calcuAdjustTravelTime(vehicleTrip2);
//		int tempSimilarity = 0;
//
//		int[] swapArray = new int[2];
//		swapArray[0] = -2;
//		double newCost = 0;
//		for (int trip1 = 0; trip1 < vehicleTrip1.size(); trip1++)
//			for (int trip2 = 0; trip2 < vehicleTrip2.size(); trip2++) {
//				ArrayList<ArrayList<Order>> newVehicleTrip1 = cf.cloneVector(vehicleTrip1);
//				ArrayList<ArrayList<Order>> newVehicleTrip2 = cf.cloneVector(vehicleTrip2);
//				int u = 0;
//				if (vehicleTrip1.size() <= 1 && vehicleTrip2.size() > 1)
//					u = 0;
//				if (vehicleTrip1.size() > 1 && vehicleTrip2.size() <= 1)
//					u = 1;
//				else
//					u = 2;
//				switch (u) {
//				case 0:
//					newVehicleTrip1.add(vehicleTrip2.get(trip2));
//					newVehicleTrip2.remove(trip2);
//					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
//							mergeSeperateTrip(startDepot2, newVehicleTrip2));
//
//					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), realK1),
//							d.con.yita2, 0, 0, 0)
//							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), realK2),
//									d.con.yita2, 0, 0, 0);
//
//					// System.err.println(initialSimilary + "\t" + tempSimilarity + "\t" + newCost +
//					// "\t" + oldCost);
//					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
//					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
//							&& travelTime <= oldTravelTime + 20) {
//						initialSimilary = tempSimilarity;
//						swapArray[0] = -1;
//						swapArray[1] = trip2;
//					}
//					break;
//				case 1:
//					newVehicleTrip2.add(vehicleTrip1.get(trip1));
//					newVehicleTrip1.remove(trip1);
//					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
//							mergeSeperateTrip(startDepot2, newVehicleTrip2));
//
//					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), realK1),
//							d.con.yita2, 0, 0, 0)
//							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), realK2),
//									d.con.yita2, 0, 0, 0);
//
//					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
//					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
//							&& travelTime <= oldTravelTime + 20) {
//						initialSimilary = tempSimilarity;
//						swapArray[0] = -1;
//						swapArray[1] = trip1;
//					}
//					break;
//				case 2:
//					newVehicleTrip1.set(trip1, vehicleTrip2.get(trip2));
//					newVehicleTrip2.set(trip2, vehicleTrip1.get(trip1));
//
//					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
//							mergeSeperateTrip(startDepot2, newVehicleTrip2));
//
//					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), realK1),
//							d.con.yita2, 0, 0, 0)
//							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), realK2),
//									d.con.yita2, 0, 0, 0);
//
//					// System.err.println(initialSimilary + "\t" + tempSimilarity + "\t" + newCost +
//					// "\t" + oldCost);
//					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
//					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
//							&& travelTime <= oldTravelTime + 20) {
//						initialSimilary = tempSimilarity;
//						swapArray[0] = trip1;
//						swapArray[1] = trip2;
//					}
//					break;
//				default:
//					break;
//				}
//			}
//
//		return swapArray;
//	}
//
//	private ArrayList<ArrayList<Order>> smoothFinalSwap(ArrayList<ArrayList<Order>> route) {
//		double oldCost = calcuNewBenfit(route);
//		double oldTravelTime = travelTime;
//		orginRoute = cf.cloneVector(route);
//		int k = 0;
//		for (int loop = 0; loop < 1; loop++) {
//			k = 0;
//			for (ArrayList<Order> path : route) {
//				ArrayList<Order> newPath = swap(path, k);
//				route.set(k, newPath);
//				k++;
//			}
//			if (calcuNewBenfit(route) >= fitnessParaThreshold * oldCost
//					|| travelTime >= oldTravelTime + travelTimeParaThreshold) {//
//				route = cf.cloneVector(orginRoute);
//				break;
//			}
//		}
//		return route;
//	}
//
//	private ArrayList<ArrayList<Order>> smoothMultiVehicleInsertion(ArrayList<ArrayList<Order>> route) {
//		double oldCost = calcuNewBenfit(route);
//		double oldTravelTime = travelTime;
//		int specificK = 0;
//		ArrayList<Order> newOrders = cf.arraylistCloneObject(lspCurrent.lspRealOrders);
//		Collections.shuffle(newOrders);
//		newOrders.removeAll(d.startEndNodes);
//		int k = 0;
//		int cnt = 0;
//		for (Order chooseNode : newOrders) {
//			if (chooseNode == null)
//				continue;
//			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(route);
//			orginRoute = cf.cloneVector(route);
//			k = 0;
//			for (ArrayList<Order> path : newRoute) {
//				if (path.contains(chooseNode)) {
//					path.remove(chooseNode);
//					specificK = k;
//					break;
//				}
//				k++;
//			}
//
//			int[] insertArray = multiVehiclesInsertionDifferVehicle(orginRoute, newRoute, chooseNode, specificK);
//			if (insertArray[1] > 0) {
//				newRoute.get(insertArray[0]).add(insertArray[1], chooseNode);
//				route = cf.cloneVector(newRoute);
//				cnt++;
//			}
//			if (calcuNewBenfit(route) >= fitnessParaThreshold * oldCost
//					|| (travelTime >= oldTravelTime + travelTimeParaThreshold)) {
//				route = cf.cloneVector(orginRoute);
//				break;
//			}
//
//		}
//		return route;
//	}
//
//	private int[] multiVehiclesInsertionDifferVehicle(ArrayList<ArrayList<Order>> originRoute,
//			ArrayList<ArrayList<Order>> newRoute, Order chooseNode, int specificK) {
//		int[] minarray = new int[2];
//		minarray[1] = -1;
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		int initialSimilarity = 10000;
//		int realK = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		needMiddleAdd = new double[vehicles.size()][2];
//		postPone = new double[vehicles.size()];
//		int start = 0;
//		for (int i = 0; i < route.size(); i++) {
//			realK = lspCurrent.vehicleMap.get((Integer) (i));
//			if (Math.abs(specificK - i) <= 1e-3)
//				continue;
//			if (chooseNode.specialVehicle > -1 && Math.abs(chooseNode.specialVehicle - i) > 1e-3)
//				continue;
//
//			initialSimilarity = calculateSimilarity(originRoute.get(specificK), originRoute.get(i));
//			increa_comp = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//			globalCost = 0;
//			start = 0;
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//			// System.out.println(human(route));
//			for (int m = start; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//
//				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//				benefi = globalCost - increa_comp + r.nextDouble() * 1;
//
//				if ((needSmooth && benefi < minBeneif && globalCost < increa_comp * fitnessParaThreshold
//						&& calcuAdjustTravelTime(route) < calcuAdjustTravelTime(orginRoute) + travelTimeParaThreshold
//						&& calculateSimilarity(route.get(specificK), route.get(i)) < initialSimilarity)) {
//					minBeneif = benefi;
//					minarray[0] = i;
//					minarray[1] = m + 1;
//				}
//
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	int calculateSimilarity(ArrayList<Order> path1, ArrayList<Order> path2) {
//		// System.out.println(" similarity threshold "+similarityThreshold);
//		int similarity = 0;
//		for (Order nodeI : path1)
//			for (Order nodeJ : path2) {
//				if (d.startEndNodes.contains(nodeI) && d.startEndNodes.contains(nodeJ))
//					continue;
//				if (d.con.disMatrix[nodeI.orderLocation()][nodeJ.orderLocation()] <= similarityThreshold)
//					similarity++;
//			}
//		return similarity;
//	}
//
//	// private ArrayList<ArrayList<Order>>
//	// reOptimizeSmallSet(ArrayList<ArrayList<Order>> route) {
//	// if (route != null && route.size() > 0) {
//	//
//	//
//	// for (int loop = 0; loop < 3; loop++) {
//	// k = 0;
//	// for (ArrayList<Order> path : route) {
//	// ArrayList<Order> newPath = swap(path, k);
//	// route.set(k, newPath);
//	// k++;
//	// }
//	// }
//	// for (int loop = 0; loop < 3; loop++) {
//	// k = 0;
//	// for (ArrayList<Order> path : route) {
//	// ArrayList<Order> newPath = removeEachOneInsertSameTrip(path, k);
//	// route.set(k, newPath);
//	// k++;
//	// }
//	// }
//	//
//	// double newCost = calcuNewBenfit(route);
//	// if ((!needSmooth && newCost >= oldCost)
//	// || (needSmooth && newCost >= 1.5 * oldCost && travelTime >= oldTravelTime +
//	// 10))
//	// route = cf.cloneVector(originalRoute);
//	// }
//	// // System.err.println(human(route));
//	// return route;
//	// }
//
//	double calculateTime(ArrayList<Order> path, int from, int to) {
//		double totalTime = 0;
//		double singTime = 0;
//		for (int i = from; i < to; i++) {
//			singTime = d.con.timeMatrix[path.get(i).orderLocation()][path.get(i + 1).orderLocation()];
//			totalTime += singTime;
//		}
//		return totalTime;
//	}
//
//	private void callPostProcess() {
//		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
//			feasiusedRouteWhole = cf.cloneVector(reoptimize(feasiusedRouteWhole));
//			feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
//		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
//			noTimeVioRoute = cf.cloneVector(reoptimize(noTimeVioRoute));
//			noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
//		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
//			bestUsedRoute = cf.cloneVector(reoptimize(bestUsedRoute));
//			bestBenifit = calcuNewBenfit(bestUsedRoute);
//		}
//	}
//
//	double calcuAdjustTravelTime(ArrayList<ArrayList<Order>> feasiusedRouteWhole) {
//		double sum = 0;
//		if (needSmooth) {
//			for (ArrayList<Order> path : feasiusedRouteWhole) {
//				sum += calculateTime(path, 0, path.size() - 1);
//			}
//		}
//		return sum;
//	}
//
//	private int calculateSmoothAll(ArrayList<ArrayList<Order>> feasiusedRouteWhole) {
//		int sum = 0;
//		if (needSmooth) {
//			for (ArrayList<Order> path : feasiusedRouteWhole) {
//				sum += calculateSmooth(path, 0, path.size() - 1);
//			}
//		}
//		return sum;
//	}
//	// @post process, reoptimize the route
//
//	private ArrayList<ArrayList<Order>> reoptimize(ArrayList<ArrayList<Order>> route) {
//		int k = 0;
//		// for (int reopt = 0; reopt < 30; reopt++) {
//		// k = 0;
//		// for (ArrayList<Order> path : route) {
//		// ArrayList<Order> newPath = twoOptAlternate(path,
//		// lspCurrent.vehicleMap.get(k));
//		// route.set(k, newPath);
//		// k++;
//		// }
//		// }
//		ArrayList<ArrayList<Order>> originalRoute = cf.cloneVector(route);
//		if (route != null && route.size() > 0) {
//			double oldCost = calcuNewBenfit(route);
//			double oldTravelTime = travelTime;
//			for (int reopt = 0; reopt < 100; reopt++) {
//				ArrayList<Order> newOrders = cf.arraylistCloneObject(lspCurrent.lspRealOrders);
//				Collections.shuffle(newOrders);
//				newOrders.removeAll(d.startEndNodes);
//				for (Order chooseNode : newOrders) {
//					if (chooseNode == null)
//						continue;
//					ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(route);
//					orginRoute = cf.cloneVector(route);
//					for (ArrayList<Order> path : newRoute) {
//						if (path.contains(chooseNode)) {
//							path.remove(chooseNode);
//							break;
//						}
//					}
//
//					int[] insertArray = multiVehiclesInsertion(newRoute, chooseNode);
//					if (insertArray[1] > 0) {
//						newRoute.get(insertArray[0]).add(insertArray[1], chooseNode);
//					}
//					route = cf.cloneVector(newRoute);
//				}
//			}
//
//			postMerge();
//
//			for (int reopt = 0; reopt < 300; reopt++) {
//				k = 0;
//				for (ArrayList<Order> path : route) {
//					ArrayList<Order> newPath = swap(path, k);
//					route.set(k, newPath);
//					k++;
//				}
//			}
//
//			for (int reopt = 0; reopt < 200; reopt++) {
//				k = 0;
//				for (ArrayList<Order> path : route) {
//					ArrayList<Order> newPath = removeEachOneInsertSameTrip(path, k);
//					route.set(k, newPath);
//					k++;
//				}
//			}
//			route = smoothBetweenTrip(route);
//			double newCost = calcuNewBenfit(route);
//			if ((!needSmooth && newCost >= oldCost) || (needSmooth && newCost >= fitnessParaThreshold * oldCost
//					&& travelTime >= oldTravelTime + travelTimeParaThreshold))
//				route = cf.cloneVector(originalRoute);
//		}
//		// System.err.println(human(route));
//		return route;
//	}
//
//	// @post process, swap operator
//	private ArrayList<Order> swap(ArrayList<Order> path, Integer k) {
//		// swap any two, check if it's better, swap same trip
//		Order nodeI = null;
//		Order nodeJ = null;
//		double orginalResult = 0;
//		double newResult = 0;
//		boolean check = false;
//		int[] depotIndex = new int[2];
//		ArrayList<Order> newpath = cf.singleCloneOrder(path);
//		ArrayList<Order> startPath = cf.arraylistCloneObject(path);
//
//		double travelTimeBest = calculateTime(startPath, 0, startPath.size() - 1);
//
//		int start = 0;
//		for (int j = 1; j < path.size() - 1; j++) {
//			if (d.startEndNodes.contains(path.get(j)) && path.get(j).location.id != path.get(0).location.id)
//				start++;
//			else
//				break;
//		}
//		for (int i = start + 1; i < path.size(); i++) {
//			for (int j = i + 1; j < path.size() - 1; j++) {
//				nodeI = newpath.get(i);
//				if (d.startEndNodes.contains(nodeI))
//					continue;
//				ArrayList<Order> originPath = cf.arraylistCloneObject(newpath);
//				depotIndex = tripDepot(cf.listCloneObject(newpath), i);
//				orginalResult = calcuResult(new PathSolution(newpath, k), d.con.yita2, 0, 0, 0);
//				// depotIndex[1] is the trip end position
//				if (j > depotIndex[1] || d.startEndNodes.contains(newpath.get(j)))
//					continue;
//				nodeJ = newpath.get(j);
//				newpath.remove(i);
//				newpath.remove(j - 1);
//				newpath.add(i, nodeJ);
//				newpath.add(j, nodeI);
//				newResult = calcuResult(new PathSolution(newpath, k), d.con.yita2, 0, 0, 0);
//				check = needSmooth
//						&& (calculateTime(newpath, i, depotIndex[1]) < calculateTime(originPath, i, depotIndex[1])
//								+ travelTimeParaThreshold)
//						&& calculateSmooth(newpath, i, depotIndex[1]) > calculateSmooth(originPath, i, depotIndex[1]);
//				check = check && (newResult < orginalResult * fitnessParaThreshold);
//
//				if ((check && calculateTime(newpath, 0, newpath.size() - 1) < travelTimeBest)
//						|| (!needSmooth && newResult < orginalResult)) {
//
//				} else {
//					newpath = cf.singleCloneOrder(originPath);
//				}
//			}
//		}
//		return newpath;
//	}
//
//	int calculateSmooth(ArrayList<Order> path, int from, int to) {
//		int smooth = 0;
//		for (int i = from; i < to; i++) {
//			if (d.con.disMatrix[path.get(i).orderLocation()][path.get(i + 1).orderLocation()] <= smoothThreshold)
//				smooth++;
//		}
//		return smooth;
//	}
//
//	int[] tripDepot(List<Order> list, int pos) {
//		int size = list.size();
//		int cnt = 0;
//		int[] depotIndex = new int[2];
//
//		for (int i = pos + 1; i < size; i++) {
//			if (d.startEndNodes.contains(list.get(i))) {
//				depotIndex[1] = i - 1;
//				break;
//			}
//		}
//
//		for (int i = pos - 1; i > 0; i--) {
//			if (d.startEndNodes.contains(list.get(i))) {
//				depotIndex[0] = i;
//				break;
//			}
//		}
//		return depotIndex;
//	}
//
//	private ArrayList<Order> removeEachOneInsertSameTrip(ArrayList<Order> path, int k) {
//		Order node = null;
//		double orginalResult = 0;
//		double newResult = 0;
//		int[] depotIndex = new int[2];
//		int minarray = -1;
//		boolean check = false;
//		ArrayList<Order> originPath = new ArrayList<Order>();
//		double travelTimeBest = calculateTime(path, 0, path.size() - 1);
//		int start = 0;
//		for (int j = 1; j < path.size() - 1; j++) {
//			if (d.startEndNodes.contains(path.get(j)) && path.get(j).location.id != path.get(0).location.id)
//				start++;
//			else
//				break;
//		}
//		for (int i = start; i < path.size(); i++) {
//			originPath = cf.listCloneObject(path);
//			minarray = -1;
//			node = path.get(i);
//			if (d.startEndNodes.contains(node))
//				continue;
//			depotIndex = tripDepot(cf.listCloneObject(path), i);
//			orginalResult = calcuResult(new PathSolution(path, k), d.con.yita2, 0, 0, 0);
//			path.remove(i);
//
//			for (int j = start + 1; j < path.size(); j++) {
//				if (i == j || j <= depotIndex[0] || j > depotIndex[1])
//					continue;
//				path.add(j, node);
//				newResult = calcuResult(new PathSolution(path, k), d.con.yita2, 0, 0, 0);
//
//				check = needSmooth
//						&& (calculateTime(path, i, depotIndex[1]) < calculateTime(originPath, i, depotIndex[1])
//								+ travelTimeParaThreshold)
//						&& calculateSmooth(path, i, depotIndex[1]) > calculateSmooth(originPath, i, depotIndex[1]);
//				check = check && (newResult < 1.8 * orginalResult);
//
//				if (check && calculateTime(path, 0, path.size() - 1) < travelTimeBest + travelTimeParaThreshold) {
//					orginalResult = newResult;
//					minarray = j;
//				} else if (!needSmooth && newResult < orginalResult) {
//					orginalResult = newResult;
//					minarray = j;
//				}
//				path.remove(j);
//			}
//			if (minarray > -1) {
//				path.add(minarray, node);
//			} else
//				path.add(i, node);
//		}
//		return path;
//	}
//
//	// @merge short trips (if one trip length less than 3) to long trips
//	private void postMerge() {
//		ArrayList<ArrayList<Order>> route;
//		double newbenfit = 0;
//		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
//			removeDepot(feasiusedRouteWhole, true);
//			route = cf.cloneVector(feasiusedRouteWhole);
//			route = cf.cloneVector(postProcessTripMerge(route));
//			newbenfit = calcuNewBenfit(route);
//			// if (newbenfit < feasibenefitWhole * 1.18) {
//			if (lspCurrent.totalSize == serveAll(route)) {
//				feasiusedRouteWhole = cf.cloneVector(route);
//				feasibenefitWhole = newbenfit;
//			}
//			// }
//		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
//			removeDepot(bestUsedRoute, true);
//			route = cf.cloneVector(bestUsedRoute);
//			route = cf.cloneVector(postProcessTripMerge(route));
//			newbenfit = calcuNewBenfit(route);
//			if (newbenfit < bestBenifit * 1.18 && lspCurrent.totalSize == serveAll(route)) {
//				bestUsedRoute = cf.cloneVector(route);
//				bestBenifit = newbenfit;
//			}
//		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
//			removeDepot(noTimeVioRoute, true);
//			route = cf.cloneVector(noTimeVioRoute);
//			route = cf.cloneVector(postProcessTripMerge(route));
//			newbenfit = calcuNewBenfit(route);
//			if (newbenfit < noTimeVioBenifit * 1.18 && lspCurrent.totalSize == serveAll(route)) {
//				noTimeVioRoute = cf.cloneVector(route);
//				noTimeVioBenifit = newbenfit;
//			}
//		}
//	}
//
//	private ArrayList<ArrayList<Order>> postProcessTripMerge(ArrayList<ArrayList<Order>> route) {
//		currentSolutionMultiTrips = new ArrayList<Trip>();
//		for (int i = 0; i < route.size(); i++) {
//			List<Integer> singlePath = new ArrayList<Integer>();
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//
//				if (!d.startEndNodes.contains(route.get(i).get(j)))
//					singlePath.add(route.get(i).get(j).id);
//				else {
//					currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
//					singlePath = new ArrayList<Integer>();
//				}
//			}
//			currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
//		}
//		List<Order> removeSet = new ArrayList<Order>();
//
//		ArrayList<Order> splitPath = new ArrayList<Order>();
//
//		for (Trip t : currentSolutionMultiTrips) {
//			if (t.path.size() < 3) {
//				removeSet.addAll(translate(t.path));
//			}
//		}
//
//		for (ArrayList<Order> path : route)
//			path.removeAll(removeSet);
//
//		int[] insertArray = new int[2];
//		for (Order chooseNode : removeSet) {
//			insertArray = finalMergeInsertion(route, chooseNode);
//			if (insertArray[1] > 0)
//				route.get(insertArray[0]).add(insertArray[1], chooseNode);
//		}
//		return route;
//
//	}
//
//	private int[] finalMergeInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
//		int[] minarray = new int[2];
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//
//		double initialMaxUtilize = 0;
//		initialMaxUtilize = calculateMaxUtilize(route);
//		int k = 0;
//		for (int i = 0; i < route.size(); i++) {
//			k = d.con.lspVechiclesHiddenMap.get(route.get(i).get(0).id) - 1;
//
//			if (!chooseNode.vehiclesIndex.contains((Integer) (k + 1))) {
//				continue;
//			}
//
//			increa_comp = calcuResult(new PathSolution(route.get(i), k), d.con.yita2, 0, 0, 0);
//			globalCost = 0;
//			// System.out.println(human(route));
//			for (int m = 0; m < route.get(i).size() - 1; m++) {
//				if ((tripSize(route.get(i), m + 1) < 2
//						|| tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1)) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), k), d.con.yita2, 0, 0, 0);
//				benefi = globalCost - increa_comp + r.nextDouble() * 1;// +
//																		// maxUtilize
//																		// *
//																		// d.totalTimeSpan
//
//				if (benefi < minBeneif) {
//					minBeneif = benefi;
//					minarray[0] = i;
//					minarray[1] = m + 1;
//
//				}
//				route.get(i).remove(m + 1);
//				maxUtilize = initialMaxUtilize;
//			}
//		}
//		return minarray;
//
//	}
//
//	// check time window related violation
//	private double vioTime(ArrayList<ArrayList<Order>> route) {
//		double sumVio = 0;
//		int carId = 0;
//		int size = 0;
//		Order nodeI = null, nodeJ = null, nodeK = null;
//		int totalTime = 0;
//		int tempWait = 0;
//		int nextTime = 0;
//		HashMap<Integer, Integer> violateOrders = new HashMap<Integer, Integer>();
//		int arrriveTime = 0;
//		int tempNodeServiceTime = 0;
//		int tempNextNodeServicetime = 0;
//
//		boolean needCheckCP = needCheckCPForWaitingTime(route);
//		if (needCheckCP) {
//			example = new CplexCPMostNoneIMD();
//			example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//			try {
//				example.dynamicSet(route, false);
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IloException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			if (example.totalWaitTime < 10e5) {
//				sumVio = example.violateOrders.size();
//			} else
//				sumVio = d.orders.size();
//		} else {
//			for (ArrayList<Order> path : route) {
//				size = path.size();
//				if (size < 2)
//					continue;
//				carId++;
//				nodeI = path.get(0);
//				nodeJ = path.get(1);
//				totalTime = Math.max(nodeI.lowerTime(), vehicles.get(carId - 1).truckLowerTime());
//				arrriveTime = totalTime + nodeI.serviceTime
//						+ d.con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation];
//				if (!d.startEndNodes.contains(nodeJ)) {
//					if (arrriveTime > nodeJ.upperTime) {
//						violateOrders.put(nodeJ.id, arrriveTime - nodeJ.upperTime);
//					}
//				}
//
//				nextTime = totalTime + nodeI.serviceTime;
//
//				if (nodeI.orderLocation != nodeJ.orderLocation)
//					nextTime += d.con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation];
//
//				// if (!d.startEndNodes.contains(nodeJ)
//				// && nextTime + nodeJ.serviceTime > vehicles.get(carId -
//				// 1).lunchTimeStart
//				// && nextTime < vehicles.get(carId - 1).lunchTimeEnd) {
//				// nextTime = vehicles.get(carId - 1).lunchTimeEnd;
//				// }
//				nextTime = Math.max(nodeJ.lowerTime, nextTime);
//				// if (d.con.slotsFalg && nodeJ.location.type.equals("IMD")) {
//				// if (nodeI.orderLocation != nodeJ.orderLocation)
//				// nextTime = (int) (d.con.splitSlot * Math.ceil(nextTime /
//				// (d.con.splitSlot +
//				// 0.0)));
//				// }
//				tempWait = nextTime
//						- (totalTime + d.con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation] + nodeI.serviceTime);
//
//				nextTime += tempWait;
//
//				for (int i = 1; i < size - 1; i++) {
//					nodeI = path.get(i - 1);
//					nodeJ = path.get(i);
//					nodeK = path.get(i + 1);
//					// System.out.println(nodeJ.id+"\t"+nodeJ.upperTime);
//
//					totalTime = nextTime;
//
//					tempNodeServiceTime = 0;
//					tempNextNodeServicetime = 0;
//					totalTime = nextTime;
//					if (nodeI.orderLocation != nodeJ.orderLocation)
//						tempNodeServiceTime = nodeJ.serviceTime;
//					arrriveTime = totalTime + tempNodeServiceTime
//							+ d.con.timeMatrix[nodeJ.orderLocation][nodeK.orderLocation];
//
//					nextTime = arrriveTime;
//					if (nodeJ.orderLocation != nodeK.orderLocation) {
//						tempNextNodeServicetime = nodeK.serviceTime;
//					}
//					if (lspCurrent.lunchLocationFlag && !d.startEndNodes.contains(nodeK)
//							&& nextTime + tempNextNodeServicetime >= locations[nodeK.orderLocation()].lunchStart
//							&& nextTime < locations[nodeK.orderLocation()].lunchEnd) {
//						nextTime = locations[nodeK.orderLocation()].lunchEnd;
//					}
//
//					nextTime = Math.max(nodeK.lowerTime, nextTime);
//
//					if (!d.startEndNodes.contains(nodeK)) {
//						if (arrriveTime > nodeK.upperTime) {
//							violateOrders.put(nodeK.id, arrriveTime - nodeK.upperTime);
//						}
//					}
//
//				}
//			}
//
//			for (Integer node : violateOrders.keySet()) {
//				sumVio += violateOrders.get(node);
//			}
//		}
//		return sumVio;
//	}
//
//	// @try to improve the solution by removing depot, been called for initial
//	// solution and every 280 iterations
//	private void removeDepot(ArrayList<ArrayList<Order>> route, boolean endOptimize) {
//		int cnt = 0;
//		int preNode = 0;
//		int curNode = 0;
//		int size = 0;
//		int servedOrderSize = 0;
//
//		ArrayList<Integer> remove = new ArrayList<>();
//		for (int i = 0; i < route.size(); i++) {
//			size = route.get(i).size();
//			servedOrderSize = 0;
//
//			if (size <= 2)
//				continue;
//			remove = new ArrayList<>();
//			preNode = route.get(i).get(0).id;
//			for (int j = 1; j < size - 1; j++) {
//				curNode = route.get(i).get(j).id;
//
//				if (Math.abs(curNode - preNode) < 1e-3
//						&& vehicles.get(i).vehicleStartMiddleOrders.contains(route.get(i).get(j)))
//					remove.add(j);
//				if (!d.startEndNodes.contains(route.get(i).get(j)))
//					servedOrderSize++;
//
//				preNode = curNode;
//			}
//			//
//			cnt = 0;
//			for (int k = 0; k < remove.size(); k++) {
//				if (servedOrderSize > 2)
//					route.get(i).remove(remove.get(k) - cnt);
//				cnt++;
//			}
//			if (endOptimize && d.startEndNodes.contains(route.get(i).get(route.get(i).size() - 2)))
//				route.get(i).remove(route.get(i).size() - 2);
//
//		}
//	}
//
//	private ArrayList<ArrayList<Order>> initilize(ArrayList<ArrayList<Order>> route) {
//		postPone = new double[vehicles.size()];
//		needMiddleAdd = new double[vehicles.size()][2];
//		route.clear();
//		int multiTripCnt = 0;
//		int maxCnt = 0;
//		for (int i = 0; i < lspCurrent.totalCar; i++) {
//			ArrayList<Order> singPath = new ArrayList<Order>();
//			multiTripCnt = Math.max(lspCurrent.multiTripCnt, 2);
//			if (!vehicles.get(i).haveTrip) {
//				if (d.lspMiddleDepotOrder.get(lspCurrent.id).size() == 0) {
//					while (multiTripCnt > 0) {
//						singPath.add(lspCurrent.lspVehiclesObject.get(i).depot);
//						multiTripCnt--;
//					}
//				} else {
//					singPath.add(lspCurrent.lspVehiclesObject.get(i).depot);
//					while (multiTripCnt > 0 && maxCnt < lspCurrent.lspVehiclesObject.size() * 2) {
//						for (Order u : d.lspMiddleDepotOrder.get(lspCurrent.id)) {
//							singPath.add(u);
//							multiTripCnt -= 1;
//						}
//						maxCnt++;
//					}
//				}
//				singPath.add(lspCurrent.lspVehiclesObject.get(i).endDepot);
//			}
//			route.add(singPath);
//		}
//
//		if (route.size() > 0) {
//			HashSet<Order> removeSet = new HashSet<Order>(lspCurrent.lspRealOrders);
//			removeSet.remove(null);
//			removeSet.removeAll(d.startEndNodes);
//			// normal based start
//			// for (Order chooseNode : removeSet) {
//			// insertArray = multiVehiclesInsertion(route, chooseNode);
//			// if (insertArray[1] > 0)
//			// route.get(insertArray[0]).add(insertArray[1], chooseNode);
//			// }
//			// cluster based
//			List<Order> data = new ArrayList<Order>();
//			for (Order item : removeSet)
//				data.add(item);
//			KMedoids Kmedoids = new KMedoids(lspCurrent.lspVehicles.size() * 2, 10000, d.con.disMatrix);
//
//			List<ArrayList<Integer>> assignment = Kmedoids.cluster(data);
//
//			// System.out.println("KM called time 2");
//
//			int insertPos = 0;
//			double lowestCost = 1e11;
//			double currentCost = 1e11;
//			ArrayList<ArrayList<Order>> initialRoute = new ArrayList<ArrayList<Order>>();
//			ArrayList<ArrayList<Order>> bestRoute = new ArrayList<ArrayList<Order>>();
//			ArrayList<ArrayList<Order>> storedRoute = new ArrayList<ArrayList<Order>>();
//
//			// System.out.println("sorting");
//
//			for (ArrayList<Integer> remove : assignment) {
//				Collections.sort(remove, new Comparator<Integer>() {
//					@Override
//					public int compare(Integer o1, Integer o2) {
//						return orders.get(o1).lowerTime - orders.get(o2).lowerTime;
//					}
//				});
//			}
//			for (ArrayList<Integer> remove : assignment) {
//				initialRoute = cf.cloneVector(route);
//				storedRoute = cf.cloneVector(route);
//				currentCost = 1e11;
//				lowestCost = 1e11;
//				for (int cnt = 0; cnt < lspCurrent.lspVehicles.size(); cnt++) {
//					for (Integer chooseNode : remove) {
//						insertPos = singleVehiclesInsertion(initialRoute, cnt, orders.get(chooseNode));
//						if (insertPos > 0)
//							initialRoute.get(cnt).add(insertPos, orders.get(chooseNode));
//					}
//
//					// System.out.println("calculating cost");
//					currentCost = calcuResult(
//							new PathSolution(initialRoute.get(cnt), lspCurrent.lspVehicles.get(cnt) - 1), d.con.yita2,
//							0, 0, 0);
//					if (currentCost < lowestCost) {
//						bestRoute = cf.cloneVector(initialRoute);
//						lowestCost = currentCost;
//					}
//					initialRoute = cf.cloneVector(storedRoute);
//				}
//				route = cf.cloneVector(bestRoute);
//			}
//		}
//
//		return route;
//	}
//
//	// @calculate the fitness value
//	private double calcuNewBenfit(ArrayList<ArrayList<Order>> route) {
//		travelTime = 0;
//		totalTravelDis = 0;
//		travelDistance = 0;
//
//		double newbenfit = 0;
//		postPone = new double[vehicles.size()];
//		needMiddleAdd = new double[vehicles.size()][2];
//		for (Lsp u : d.lsps) {
//			u.setMaxUtilize(0);
//			u.setMaxTime(0);
//			u.setMinTime(10e3);
//			u.setTripDisatance(0);
//			u.setSimpleTripDistance(0);
//		}
//
//		for (int i = 0; i < route.size(); i++) {
//			if (route.get(i) == null || route.get(i).size() <= 2)
//				continue;
//			if (route.get(i) != null) {
//				newbenfit += calcuResult(
//						new PathSolution(route.get(i), d.con.lspVechiclesHiddenMap.get(route.get(i).get(0).id) - 1),
//						d.con.yita2, 0, 0, 0);
//			}
//		}
//
//		if (globalCnt >= maxCnt / 2) {
//			if (d.con.objFlag == 0) {
//				for (Lsp u : d.lsps) {
//					u.setTotalDistance(newbenfit);
//					// System.err.println(d.parameterMatrix.get(u.id).getMaxSpanWeight());
//					newbenfit += // d.parameterMatrix.get(u.id).getMaxSpanWeight()
//							(lspCurrent.totalSize - serveAll(route)) * d.con.price
//									+ u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight();
//					maxTripDis = u.getMaxTripDistance();
//					// System.err.println(u.getMaxTime()+"\t"+d.parameterMatrix.get(u.id).getMaxSpanWeight()+"\t"+d.parameterMatrix.get(u.id).getWaitWeight());
//				}
//			}
//			if (d.con.objFlag == 1) {
//				totalTravelDis = newbenfit;
//				for (Lsp u : d.lsps) {
//					u.setTotalDistance(newbenfit);
//					// d.parameterMatrix.get(u.id).getMaxSpanWeight()
//
//					newbenfit += (lspCurrent.totalSize - serveAll(route)) * d.con.price
//							+ u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight();
//					maxTripDis = u.getMaxTripDistance();
//				}
//			}
//		}
//
//		if (newbenfit < 0) {
//			System.err.println("error");
//			// newbenfit = (int) 1e10;
//		}
//		return newbenfit;
//	}
//
//	private double calcuNewBenfitPostProc(ArrayList<ArrayList<ArrayList<Order>>> lspRoute) {
//		// TODO Auto-generated method stub
//		travelTime = 0;
//		totalTravelDis = 0;
//		double newbenfit = 0;
//		postPone = new double[vehicles.size()];
//		travelDistance = 0;
//
//		for (ArrayList<ArrayList<Order>> route : lspRoute) {
//
//			for (int i = 0; i < route.size(); i++) {
//				if (route.get(i) == null || route.get(i).size() <= 2)
//					continue;
//
//				// System.out.println("lspCurrent.vehicleMap.get(i)\t" +
//				// lspidCheck+"\t"+lspCurrent.id+"\t"+lsps.get(lspidCheck).id+"\t"+lspCurrent.vehicleMap
//				// + "\t" + human(route));
//				newbenfit += calcuResult(
//						new PathSolution(route.get(i), d.con.lspVechiclesHiddenMap.get(route.get(i).get(0).id) - 1),
//						d.con.yita2, 0, 0, 0);
//				totalTravelDis += travelDistance;
//
//			}
//
//			if (globalCnt >= maxCnt / 2) {
//				// newbenfit = 0;
//				if (d.con.objFlag == 0) {
//					for (Lsp u : d.lsps) {
//						u.setTotalDistance(newbenfit);
//						newbenfit += // d.parameterMatrix.get(u.id).getMaxSpanWeight()
//								(u.totalSize - serveAll(route)) * d.con.price
//										// + u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight()
//										+ route.size() * d.con.price;
//						maxTripDis = u.getMaxTripDistance();
//					}
//				}
//				if (d.con.objFlag == 1) {
//					totalTravelDis = newbenfit;
//					for (Lsp u : d.lsps) {
//						u.setTotalDistance(newbenfit);
//						newbenfit = (u.totalSize - serveAll(route)) * d.con.price
//								// + u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight()
//								+ route.size() * d.con.price;
//
//						maxTripDis = u.getMaxTripDistance();
//					}
//				}
//			}
//
//			if (newbenfit < 0) {
//				newbenfit = (int) 1e10;
//			}
//		}
//		System.out.println("parameter\t" + travelTime + "\t" + totalTravelDis);
//		// System.out.println("parameter\t"+d.con.maxSpanWeight+"\t"+d.con.loadBalanceWeight+"\t"+d.con.waitingTimePara+"\t"+d.con.travelWeight);
//		return newbenfit;
//	}
//
//	// @used for translate to human read output but not objects
//	static ArrayList<ArrayList<Integer>> human(ArrayList<ArrayList<Order>> route) {
//		ArrayList<ArrayList<Integer>> humanReadRoute = new ArrayList<ArrayList<Integer>>();
//		for (ArrayList<Order> path : route) {
//			ArrayList<Integer> newPath = new ArrayList<Integer>();
//
//			for (Order node : path) {
//				newPath.add(node.id);
//			}
//
//			humanReadRoute.add(newPath);
//		}
//		return humanReadRoute;
//	}
//
//	// @used for tabu list, no need currently
//	private void updateTabuList() {
//		for (ArrayList<Order> path : bestUsedRoute)
//			tabuList.add(path.hashCode());
//		for (ArrayList<Order> path : feasiusedRouteWhole)
//			tabuList.add(path.hashCode());
//		if (tabuList.size() > 200)
//			tabuList.clear();
//	}
//
//	// @find best solution
//	private void finalAllBest() throws IloException, FileNotFoundException, InterruptedException, ExecutionException {
//		System.out.println("final all best size " + allBestfeasiusedRouteWhole.size()+" "+maxCnt+" "+lspCurrent.id);
//		globalCnt = maxCnt;
//
//		if (allBestfeasiusedRouteWhole != null && allBestfeasiusedRouteWhole.size() > 0) {
//			removeDepot(allBestfeasiusedRouteWhole, true);
//			removeDuplicate(allBestfeasiusedRouteWhole);
//			record(allBestfeasiusedRouteWhole);
//			example = new CplexCPMostNoneIMD();
//			example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//			example.dynamicSet(allBestfeasiusedRouteWhole, true);
//
//		} else if (allBestbestUsedRoute != null && allBestbestUsedRoute.size() > 0) {
//			bestBenifit += 1e4;
//			// best infeasible route but without order missing
//			removeDepot(allBestbestUsedRoute, true);
//			removeDuplicate(allBestbestUsedRoute);
//			allBestbestUsedRoute = addSolution(allBestbestUsedRoute);
//			infeasibleInsert(allBestbestUsedRoute);
//			record(allBestbestUsedRoute);
//			example = new CplexCPMostNoneIMD();
//			example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//			example.dynamicSet(allBestbestUsedRoute, true);
//		} else if (allBestnoTimeVioRoute != null && allBestnoTimeVioRoute.size() > 0) {
//			removeDepot(allBestnoTimeVioRoute, true);
//			removeDuplicate(allBestnoTimeVioRoute);
//			record(allBestnoTimeVioRoute);
//			example = new CplexCPMostNoneIMD();
//			example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//			example.dynamicSet(allBestnoTimeVioRoute, true);
//		}
//	}
//
//	private void callCP() throws IloException, FileNotFoundException, InterruptedException, ExecutionException {
//		System.out.println("final all best size in CallCP " + allBestfeasiusedRouteWhole.size());
//		// 30 impossible to get solution, super long
//		int i = 0;
//		if (overallMergedRoute != null && overallMergedRoute.size() > 0) {
//			while (i < d.con.callCPTimes && timeUsed < d.con.timeLimit - 2) {
//				allLspRemoveInsertCPCall();
//				timeUsed = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - globalStart) / 60.0;
//				i++;
//			}
//		}
//		setLspRoute();
//	}
//
//	private void setLspRoute() {
//		lspRoute.clear();
//		lspRoute.add(new ArrayList<ArrayList<Order>>());
//		int lspId = overallMergedRoute.get(0).get(0).originLSP;
//		lspRoute.get(0).add(overallMergedRoute.get(0));
//		int cnt = 0;
//		for (int i = 1; i < overallMergedRoute.size(); i++) {
//			if (overallMergedRoute.get(i).get(0).originLSP == lspId)
//				lspRoute.get(cnt).add(overallMergedRoute.get(i));
//			else {
//				lspRoute.add(new ArrayList<ArrayList<Order>>());
//				lspId = overallMergedRoute.get(i).get(0).originLSP;
//				cnt++;
//				lspRoute.get(cnt).add(overallMergedRoute.get(i));
//			}
//		}
//
//		for (cnt = 0; cnt < lspRoute.size(); cnt++) {
//			//System.err.println(cnt + "\t" + human(lspRoute.get(cnt)));
//	}
//	}
//
//	private void allLspRemoveInsertCPCall() throws FileNotFoundException, IloException {
//		// remove
//		ArrayList<ArrayList<Order>> storeRoute = cf.cloneVector(overallMergedRoute);
//		double preObj = calculate(storeRoute);
//
//		List<Order> allServedNode = cf.mergeAll(overallMergedRoute);
//		Collections.shuffle(allServedNode);
//
//		HashSet<Order> removeSet = new HashSet<Order>();
//		if (allServedNode.size() > 1)
//			removeSet.addAll(allServedNode.subList(0, Math.max(2, allServedNode.size() / 30)));
//		cf.removeDepot(removeSet, d.startEndNodes);
//		// repair
//		for (ArrayList<Order> path : overallMergedRoute)
//			path.removeAll(removeSet);
//		int[] insertArray = new int[2];
//		for (Order chooseNode : removeSet) {
//			insertArray = CPInsertion(overallMergedRoute, chooseNode);
//			if (insertArray[1] > 0)
//				overallMergedRoute.get(insertArray[0]).add(insertArray[1], chooseNode);
//		}
//
//		if (calculate(overallMergedRoute) > preObj)
//			overallMergedRoute = cf.cloneVector(storeRoute);
//
//	}
//
//	private double calculate(ArrayList<ArrayList<Order>> route) throws FileNotFoundException {
//		double globalCost = 0;
//		example = new CplexCPMostNoneIMD();
//		example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//		try {
//			example.dynamicSet(route, false);
//		} catch (IloException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (example.callCP)
//			globalCost = example.objective + example.totalTraPlusServ
//					+ (lspCurrent.totalSize - serveAll(route)) * d.con.price;
//		else {
//			globalCost = 0;
//			for (int id = 0; id < route.size(); id++) {
//				if (route.get(id) == null || route.get(id).size() <= 2)
//					continue;
//				if (route.get(id) != null) {
//					globalCost += calcuResult(
//							new PathSolution(route.get(id),
//									d.con.lspVechiclesHiddenMap.get(route.get(id).get(0).id) - 1),
//							d.con.yita2, 0, 0, 0);
//				}
//			}
//		}
//		return globalCost;
//
//	}
//
//	private int[] CPInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode)
//			throws FileNotFoundException, IloException {
//		int[] minarray = new int[2];
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		int realK = 0;
//		int start = 0;
//		for (int i = 0; i < route.size(); i++) {
//
//			realK = d.con.lspVechiclesHiddenMap.get(route.get(i).get(0).id);
//			if (chooseNode.originLSP != vehicles.get(realK - 1).lsp.id) {
//				continue;
//			}
//
//			start = 0;
//			// useless depot service time equal to 1
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//
//			globalCost = 0;
//			// System.out.println(human(route));
//			for (int m = start; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//				lspCurrent = lsps.get(route.get(i).get(0).originLSP);
//				if (lspCurrent.needCheckMaxDrop && tripSize(route.get(i), m + 1) > lspCurrent.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				example = new CplexCPMostNoneIMD();
//				example.set(false, lspCurrent.id, vehicles, orders, locations, d.con, d.lspDocks);
//				example.dynamicSet(route, false);
//
//				if (example.callCP)
//					globalCost = example.objective + example.totalTraPlusServ
//							+ (lspCurrent.totalSize - serveAll(route)) * d.con.price;
//				else {
//					globalCost = 0;
//					for (int id = 0; id < route.size(); id++) {
//						if (route.get(id) == null || route.get(id).size() <= 2)
//							continue;
//						if (route.get(id) != null) {
//							globalCost += calcuResult(
//									new PathSolution(route.get(id),
//											d.con.lspVechiclesHiddenMap.get(route.get(id).get(0).id) - 1),
//									d.con.yita2, 0, 0, 0);
//						}
//					}
//				}
//				benefi = globalCost + r.nextDouble() * 1;
//
//				if (benefi < minBeneif) {
//					minBeneif = benefi;
//					minarray[0] = i;
//					minarray[1] = m + 1;
//				}
//
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	private HashSet<Order> missOrder(ArrayList<ArrayList<Order>> route) {
//		HashSet<Order> howMany = new HashSet<Order>(cf.deepCopySet(lspCurrent.lspRealOrders));
//		howMany.removeAll(d.startEndNodes);
//		for (ArrayList<Order> path : route) {
//			for (Order node : path) {
//				if (orders.contains(node) && !d.startEndNodes.contains(node))
//					howMany.remove(node);
//			}
//		}
//		return howMany;
//
//	}
//
//	private ArrayList<ArrayList<Order>> infeasibleInsert(ArrayList<ArrayList<Order>> solution)
//			throws InterruptedException, ExecutionException {
//		globalInsertInfeasible = true;
//		double temp = calcuNewBenfit(solution);
//		globalInsertInfeasible = true;
//		infeasibleOrder.removeAll(d.startEndNodes);
//		for (ArrayList<Order> path : solution) {
//			path.removeAll(infeasibleOrder);
//		}
//		insertChoose = 1;
//		insert(solution, cf.cloneToArray(infeasibleOrder));
//		globalInsertInfeasible = false;
//		infeasibleOrder.clear();
//		temp = calcuNewBenfit(solution);
//		infeasibleOrder.clear();
//		return solution;
//	}
//
//	private void record(ArrayList<ArrayList<Order>> allBestfeasiusedRouteWhole) {
//		// int k = 0;
//		// for (ArrayList<Order> route : allBestfeasiusedRouteWhole) {
//		// if(route!=null&&route.size()>0)
//		// overallMergedRoute.add(cf.listCloneObject(route));
//		// k++;
//		// }
//		for (ArrayList<Order> route : allBestfeasiusedRouteWhole) {
//			overallMergedRoute.add(cf.listCloneObject(route));
//		}
//		//System.out.println(
//				//"allBestfeasiusedRouteWhole\t" + human(allBestfeasiusedRouteWhole) + "\t" + human(overallMergedRoute));
//	}
//
//	// @remove duplicate consecutive depot nodes
//	private void removeDuplicate(ArrayList<ArrayList<Order>> route) {
//		Order pre = null;
//		int cnt = 0;
//		for (ArrayList<Order> path : route) {
//			pre = null;
//			Iterator<Order> i = path.iterator();
//			cnt = 0;
//			while (i.hasNext()) {
//				Order s = i.next(); // must be called before you can call
//									// i.remove()
//				if (pre != null && s.id == pre.id)
//					i.remove();
//				pre = s;
//				cnt++;
//			}
//
//			if (path.size() >= 3 && path.get(path.size() - 1).equals(path.get(path.size() - 2)))
//				path.remove(path.size() - 1);
//		}
//
//	}
//
//
//
//	// @remove operator
//	private ArrayList<Order> remove(ArrayList<ArrayList<Order>> route, int removeChoose) {
//		// some nodes remove will be insert and same location, then, no need to
//		// remove
//		// out
//		Random rand = new Random();
//		HashSet<Order> removeSet = new HashSet<Order>(lspCurrent.lspRealOrders);
//		removeSet.remove(null);
//
//		int numberRemove = Math.max(2, (int) ((0.05 + rand.nextDouble() * 0.10) * (lspCurrent.totalSize)));// -d.staticNodes.size())));
//		int cnt = 0;
//		List<Order> allServedNode = new ArrayList<Order>();
//		ArrayList<ArrayList<Order>> splitPath = new ArrayList<ArrayList<Order>>();
//		for (ArrayList<Order> path : route)
//			removeSet.removeAll(path);
//		int id = 0;
//
//		switch (removeChoose) {
//		case 0:
//			// random based
//			allServedNode = cf.mergeAll(route);
//			Collections.shuffle(allServedNode);
//
//			if (allServedNode.size() > 1)
//				removeSet.addAll(allServedNode.subList(0, Math.min(numberRemove, allServedNode.size())));
//			break;
//		case 1:
//			// remove shopping mall
//			int chooseLocation = 0;
//			for (int i = 0; i < Math.max(2, lspCurrent.totalSize / lspCurrent.locations.size()); i++) {
//				chooseLocation = lspCurrent.realLocations.get(rand.nextInt(lspCurrent.locations.size()));
//				for (ArrayList<Order> path : route) {
//					for (Order o : path) {
//						if (o.orderLocation == chooseLocation)
//							removeSet.add(o);
//					}
//				}
//			}
//			break;
//		case 2:
//			// trip based
//			currentSolutionMultiTrips = new ArrayList<Trip>();
//			for (int i = 0; i < route.size(); i++) {
//				List<Integer> singlePath = new ArrayList<Integer>();
//				for (int j = 1; j < route.get(i).size() - 1; j++) {
//					if (!d.startEndNodes.contains(route.get(i).get(j)))
//						singlePath.add(route.get(i).get(j).id);
//					else {
//						currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
//						singlePath = new ArrayList<Integer>();
//					}
//				}
//				currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
//			}
//			removeSet.addAll(splitAll(d.staticPath));
//			break;
//		case 3:
//			// long route
//			int averageSize = calculateAverage(route);
//			for (int i = 0; i < route.size(); i++) {
//				if (route.get(i).size() > averageSize) {
//					removeSet.addAll(route.get(i).subList(route.get(i).size() - averageSize, route.get(i).size()));
//				}
//			}
//
//			break;
//		case 4:
//			// time window group based
//			int chooseGroup = rand.nextInt(d.timeWindowGroup.size());
//			int size = d.timeWindowGroup.get(chooseGroup).size();
//			int i = 0;
//			while (removeSet.size() < numberRemove && i <= 10) {
//				id = rand.nextInt(size);
//				for (ArrayList<Order> path : route) {
//					for (Order o : path) {
//						if (o.id == id)
//							removeSet.add(o);
//					}
//				}
//				i++;
//			}
//			break;
//		case 5:
//			// infeasible based
//			globalInsertInfeasible = true;
//			calcuNewBenfit(route);
//			infeasibleOrder.removeAll(d.startEndNodes);
//			for (Order node : infeasibleOrder) {
//				if (rand.nextBoolean())
//					removeSet.add(node);
//			}
//
//			globalInsertInfeasible = false;
//			infeasibleOrder.clear();
//			break;
//		default:
//			break;
//		}
//		cf.removeDepot(removeSet, d.startEndNodes);
//		ArrayList<Order> newSet = cf.cloneToArray(removeSet);
//		// cf.removeDepot(newSet, d.con.totalCar);
//
//		// System.out.println("numberRemove\t" + d.con.humanU(newSet));
//		return newSet;
//	}
//
//	private int calculateAverage(ArrayList<ArrayList<Order>> route) {
//		int averageSize = 0;
//		for (ArrayList<Order> path : route) {
//			averageSize += path.size();
//		}
//		return averageSize / route.size();
//	}
//
//	private ArrayList<Order> splitAll(Set<List<Order>> staticPath) {
//		ArrayList<Order> splitPath = new ArrayList<Order>();
//		for (Integer u : d.trips.keySet()) {
//			for (Trip t : d.trips.get(u)) {
//				if (staticPath.contains(t.path) && rand.nextDouble() > 0.10)
//					continue;
//				if (rand.nextDouble() < 0.18) {
//					splitPath.addAll(translate(t.path));
//					splitPath.add(t.Start);
//					splitPath.add(t.End);
//				}
//			}
//		}
//
//		for (Trip t : currentSolutionMultiTrips) {
//			// if (t.path.size() <= 2)
//			// continue;
//
//			if (rand.nextDouble() < 0.5 && (t.path.size()) < 3) {
//				splitPath.addAll(translate(t.path));
//			}
//			int chooseSpliPos = (int) (7 + Math.random() * 2);
//			if (t.path.size() > chooseSpliPos) {
//				List<Order> newOrder = translate(t.path);
//				Collections.shuffle(newOrder);
//				splitPath.addAll(newOrder.subList(chooseSpliPos, newOrder.size()));
//			}
//		}
//
//		return splitPath;
//	}
//
//	public void setWeight() {
//		removeWeigt = new double[removeSize];
//		insertWeigt = new double[insertSize];
//		double uv = 1.0 / (removeSize + 0.0D);
//		double xy = 1.0 / (insertSize + 0.0D);
//		for (int p = 0; p < removeSize; p++) {
//			removeWeigt[p] = uv;
//		}
//		for (int p = 0; p < insertSize; p++) {
//			insertWeigt[p] = xy;
//		}
//	}
//
//	public double calcuTemp(double u, double newbenfit, double oldbenfit) {
//		T0 *= u;
//		double probability = Math.exp((oldbenfit - newbenfit) / T0);
//		return probability;
//	}
//
//	// @insert operator
//	private void insert(ArrayList<ArrayList<Order>> route, List<Order> removeSet)
//			throws InterruptedException, ExecutionException {
//		removeSet.removeAll(d.startEndNodes);
//		// insertChoose = 2;
//		Order chooseNode = null;
//		int[] insertArray = new int[2];
//		boolean wrong = false;
//
//		switch (insertChoose) {
//		case 0:
//			for (ArrayList<Order> path : route)
//				path.removeAll(removeSet);
//
//			for (int index = 0; index < removeSet.size(); index++) {
//				chooseNode = removeSet.get(index);
//
//				wrong = false;
//				for (ArrayList<Order> path : route) {
//					if (path.contains(chooseNode))
//						wrong = true;
//				}
//				if (!wrong) {
//					insertArray = multiVehiclesInsertion(route, chooseNode);
//					if (insertArray[1] > 0)
//						route.get(insertArray[0]).add(insertArray[1], chooseNode);
//				}
//			}
//			break;
//		case 1:
//			Collections.shuffle(removeSet);
//			chooseNode = null;
//
//			for (int index = 0; index < removeSet.size(); index++) {
//				chooseNode = removeSet.get(index);
//				ArrayList<Order> temp = new ArrayList<Order>();
//				temp.add(chooseNode);
//				for (ArrayList<Order> path : route)
//					path.removeAll(temp);
//				wrong = false;
//				for (ArrayList<Order> path : route) {
//					if (path.contains(chooseNode))
//						wrong = true;
//				}
//				if (!wrong) {
//					insertArray = multiVehiclesInsertion(route, chooseNode);
//					if (insertArray[1] > 0)
//						route.get(insertArray[0]).add(insertArray[1], chooseNode);
//				}
//			}
//			// removeExtraZero(route);
//			break;
//
//		// insert to smallest
//		case 2:
//			Collections.shuffle(removeSet);
//			chooseNode = null;
//
//			for (int index = 0; index < removeSet.size(); index++) {
//				chooseNode = removeSet.get(index);
//				ArrayList<Order> temp = new ArrayList<Order>();
//				temp.add(chooseNode);
//				for (ArrayList<Order> path : route)
//					path.removeAll(temp);
//				wrong = false;
//				for (ArrayList<Order> path : route) {
//					if (path.contains(chooseNode))
//						wrong = true;
//				}
//				if (!wrong) {
//					insertArray = multiVehicleInsertionRandomAddConsider(route, chooseNode);
//					if (insertArray[1] > 0)
//						route.get(insertArray[0]).add(insertArray[1], chooseNode);
//				}
//			}
//			break;
//		case 3:
//			// insert to the shortest max time one
//			Collections.shuffle(removeSet);
//			chooseNode = null;
//
//			for (int index = 0; index < removeSet.size(); index++) {
//				chooseNode = removeSet.get(index);
//				ArrayList<Order> temp = new ArrayList<Order>();
//				temp.add(chooseNode);
//				for (ArrayList<Order> path : route)
//					path.removeAll(temp);
//				wrong = false;
//				for (ArrayList<Order> path : route) {
//					if (path.contains(chooseNode))
//						wrong = true;
//				}
//				if (!wrong) {
//					insertArray = multiVehicleMinSpanInsertion(route, chooseNode);
//					if (insertArray[1] > 0)
//						route.get(insertArray[0]).add(insertArray[1], chooseNode);
//				}
//			}
//			break;
//		default:
//			break;
//		}
//	}
//
//	private int[] multiVehicleInsertionRandomAddConsider(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
//		int[] minarray = new int[2];
//		minarray[1] = -1;
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		needMiddleAdd = new double[vehicles.size()][2];
//		postPone = new double[vehicles.size()];
//		int start = 0;
//		int k = 0;
//		for (int i = 0; i < route.size(); i++) {
//			k = lspCurrent.vehicleMap.get((Integer) i);
//			// System.out.println("checking in veh "+i);
//			increa_comp = calcuResult(new PathSolution(route.get(i), k), d.con.yita2, 0, 0, 0);
//			if (increa_comp > 1e5 / 2)
//				continue;
//			globalCost = 0;
//			start = 0;
//			// useless depot service time equal to 1
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//			for (int m = start; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//
//				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//
//				if (d.con.disMatrix[chooseNode.orderLocation()][route.get(i).get(m + 1).orderLocation()] > 1e-3
//						&& route.get(i).get(m + 1).upperTime - chooseNode.lowerTime < chooseNode.serviceTime()
//								+ d.con.disMatrix[chooseNode.orderLocation()][route.get(i).get(m + 1).orderLocation()])
//					continue;
//
//				if (d.con.disMatrix[route.get(i).get(m).orderLocation()][chooseNode.orderLocation()] > 1e-3
//						&& chooseNode.upperTime - route.get(i).get(m).lowerTime < route.get(i).get(m).serviceTime()
//								+ d.con.disMatrix[route.get(i).get(m).orderLocation()][chooseNode.orderLocation()])
//					continue;
//
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), k), d.con.yita2, 0, 0, 0);
//				benefi = globalCost - increa_comp + r.nextDouble() * 10;
//				if (finalOptimizeFlag) {
//					if (benefi < minBeneif && volumeFeasible) {
//						minBeneif = benefi;
//						minarray[0] = i;
//						minarray[1] = m + 1;
//					}
//				} else {
//					if (benefi < minBeneif) {
//						minBeneif = benefi;
//						minarray[0] = i;
//						minarray[1] = m + 1;
//
//					}
//				}
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//
//	}
//
//	private int singleVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, int choosedVehicle, Order chooseNode) {
//		int minInsertPos = -1;
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e11;
//		double globalCost = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//
//		increa_comp = calcuResult(
//				new PathSolution(route.get(choosedVehicle), lspCurrent.vehicleMap.get(choosedVehicle)), d.con.yita2, 0,
//				0, 0);// +
//		// maxUtilize
//		// *
//		// d.totalTimeSpan
//		globalCost = 0;
//		int start = 0;
//		start = 0;
//		// useless depot service time equal to 1
//		for (int j = 1; j < route.get(choosedVehicle).size() - 1; j++) {
//			if (d.startEndNodes.contains(route.get(choosedVehicle).get(j))
//					&& route.get(choosedVehicle).get(j).location.id != route.get(choosedVehicle).get(0).location.id)
//				start++;
//			else
//				break;
//		}
//		// System.out.println(human(route));
//		for (int m = start; m < route.get(choosedVehicle).size() - 1; m++) {
//			indexNextDepot = 0;
//			if (lspCurrent.needCheckMaxDrop
//					&& tripSize(route.get(choosedVehicle), m + 1) > vehicles.get(choosedVehicle).lsp.maxDrop - 1) {
//				if (indexNextDepot > m + 1 && indexNextDepot < route.get(choosedVehicle).size() - 1)
//					m = indexNextDepot - 1;
//				continue;
//			}
//			route.get(choosedVehicle).add(m + 1, chooseNode);
//			globalCost = calcuResult(
//					new PathSolution(route.get(choosedVehicle), lspCurrent.vehicleMap.get(choosedVehicle)), d.con.yita2,
//					0, 0, 0);
//			benefi = globalCost - increa_comp + r.nextDouble() * 1;
//
//			if (benefi < minBeneif) {
//				minBeneif = benefi;
//				minInsertPos = m + 1;
//			}
//			route.get(choosedVehicle).remove(m + 1);
//		}
//		return minInsertPos;
//	}
//
//	private int[] multiVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
//		int[] minarray = new int[2];
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		int start = 0;
//		double initialMaxUtilize = 0;
//		int realK = 0;
//		for (int i = 0; i < route.size(); i++) {
//			realK = lspCurrent.vehicleMap.get((Integer) (i));
//			// if (!chooseNode.vehiclesIndex.contains(realK+1)) {
//			// continue;
//			// }
//			start = 0;
//			// useless depot service time equal to 1
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//
//			increa_comp = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//			globalCost = 0;
//			// System.out.println(human(route));
//			for (int m = 0; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//				if (lspCurrent.needCheckMaxDrop && tripSize(route.get(i), m + 1) > lspCurrent.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), realK), d.con.yita2, 0, 0, 0);
//				benefi = globalCost - increa_comp + r.nextDouble() * 1;
//
//				if (finalOptimizeFlag) {
//					if (benefi < minBeneif && volumeFeasible) {
//						minBeneif = benefi;
//						minarray[0] = i;
//						minarray[1] = m + 1;
//					}
//				} else {
//					if ((needSmooth && benefi < fitnessParaThreshold * minBeneif
//							&& calcuAdjustTravelTime(route) < calcuAdjustTravelTime(orginRoute)
//									+ travelTimeParaThreshold
//							&& calculateSmoothAll(route) > calculateSmoothAll(orginRoute))
//							|| (!needSmooth && benefi < minBeneif)) {
//						minBeneif = benefi;
//						minarray[0] = i;
//						minarray[1] = m + 1;
//
//					}
//				}
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	double calculateMaxUtilize(ArrayList<ArrayList<Order>> route) {
//		maxUtilize = 0;
//		for (int i = 0; i < route.size(); i++)
//			calcuResult(new PathSolution(route.get(i), i), d.con.yita2, 0, 0, 0);
//		return maxUtilize;
//	}
//
//	private int[] multiVehicleSmallestInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
//		int[] minarray = new int[2];
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		int start = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		for (int i = 0; i < route.size(); i++) {
//			if (!chooseNode.vehiclesIndex.contains((Integer) (i + 1))) {
//				continue;
//			}
//			start = 0;
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//			// System.out.println(human(route));
//			for (int m = start; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//				if (lspCurrent.needCheckMaxDrop && tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				globalCost = calcuResult(new PathSolution(route.get(i), i), d.con.yita2, 0, 0, 0);
//				benefi = globalCost + r.nextDouble() * 1;
//
//				if (benefi < minBeneif) {
//					minBeneif = benefi;
//					minarray[0] = i;
//					minarray[1] = m + 1;
//				}
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	private int[] multiVehicleMinSpanInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
//		int[] minarray = new int[2];
//		double increa_comp = 0;
//		double benefi = 0;
//		double minBeneif = 1e10;
//		double globalCost = 0;
//		int start = 0;
//		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
//		int k = 0;
//		for (int i = 0; i < route.size(); i++) {
//			k = lspCurrent.vehicleMap.get((Integer) i);
//			if (!chooseNode.vehiclesIndex.contains((Integer) (k + 1))) {
//				continue;
//			}
//			// System.out.println(human(route));
//			start = 0;
//			// useless depot service time equal to 1
//			for (int j = 1; j < route.get(i).size() - 1; j++) {
//				if (d.startEndNodes.contains(route.get(i).get(j))
//						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
//					start++;
//				else
//					break;
//			}
//			for (int m = start; m < route.get(i).size() - 1; m++) {
//				indexNextDepot = 0;
//				if (lspCurrent.needCheckMaxDrop && tripSize(route.get(i), m + 1) > lspCurrent.maxDrop - 1) {
//					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
//						m = indexNextDepot - 1;
//					continue;
//				}
//				route.get(i).add(m + 1, chooseNode);
//				calcuResult(new PathSolution(route.get(i), k), d.con.yita2, 0, 0, 0);
//				benefi = globalEndTime;
//
//				if (benefi < minBeneif) {
//					minBeneif = benefi;
//					minarray[0] = i;
//					minarray[1] = m + 1;
//				}
//				route.get(i).remove(m + 1);
//			}
//		}
//		return minarray;
//	}
//
//	private int tripSize(ArrayList<Order> arrayList, int pos) {
//		int size = arrayList.size();
//		int cnt = 0;
//		indexNextDepot = pos;
//		int prelocation = arrayList.get(pos).orderLocation;
//
//		if (!d.startEndNodes.contains(arrayList.get(pos))) {
//			cnt = 1;
//			for (int i = pos + 1; i < size - 1; i++) {
//				if (d.startEndNodes.contains(arrayList.get(i))) {
//					indexNextDepot = i;
//					break;
//				} else {
//					if (arrayList.get(i).orderLocation != prelocation) {
//						cnt++;
//						prelocation = arrayList.get(i).orderLocation;
//					}
//				}
//			}
//		}
//		int nextlocation = arrayList.get(pos).orderLocation;
//		for (int i = pos - 1; i > 0; i--) {
//			if (d.startEndNodes.contains(arrayList.get(i))) {
//				break;
//			} else {
//				if (arrayList.get(i).orderLocation != nextlocation) {
//					cnt++;
//					nextlocation = arrayList.get(i).orderLocation;
//				}
//			}
//		}
//		return cnt;
//	}
//
//	List<Order> translate(List<Integer> path) {
//		List<Order> newPath = new ArrayList<Order>();
//		for (Integer node : path)
//			newPath.add(orders.get(node));
//		return newPath;
//	}
//
//	private void addToEmpty(ArrayList<Order> route, Order start, Order end) {
//		route.add(start);
//		route.add(end);
//	}
//
//	private void initial_2cnt() {
//		cntRemove = new double[removeSize];
//		scoreRemove = new double[removeSize];
//
//		cntInsert = new double[insertSize];
//		scoreInsert = new double[insertSize];
//		for (int i = 0; i < removeSize; i++) {
//			cntRemove[i] = 1.0D;
//			scoreRemove[i] = 0.0D;
//		}
//
//		for (int i = 0; i < insertSize; i++) {
//			cntInsert[i] = 1.0D;
//			scoreInsert[i] = 0.0D;
//		}
//	}
//
//	public void setRoutes(Vector<Integer> path) {
//		startServTime = new int[d.con.stops() + d.con.totalCar() * 2 + 1];
//	}
//
//	public double calcuResult(PathSolution pathSolution, double yita2, int pos, double totalTime, double value) {
//		boolean feasible = true;
//		int maxViolate = 0;
//		travelDistance = 0;
//		if (!d.reSequenceFlagOn)
//			feasible = calculateMultiDepotCapacityConstraint(pathSolution, pos);
//		if (!feasible) {
//			value = 1e9 + vioVolume * 100;
//			volumeFeasible = false;
//			// System.out.println("violation because of volume");
//			return value;
//		} else
//			volumeFeasible = true;
//
//		int size = pathSolution.getPath().size();
//		Order currentNode = pathSolution.getPath().get(pos);
//		Order nextNode = null, nodeK = null;
//		double arrivalTime = 0;
//
//		double temp = 0;
//		int tempValue = 0;
//		int k = pathSolution.getVehicleID();
//		double totalDis = 0;
//		double obj = 0;
//		maxWait = 0;
//		maxWaitPos = -1;
//		totalTrip = 1;
//
//		double tempTravelTime = 0;
//		double initialTotalTime = 0;
//
//		int tempNodeServiceTime = 0;
//		int tempNextNodeServiceTime = 0;
//		if (pos == 0) {
//			initialTotalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime());
//			totalTime = initialTotalTime;
//		}
//
//		arrivalTime = totalTime;
//		boolean needWait = true;
//		lunchFeasible = false;
//		ArrayList<Integer> postponeTime = new ArrayList<Integer>();
//		double waitTime = 0;
//		double simpleTripDistance = 0;
//		double tempDis = 0;
//		needMiddleAdd[k] = new double[2];
//		boolean CheckServiceTimeNoNeed = false;
//		int start = 1;
//		postPone[k] = 0;
//		for (int j = 1; j < size - 1; j++) {
//			if (d.startEndNodes.contains(pathSolution.getPath().get(j))
//					&& pathSolution.getPath().get(j).id != pathSolution.getPath().get(0).id)
//				start++;
//			else
//				break;
//		}
//		int upperWait = (int) 1e6;
//		for (int i = pos + 1; i < size - 1; i++) {
//			needWait = true;
//			tempValue = 0;
//			nextNode = pathSolution.getPath().get(i);
//			nodeK = pathSolution.getPath().get(i + 1);
//
//			tempNodeServiceTime = 0;
//			tempNextNodeServiceTime = 0;
//			temp = d.con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//
//			if (i < size - 2) {
//				tempDis = d.con.disMatrix[nextNode.orderLocation()][nodeK.orderLocation()];
//				simpleTripDistance += tempDis;
//			}
//
//			tempTravelTime += temp;
//			travelTime += temp;
//			// (d.dynamicFlag &&
//			CheckServiceTimeNoNeed = Math.abs(currentNode.orderLocation() - nextNode.orderLocation()) < 1e-3;
//			CheckServiceTimeNoNeed = CheckServiceTimeNoNeed && d.dynamicFlag
//					&& (d.startEndNodes.contains(nextNode) || d.newOrdersCorresPickup.contains(nextNode.id));
//			if (i == 1 || (i > 1 && currentNode.orderLocation() != pathSolution.getPath().get(i - 2).orderLocation()))
//				tempNodeServiceTime = currentNode.serviceTime();
//			totalTime += temp + tempNodeServiceTime;
//			initialTotalTime = totalTime;
//			totalDis += d.con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//			travelDistance += d.con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//			arrivalTime = totalTime;
//			if (nextNode.orderLocation != currentNode.orderLocation)
//				tempNextNodeServiceTime = nextNode.serviceTime();
//			if (d.dynamicFlag
//					&& (d.startEndNodes.contains(nextNode) || d.newOrdersCorresPickup.contains(nextNode.id))) {
//				// delete
//			} else if (lsps.get(0).lunchLocationFlag && !d.startEndNodes.contains(nextNode)
//					&& totalTime + tempNextNodeServiceTime >= locations[nextNode.orderLocation()].lunchStart
//					&& totalTime <= locations[nextNode.orderLocation()].lunchEnd) {
//				tempValue = (int) (locations[nextNode.orderLocation()].lunchEnd - totalTime);
//				totalTime = locations[nextNode.orderLocation()].lunchEnd;
//				if (tempValue >= 20) {
//					lunchFeasible = true;
//					lunchFeasibleCheckNeed = false;
//				}
//				// needWait = false;
//			}
//
//			if (d.startEndNodes.contains(nextNode)) {
//				totalTrip += 1;
//				tempDis = d.con.disMatrix[nextNode.orderLocation()][nodeK.orderLocation()];
//				simpleTripDistance -= tempDis;
//				tempDis = d.con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//				simpleTripDistance -= tempDis;
//
//				if (vehicles.get(k).lsp.getMaxSimpleTripDistance() < simpleTripDistance)
//					vehicles.get(k).lsp.setSimpleTripDistance(simpleTripDistance);
//
//				simpleTripDistance = 0;
//			}
//
//			if (totalTime < nextNode.lowerTime()) {
//				tempValue += (nextNode.lowerTime() - totalTime);
//				totalTime = nextNode.lowerTime();
//			}
//
//			if (i == 1) {
//				startTimeFirstOrder = (int) totalTime;
//			}
//			// if (con.slotsFalg) {
//			// if (currentNode.orderLocation != nextNode.orderLocation &&
//			// nextNode.location.type.equals("IMD")) {
//			// tempValue = (int) Math.max(0,
//			// con.splitSlot * Math.ceil(totalTime / (con.splitSlot + 0.0))
//			// -
//			// totalTime);
//			// }
//			// }
//
//			if (totalTime > nextNode.upperTime()) {
//				tempValue = 0;
//				if (totalTime > nextNode.upperTime() + 120)
//					feasible = false;
//
//				if (globalInsertInfeasible) {
//					// if (!d.startEndNodes.contains(nextNode))
//					infeasibleOrder.add(nextNode);
//					// else
//					// infeasibleOrder.add(currentNode);
//				}
//
//				value += lsps.get(vehicles.get(k).originLSP).parameter.getTimeVioCost(totalTime - nextNode.upperTime());
//				if (totalTime - nextNode.upperTime() > maxViolate)
//					maxViolate = (int) (totalTime - nextNode.upperTime());
//				totalDis += lsps.get(vehicles.get(k).originLSP).parameter
//						.getTimeVioCost(totalTime - nextNode.upperTime());
//
//			}
//
//			if (i == start)
//				postPone[k] = tempValue;
//			if (nextNode.upperTime() - totalTime < upperWait)
//				upperWait = (int) (nextNode.upperTime() - totalTime);
//
//			if (needWait && tempValue > 0 && i > start) {
//				postponeTime.add(Math.min(tempValue, upperWait));
//			}
//
//			if (maxWait < totalTime - initialTotalTime) {
//				maxWait = tempValue;
//				maxWaitPos = i;
//				nextTripSize = size - 2 - i;
//			}
//			currentNode = nextNode;
//			waitTime += tempValue;
//		}
//
//		if (postponeTime.size() > 0)
//			postPone[k] = checkFirstThree(postPone[k], postponeTime, pathSolution, k, 2);
//
//		// if (!lunchFeasible && sizeTimeRecord > 0)
//		// needMiddleAdd[k] = checkFeasibleLunchForward(pathSolution, 3, k);
//
//		// if (!lunchFeasible) {
//		// needMiddleAdd[k] = checkFeasibleLunchBackward(pathSolution, 30, k);//
//		// start
//		// 30 minutes earlier
//		// }
//
//		arrivalTime = totalTime;
//		arrivalTime += d.con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
//				.orderLocation()] + tempNextNodeServiceTime;
//		tempTravelTime += d.con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
//				.orderLocation()];
//		travelTime += d.con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
//				.orderLocation()];
//
//		if (arrivalTime > vehicles.get(k).truckUpperTime()) {
//			if (arrivalTime > vehicles.get(k).truckUpperTime() + 120)
//				feasible = false;
//			value += lsps.get(vehicles.get(k).originLSP).parameter
//					.getTimeVioCost(arrivalTime - vehicles.get(k).truckUpperTime());
//			totalDis += lsps.get(vehicles.get(k).originLSP).parameter
//					.getTimeVioCost(arrivalTime - vehicles.get(k).truckUpperTime());
//		}
//		globalEndTime = arrivalTime;
//
//		value += lsps.get(vehicles.get(k).originLSP).parameter.getMaxSpanWeight()
//				* (arrivalTime - vehicles.get(k).lsp.getMaxTime());
//
//		// System.err.println("k\t" + k + "\t" + vehicles.get(k).originLSP);
//		travelDistance += d.con.disMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
//				.orderLocation()];
//		totalDis += d.con.disMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()];
//		if (d.con.objFlag == 0)
//			obj = value + lsps.get(vehicles.get(k).originLSP).parameter.getTravelWeight() * tempTravelTime;
//		if (d.con.objFlag == 1)
//			obj = value + lsps.get(vehicles.get(k).originLSP).parameter.getTravelWeight() * totalDis * scalingFactor;
//
//		waitTime -= postPone[k];
//		obj += 1e5 * tripSizeWhole(pathSolution.getPath(), k);
//		double waitPara = lsps.get(vehicles.get(k).originLSP).parameter.getWaitWeight();
//		if (waitTime >= 120)
//			waitPara *= 20;
//		else if (waitTime >= 60)
//			waitPara *= 10;
//		if (d.con.objFlag == 0) {
//			obj += waitPara * waitTime;
//		} else {
//			obj += waitPara * waitTime;
//		}
//		if (!feasible)
//			check = false;
//		// if (!feasible)
//		// routesColumn.add(new Column(pathSolution.getPath(), value));
//		return obj;
//	}
//
//	private double checkFirstThree(double postPone, ArrayList<Integer> postponeTime, PathSolution pathSolution, int k,
//			int limit) {
//		int postponeValue = (int) postPone;
//		limit = Math.min(limit, postponeTime.size());
//		for (int i = 0; i < limit; i++) {
//			postponeValue += postponeTime.get(i);
//			if (!checkFeasible(postponeValue, pathSolution.getPath(),
//					Math.max(pathSolution.getPath().get(0).lowerTime, vehicles.get(k).truckLowerTime()), 0, k, true)) {
//				// move backward
//				postponeValue -= postponeTime.get(i);
//				checkFeasible(postponeValue, pathSolution.getPath(),
//						Math.max(pathSolution.getPath().get(0).lowerTime, vehicles.get(k).truckLowerTime()), 0, k,
//						true);
//				break;
//			}
//		}
//		return postponeValue;
//	}
//
//	private boolean checkFeasible(double min, ArrayList<Order> pathSolution, double startTime, int startPos, int k,
//			boolean needUpdate) {
//		boolean feasible = true;
//
//		int size = pathSolution.size();
//		int tempNodeServiceTime = 0;
//		int tempNextNodeServiceTime = 0;
//		Order currentNode = pathSolution.get(startPos);
//		Order nextNode = null;
//
//		double temp = 0;
//		double totalTime = startTime + min;
//
//		for (int i = startPos + 1; i < size - 1; i++) {
//			nextNode = pathSolution.get(i);
//			tempNodeServiceTime = 0;
//			tempNextNodeServiceTime = 0;
//			temp = d.con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//
//			if (i == 1 || (i > 1 && currentNode.orderLocation() != pathSolution.get(i - 2).orderLocation()))
//				tempNodeServiceTime = currentNode.serviceTime();
//			totalTime += temp + tempNodeServiceTime;
//
//			if (nextNode.orderLocation != currentNode.orderLocation)
//				tempNextNodeServiceTime = nextNode.serviceTime();
//
//			if (pathSolution.get(0).serviceTime > 2 && !d.startEndNodes.contains(nextNode)
//					&& totalTime + tempNextNodeServiceTime > locations[nextNode.orderLocation()].lunchStart
//					&& totalTime < locations[nextNode.orderLocation()].lunchEnd) {
//				totalTime = locations[nextNode.orderLocation()].lunchEnd;
//			}
//
//			if (totalTime < nextNode.lowerTime()) {
//				totalTime = nextNode.lowerTime();
//			}
//			currentNode = nextNode;
//
//			if (totalTime > nextNode.upperTime()) {
//				feasible = false;
//				break;
//			}
//
//		}
//
//		totalTime += tempNextNodeServiceTime
//				+ d.con.timeMatrix[currentNode.orderLocation()][pathSolution.get(size - 1).orderLocation()];
//		if (totalTime > vehicles.get(k).truckUpperTime())
//			feasible = false;
//
//		return feasible;
//	}
//
//	private int tripSizeWhole(ArrayList<Order> arrayList, int k) {
//		int size = arrayList.size();
//		int cnt = 0;
//		indexNextDepot = 0;
//		int prelocation = arrayList.get(0).orderLocation;
//
//		while (indexNextDepot < size - 1) {
//			cnt = 0;
//			for (int i = indexNextDepot + 1; i < size; i++) {
//				if (d.startEndNodes.contains(arrayList.get(i))) {
//					indexNextDepot = i;
//					break;
//				} else {
//					if (arrayList.get(i).orderLocation != prelocation) {
//						cnt++;
//						prelocation = arrayList.get(i).orderLocation;
//					}
//				}
//			}
//			if (cnt > vehicles.get(k).lsp.maxDrop) {
//				break;
//			}
//		}
//
//		return Math.max(0, cnt - vehicles.get(k).lsp.maxDrop);
//	}
//
//	public double calculateAfterPostPone(PathSolution pathSolution, double postPoneTime) {
//		int size = pathSolution.getPath().size();
//		Order currentNode = pathSolution.getPath().get(0);
//		Order nextNode = null, nodeK = null;
//		double temp = 0;
//		double tempValue = 0;
//		int k = pathSolution.getVehicleID();
//		double initialTotalTime = 0;
//		double thisVehicleWaitMax = 0;
//
//		initialTotalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime()) + postPoneTime;
//		totalTime = initialTotalTime;
//
//		boolean needWait = true;
//		int tempNodeServiceTime = 0;
//		int tempNextNodeServiceTime = 0;
//		for (int i = 1; i < size - 1; i++) {
//			tempNodeServiceTime = 0;
//			tempNextNodeServiceTime = 0;
//			needWait = true;
//			tempValue = 0;
//			nextNode = pathSolution.getPath().get(i);
//			nodeK = pathSolution.getPath().get(i + 1);
//			temp = d.con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
//
//			if (i == 1 || (i > 1 && currentNode.orderLocation() != pathSolution.getPath().get(i - 2).orderLocation()))
//				tempNodeServiceTime = currentNode.serviceTime();
//			totalTime += temp + tempNodeServiceTime;
//			initialTotalTime = totalTime;
//
//			if (nextNode.orderLocation() != currentNode.orderLocation())
//				tempNextNodeServiceTime = nextNode.serviceTime;
//
//			if (pathSolution.getPath().get(0).serviceTime > 1 && !d.startEndNodes.contains(nextNode)
//					&& totalTime + tempNextNodeServiceTime >= locations[nextNode.orderLocation()].lunchStart
//					&& totalTime <= locations[nextNode.orderLocation()].lunchEnd) {
//				tempValue = (locations[nextNode.orderLocation()].lunchEnd - totalTime);
//				totalTime = locations[nextNode.orderLocation()].lunchEnd;
//			}
//
//			if (i == 1) {
//				needWait = false;
//			}
//
//			if (totalTime < nextNode.lowerTime()) {
//				tempValue += (nextNode.lowerTime() - totalTime);
//				totalTime = nextNode.lowerTime();
//			}
//
//			if (needWait && tempValue > 0) {
//				if (thisVehicleWaitMax < tempValue) {
//					thisVehicleWaitMax = tempValue;
//				}
//			}
//			currentNode = nextNode;
//		}
//
//		return thisVehicleWaitMax;
//	}
//
//	// @swap operator
//	static ArrayList<Order> swap(ArrayList<Order> cities, int i, int j) {
//		// conducts a 2 opt swap by inverting the order of the points between i
//		// and j
//		ArrayList<Order> newTour = new ArrayList<>();
//
//		// take array up to first point i and add to newTour
//		int size = cities.size();
//		for (int c = 0; c <= i - 1; c++) {
//			newTour.add(cities.get(c));
//		}
//
//		// invert order between 2 passed points i and j and add to newTour
//		int dec = 0;
//		for (int c = i; c <= j; c++) {
//			newTour.add(cities.get(j - dec));
//			dec++;
//		}
//
//		// append array from point j to end to newTour
//		for (int c = j + 1; c < size; c++) {
//			newTour.add(cities.get(c));
//		}
//
//		return newTour;
//	}
//
//	// @check if CP need
//	public void calcuDockInterval(ArrayList<Order> path, Set<Integer> noNeedCheckMall) {
//		int size = path.size();
//		Order currentNode = path.get(0);
//		Order nextNode = path.get(0);
//		Order nodeK = null;
//		int totalTime = currentNode.lowerTime();
//		if (lspCurrent.lunchLocationFlag
//				&& totalTime + currentNode.serviceTime() > locations[currentNode.orderLocation()].lunchStart
//				&& totalTime < locations[currentNode.orderLocation()].lunchEnd) {
//			totalTime = locations[currentNode.orderLocation()].lunchEnd;
//		}
//		for (int i = 1; i < size - 1; i++) {
//			nextNode = path.get(i);
//			nodeK = path.get(i + 1);
//			totalTime += d.con.timeMatrix[currentNode.orderLocation][nextNode.orderLocation]
//					+ currentNode.serviceTime();
//			if (lspCurrent.lunchLocationFlag
//					&& totalTime + nextNode.serviceTime > locations[nextNode.orderLocation()].lunchStart
//					&& totalTime < locations[nextNode.orderLocation()].lunchEnd) {
//				totalTime = locations[nextNode.orderLocation()].lunchEnd;
//			}
//
//			if (totalTime < nextNode.lowerTime) {
//				totalTime = nextNode.lowerTime;
//			}
//			// if (con.slotsFalg && currentNode.orderLocation !=
//			// nextNode.orderLocation
//			// && nextNode.location.type.equals("IMD"))
//			// totalTime += (int) Math.max(0,
//			// con.splitSlot * Math.ceil(totalTime / (con.splitSlot + 0.0)) -
//			// totalTime);
//
//			currentNode = nextNode;
//
//			if (orders.contains(currentNode) && !d.startEndNodes.contains(currentNode)
//					&& currentNode.location.type.equals("IMD")
//					&& lspCurrent.locationCheckCP.get(currentNode.orderLocation()) != null
//					&& lspCurrent.locationCheckCP.get(currentNode.orderLocation())
//					&& !noNeedCheckMall.contains((Integer) currentNode.orderLocation)) {
//				if (timeIntervalsList.containsKey(d.sameGroup.get(nextNode.id))) {
//					timeIntervalsList.get(d.sameGroup.get(nextNode.id)).add(new TimeInterval(
//							d.sameGroup.get(nextNode.id), totalTime, totalTime + nextNode.serviceTime()));
//				} else {
//					Vector<TimeInterval> tempInterval = new Vector<TimeInterval>();
//					tempInterval.add(new TimeInterval(d.sameGroup.get(nextNode.id), totalTime,
//							totalTime + nextNode.serviceTime()));
//					timeIntervalsList.put(d.sameGroup.get(nextNode.id), tempInterval);
//				}
//			}
//		}
//	}
//
//	// @check if CP need
//	private boolean globalNeedCheck(ArrayList<ArrayList<Order>> route) {
//		// record each vehicle's imd mall visited(either 0 or 1)
//		HashSet<ArrayList<Integer>> storedMallVisit = new HashSet<ArrayList<Integer>>();
//		noNeedCheckMall = new HashSet<Integer>();
//		int cnt = 0;
//		int car = 0;
//
//		for (ArrayList<Order> path : route) {
//			for (Order node : path) {
//				if (node.location.type == "IMD") {
//					ArrayList<Integer> temp = new ArrayList<Integer>();
//					temp.add(node.orderLocation);
//					temp.add(car);
//					storedMallVisit.add(temp);
//				}
//			}
//		}
//
//		HashMap<Integer, Integer> vistSummary = new HashMap<Integer, Integer>();
//		for (ArrayList<Integer> mallVisit : storedMallVisit) {
//			if (vistSummary.containsKey(mallVisit.get(0)))
//				vistSummary.put(mallVisit.get(0), 1 + vistSummary.get(mallVisit.get(0)));
//			else
//				vistSummary.put(mallVisit.get(0), 1);
//		}
//
//		for (Integer mall : vistSummary.keySet()) {
//			if (vistSummary.get(mall) <= d.Ucapacity[mall])
//				noNeedCheckMall.add(mall);
//			else
//				cnt++;
//		}
//
//		if (cnt > 0)
//			return true;
//		else
//			return false;
//	}
//
//	// @check if CP need
//	public boolean needCheckCPForWaitingTime(ArrayList<ArrayList<Order>> feasiusedRouteWhole) {
//		boolean needCheck = false;
//		if (!d.globalInitialCheckCP)
//			return false;
//
//		if (!globalNeedCheck(feasiusedRouteWhole))
//			return false;
//		timeIntervalsList.clear();
//
//		for (ArrayList<Order> path : feasiusedRouteWhole) {
//			calcuDockInterval(path, noNeedCheckMall);
//		}
//		if (timeIntervalsList == null || timeIntervalsList.size() < 1)
//			return false;
//
//		for (DummyOrder dummyOrder : d.dummyOrders) {
//			for (int i = 0; i < dummyOrder.usedDock; i++) {
//				timeIntervalsList.get(dummyOrder.getMallId())
//						.add(new TimeInterval(dummyOrder.getMallId(), dummyOrder.getMin(), dummyOrder.getMax()));
//			}
//		}
//
//		int cntU = 0;
//		for (Integer key : timeIntervalsList.keySet()) {
//			for (int i = 0; i < timeIntervalsList.get(key).size(); i++) {
//				cntU = 1;
//				for (int j = 0; j < timeIntervalsList.get(key).size(); j++) {
//					if (i == j)
//						continue;
//					if ((timeIntervalsList.get(key).get(i).to - timeIntervalsList.get(key).get(j).from)
//							* (timeIntervalsList.get(key).get(j).to - timeIntervalsList.get(key).get(i).from) > 0)
//						cntU++;
//				}
//				if (cntU > d.Ucapacity[key]) {
//					needCheck = true;
//					return true;
//				}
//			}
//		}
//		return needCheck;
//	}
//
//}
