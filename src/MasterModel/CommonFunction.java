package MasterModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

import org.apache.commons.collections15.list.SynchronizedList;

import java.util.Map.Entry;

import Data.Constants;
import Data.Location;
import Data.Lsp;
import Data.Order;
import Data.Trip;
import Data.Vehicle;
import edu.princeton.cs.algs4.DirectedEdge;

public class CommonFunction {
	String s;

	public CommonFunction() {
	}


	public void removeDepot(HashSet<Order> removeSet, Set<Order> startEndNodes) {
		Iterator it = removeSet.iterator();
		Order node = null;
		while (it.hasNext()) {// tabuInsert(route, node);
			node = (Order) it.next();
			if (startEndNodes.contains(node))
				it.remove();
		}
	}


	protected int calAllStops(Vector<Vector<Integer>> previousRoute) {
		int totalStops = 0;
		for (Vector<Integer> route : previousRoute) {
			for (int i = 1; i < route.size() - 1; i++) {
				totalStops++;
			}
		}
		return totalStops;
	}

	protected void removeNegtiveSingle(List<Integer> path) {
		int node = 0;
		Iterator it = path.iterator();
		while (it.hasNext()) {// tabuInsert(route, node);
			node = (Integer) it.next();
			if (node == -1)
				it.remove();
		}

	}

	protected void removeNegtive(Vector<Vector<Integer>> route) {

		for (Vector<Integer> path : route) {
			removeNegtiveSingle(path);
		}
	}

	private void removeNegtiveSingle(Vector<Integer> path) {
		int node = 0;
		Iterator it = path.iterator();
		while (it.hasNext()) {// tabuInsert(route, node);
			node = (Integer) it.next();
			if (node == -1)
				it.remove();
		}
	}

	// public static Vector<Vector<Integer>>
	// cloneVectorTransfer(Vector<Vector<Integer>> oldRoute) {
	// Vector<Vector<Integer>> route = new Vector<Vector<Integer>>();
	// int cntI = 0;
	// int cntJ = 0;
	// int zerosBetween = 0;
	// for (Vector<Integer> p : oldRoute) {
	// route.add(new Vector<Integer>());
	// cntJ = 0;
	// for (Integer q : p) {
	// if (cntJ>0&&q <= totalCar)
	// {zerosBetween++;
	// q = sumAllOrder - 1 + zerosBetween;
	// }
	// route.get(cntI).add(q);
	// cntJ++;
	// }
	// cntI++;
	// }
	// return route;
	// }
	protected List<Integer> removeGroup(Vector<Vector<Integer>> route) {
		Vector<Integer> chooseNode = new Vector<Integer>();
		// System.out.println(route);
		for (Vector<Integer> path : route)
			chooseNode.addAll(path.subList(1, path.size() - 1));

		Collections.shuffle(chooseNode);
		if (chooseNode.size() < 1)
			return chooseNode;
		return chooseNode.subList(0, (int) Math.max(1, chooseNode.size() * 0.25));
	}



//	public double[] calcuTravelPlusServe(Vector<Integer> vector, int k) {
//		double[] globalCost = new double[2];
//		int size = vector.size();
//		int currentNode = vector.get(0);
//		int nextNode = currentNode;
//		// System.out.println(nodeSetGroup);
//		for (int i = 1; i < size; i++) {
//			nextNode = vector.get(i);
//			globalCost[0] += con.timeMatrix[orders.get(currentNode).orderLocation()][orders.get(nextNode).orderLocation()];
//			if(orders.get(currentNode).orderLocation()!=orders.get(nextNode).orderLocation())
//			globalCost[1] += con.timeMatrix[orders.get(currentNode).orderLocation()][orders.get(nextNode).orderLocation()] + orders.get(currentNode).serviceTime();
//			// System.out.println(currentNode+"\t"+nextNode+"\t"+con.timeMatrix[currentNode][nextNode]+"\t"+orders.get(currentNode).serviceTime());
//			currentNode = nextNode;
//		}
//		return globalCost;
//	}
//
//	public double calcuResultSeperate(Vector<Integer> vector, int k) {
//		double globalCost = 0;
//		int size = vector.size();
//		int currentNode = vector.get(0);
//		int nextNode = vector.get(0);
//		double totalTime = Math.max(orders.get(vector.get(1)).lowerTime(), vehicles.get(k).truckLowerTime());
//		double currCapacity = 0;
//		double posCapacity = 0;
//		double negCapacity = 0;
//		// System.out.println(nodeSetGroup);
//		for (int i = 1; i < size; i++) {
//			nextNode = vector.get(i);
//			if(orders.get(currentNode).orderLocation()==orders.get(nextNode).orderLocation())
//				continue;
//			globalCost += con.timeMatrix[orders.get(currentNode).orderLocation()][orders.get(nextNode).orderLocation()] + orders.get(currentNode).serviceTime();
//			totalTime += con.timeMatrix[orders.get(currentNode).orderLocation()][orders.get(nextNode).orderLocation()] + orders.get(currentNode).serviceTime();
//			if (currentNode > con.totalCar) {
//			
//				currCapacity += orders.get(nextNode).weight();
//				posCapacity += orders.get(nextNode).posWeight();
//				negCapacity -= orders.get(nextNode).negWeight();
//			}
//
//			if (currentNode < con.totalCar + 1) {
//				currCapacity = orders.get(nextNode).weight();
//				posCapacity = orders.get(nextNode).posWeight();
//				negCapacity = -orders.get(nextNode).negWeight();
//			}
//
//			currentNode = nextNode;
//
//			if (totalTime < orders.get(nextNode).lowerTime())
//				totalTime = orders.get(nextNode).lowerTime();
//			if (currCapacity > vehicles.get(k).capacity() || currCapacity < 0) {
//				globalCost += 1e10;
//			}
//			if (posCapacity > vehicles.get(k).capacity()) {
//				globalCost += 1e10;
//				break;
//			}
//			if (negCapacity > vehicles.get(k).capacity()) {
//				globalCost += 1e10;
//				break;
//			}
//			if (totalTime > orders.get(nextNode).upperTime()) {
//				globalCost += 1e10;
//			}
//		}
//		return globalCost;
//	}

	public List<Order> mergeAll(ArrayList<ArrayList<Order>> route) {
		HashSet<Order> mergeAllNode = new HashSet<Order>();
		for (ArrayList<Order> path : route) {
			if (path.size() < 2)
				continue;

			mergeAllNode.addAll(path.subList(1, path.size() - 1));
		}
		List<Order> updated = deepCopySet(mergeAllNode);
		
		return updated;
	}



	
	public ArrayList<Integer> deepcopyArray(ArrayList<Integer> Route) {
		ArrayList<Integer> copyRoute = new ArrayList<Integer>();
		for (Integer item : Route) {
			copyRoute.add(item);
		}
		return copyRoute;
	}

	public ArrayList<Integer> routeToger(ArrayList<ArrayList<Integer>> staterouteall) {
		ArrayList<Integer> routeTogether = new ArrayList<Integer>();
		for (ArrayList<Integer> route : staterouteall) {
			routeTogether.addAll(singleClone(route.subList(1, route.size() - 1)));
		}
		return routeTogether;
	}

	public List<DirectedEdge> sortByWeight(List<DirectedEdge> toSort) {
		Collections.sort(toSort, new Comparator<DirectedEdge>() {
			public int compare(DirectedEdge c1, DirectedEdge c2) {
				if (c1.weight() > c2.weight())
					return -1;
				if (c1.weight() < c2.weight())
					return 1;
				return 0;
			}
		});
		return toSort;
	}

	public <K extends Comparable, V extends Comparable> Map<K, V> sortByValues(Map<K, V> map) {
		List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();

		for (Map.Entry<K, V> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public int anyItem(ArrayList<Integer> catalogue) {
		Random randomGenerator = new Random();
		int index = randomGenerator.nextInt(catalogue.size());
		int it = catalogue.get(index);
		return it;
	}

	public ArrayList<ArrayList<Integer>> clone(ArrayList<ArrayList<Integer>> initial_route) {
		ArrayList<ArrayList<Integer>> route = new ArrayList<ArrayList<Integer>>();
		int cnt = 0;
		for (ArrayList<Integer> p : initial_route) {
			route.add(new ArrayList<Integer>());
			for (Integer q : p)
				route.get(cnt).add(q);
			cnt++;
		}
		return route;
	}

	public ArrayList<Integer> singleClone(ArrayList<Integer> initial_route) {
		ArrayList<Integer> route = new ArrayList<Integer>();
		for (Integer p : initial_route)
			route.add(p);
		return route;
	}

	public ArrayList<Order> singleCloneOrder(ArrayList<Order> initial_route) {
		ArrayList<Order> route = new ArrayList<Order>();
		for (Order p : initial_route)
			route.add(p);
		return route;
	}
	
	public ArrayList<Integer> singleClone(List<Integer> initial_route) {
		ArrayList<Integer> route = new ArrayList<Integer>();
		for (Integer p : initial_route)
			route.add(p);
		return route;
	}

	public ArrayList<List<Integer>> clone(List<List<Integer>> cycles) {
		ArrayList<List<Integer>> route = new ArrayList<List<Integer>>();
		for (List<Integer> p : cycles)
			route.add(singleClone(p));
		return route;
	}

	public List<ArrayList<Integer>> clone2List(List<ArrayList<Integer>> cycles) {
		List<ArrayList<Integer>> route = new ArrayList<ArrayList<Integer>>();
		for (ArrayList<Integer> p : cycles)
			route.add(singleClone(p));
		return route;
	}

	public ArrayList<double[]> cloneList(ArrayList<double[]> newLambdaList) {
		ArrayList<double[]> lambdaList = new ArrayList<double[]>();
		for (double[] p : newLambdaList)
			lambdaList.add(p);
		return lambdaList;
	}

	public ArrayList<Order> deepCopySet(Set<Order> servedBid) {
		ArrayList<Order> list = new ArrayList<Order>();
		for (Order p : servedBid)
			list.add(p);
		return list;
	}

	public double[] cloneArray(double[] choose) {
		int size = choose.length;
		double[] lambdaList = new double[size];
		for (int i = 0; i < size; i++)
			lambdaList[i] = choose[i];
		return lambdaList;
	}

	public HashMap<Integer, Integer> hashMapClone(HashMap<Integer, Integer> map) {
		HashMap<Integer, Integer> freezeMap = new HashMap<Integer, Integer>();
		for (Integer p : map.keySet())
			freezeMap.put(p, map.get(p));
		return freezeMap;
	}

	public ArrayList<Double> cloneListDouble(ArrayList<Double> choose) {
		ArrayList<Double> lambdaList = new ArrayList<Double>();
		for (int i = 0; i < choose.size(); i++) {
			lambdaList.add(choose.get(i));
		}
		return lambdaList;
	}

	public ArrayList<ArrayList<Double>> cloneDouble(ArrayList<ArrayList<Double>> ri) {
		ArrayList<ArrayList<Double>> route = new ArrayList<ArrayList<Double>>();
		int cnt = 0;
		for (ArrayList<Double> p : route) {
			route.add(new ArrayList<Double>());
			for (Double q : p)
				route.get(cnt).add(q);
			cnt++;
		}
		return route;
	}

	public ArrayList<Double> cloneListDouble(List<Double> subList) {
		ArrayList<Double> route = new ArrayList<Double>();
		int cnt = 0;
		for (Double p : subList) {
			route.add(p);
		}
		return route;
	}

	public Set<Integer> CopySet(Set<Integer> nodeSet) {
		Set<Integer> list = new HashSet<Integer>();
		for (Integer p : nodeSet)
			list.add(p);
		return list;
	}

	public ArrayList<Integer> setClone(Set<Integer> nodeSet) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (Integer p : nodeSet)
			list.add(p);
		return list;
	}

	public ArrayList<List<Integer>> DimensionCopySet(Set<ArrayList<Integer>> finalCycles) {
		ArrayList<List<Integer>> route = new ArrayList<List<Integer>>();
		int cnt = 0;
		for (ArrayList<Integer> p : finalCycles) {
			route.add(new ArrayList<Integer>());
			for (Integer q : p)
				route.get(cnt).add(q);
			cnt++;
		}
		return route;
	}

	public Stack<Integer> CopyStack(Stack<Integer> oldPath) {
		Stack<Integer> path = new Stack<Integer>();
		for (Integer p : oldPath)
			path.push(p);
		return path;
	}

	public int[] cloneSingleArray(int[] data0) {
		int[] data = new int[data0.length];
		for (int i = 0; i < data0.length; i++)
			data[i] = data0[i];
		return data;
	}

	public ArrayList<Integer> stackClone(edu.princeton.cs.algs4.Stack<Integer> path2) {
		ArrayList<Integer> path = new ArrayList<Integer>();
		for (Integer p : path2)
			path.add(p);
		Collections.reverse(path);
		return path;
	}

	public ArrayList<ArrayList<Order>>  cloneVector(ArrayList<ArrayList<Order>> route2) {
		ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>();
		int cnt = 0;
		for (ArrayList<Order> p : route2) {
			route.add(new ArrayList<Order>());
			for (Order q : p)
				route.get(cnt).add(q);
			cnt++;
		}
		return route;
	}

	public Vector<Vector<Integer>> cloneVectorTransfer(Vector<Vector<Integer>> oldRoute, int totalCar,
			int sumAllOrder) {
		Vector<Vector<Integer>> route = new Vector<Vector<Integer>>();
		int cntI = 0;
		int cntJ = 0;
		int zerosBetween = 0;
		for (Vector<Integer> p : oldRoute) {
			route.add(new Vector<Integer>());
			cntJ = 0;
			for (Integer q : p) {
				if (cntJ > 0 && q <= totalCar) {
					zerosBetween++;
					q = sumAllOrder - 1 + zerosBetween;
				}
				route.get(cntI).add(q);
				cntJ++;
			}
			cntI++;
		}
		return route;
	}

	public Vector<Order> cloneSingleVector(List<Order> list) {
		Vector<Order> route = new Vector<Order>();
		for (Order p : list) {
			route.add(p);
		}
		return route;
	}

	public Vector<Vector<Integer>> cloneRouteAll(ArrayList<ArrayList<Integer>> bestRoutes, int stops) {
		Vector<Vector<Integer>> bestroute = new Vector<Vector<Integer>>();
		int k = 0;
		for (ArrayList<Integer> r : bestRoutes) {
			Vector<Integer> single = new Vector<Integer>();
			int cnt = 0;
			for (Integer i : r) {
				if (i == 0 && cnt > 0)
					single.add(stops + 1);
				else
					single.add(i);
				cnt++;
			}
			bestroute.add(single);
			k++;
		}

		return bestroute;
	}

	public int removeRoulwheel(double[] removeWeigt) {
		Random r11 = new Random();
		double rand = r11.nextDouble();
		int M = removeWeigt.length;
		double S = 0;
		for (int index = 0; index < M; index++) {
			S = S + removeWeigt[index];
		}

		double[] probability = new double[M];
		probability[0] = removeWeigt[0] / S;

		int parent1 = 0;
		for (int index = 1; index < M; index++) {
			probability[index] = probability[index - 1] + removeWeigt[index] / S;
		}
		/*************************************************************/
		if (rand < probability[0])
			parent1 = 0;
		for (int index = 1; index < M; index++) {
			if (rand >= probability[index - 1] && rand < probability[index])
				parent1 = index;
		}
		return (parent1);
	}

	public Vector<Integer> reverseSingleVector(Vector<Integer> vector) {
		Vector<Integer> route = new Vector<Integer>();
		for (int i = vector.size() - 1; i > 0; i--) {
			route.add(vector.get(i));
		}
		return route;
	}

	public int removeRoulwheel(ArrayList<Double> removeWeigt) {
		Random r11 = new Random();
		double rand = r11.nextDouble();
		int M = removeWeigt.size();
		double S = 1e-20;
		for (int index = 0; index < M; index++) {
			S = S + removeWeigt.get(index);
		}

		double[] probability = new double[M];
		probability[0] = removeWeigt.get(0) / S;

		int parent1 = 0;
		for (int index = 1; index < M; index++) {
			probability[index] = probability[index - 1] + removeWeigt.get(index) / S;
		}
		/*************************************************************/
		if (rand < probability[0])
			parent1 = 0;
		for (int index = 1; index < M; index++) {
			if (rand >= probability[index - 1] && rand < probability[index])
				parent1 = index;
		}
		return (parent1);
	}

	public Vector<Vector<Integer>> cloneToVector(Vector<HashSet<Integer>> carSet) {
		Vector<Vector<Integer>> tempSet = new Vector<Vector<Integer>>();
		int node = 0;
		for (HashSet<Integer> path : carSet) {
			Iterator it = path.iterator();
			Vector<Integer> newSet = new Vector<Integer>();
			while (it.hasNext()) {// tabuInsert(route, node);
				node = (Integer) it.next();
				newSet.add(node);
			}
			tempSet.add(newSet);
		}
		return tempSet;
	}



	public double calcuTemp(double T0, double newbenfit, double oldbenfit) {
		double probability = Math.exp((oldbenfit - newbenfit) / T0);
		return probability;
	}

	public List<Integer> listClone(List<Integer> path2) {
		List<Integer> path = new ArrayList<Integer>();
		for (Integer p : path2)
			path.add(p);
		return path;
	}

	public Vector<Integer> listToVector(List<Integer> path2) {
		Vector<Integer> path = new Vector<Integer>();
		for (Integer p : path2)
			path.add(p);
		return path;
	}

	public ArrayList<Order> cloneToArray(HashSet<Order> path2) {
		ArrayList<Order> path = new ArrayList<Order>();
		for (Order p : path2)
			path.add(p);
		return path;
	}

	public int maxSize(ArrayList<ArrayList<Integer>> splitPath) {
		int maxSize = 0;
		for (ArrayList<Integer> trip : splitPath) {
			if (trip.size() > maxSize)
				maxSize = trip.size();
		}
		return maxSize;
	}

	public List<Trip> cloneObjectList(List<Trip> newRoute2) {
		List<Trip> newRoute = new ArrayList<Trip> ();
		for (Trip trip : newRoute2) {
			newRoute.add(trip);
		}
		return newRoute;
	}

	public ArrayList<Order> listCloneObject(List<Order> path) {
		ArrayList<Order> path2 = new ArrayList<Order>();
		for (Order p : path)
			path2.add(p);
		return path2;
	}
	
	public ArrayList<Order> arraylistCloneObject(ArrayList<Order> path) {
		ArrayList<Order> path2 = new ArrayList<Order>();
		for (Order p : path)
			path2.add(p);
		return path2;
	}
	
	public HashSet<Order> deepCopySet(ArrayList<Order> orders) {
		HashSet<Order> newOrder = new HashSet<Order> ();
		for(Order p : orders)
			newOrder.add(p);
		return newOrder;
	}


	public ArrayList<Order> arraylistCloneObject(HashSet<Order> path) {
		ArrayList<Order> path2 = new ArrayList<Order>();
		for (Order p : path)
			path2.add(p);
		return path2;
	}


}
