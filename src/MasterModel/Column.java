package MasterModel;

import java.util.ArrayList;

import Data.Order;

public class Column {
	ArrayList<Order> path;
	double cost;

	public double getcost() {
		// TODO Auto-generated method stub
		return cost;
	}

	public Column(ArrayList<Order> vector, double cost) {
		this.path = vector;
		this.cost = cost;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Column clm = (Column) obj;
		for (int i = 0; i < clm.path.size(); i++) {
			if (Math.abs(this.path.get(i).id - clm.path.get(i).id) > 1e-5)
				return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		ArrayList<Integer> newHash = new ArrayList<Integer>();
		for (Order node : path)
			newHash.add(node.id);

		return newHash.hashCode();
	}
}
