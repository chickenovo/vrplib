
package MasterModel;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import Data.ArrayIndexComparator;
import Data.BasicData;
import Data.Constants;
import Data.CriticalFeature;
import Data.DummyOrder;
import Data.KMedoids;
import Data.Location;
import Data.Lsp;

import Data.Order;
import Data.Parameter;
import Data.PathSolution;
import Data.TimeInterval;
import Data.Trip;
import Data.Vehicle;
import DynamicEngineRelated.DynamicBasicData;

//import ilog.cplex.IloCplex;

public class LspMixedALNS {
	HashSet<Column> routesColumn = new HashSet<Column>();
	double fitnessParaThreshold = 1.1;
	int maxViolate = 0;
	int travelTimeParaThreshold = 20;
	double smoothThreshold = 6.0;
	double similarityThreshold = 6.0;
	double scalingFactor = 2.0;
	// int paramALNSItr = 8;
	boolean lunchFeasible = false;
	boolean lunchFeasibleCheckNeed = true;
	// vehicle end at 6:00
	int vioVolume = 0;
	HashMap<Integer, Double> forWardVolume = new HashMap<Integer, Double>();
	HashMap<Integer, Integer> dummyOrderPosIndex = new HashMap<Integer, Integer>();

	boolean needSmooth = false;
	boolean finalOptimizeFlag = false;
	boolean volumeFeasible = false;
	ConcurrentHashMap<Integer, Vector<TimeInterval>> timeIntervalsList = new ConcurrentHashMap<Integer, Vector<TimeInterval>>();
	double threshold;
	double timeUsed = 0;
	long start = 0;
	int totalSize = 0;
	int indexNextDepot = 0;
	double maxWait = 0;
	int maxWaitPos = 0;
	int totalTrip = 0;
	int nextTripSize = 0;
	public double travelDistance = 0;
	public List<List<Order>> allPaths = new ArrayList<List<Order>>();
	public HashMap<Integer, Vector<Vector<Integer>>> completed = new HashMap<Integer, Vector<Vector<Integer>>>();
	public HashMap<Vector<Integer>, Integer> initialRoutes = new HashMap<Vector<Integer>, Integer>();
	public ArrayList<ArrayList<Order>> bestUsedRoute = new ArrayList<ArrayList<Order>>();
	public ArrayList<ArrayList<Order>> feasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
	public ArrayList<ArrayList<Order>> noTimeVioRoute = new ArrayList<ArrayList<Order>>();
	public static ArrayList<ArrayList<Order>> allBestfeasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
	public ArrayList<ArrayList<Order>> allBestnoTimeVioRoute = new ArrayList<ArrayList<Order>>();
	public ArrayList<ArrayList<Order>> allBestbestUsedRoute = new ArrayList<ArrayList<Order>>();
	public int startTimeFirstOrder = 0;
	ArrayList<Trip> currentSolutionMultiTrips = new ArrayList<Trip>();
	HashSet<Integer> notAllowedVehicle = new HashSet<Integer>();
	HashMap<Integer, int[]> timeRecord = new HashMap<Integer, int[]>();
	public int[] startServTime;
	public boolean check = true;
	public int[] arriveTime;
	public int tabuCnt;
	public int removeSize = 6;
	public int insertSize = 4;
	public boolean noNeedMore = false;
	public int loopCnt = 1;
	public double[] cntRemove;
	public double[] scoreRemove;
	public double[] removeWeigt;
	public int globalCnt = 0;
	public double[] cntInsert;
	public double[] scoreInsert;
	public double[] insertWeigt;
	public int totalDepot;
	public int insertChoose;
	public Random rand = new Random();
	public double T0;
	public int oldHash;
	public double bestBenifit;
	public double noTimeVioBenifit = 1e8;
	public HashSet<Integer> noNeedCheckMall;
	public double toursCost = -1;
	public boolean serveAll;
	public Vector<Integer> feaAndPos;

	public ArrayList<Vehicle> vehicles = new ArrayList<>();
	public LinkedHashSet<Integer> tabuList = new LinkedHashSet<>();
	public ArrayList<Order> orders;
	public Location[] locations;
	public double totalTime = 1e6;
	public double travelTime = 0;
	public double[] postPone;
	public double[][] needMiddleAdd;
	public double travelServe = 0;
	CommonFunction cf;
	Random r = new Random();
	double feasibenefitWhole = 1e6;
	boolean canCallMIP = true;
	double[] tempTime = new double[2];
	HashMap<Integer, Integer> carIndex = new HashMap<Integer, Integer>();
	ArrayList<Lsp> lsps;
	BasicData d;
	HashMap<Integer, double[]> StoredCriticalPoint = new HashMap<Integer, double[]>();
	HashMap<Integer, CriticalFeature> StoredMultiCriticalPoint = new HashMap<Integer, CriticalFeature>();
	ArrayList<Integer> usedVehicle = new ArrayList<Integer>();
	double allBestfeasibenefitWhole = 9e10;
	double allBestNoTimeVioBenifit = 9e10;
	public double allBestbestBenifit = 9e10;
	public double maxUtilize = 0;
	public double utilize = 0;
	public double globalEndTime = 0;
	public double maxTime = 0;

	public double maxTripDis = 0;
	public int maxCnt = 0;
	public ArrayList<ArrayList<Order>> orginRoute;
	Constants con;

	public int k = 0;
	public double HARD_CODED_FACTOR_TO_DECIDE_HALF_TRIP_USED = 0.8;

	// @initialize function for dynamic engine
	public LspMixedALNS(DynamicBasicData d) throws FileNotFoundException, InterruptedException, ExecutionException {
		this.d = d;
		this.con = d.con;
		this.totalSize = con.stops;
		this.vehicles = d.vehicles;
		this.lsps = d.lsps;
		this.orders = d.orders;
		this.locations = d.locations;
		start = System.currentTimeMillis();
		timeUsed = 0;
		// this.k = kValue();
		cf = new CommonFunction();
	}

	// @main function
	public LspMixedALNS(BasicData d, ArrayList<ArrayList<Order>> allRoute)
			throws FileNotFoundException, InterruptedException, ExecutionException {
		this.totalSize = d.con.stops - d.impossibleServeOrder.size();
		this.d = d;
		this.vehicles = d.vehicles;
		this.lsps = d.lsps;
		this.orders = d.orders;
		this.locations = d.locations;
		this.con = d.con;
		start = System.currentTimeMillis();
		timeUsed = 0;
		// this.k = kValue();
		cf = new CommonFunction();
		ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>(allRoute);
		initialStatic();
		noNeedMore = false;
		tabuCnt = (int) (7.5 * Math.log(con.totalCar()));
		allBestfeasibenefitWhole = 9e10;
		allBestNoTimeVioBenifit = 9e10;
		allBestbestBenifit = 9e10;

		needSmooth = false;
		finalOptimizeFlag = false;
		route = new ArrayList<ArrayList<Order>>();
		for (Integer car : d.trips.keySet()) {
			Collections.sort(d.trips.get(car), new Comparator<Trip>() {
				@Override
				public int compare(Trip o1, Trip o2) {
					return orders.get(o1.path.get(0)).lowerTime - orders.get(o2.path.get(0)).lowerTime;
				}
			});
		}

		if (d.totalSize(route) < 1) {
			route = initilize(route);
		}
		int cnt = 1;
		for (int i = 0; i < d.totalCar; i++) {
			d.con.lspVechiclesHiddenMap.put(d.vehicles.get(i).depot.id, cnt);
			cnt++;
		}
		bestUsedRoute = new ArrayList<ArrayList<Order>>();
		feasiusedRouteWhole = new ArrayList<ArrayList<Order>>();
		noTimeVioRoute = new ArrayList<ArrayList<Order>>();

		for (Location l : locations) {
			if (l.close >= 1439) {
				System.out.println("midnight location id " + l.id + ". Setting closing time " + l.close);
			}
		}

		for (int i = 0; i < d.con.paramALNSItr; i++) {
			feasibenefitWhole = 1e9;
			noTimeVioBenifit = 1e9;
			bestBenifit = 1e9;
			if (i == d.con.paramALNSItr % 2
					&& (allBestfeasiusedRouteWhole == null || allBestfeasiusedRouteWhole.size() < 1)) {
				lsps.get(0).setMaxDrop((int) (lsps.get(0).maxDrop + Math.ceil(0.1 * lsps.get(0).maxDrop)));
				lsps.get(0).increasedDrops = true;
			}
			alnsSearch(cf.cloneVector(route), i);
		}
		System.out.println("before final all best size " + allBestfeasiusedRouteWhole.size());
		globalCnt = 25000;
		// finalAllBest();
	}

	private void addDepot(ArrayList<ArrayList<Order>> route) {
		boolean feasible = true;
		boolean allFeasible = false;
		int cnt = 0;
		double orginalResult = 1e10;
		double bestResult = 1e10;
		Order insertNode = null;
		int bestVioVolume = 0;
		int insertPos = 0;
		while (!allFeasible && cnt < 2) {
			allFeasible = true;
			for (int k = 0; k < route.size(); k++) {
				if (calculateNumberTrips(route.get(k)) >= 4)
					continue;

				feasible = calculateMultiDepotCapacityConstraint(new PathSolution(route.get(k), k), 0);
				if (!feasible) {
					allFeasible = false;
					bestResult = calcuResult(new PathSolution(route.get(k), k), con.yita2, 0, 0, 0);
					bestVioVolume = vioVolume;
					insertNode = null;
					insertPos = -1;
					for (Order node : vehicles.get(k).vehicleStartMiddleOrders) {
						// System.err.println("addDepot\t" + node.id);
						for (int j = 0; j < route.get(k).size() - 2; j++) {
							if (calculateNumberTrips(route.get(k)) >= 4)
								break;
							if (node.id == route.get(k).get(0).id
									&& (route.get(k).get(0).serviceTime < 2 || route.get(k).get(1).serviceTime < 2))
								continue;
							ArrayList<Order> newPath = cf.arraylistCloneObject(route.get(k));
							newPath.add(j + 1, node);
							orginalResult = calcuResult(new PathSolution(newPath, k), con.yita2, 0, 0, 0);
							bestVioVolume = vioVolume;
							// if ((vioVolume==0&&orginalResult <
							// bestResult)||vioVolume<bestVioVolume) {
							if (orginalResult < bestResult) {
								bestResult = orginalResult;
								insertNode = node;
								insertPos = j + 1;
							}
						}
					}

					if (insertNode != null) {
						route.get(k).add(insertPos, insertNode);
					}
				}
			}
			cnt++;
		}
	}

	private int calculateNumberTrips(ArrayList<Order> path) {
		int numberTrips = 0;
		boolean before = true;
		boolean after = true;
		Order preNode = null;
		for (int i = 0; i < path.size() - 1; i++) {
			before = true;
			after = true;
			if (d.startEndNodes.contains(path.get(i)))
				before = false;
			if (d.startEndNodes.contains(path.get(i + 1)))
				after = false;
			if (before != after)
				numberTrips += 1;
			else if (path.get(i).id == path.get(i + 1).id)
				numberTrips += 2;
		}
		return numberTrips / 2;
	}

	private void initialStatic() {
		d.staticNodes = new ArrayList<Order>();
	}

	// main algorithm
	public void alnsSearch(ArrayList<ArrayList<Order>> route, int iter)
			throws FileNotFoundException, InterruptedException, ExecutionException {
		bestBenifit = 1e9;
		initial_2cnt();
		setWeight();
		con.initial_yita();
		threshold = 0.501;
		ArrayList<ArrayList<Order>> recRouteall = new ArrayList<ArrayList<Order>>();
		List<Integer> vehicleSet = new ArrayList<Integer>();
		for (int i = 0; i < con.totalCar(); i++) {
			vehicleSet.add(i);
		}

		Random rand = new Random();
		int stop = 0;
		double feasibenefit = 0;

		double newbenfit = 0;
		double newFeaBenfit = 0;
		double benefi = 0;
		int nextNode = 0;
		maxCnt = 15000;// d.iterations;
		HashSet<Integer> storedHashSet = new HashSet<Integer>();
		con.setInitialYita1(1);
		con.setInitialYita2(1);
		con.setInitialYita3(1);
		check = true;
		newbenfit = calcuNewBenfit(route) + 1000 * (totalSize - serveAll(route));

		// System.out.println("calcuNewBenfit "+calcuNewBenfit(route)+ " " + (totalSize
		// - alns.serveAll(route))+" "+human(route));

		int kk = 0;
		feasiusedRouteWhole.clear();
		bestUsedRoute = cf.cloneVector(route);
		bestBenifit = newbenfit;

		boolean needCheckCP = true;
		// System.out.println("Serve check "+totalSize+" "+alns.serveAll(route));
		if (check && (totalSize == serveAll(route))) {
			feasibenefitWhole = newbenfit + (totalSize - serveAll(route)) * con.unserved_price;
			feasiusedRouteWhole = cf.cloneVector(route);
		}
		//
		int removeChoose = 0;
		int k = 0;
		double oldBenfit = feasibenefitWhole;
		double probability = 0.5;
		T0 = -Math.abs(oldBenfit) / Math.log(probability);
		oldHash = 0;
		// recRouteall = SortCopy.cloneVector(route);
		startServTime = new int[con.stops() + con.totalCar() * 2 + 1];
		double preTimeUsed = 0;

		long last = System.currentTimeMillis();// maxCnt
		globalCnt = 0;
		int middTime = 0;
		// int initialObjFlag = con.objFlag;
		// con.setObjFlag(0);
		// System.out.println("before while loop: "+humanLoc(bestUsedRoute));
		while (globalCnt < d.iterations && timeUsed < d.timeLimit) {

			ArrayList<Order> newPath = new ArrayList<Order>();
			for (ArrayList<Order> path : bestUsedRoute) {
				for (Order o : path) {
					// if(o.orderLocation() == 16)
					// System.out.println("in while loop: "+humanLoc(bestUsedRoute));
				}
			}
			if (globalCnt == maxCnt / 2) {
				// con.setObjFlag(initialObjFlag);
				insertSize = 4;
				initial_2cnt();
				setWeight();
				insertWeigt[0] = 0.25;
				insertWeigt[1] = 0.25;
				insertWeigt[2] = 0.25;
				insertWeigt[3] = 0.25;
				if (feasiusedRouteWhole != null) {
					feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
				}
				if (bestUsedRoute != null) {
					bestBenifit = calcuNewBenfit(bestUsedRoute);
				}
				if (noTimeVioRoute != null) {
					noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
				}
				newbenfit = calcuNewBenfit(route);
			}
			needCheckCP = true;
			globalCnt++;
			preTimeUsed = timeUsed;
			removeChoose = cf.removeRoulwheel(removeWeigt);
			List<Order> removeSet = remove(route, removeChoose);

			cntRemove[removeChoose] += 1.0D;

			insertChoose = cf.removeRoulwheel(insertWeigt);
			if (removeSet.size() < 1)
				continue;
			cntInsert[insertChoose] += 1.0D;

//			if ((globalCnt + 1) % 20 == 0) {
//				k = 0;
//				for (ArrayList<Order> path : route) {
//					newPath = twoOptAlternate(path, k);
//					route.set(k, newPath);
//					k++;
//				}
//			} else {
			insert(route, removeSet);
//			}

			// if (globalCnt % 10000 == 0)
			// route = longTripsMIP();

			last = System.currentTimeMillis();
			timeUsed = TimeUnit.MILLISECONDS.toSeconds(last - start) / 60.0;
			check = true;
			newbenfit = calcuNewBenfit(route) + 1000 * (totalSize - serveAll(route));

			if (newbenfit > 0 && newbenfit < bestBenifit) {
				bestBenifit = newbenfit;
				bestUsedRoute = cf.cloneVector(route);
				//
			}

			if (check && newbenfit < noTimeVioBenifit && (totalSize == serveAll(route))) {
				scoreRemove[removeChoose] += con.x()[5] * 0.8;
				scoreInsert[insertChoose] += con.x()[5] * 0.8;
				noTimeVioBenifit = newbenfit;
				noTimeVioRoute = cf.cloneVector(route);
			}

			if (check) {
				scoreRemove[removeChoose] += con.x()[5] * 1.0;
				scoreInsert[insertChoose] += con.x()[5] * 1.0;
				if (!storedHashSet.contains(route.hashCode())) {
					noNeedMore = true;
					// maxCnt = 15000;
					con.setVioCheck1(true);
					con.setVioCheck2(true);

					if (newFeaBenfit < feasibenefitWhole) {
						feasibenefitWhole = newFeaBenfit;
						feasiusedRouteWhole = cf.cloneVector(route);

						con.decreaseYita3();
						scoreRemove[removeChoose] += con.x()[5] * 1.2;
						scoreInsert[insertChoose] += con.x()[5] * 1.2;
					}
					if (newFeaBenfit < bestBenifit && (totalSize == serveAll(route))) {
						bestBenifit = newbenfit;
						bestUsedRoute = cf.cloneVector(route);

					}

					storedHashSet.add(feasiusedRouteWhole.hashCode());
					if (storedHashSet.size() > 100)
						storedHashSet = new HashSet<Integer>();
				}
			}
			con.resetyita(con.vioCheck1(), con.vioCheck2());
			recRouteall = cf.cloneVector(route);
			if (newbenfit < oldBenfit) {
				stop = 0;
				scoreRemove[removeChoose] += con.x()[6] * 0.4;
				scoreInsert[insertChoose] += con.x()[6] * 0.4;
				oldBenfit = newbenfit;
			} else {
				stop++;
				probability = calcuTemp(con.x()[4], newbenfit, oldBenfit);
				double randprob = rand.nextDouble();

				if (randprob <= probability) {
					scoreRemove[removeChoose] += con.x()[7] * 0.2;
					scoreInsert[insertChoose] += con.x()[7] * 0.2;
					oldBenfit = newbenfit;
				} else
					route = cf.cloneVector(recRouteall);
			}
			if ((globalCnt + 1) % 280 == 0) {
				addDepot(route);
				// System.err.println("after\t"+human(route));
				// too many depot, remove them
				// removeInsertDepot(route, example);
				for (int i = 0; i < removeSize; i++) {
					removeWeigt[i] = (0.6 * removeWeigt[i] + 0.4 * scoreRemove[i] / cntRemove[i]);
				}
				for (int i = 0; i < insertSize; i++) {
					insertWeigt[i] = (0.6 * insertWeigt[i] + 0.4 * scoreInsert[i] / cntInsert[i]);
				}
				scoreInsert = new double[insertSize];
				scoreRemove = new double[removeSize];
				cntRemove = new double[removeSize];
				cntInsert = new double[insertSize];
				initial_2cnt();
			}
			if ((globalCnt + 1) % 290 == 0)
				removeDepot(route, false);

			tabuList.clear();
			for (ArrayList<Order> path : route)
				tabuList.add(path.hashCode());
			updateTabuList();
			if (globalCnt == 10000) {
				con.setTimeVioCost();
				newbenfit = calcuNewBenfit(route);
				bestBenifit = calcuNewBenfit(bestUsedRoute);
			}
			// while (tabuList.size() > con.totalCar*3) {
			// Iterator<Integer> hscode = tabuList.iterator();
			// Integer next = hscode.next();
			// hscode.remove();
			// }

			if (globalCnt > 15000 && stop > 5000)
				break;
		}

		threshold = 0.1;
		withSmoothAndWithoutSmooth(iter);
	}

	// @initialize solution, use KMedoids cluster
	int serveAll(ArrayList<ArrayList<Order>> route) {
		HashSet<Integer> howMany = new HashSet<Integer>();
		for (ArrayList<Order> path : route) {
			for (Order node : path) {
				if (orders.contains(node) && !d.startEndNodes.contains(node)) {
					if (d.dummyOrderMap != null && !d.dummyOrderMap.values().contains(node))
						howMany.add(node.id);
					else if (d.dummyOrderMap == null || d.dummyOrderMap.size() < 1)
						howMany.add(node.id);
				}
			}
		}
		// System.out.println("serveAll size: "+howMany.size()+" "+orders.size());
		return howMany.size();
	}

	private void doubleCheckDepot(ArrayList<ArrayList<Order>> route) {
		for (int i = 0; i < route.size(); i++) {
			if (route.get(i).get(0).id != d.vehicles.get(i).endDepot.id)
				route.get(i).add(0, d.vehicles.get(i).endDepot);
			else if (!d.startEndNodes.contains(route.get(i).get(1)))
				route.get(i).add(0, d.vehicles.get(i).middleDepots.get(0));
		}
	}

	/**
	 * Find the lowerbound of vehicles = max[ (number of orders)/(2*max(orders per
	 * vehicle)), (order capacity)/(2*max(cap per vehicle))] 2 because 2 trips per
	 * day Number of orders: find sum of all merged orders max(orders per vehicles):
	 * this is pre-defined and can be changed in FileManager when read from
	 * BR1_*input.txt order capacity: sum of capacity of merged orders max(cap per
	 * vehicle): maximum of capacity per vehicle
	 * 
	 * @return k
	 */
	private int kValue() {

		// Find MAX_ORDERS_PER_VEHICLE
		int MAX_ORDERS_PER_VEHICLE = this.lsps.get(0).getMaxDrop();

		// Find sum of all merged orders
		int numOfOrders = this.orders.size();

		// Find sum of capacity of merged orders
		double totalCapacityOfMergedOrders = 0.0;
		for (Order order : this.orders) {
			if (order != null)
				totalCapacityOfMergedOrders += order.volume;
		}

		// Find the maximum capacity of all vehicles
		Comparator<Vehicle> cmpCap = new Comparator<Vehicle>() {
			@Override
			public int compare(Vehicle v1, Vehicle v2) {
				return Double.valueOf(v1.capacity).compareTo(Double.valueOf(v2.capacity));
			}
		};
		double maxCapPerVehicle = Collections.max(this.vehicles, cmpCap).capacity;

		int metric1 = (int) Math.ceil((double) numOfOrders / (MAX_ORDERS_PER_VEHICLE * 2));
		int metric2 = (int) Math.ceil((double) totalCapacityOfMergedOrders / (maxCapPerVehicle * 2));

		System.out.println(numOfOrders + " " + MAX_ORDERS_PER_VEHICLE + " " + totalCapacityOfMergedOrders + " "
				+ maxCapPerVehicle);
		System.out.println(metric1 + " " + metric2 + " " + this.vehicles.size());

		return Math.min(Math.max(metric1, metric2), this.vehicles.size()); // min outside is to protect if the max
																			// inside bigger than the number of vehicles
																			// LSP gives to us

	}

	// post process for reducing number of vehicles
	// sort vehicle according to the number of orders
	private void sortVehicles(ArrayList<ArrayList<Order>> route) {
		// sort vehicle according to the capacity
		Collections.sort(route, (Comparator<ArrayList<Order>>) (p1, p2) -> {
			if (p1.size() == p2.size())
				return 0;
			return p1.size() < p2.size() ? -1 : 1;
		});
	}

	// @post process
	private void withSmoothAndWithoutSmooth(int iter) {
		needSmooth = false;
		finalOptimizeFlag = true;
		callPostProcess();
		needSmooth = true;
		callPostProcessOnlyIJ(iter);
	}

	// @post process for smoothness and similarity
	private void callPostProcessOnlyIJ(int iter) {
		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
			// System.out.println("in call post process feasiusedRouteWhole" +
			// feasiusedRouteWhole.size());
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(feasiusedRouteWhole);
			reOptimizeSmallSet(newRoute, iter);
			check = true;
			calcuNewBenfit(newRoute);
			if (check) {
				feasiusedRouteWhole = cf.cloneVector(newRoute);
				feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
			}
		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
			// System.out.println("in call post process noTimeVioRoute" +
			// noTimeVioRoute.size());
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(noTimeVioRoute);
			reOptimizeSmallSet(newRoute, iter);
			check = true;
			calcuNewBenfit(newRoute);
			if (check) {
				noTimeVioRoute = cf.cloneVector(newRoute);
				noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
			}

		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
			bestUsedRoute = cf.cloneVector(reOptimizeSmallSet(bestUsedRoute, iter));
			bestBenifit = calcuNewBenfit(bestUsedRoute);
		}
	}

	// @post process for smoothness and similarity
	private ArrayList<ArrayList<Order>> reOptimizeSmallSet(ArrayList<ArrayList<Order>> route, int iter) {
		removeDepot(route, true);
		// System.out.println("post process before " + humanLoc(route));
		if (route != null && route.size() > 0) {
			route = smoothMultiVehicleInsertion(route);
			// route = smoothFinalRemoveEachOneInsertSameTrip(route);
			route = smoothBetweenTrip(route);
			route = smoothFinalSwap(route);
			// Reducing Vehicles
			// if (kValue() < route.size()) {
			// if(iter == 9) {
			// route = mergeTwoPossibleVehicles(route);
			// System.out.println("route size after checking k value " + route.size());
			// System.out.println("before sorting " + human(route));
			// sortVehicles(route);
			// System.out.println("after sorting" + human(route));
			//
			// route = assignToDifferentVehicles(route);
			// System.out.println("After reassigning " + human(route));
			// }

		}
		// System.out.println("post process after " + route.size() +
		// "\t"+humanLoc(route));
		return route;
	}

	private ArrayList<ArrayList<Order>> mergeTwoPossibleVehicles(ArrayList<ArrayList<Order>> route) {
		// First is to filter all the vehicles has half trip in a day
		ArrayList<ArrayList<Order>> possibleMergedRoute = new ArrayList<ArrayList<Order>>();
		ArrayList<Integer> possibleMergedRouteId = new ArrayList<Integer>();

		int carId = 0;
		Order nodeI, nodeJ, nodeK;
		int totalTime = 0;
		int nextTime = 0;
		int arriveTime = 0;
		int tempWait = 0;
		int sumWait = 0;
		int beginTime = 0;
		int tempNodeServiceTime = 0;
		int tempNextNodeServiceTime = 0;

		System.out.println("Current route :" + human(route));
		System.out.println("Current cost: " + calcuNewBenfitPostProc(route));

		for (ArrayList<Order> path : route) { // This loop through each route (each route corresponding to 1 vehicle)

			int size = path.size();
			carId++;
			nodeI = path.get(0);
			nodeJ = path.get(1);

			totalTime = (int) (Math.max(nodeI.lowerTime(), this.vehicles.get(carId - 1).truckLowerTime()));
			beginTime = totalTime;

			nextTime = totalTime + nodeI.serviceTime;

			if (nodeI.orderLocation != nodeJ.orderLocation)
				nextTime += con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation];

			arriveTime = nextTime;

			if (con.lsps.get(vehicles.get(carId - 1).originLSP).lunchLocationFlag && !con.startEndNodes.contains(nodeJ)
					&& nextTime + nodeJ.serviceTime > locations[nodeJ.orderLocation()].lunchStart
					&& nextTime < locations[nodeJ.orderLocation()].lunchEnd) {
				nextTime = locations[nodeJ.orderLocation()].lunchEnd;
			}

			tempWait = nextTime
					- (totalTime + con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation] + nodeI.serviceTime);

			tempWait += Math.max(0, nodeJ.lowerTime - nextTime);
			nextTime = Math.max(nextTime, nodeJ.lowerTime);
			sumWait += tempWait;

			for (int i = 1; i < size - 1; i++) {
				nodeI = path.get(i - 1);
				nodeJ = path.get(i);
				nodeK = path.get(i + 1);
				// System.out.println(nodeJ.id+"\t"+nodeJ.upperTime);
				tempNodeServiceTime = 0;
				tempNextNodeServiceTime = 0;
				if (nodeJ.orderLocation != nodeI.orderLocation)
					tempNodeServiceTime = nodeJ.serviceTime;
				nextTime = nextTime + tempNodeServiceTime + d.con.timeMatrix[nodeJ.orderLocation][nodeK.orderLocation];

				arriveTime = nextTime;
				if (nodeJ.orderLocation != nodeK.orderLocation)
					tempNextNodeServiceTime = nodeK.serviceTime;

				if (con.lsps.get(vehicles.get(carId - 1).originLSP).lunchLocationFlag
						&& !con.startEndNodes.contains(nodeK)
						&& nextTime + tempNextNodeServiceTime > locations[nodeJ.orderLocation()].lunchStart
						&& nextTime < locations[nodeJ.orderLocation()].lunchEnd) {
					nextTime = locations[nodeJ.orderLocation()].lunchEnd;
				}

				nextTime = Math.max(nodeK.lowerTime, nextTime);

				tempWait = nextTime
						- (totalTime + con.timeMatrix[nodeJ.orderLocation][nodeK.orderLocation] + tempNodeServiceTime);

				if (i == size - 2)
					tempWait = 0;
				sumWait += tempWait;
			}
			nodeI = nodeJ;
			nodeJ = path.get(size - 1);
			totalTime += con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation] + tempNextNodeServiceTime;

			int truckTime = this.vehicles.get(carId - 1).truckUpperTime - this.vehicles.get(carId - 1).truckLowerTime;
			int orderTime = totalTime - beginTime;
			// System.out.println("Total time 1 for vehicle" + carId + ": " + totalTime + ".
			// Total time 2: " + orderTime + ". Truck time: " + truckTime);

			if ((orderTime <= (truckTime * HARD_CODED_FACTOR_TO_DECIDE_HALF_TRIP_USED))) { // Currently, I limit this to
																							// 2 vehicles only
				// System.out.println("** Total time 1 for vehicle" + carId + ": " + totalTime +
				// ". Total time 2: " + orderTime + ". Truck time: " + truckTime);
				possibleMergedRoute.add(cf.arraylistCloneObject(path));
				possibleMergedRouteId.add(route.indexOf(path));
			}
		}

		// possibleMergedRoute contains half-day trip
		ArrayList<ArrayList<Order>> bestRoute = cf.cloneVector(route);
		ArrayList<ArrayList<Order>> currentRoute = cf.cloneVector(route);
		ArrayList<ArrayList<Order>> tempRoute;

		ArrayList<int[]> trackInsertArray;
		double bestCost = calcuNewBenfitPostProc(bestRoute);

		// Here is the logic:
		// Loop through each path and select that as a newPath, put it into newRoute
		// 'cuz multiVehInsert use List<List<Order>>
		// Loop through each path and select the path that has the ID that is not the
		// same as newPath, called it pathNeedRemoved
		// Reorder the orders in pathNeedRemoved into newPath
		// If there is orders left in pathNeedRemoved, then reorder the orders in all
		// paths of routes, where routes are adjusted to include the newPath
		// If there is still orders left, discard this new reorder
		// Loop until finding the best one

		System.out.println("Possible Merged route: " + human(possibleMergedRoute));
		System.out.println(possibleMergedRouteId);

		ArrayList<ArrayList<Order>> trackingPossibleMergedRoute = cf.cloneVector(possibleMergedRoute);

		ArrayList<Integer> trackingPossibleMergedRouteId = (ArrayList<Integer>) possibleMergedRouteId.clone();

		while (possibleMergedRoute.size() >= 2) {
			// if (possibleMergedRoute.size() >= 2) {

			for (int i = 0; i < possibleMergedRoute.size(); i++) { // we loop through each path to choose one best to
																	// merge to
				for (int j = 0; j < possibleMergedRoute.size(); j++) {
					if (j != i) {

						ArrayList<Order> pathNeedRemoved = cf.arraylistCloneObject(possibleMergedRoute.get(j));
						pathNeedRemoved.removeAll(d.startEndNodes);
						ArrayList<Order> trackingPathNeedRemoved = cf.arraylistCloneObject(pathNeedRemoved);

						// System.out.println("-----------------------------");
						// System.out.println("PathNeedRemoved: " + human2(pathNeedRemoved));
						// System.out.println("PathNeedMerged: " + human2(possibleMergedRoute.get(i)));

						// 3 tracking variables: currenRoute is the route before merging; tempRoute is
						// currentRoute + new order position if yes; bestLocalRoute is the best route,
						// this variable is used only for each path only
						/*
						 * for each order: while we can find the insertion position of currenrRoute
						 * (insertArray[1] > 0)
						 * 
						 * set tempRoute = currentRoute if currentRoute contains this order remove this
						 * order from tempRoute set tempRoute to include the new insert position of this
						 * order if obj value of tempRoute is smaller than 1.1*currentCost and
						 * 1.1*bestCost: remove the old order position, if any, in the currentRoute and
						 * newRoute adjust currentRoute and newRoute to include this new order position
						 */
						ArrayList<ArrayList<Order>> localBestRoute = cf.cloneVector(route);

						for (Order order : pathNeedRemoved) {
							trackInsertArray = new ArrayList<int[]>();

							ArrayList<Order> pathNeedMerged = localBestRoute.get(possibleMergedRouteId.get(i));
							ArrayList<ArrayList<Order>> routeNeedMerged = new ArrayList<ArrayList<Order>>();
							routeNeedMerged.add(pathNeedMerged);
							double localBestRouteCost = fitnessParaThreshold * calcuNewBenfitPostProc(localBestRoute);
							while (true) {

								int insertArray[] = multiVehiclesInsertionVersion2(routeNeedMerged, order,
										trackInsertArray);

								if (insertArray[1] < 0) // They can't be merged
									break;
								else {
									trackInsertArray.add(insertArray);
									// System.out.print("trackingInsertArray: ");
									// for (int[] ahihi : trackInsertArray)
									// System.out.print(Arrays.toString(ahihi));
									// System.out.println("");

									if (insertArray[1] == currentRoute.get(possibleMergedRouteId.get(i)).size())
										continue;
									tempRoute = cf.cloneVector(currentRoute);
									// System.out.println("TempRoute1 before add/remove: " + human(tempRoute));

									tempRoute.get(possibleMergedRouteId.get(i)).add(insertArray[1], order);
									tempRoute.get(possibleMergedRouteId.get(j)).remove(order);
									// System.out.println("TempRoute1 after add/remove: " + human(tempRoute));
									// System.out.println(
									// "TempRoute1 Cost after add/remove: " + calcuNewBenfitPostProc(tempRoute));
									// System.out.println("bestCost Cost: " + bestCost + ", localBestRoute Cost: "
									// + calcuNewBenfitPostProc(localBestRoute));
									if ((calcuNewBenfitPostProc(tempRoute) < (fitnessParaThreshold * bestCost))
											&& (calcuNewBenfitPostProc(tempRoute) <= localBestRouteCost)) {
										localBestRoute = tempRoute;
										localBestRouteCost = calcuNewBenfitPostProc(localBestRoute);
										trackingPathNeedRemoved.remove(order);
									}

								}

								// System.out.println(
								// "TrackingPathNeedRemoved in iteration 1: " +
								// human2(trackingPathNeedRemoved));
								// System.out.println("LocalBestRoute in iteration 1: " +
								// human(localBestRoute));
								// System.out.println("LocalBestRoute Cost in iteration 1: "
								// + calcuNewBenfitPostProc(localBestRoute));
							}
						}

						System.out.println("Testing AFTER");

						System.out.println("TrackingPathNeedRemoved after 1: " + human2(trackingPathNeedRemoved));
						System.out.println("Current route after 1: " + human(currentRoute));
						System.out.println("Current cost after 1: " + calcuNewBenfitPostProc(currentRoute));
						System.out.println("LocalBestRoute in iteration 1: " + human(localBestRoute));
						System.out.println(
								"LocalBestRoute Cost in iteration 1: " + calcuNewBenfitPostProc(localBestRoute));

						pathNeedRemoved = cf.arraylistCloneObject(trackingPathNeedRemoved);
						currentRoute = cf.cloneVector(localBestRoute);
						// 2 tracking variables: currentRoute updates the path in newRoute; tempRoute is
						// for temporarily assess the new order insertion
						/*
						 * for each order: while we can find the insertion position (insertArray[1] > 0)
						 * set tempRoute = currentRoute if currentOrder has order remove this order from
						 * tempRoute set tempRoute to include the new insert position of this order if
						 * obj value of tempRoute is smaller than 1.1*currentCost and 1.1*bestCost:
						 * remove the old order position, if any, in the currentRoute adjust
						 * currentRoute to include this new order position
						 */
						if (pathNeedRemoved.size() > 0) { // There is still some orders in the old vehicle, try to
															// distribute to other vehicles
							for (Order order : pathNeedRemoved) {
								trackInsertArray = new ArrayList<int[]>();
								double localBestRouteCost = fitnessParaThreshold
										* calcuNewBenfitPostProc(localBestRoute);
								while (true) {
									int insertArray[] = multiVehiclesInsertionVersion2(localBestRoute, order,
											trackInsertArray);

									if (insertArray[1] < 0)
										break;
									else {
										trackInsertArray.add(insertArray);
										if (insertArray[1] == currentRoute.get(insertArray[0]).size())
											continue;

										// System.out.print("trackingInsertArray: ");
										// for (int[] ahihi : trackInsertArray)
										// System.out.print(Arrays.toString(ahihi));
										// System.out.println("");

										tempRoute = cf.cloneVector(currentRoute);
										tempRoute.get(insertArray[0]).add(insertArray[1], order);
										tempRoute.get(possibleMergedRouteId.get(j)).remove(order);

										if ((calcuNewBenfitPostProc(tempRoute) < (fitnessParaThreshold * bestCost))
												&& (calcuNewBenfitPostProc(tempRoute) <= localBestRouteCost)) {
											localBestRoute = tempRoute;
											localBestRouteCost = calcuNewBenfitPostProc(localBestRoute);
											trackingPathNeedRemoved.remove(order);
										}
									}

									// System.out.println("TrackingPathNeedRemoved in iteration 2: "
									// + human2(trackingPathNeedRemoved));
									// System.out.println("LocalBestRoute in iteration 2: " +
									// human(localBestRoute));
									// System.out.println("LocalBestRoute Cost in iteration 2: "
									// + calcuNewBenfitPostProc(localBestRoute));
								}
							}
						}

						System.out.println("PathNeedRemoved after processing 2: " + human2(trackingPathNeedRemoved));

						if (trackingPathNeedRemoved.size() == 0) {

							System.out.println("New route after process 2: " + human(localBestRoute));
							System.out.println("Cost: " + calcuNewBenfitPostProc(localBestRoute));
							if (calcuNewBenfitPostProc(localBestRoute) < (fitnessParaThreshold * bestCost)) {
								bestRoute = cf.cloneVector(localBestRoute);
								bestCost = calcuNewBenfitPostProc(bestRoute);

								trackingPossibleMergedRoute = cf.cloneVector(possibleMergedRoute);
								trackingPossibleMergedRoute.remove(j);
								trackingPossibleMergedRouteId = (ArrayList<Integer>) possibleMergedRouteId.clone();
								trackingPossibleMergedRouteId.remove(j);
							}
						}
						currentRoute = cf.cloneVector(route);
					}
				}
			}

			// If there are possible way to reduce this
			if (trackingPossibleMergedRoute.size() < possibleMergedRoute.size()) {
				possibleMergedRoute = cf.cloneVector(trackingPossibleMergedRoute);
				possibleMergedRouteId = (ArrayList<Integer>) trackingPossibleMergedRouteId.clone();
				route = cf.cloneVector(bestRoute);
				currentRoute = cf.cloneVector(route);
				continue;
			} else {
				break;
			}
		}
		System.out.println("Previous route: " + human(route));
		System.out.println("Previous cost: " + calcuNewBenfitPostProc(route));
		System.out.println("Best route :" + human(bestRoute));
		System.out.println("Best cost: " + bestCost);
		System.out.println("TRAN");
		return bestRoute;
	}

	// private ArrayList<ArrayList<Order>> longTripsMIP() throws
	// InterruptedException, ExecutionException {
	// int i;
	// double costTemp;
	// double obj = 0;
	// double[] returnArray = new double[2];
	// returnArray[0] = 10e8;
	// returnArray[1] = 10e8;
	// int cnt;
	// IloNumVar[] z;
	// ArrayList<ArrayList<Order>> solution = new ArrayList<ArrayList<Order>>();
	// HashSet<Order> removeSet = new HashSet<Order>(d.orders);
	// removeSet.remove(null);
	// cf.removeDepot(removeSet, d.startEndNodes);
	// try {
	//
	// int size = routesColumn.size();
	// z = new IloNumVar[size];
	// IloCplex cplex = new IloCplex();
	//
	// for (i = 0; i < size; i++)
	// z[i] = cplex.numVar(0, 1);
	//
	// IloLinearNumExpr tempo = cplex.linearNumExpr();
	//
	// for (Order uv : removeSet) {
	// tempo.clear();
	// cnt = 0;
	// for (Column r : routesColumn) {
	// if (r.path.contains(uv)) {
	// tempo.addTerm(1, z[cnt]);
	// }
	// cnt++;
	// }
	// cplex.addEq(tempo, 1);
	// }
	// cnt = 0;
	// // cplex.addLe(cplex.sum(z), vehicleSize);
	// IloLinearNumExpr objMu = cplex.linearNumExpr();
	// // cplex.addLe(objMu, upperbound);
	// cnt = 0;
	// for (Column r : routesColumn) {
	// objMu.addTerm(r.getcost(), z[cnt]);
	// cnt++;
	// }
	//
	// cplex.addMinimize(objMu, "TotalCost");
	// // cplex.setOut(null);
	// cplex.setParam(IloCplex.DoubleParam.TiLim, 80); // max number of seconds:
	// 2h=7200 24h=86400
	// if (cplex.solve()) {
	// obj = cplex.getValue(objMu);// getObjValue();
	// i = 0;
	// for (Column r : routesColumn) {
	// costTemp = cplex.getValue(z[i]);
	// // System.err.println("r.travelWait\t" + r.travelWait);
	// if (costTemp > 0.5)
	// solution.add(r.path);
	// i++;
	// }
	// System.err.println(human(solution));
	// removeDuplicateAll(solution);
	// System.err.println("1\t" + human(solution));
	// leftOverInsert(solution);
	// System.err.println("2\t" + human(solution));
	// }
	// cplex.end();
	// } catch (IloException e) {
	// System.err.println("Concert exception caught '" + e + "' caught");
	// }
	// return solution;
	// }

	private void leftOverInsert(ArrayList<ArrayList<Order>> solution) throws InterruptedException, ExecutionException {
		HashSet<Order> removeSet = new HashSet<Order>(d.orders);
		removeSet.remove(null);
		cf.removeDepot(removeSet, d.startEndNodes);

		for (ArrayList<Order> path : solution) {
			removeSet.remove(path);
		}
		insert(solution, cf.cloneToArray(removeSet));
	}

	private void removeDuplicateAll(ArrayList<ArrayList<Order>> solution) {
		HashSet<Integer> removeSet = new HashSet<Integer>();
		for (ArrayList<Order> path : solution) {
			for (int i = 1; i < path.size() - 1; i++) {
				if (!removeSet.contains(path.get(i).id) && !d.startEndNodes.contains(path.get(i)))
					removeSet.add(path.get(i).id);
				else
					path.remove(i);
			}
		}
		ArrayList<Order> collectionDepot = new ArrayList<Order>();
		for (int i = 0; i < d.totalCar; i++)
			collectionDepot.add(d.vehicles.get(i).endDepot);

		solution = cf.cloneVector(addSolution(solution));
		// sort solution based on
	}

	public int[] multiVehiclesInsertionVersion2(ArrayList<ArrayList<Order>> newRoute, Order chooseNode,
			ArrayList<int[]> trackInsertArray) {
		int[] minarray = new int[2];
		minarray[1] = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		boolean flag;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		needMiddleAdd = new double[vehicles.size()][2];
		postPone = new double[vehicles.size()];

		for (int i = 0; i < route.size(); i++) {
			if (chooseNode.specialVehicle > -1 && Math.abs(chooseNode.specialVehicle - i) > 1e-3)
				continue;
			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			globalCost = 0;
			// System.out.println(human(route));
			for (int m = 0; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;

				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}
				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp + r.nextDouble() * 1;
				if (finalOptimizeFlag) {
					flag = false;
					if (benefi < minBeneif && volumeFeasible) {
						for (int[] insertArray : trackInsertArray) {
							if (insertArray[0] == i && insertArray[1] == (m + 1)) {
								flag = true;
								break;
							}
						}
						if (flag == false) {
							minBeneif = benefi;
							minarray[0] = i;
							minarray[1] = m + 1;
						}
					}
				} else {
					flag = false;
					if (benefi < minBeneif) {
						for (int[] insertArray : trackInsertArray) {
							if (insertArray[0] == i && insertArray[1] == (m + 1)) {
								flag = true;
								break;
							}
						}
						if (flag == false) {
							minBeneif = benefi;
							minarray[0] = i;
							minarray[1] = m + 1;
						}
					}
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	public ArrayList<Integer> human2(ArrayList<Order> path) {
		ArrayList<Integer> humanReadRoute = new ArrayList<Integer>();
		for (Order node : path) {
			humanReadRoute.add(node.id);
		}
		return humanReadRoute;
	}

	private ArrayList<ArrayList<Order>> assignToDifferentVehicles(ArrayList<ArrayList<Order>> route) {

		// System.out.println("k value " + kValue());

		double oldCost = calcuNewBenfitPostProc(route);
		double oldTravelTime = travelTime;
		int specificK = 0;
		// ArrayList<Order> newOrders = cf.arraylistCloneObject(orders);
		ArrayList<Order> newOrders = cf.arraylistCloneObject(route.get(0));
		ArrayList<ArrayList<Order>> routeCopy = new ArrayList<ArrayList<Order>>();

		int ordercnt = 0;
		int addCount = 0;
		for (ArrayList<Order> p : route) {
			if (addCount != 0) {
				routeCopy.add(new ArrayList<Order>());
				for (Order q : p)
					routeCopy.get(ordercnt).add(q);
				ordercnt++;
			}
			addCount++;
		}

		newOrders.removeAll(d.startEndNodes);
		int k = 0;
		int cnt = 0;
		for (Order chooseNode : newOrders) {
			if (chooseNode == null)
				continue;
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(route);
			orginRoute = cf.cloneVector(route);
			k = 0;
			for (ArrayList<Order> path : newRoute) {
				if (path.contains(chooseNode)) {
					path.remove(chooseNode);
					specificK = k;
					break;
				}
				k++;
			}

			System.out.println("Choose Node " + chooseNode.id);
			// System.out.println("after removing first path "+routeCopy.size());

			int[] insertArray = multiVehiclesInsertion(routeCopy, chooseNode);
			if (insertArray[1] > 0) {
				System.out.println(insertArray[0] + " " + insertArray[1]);
				// System.out.println("previous size "+routeCopy.get(insertArray[0]).size());
				routeCopy.get(insertArray[0]).add(insertArray[1], chooseNode);
				// System.out.println("new size "+routeCopy.get(insertArray[0]).size());
				route = cf.cloneVector(routeCopy);
				cnt++;
			}

			/*
			 * if ((needSmooth && (calcuNewBenfit(route) > oldCost)) || (needSmooth &&
			 * (calcuNewBenfit(route) >= fitnessParaThreshold * oldCost || (travelTime >=
			 * oldTravelTime + travelTimeParaThreshold)))) { route =
			 * cf.cloneVector(orginRoute); System.out.println("stay with original route");
			 * break; }
			 */

			if (calcuNewBenfitPostProc(route) >= fitnessParaThreshold * oldCost) {
				System.out.println(calcuNewBenfitPostProc(route) + " " + fitnessParaThreshold + " " + oldCost);
				System.out.println(travelTime + " " + travelTimeParaThreshold + " " + oldTravelTime);
				route = cf.cloneVector(orginRoute);
				System.out.println("stay with original route");
				break;
			}

			System.out.println("Comparing costs " + oldCost + " " + calcuNewBenfitPostProc(route));

		}
		return route;

	}

	private ArrayList<ArrayList<Order>> smoothBetweenTrip(ArrayList<ArrayList<Order>> route) {
		double oldCost = calcuNewBenfit(route);
		double oldTravelTime = travelTime;
		double newCost = 0;
		int cnt = 0;
		Order startDepot1 = null;
		Order startDepot2 = null;
		for (int loop = 0; loop < 2; loop++) {
			HashMap<Integer, ArrayList<ArrayList<Order>>> routeTrips = splitRouteToTrips(route);
			ArrayList<Order> tempTrip1 = new ArrayList<Order>();
			ArrayList<Order> tempTrip2 = new ArrayList<Order>();
			cnt = 0;

			for (int i = 0; i < route.size(); i++) {
				for (int j = i + 1; j < route.size(); j++) {
					if (vehicles.get(i).vehicleStartMiddleLocations.size() > 1
							|| vehicles.get(j).vehicleStartMiddleLocations.size() > 1
							|| vehicles.get(i).vehicleStartMiddleLocations
									.hashCode() != vehicles.get(j).vehicleStartMiddleLocations.hashCode())
						continue;
					if (routeTrips.get((Integer) i).size() <= 1 && routeTrips.get((Integer) j).size() <= 1)
						continue;
					orginRoute = cf.cloneVector(route);

					oldCost = calcuNewBenfit(route);
					startDepot1 = route.get(i).get(0);
					startDepot2 = route.get(j).get(0);
					int[] swapArray = swapTrips(i, routeTrips.get(i), startDepot1, j, routeTrips.get(j), startDepot2);
					if (swapArray[0] > -2) {
						if (routeTrips.get(i).size() > 1 && routeTrips.get(j).size() > 1) {
							tempTrip1 = routeTrips.get(i).get(swapArray[0]);
							tempTrip2 = routeTrips.get(j).get(swapArray[1]);
							routeTrips.get(i).set(swapArray[0], tempTrip2);
							routeTrips.get(j).set(swapArray[1], tempTrip1);
						} else if ((routeTrips.get(i).size() <= 1 && routeTrips.get(j).size() > 1)) {
							routeTrips.get(i).add(routeTrips.get(j).get(swapArray[1]));
							routeTrips.get(j).remove(swapArray[1]);
						} else if ((routeTrips.get(i).size() > 1 && routeTrips.get(j).size() <= 1)) {
							routeTrips.get(j).add(routeTrips.get(i).get(swapArray[1]));
							routeTrips.get(i).remove(swapArray[1]);
						}

						route.set(i, mergeSeperateTrip(startDepot1, routeTrips.get(i)));
						route.set(j, mergeSeperateTrip(startDepot2, routeTrips.get(j)));
						cnt++;
						// System.err.println("ever happen");
						// System.out.println("origin\t" + human(orginRoute));
						// System.out.println("final\t" + human(route));
						newCost = calcuNewBenfit(route);
						if ((!needSmooth && newCost > oldCost)
								|| (needSmooth && (newCost >= fitnessParaThreshold * oldCost)
										|| (cnt > 1 && travelTime >= oldTravelTime + travelTimeParaThreshold))) {
							route = cf.cloneVector(orginRoute);
							// break;
						}
					}
				}
			}
		}
		return route;
	}

	HashMap<Integer, ArrayList<ArrayList<Order>>> splitRouteToTrips(ArrayList<ArrayList<Order>> route) {
	HashMap<Integer, ArrayList<ArrayList<Order>>> routeTrips = new HashMap<Integer, ArrayList<ArrayList<Order>>>();

	for (int key = 0; key < route.size(); key++) {
		ArrayList<ArrayList<Order>> splitTrips = new ArrayList<ArrayList<Order>>();

		ArrayList<Order> trip = new ArrayList<Order>();
		for (int i = 0; i < route.get(key).size() - 1; i++) {
			if (i > 0 && d.startEndNodes.contains(route.get(key).get(i))) {
				trip.add(route.get(key).get(i));
				if (trip.size() > 2)
					splitTrips.add(trip);
				trip = new ArrayList<Order>();
				trip.add(route.get(key).get(i));
			} else
				trip.add(route.get(key).get(i));
		}
		trip.add(route.get(key).get(route.get(key).size() - 1));
		if (trip.size() > 2)
			splitTrips.add(trip);

		routeTrips.put(key, splitTrips);
	}
	return routeTrips;
}
	
	private ArrayList<ArrayList<Order>> smoothMultiVehicleInsertion(ArrayList<ArrayList<Order>> route) {
		double oldCost = calcuNewBenfit(route);
		double oldTravelTime = travelTime;
		int specificK = 0;
		ArrayList<Order> newOrders = cf.arraylistCloneObject(orders);
		Collections.shuffle(newOrders);
		newOrders.removeAll(d.startEndNodes);
		int k = 0;
		int cnt = 0;
		for (Order chooseNode : newOrders) {
			if (chooseNode == null)
				continue;
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(route);
			orginRoute = cf.cloneVector(route);
			k = 0;
			for (ArrayList<Order> path : newRoute) {
				if (path.contains(chooseNode)) {
					path.remove(chooseNode);
					specificK = k;
					break;
				}
				k++;
			}

			int[] insertArray = multiVehiclesInsertionDifferVehicle(orginRoute, newRoute, chooseNode, specificK);
			if (insertArray[1] > 0) {
				newRoute.get(insertArray[0]).add(insertArray[1], chooseNode);
				route = cf.cloneVector(newRoute);
				cnt++;
			}
			if ((needSmooth && (calcuNewBenfit(route) > oldCost))
					|| (needSmooth && (calcuNewBenfit(route) >= fitnessParaThreshold * oldCost
							|| (travelTime >= oldTravelTime + travelTimeParaThreshold)))) {
				route = cf.cloneVector(orginRoute);
				break;
			}

		}
		return route;
	}

	private ArrayList<ArrayList<Order>> smoothFinalSwap(ArrayList<ArrayList<Order>> route) {
		double oldCost = calcuNewBenfit(route);
		double oldTravelTime = travelTime;
		orginRoute = cf.cloneVector(route);
		int k = 0;
		for (int loop = 0; loop < 5; loop++) {
			k = 0;
			for (ArrayList<Order> path : route) {
				ArrayList<Order> newPath = swap(path, k);
				route.set(k, newPath);
				k++;
			}
			if (calcuNewBenfit(route) >= fitnessParaThreshold * oldCost
					|| travelTime >= oldTravelTime + travelTimeParaThreshold) {//
				route = cf.cloneVector(orginRoute);
				break;
			}
		}
		return route;
	}

	private int[] swapTrips(int vehicle1, ArrayList<ArrayList<Order>> vehicleTrip1, Order startDepot1, int vehicle2,
			ArrayList<ArrayList<Order>> vehicleTrip2, Order startDepot2) {
		int initialSimilary = calculateSimilarity(mergeSeperateTrip(startDepot1, vehicleTrip1),
				mergeSeperateTrip(startDepot2, vehicleTrip2));
		double oldCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, vehicleTrip1), vehicle1),
				con.yita2, 0, 0, 0)
				+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, vehicleTrip2), vehicle2), con.yita2, 0, 0,
						0);
		double oldTravelTime = calcuAdjustTravelTime(vehicleTrip1) + calcuAdjustTravelTime(vehicleTrip2);
		int tempSimilarity = 0;

		int[] swapArray = new int[2];
		swapArray[0] = -2;
		double newCost = 0;
		for (int trip1 = 0; trip1 < vehicleTrip1.size(); trip1++)
			for (int trip2 = 0; trip2 < vehicleTrip2.size(); trip2++) {
				ArrayList<ArrayList<Order>> newVehicleTrip1 = cf.cloneVector(vehicleTrip1);
				ArrayList<ArrayList<Order>> newVehicleTrip2 = cf.cloneVector(vehicleTrip2);
				int u = 0;
				if (vehicleTrip1.size() <= 1 && vehicleTrip2.size() > 1)
					u = 0;
				if (vehicleTrip1.size() > 1 && vehicleTrip2.size() <= 1)
					u = 1;
				else
					u = 2;
				switch (u) {
				case 0:
					newVehicleTrip1.add(vehicleTrip2.get(trip2));
					newVehicleTrip2.remove(trip2);
					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
							mergeSeperateTrip(startDepot2, newVehicleTrip2));

					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), vehicle1),
							con.yita2, 0, 0, 0)
							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), vehicle2),
									con.yita2, 0, 0, 0);

					// System.err.println(initialSimilary + "\t" + tempSimilarity + "\t" + newCost +
					// "\t" + oldCost);
					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
							&& travelTime <= oldTravelTime + 20) {
						initialSimilary = tempSimilarity;
						swapArray[0] = -1;
						swapArray[1] = trip2;
					}
					break;
				case 1:
					newVehicleTrip2.add(vehicleTrip1.get(trip1));
					newVehicleTrip1.remove(trip1);
					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
							mergeSeperateTrip(startDepot2, newVehicleTrip2));

					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), vehicle1),
							con.yita2, 0, 0, 0)
							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), vehicle2),
									con.yita2, 0, 0, 0);

					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
							&& travelTime <= oldTravelTime + 20) {
						initialSimilary = tempSimilarity;
						swapArray[0] = -1;
						swapArray[1] = trip1;
					}
					break;
				case 2:
					newVehicleTrip1.set(trip1, vehicleTrip2.get(trip2));
					newVehicleTrip2.set(trip2, vehicleTrip1.get(trip1));

					tempSimilarity = calculateSimilarity(mergeSeperateTrip(startDepot1, newVehicleTrip1),
							mergeSeperateTrip(startDepot2, newVehicleTrip2));

					newCost = calcuResult(new PathSolution(mergeSeperateTrip(startDepot1, newVehicleTrip1), vehicle1),
							con.yita2, 0, 0, 0)
							+ calcuResult(new PathSolution(mergeSeperateTrip(startDepot2, newVehicleTrip2), vehicle2),
									con.yita2, 0, 0, 0);

					// System.err.println(initialSimilary + "\t" + tempSimilarity + "\t" + newCost +
					// "\t" + oldCost);
					travelTime = calcuAdjustTravelTime(newVehicleTrip1) + calcuAdjustTravelTime(newVehicleTrip2);
					if (tempSimilarity < initialSimilary && newCost <= 1.1 * oldCost
							&& travelTime <= oldTravelTime + 20) {
						initialSimilary = tempSimilarity;
						swapArray[0] = trip1;
						swapArray[1] = trip2;
					}
					break;
				default:
					break;
				}
			}

		return swapArray;
	}

	ArrayList<Order> mergeSeperateTrip(Order startDepot, ArrayList<ArrayList<Order>> vehicleTrip) {
		ArrayList<Order> mergeTrip = new ArrayList<Order>();
		if (vehicleTrip.size() < 1 || vehicleTrip == null)
			return mergeTrip;
		mergeTrip.add(startDepot);
		mergeTrip.addAll(vehicleTrip.get(0));
		for (int i = 1; i < vehicleTrip.size(); i++) {
			if (vehicleTrip.get(i).get(0).orderLocation == vehicleTrip.get(i - 1)
					.get(vehicleTrip.get(i - 1).size() - 1).orderLocation)
				mergeTrip.addAll(vehicleTrip.get(i).subList(1, vehicleTrip.get(i).size()));
			else
				mergeTrip.addAll(vehicleTrip.get(i));
		}
		return mergeTrip;
	}

	// @post process
	private void callPostProcess() {
		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(feasiusedRouteWhole);
			reoptimize(newRoute);
			check = true;
			calcuNewBenfit(newRoute);
			if (check) {
				feasiusedRouteWhole = cf.cloneVector(newRoute);
				feasibenefitWhole = calcuNewBenfit(feasiusedRouteWhole);
			}
		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
			ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(noTimeVioRoute);
			reoptimize(newRoute);
			check = true;
			calcuNewBenfit(newRoute);
			if (check) {
				noTimeVioRoute = cf.cloneVector(newRoute);
				noTimeVioBenifit = calcuNewBenfit(noTimeVioRoute);
			}

		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
			bestUsedRoute = cf.cloneVector(reoptimize(bestUsedRoute));
			bestBenifit = calcuNewBenfit(bestUsedRoute);
		}
	}

	private ArrayList<ArrayList<Order>> reoptimize(ArrayList<ArrayList<Order>> route) {
		int k = 0;
		for (int reopt = 0; reopt < 50; reopt++) {
			k = 0;
			for (ArrayList<Order> path : route) {
				ArrayList<Order> newPath = twoOptAlternate(path, k);
				route.set(k, newPath);
				k++;
			}
		}
		ArrayList<ArrayList<Order>> originalRoute = cf.cloneVector(route);
		if (route != null && route.size() > 0) {
			double oldCost = calcuNewBenfit(route);
			double oldTravelTime = travelTime;
			for (int reopt = 0; reopt < 200; reopt++) {
				ArrayList<Order> newOrders = cf.arraylistCloneObject(orders);
				Collections.shuffle(newOrders);
				newOrders.removeAll(d.startEndNodes);
				for (Order chooseNode : newOrders) {
					if (chooseNode == null)
						continue;
					ArrayList<ArrayList<Order>> newRoute = cf.cloneVector(route);
					orginRoute = cf.cloneVector(route);
					for (ArrayList<Order> path : newRoute) {
						if (path.contains(chooseNode)) {
							path.remove(chooseNode);
							break;
						}
					}

					int[] insertArray = multiVehiclesInsertion(newRoute, chooseNode);
					if (insertArray[1] > 0) {
						newRoute.get(insertArray[0]).add(insertArray[1], chooseNode);
					}
					route = cf.cloneVector(newRoute);
				}
			}
			postMerge();

			for (int reopt = 0; reopt < 200; reopt++) {
				k = 0;
				for (ArrayList<Order> path : route) {
					ArrayList<Order> newPath = swap(path, k);
					route.set(k, newPath);
					k++;
				}
			}

			for (int reopt = 0; reopt < 200; reopt++) {
				k = 0;
				for (ArrayList<Order> path : route) {
					ArrayList<Order> newPath = removeEachOneInsertSameTrip(path, k);
					route.set(k, newPath);
					k++;
				}
			}

			route = smoothBetweenTrip(route);
			double newCost = calcuNewBenfit(route);

			if ((!needSmooth && newCost >= oldCost) || (needSmooth && newCost >= fitnessParaThreshold * oldCost
					&& travelTime >= oldTravelTime + travelTimeParaThreshold))
				route = cf.cloneVector(originalRoute);
		}
		// System.err.println(human(route));
		return route;
	}

	public ArrayList<Order> swap(ArrayList<Order> path, Integer k) {
		// swap any two, check if it's better, swap same trip
		Order nodeI = null;
		Order nodeJ = null;
		double orginalResult = 0;
		double newResult = 0;
		boolean check = false;
		int[] depotIndex = new int[2];
		ArrayList<Order> newpath = cf.singleCloneOrder(path);
		ArrayList<Order> startPath = cf.arraylistCloneObject(path);

		double travelTimeBest = calculateTime(startPath, 0, startPath.size() - 1);
		int start = 0;
		for (int j = 1; j < path.size() - 1; j++) {
			if (d.startEndNodes.contains(path.get(j)) && path.get(j).location.id != path.get(0).location.id)
				start++;
			else
				break;
		}
		for (int i = start + 1; i < path.size(); i++) {
			for (int j = i + 1; j < path.size() - 1; j++) {
				nodeI = newpath.get(i);
				if (d.startEndNodes.contains(nodeI))
					continue;
				ArrayList<Order> originPath = cf.arraylistCloneObject(newpath);
				depotIndex = tripDepot(cf.listCloneObject(newpath), i);
				orginalResult = calcuResult(new PathSolution(newpath, k), con.yita2, 0, 0, 0);
				// depotIndex[1] is the trip end position
				if (j > depotIndex[1] || d.startEndNodes.contains(newpath.get(j)))
					continue;
				nodeJ = newpath.get(j);
				newpath.remove(i);
				newpath.remove(j - 1);
				newpath.add(i, nodeJ);
				newpath.add(j, nodeI);
				newResult = calcuResult(new PathSolution(newpath, k), con.yita2, 0, 0, 0);
				check = needSmooth
						&& (calculateTime(newpath, i, depotIndex[1]) < calculateTime(originPath, i, depotIndex[1])
								+ travelTimeParaThreshold)
						&& calculateSmooth(newpath, i, depotIndex[1]) > calculateSmooth(originPath, i, depotIndex[1]);
				check = check && (newResult < orginalResult * fitnessParaThreshold);

				if ((check && calculateTime(newpath, 0, newpath.size() - 1) < travelTimeBest)
						|| (!needSmooth && newResult < orginalResult)) {

				} else {
					newpath = cf.singleCloneOrder(originPath);
				}
			}
		}
		return newpath;
	}

	int calculateSmooth(ArrayList<Order> path, int from, int to) {
		// System.out.println(" smoothness threshold "+smoothThreshold);
		int smooth = 0;
		for (int i = from; i < to; i++) {
			if (d.con.disMatrix[path.get(i).orderLocation()][path.get(i + 1).orderLocation()] <= smoothThreshold)
				smooth++;
		}
		return smooth;
	}

	double calculateTime(ArrayList<Order> path, int from, int to) {
		double totalTime = 0;
		double singTime = 0;
		for (int i = from; i < to; i++) {
			singTime = d.con.timeMatrix[path.get(i).orderLocation()][path.get(i + 1).orderLocation()];
			totalTime += singTime;
		}
		return totalTime;
	}

	int[] tripDepot(List<Order> list, int pos) {
		int size = list.size();
		int cnt = 0;
		int[] depotIndex = new int[2];

		for (int i = pos + 1; i < size; i++) {
			if (d.startEndNodes.contains(list.get(i))) {
				depotIndex[1] = i - 1;
				break;
			}
		}

		for (int i = pos - 1; i > 0; i--) {
			if (d.startEndNodes.contains(list.get(i))) {
				depotIndex[0] = i;
				break;
			}
		}
		return depotIndex;
	}

	public ArrayList<Order> removeEachOneInsertSameTrip(ArrayList<Order> path, int k) {
		Order node = null;
		double orginalResult = 0;
		double bestResult = 1e10;

		double newResult = 0;
		int[] depotIndex = new int[2];
		int minarray = -1;
		boolean check = false;
		ArrayList<Order> originPath = new ArrayList<Order>();
		double travelTimeBest = calculateTime(path, 0, path.size() - 1);
		int start = 0;
		for (int j = 1; j < path.size() - 1; j++) {
			if (d.startEndNodes.contains(path.get(j)) && path.get(j).location.id != path.get(0).location.id)
				start++;
			else
				break;
		}
		for (int i = start; i < path.size(); i++) {
			originPath = cf.listCloneObject(path);
			minarray = -1;
			node = path.get(i);
			if (d.startEndNodes.contains(node))
				continue;

			depotIndex = tripDepot(cf.listCloneObject(path), i);
			orginalResult = calcuResult(new PathSolution(path, k), con.yita2, 0, 0, 0);
			path.remove(i);

			for (int j = start + 1; j < path.size(); j++) {
				if (i == j || j <= depotIndex[0] || j > depotIndex[1])
					continue;
				path.add(j, node);
				newResult = calcuResult(new PathSolution(path, k), con.yita2, 0, 0, 0);

				check = needSmooth
						&& (calculateTime(path, i, depotIndex[1]) < calculateTime(originPath, i, depotIndex[1])
								+ travelTimeParaThreshold)
						&& calculateSmooth(path, i, depotIndex[1]) > calculateSmooth(originPath, i, depotIndex[1]);
				check = check && (newResult - orginalResult) < bestResult
						&& (newResult < fitnessParaThreshold * orginalResult);

				if (check && calculateTime(path, 0, path.size() - 1) < travelTimeBest + travelTimeParaThreshold) {
					bestResult = newResult - orginalResult;
					minarray = j;
				} else if (!needSmooth && newResult < orginalResult) {
					orginalResult = newResult;
					minarray = j;
				}
				path.remove(j);
			}
			if (minarray > -1) {
				path.add(minarray, node);
			} else
				path.add(i, node);
		}
		return path;
	}

	// @merge
	private void postMerge() {
		ArrayList<ArrayList<Order>> route;
		double newbenfit = 0;
		if (feasiusedRouteWhole != null && feasiusedRouteWhole.size() > 0) {
			removeDepot(feasiusedRouteWhole, true);
			route = cf.cloneVector(feasiusedRouteWhole);
			route = cf.cloneVector(postProcessTripMerge(route));
			check = true;
			newbenfit = calcuNewBenfit(route);
			// if (newbenfit < feasibenefitWhole * 1.18) {
			if (check && totalSize == serveAll(route)) {
				feasiusedRouteWhole = cf.cloneVector(route);
				feasibenefitWhole = newbenfit;
			}
			// }
		} else if (bestUsedRoute != null && bestUsedRoute.size() > 0) {
			removeDepot(bestUsedRoute, true);
			route = cf.cloneVector(bestUsedRoute);
			route = cf.cloneVector(postProcessTripMerge(route));
			newbenfit = calcuNewBenfit(route);
			if (newbenfit < bestBenifit * 1.18 && totalSize == serveAll(route)) {
				bestUsedRoute = cf.cloneVector(route);
				bestBenifit = newbenfit;
			}
		} else if (noTimeVioRoute != null && noTimeVioRoute.size() > 0) {
			removeDepot(noTimeVioRoute, true);
			route = cf.cloneVector(noTimeVioRoute);
			route = cf.cloneVector(postProcessTripMerge(route));
			newbenfit = calcuNewBenfit(route);
			if (newbenfit < noTimeVioBenifit * 1.18 && totalSize == serveAll(route)) {
				noTimeVioRoute = cf.cloneVector(route);
				noTimeVioBenifit = newbenfit;
			}
		}
	}

	private ArrayList<ArrayList<Order>> postProcessTripMerge(ArrayList<ArrayList<Order>> route) {
		currentSolutionMultiTrips = new ArrayList<Trip>();
		for (int i = 0; i < route.size(); i++) {
			List<Integer> singlePath = new ArrayList<Integer>();
			for (int j = 1; j < route.get(i).size() - 1; j++) {

				if (!d.startEndNodes.contains(route.get(i).get(j)))
					singlePath.add(route.get(i).get(j).id);
				else {
					currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
					singlePath = new ArrayList<Integer>();
				}
			}
			currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
		}
		List<Order> removeSet = new ArrayList<Order>();

		ArrayList<Order> splitPath = new ArrayList<Order>();

		for (Trip t : currentSolutionMultiTrips) {
			if (t.path.size() < 3) {
				removeSet.addAll(translate(t.path));
			}
		}

		for (ArrayList<Order> path : route)
			path.removeAll(removeSet);

		int[] insertArray = new int[2];
		for (Order chooseNode : removeSet) {
			insertArray = finalMergeInsertion(route, chooseNode);
			if (insertArray[1] > 0)
				route.get(insertArray[0]).add(insertArray[1], chooseNode);
		}
		return route;
	}

	private int[] finalMergeInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		int[] minarray = new int[2];
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);

		double initialMaxUtilize = 0;
		for (int i = 0; i < route.size(); i++) {
			if (notAllowedVehicle.contains(i) || !chooseNode.vehiclesIndex.contains(i + 1)) {
				continue;
			}

			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			globalCost = 0;
			// System.out.println(human(route));
			for (int m = 0; m < route.get(i).size() - 1; m++) {
				if ((tripSize(route.get(i), m + 1) < 2
						|| tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1)) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}
				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp + r.nextDouble() * 1;

				if (benefi < minBeneif) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;

				}
				route.get(i).remove(m + 1);
				maxUtilize = initialMaxUtilize;
			}
		}
		return minarray;

	}

	private double vioTime(ArrayList<ArrayList<Order>> route) {
		double sumVio = 0;
		int carId = 0;
		int size = 0;
		Order nodeI = null, nodeJ = null, nodeK = null;
		int totalTime = 0;
		int tempWait = 0;
		int nextTime = 0;
		HashMap<Integer, Integer> violateOrders = new HashMap<Integer, Integer>();
		int arrriveTime = 0;
		int tempNodeServiceTime = 0;
		int tempNextNodeServicetime = 0;

		for (ArrayList<Order> path : route) {
			size = path.size();
			carId++;
			nodeI = path.get(0);
			nodeJ = path.get(1);
			totalTime = Math.max(nodeI.lowerTime(), vehicles.get(carId - 1).truckLowerTime());
			arrriveTime = totalTime + nodeI.serviceTime + con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation];
			if (!d.startEndNodes.contains(nodeJ)) {
				if (arrriveTime > nodeJ.upperTime) {
					violateOrders.put(nodeJ.id, arrriveTime - nodeJ.upperTime);
				}
			}

			nextTime = totalTime + nodeI.serviceTime;

			if (nodeI.orderLocation != nodeJ.orderLocation)
				nextTime += con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation];

			nextTime = Math.max(nodeJ.lowerTime, nextTime);
			tempWait = nextTime
					- (totalTime + con.timeMatrix[nodeI.orderLocation][nodeJ.orderLocation] + nodeI.serviceTime);

			nextTime += tempWait;

			for (int i = 1; i < size - 1; i++) {
				nodeI = path.get(i - 1);
				nodeJ = path.get(i);
				nodeK = path.get(i + 1);

				totalTime = nextTime;
				tempNodeServiceTime = 0;
				tempNextNodeServicetime = 0;
				totalTime = nextTime;
				if (nodeI.orderLocation != nodeJ.orderLocation)
					tempNodeServiceTime = nodeJ.serviceTime;
				arrriveTime = totalTime + tempNodeServiceTime
						+ d.con.timeMatrix[nodeJ.orderLocation][nodeK.orderLocation];

				nextTime = arrriveTime;
				if (nodeJ.orderLocation != nodeK.orderLocation) {
					tempNextNodeServicetime = nodeK.serviceTime;
				}
				if (con.lsps.get(vehicles.get(carId - 1).originLSP).lunchLocationFlag
						&& !d.startEndNodes.contains(nodeK)
						&& nextTime + tempNextNodeServicetime >= locations[nodeK.orderLocation()].lunchStart
						&& nextTime < locations[nodeK.orderLocation()].lunchEnd) {
					nextTime = locations[nodeK.orderLocation()].lunchEnd;
				}

				nextTime = Math.max(nodeK.lowerTime, nextTime);

				if (!d.startEndNodes.contains(nodeK)) {
					if (arrriveTime > nodeK.upperTime) {
						violateOrders.put(nodeK.id, arrriveTime - nodeK.upperTime);
					}
				}

			}

			for (Integer node : violateOrders.keySet()) {
				sumVio += violateOrders.get(node);
			}
		}
		return sumVio;
	}

	private void removeDepot(ArrayList<ArrayList<Order>> route, boolean endOptimize) {
		int cnt = 0;
		int preNode = 0;
		int curNode = 0;
		int size = 0;
		int servedOrderSize = 0;

		ArrayList<Integer> remove = new ArrayList<>();
		for (int i = 0; i < route.size(); i++) {
			size = route.get(i).size();
			servedOrderSize = 0;

			if (size <= 2)
				continue;

			remove = new ArrayList<>();
			preNode = route.get(i).get(0).id;
			for (int j = 1; j < size - 1; j++) {
				curNode = route.get(i).get(j).id;

				if (Math.abs(curNode - preNode) < 1e-3
						&& vehicles.get(i).vehicleStartMiddleOrders.contains(route.get(i).get(j)))
					remove.add(j);

				if (!d.startEndNodes.contains(route.get(i).get(j)))
					servedOrderSize++;

				preNode = curNode;
			}
			//
			cnt = 0;
			for (int k = 0; k < remove.size(); k++) {
				if (servedOrderSize > 2)
					route.get(i).remove(remove.get(k) - cnt);
				cnt++;
			}
			if (endOptimize && size > 3 && d.startEndNodes.contains(route.get(i).get(route.get(i).size() - 2)))
				route.get(i).remove(route.get(i).size() - 2);

		}

		for (ArrayList<Order> path : route) {
			Iterator<Order> i = path.iterator();
			while (i.hasNext()) {
				Order s = i.next(); // must be called before you can call
				// i.remove()
				if (s != null && d.dummyOrderMap != null && d.dummyOrderMap.values().contains(s))
					i.remove();
			}
		}

	}

	public void merge(ArrayList<ArrayList<Order>> route, boolean notAllowed) {
		notAllowedVehicle.clear();
		int cnt = 0;
		int[] insertArray = new int[2];
		ArrayList<Order> shortTrips = new ArrayList<Order>();
		int tempCnt = -1;

		for (ArrayList<Order> singleRoute : route) {
			tempCnt++;
			if (singleRoute.size() <= 2 && notAllowed) {
				notAllowedVehicle.add(tempCnt);
				continue;
			}
			if (singleRoute.size() - 2 <= Math.max(3, 0.5 * d.stops / d.totalCar)) {
				cnt++;
				shortTrips.addAll(singleRoute.subList(1, singleRoute.size() - 1));
				shortTrips.removeAll(d.startEndNodes);
				singleRoute.removeAll(shortTrips);
				if (rand.nextDouble() > threshold)
					notAllowedVehicle.add(tempCnt);
			}
		}

		if (cnt == 1) {
			tempCnt = 0;
			// notAllowedVehicle.clear()
			for (ArrayList<Order> singleRoute : route) {
				tempCnt++;
				if (singleRoute.size() <= 2) {
					notAllowedVehicle.add(tempCnt);
					continue;
				}
				if (singleRoute.size() - 2 <= Math.max(3, 0.5 * d.stops / d.totalCar)) {
					notAllowedVehicle.add(tempCnt);
					break;
				}
			}
		}
		for (Order chooseNode : shortTrips) {
			insertArray = multiVehiclesInsertion(route, chooseNode);
			if (insertArray[1] > 0)
				route.get(insertArray[0]).add(insertArray[1], chooseNode);
		}

	}

	public double calcuNewBenfit(ArrayList<ArrayList<Order>> route) {
		// TODO Auto-generated method stub
		travelTime = 0;
		travelDistance = 0;
		double newbenfit = 0;
		postPone = new double[vehicles.size()];
		needMiddleAdd = new double[vehicles.size()][2];

		check = true;
		for (Lsp u : d.lsps) {
			u.setMaxUtilize(0);
			u.setMaxTime(0);
			u.setMinTime(10e3);
			u.setTripDisatance(0);
			u.setSimpleTripDistance(0);
		}

		for (int i = 0; i < route.size(); i++) {
			if (route.get(i) == null || route.get(i).size() <= 2)
				continue;
			if (route.get(i) != null) {
				newbenfit += calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			}
		}

		if (globalCnt >= maxCnt / 2) {
			if (con.objFlag == 0) {
				for (Lsp u : d.lsps) {
					u.setTotalDistance(newbenfit);
					// engine update
					// System.err.println(d.parameterMatrix.get(u.id).getMaxSpanWeight());
					newbenfit += // d.parameterMatrix.get(u.id).getMaxSpanWeight()
							(totalSize - serveAll(route)) * con.unserved_price;
					// + u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight();
					// System.err.println(u.getMaxTime()+"\t"+d.parameterMatrix.get(u.id).getMaxSpanWeight()+"\t"+d.parameterMatrix.get(u.id).getWaitWeight());
				}
			}
			if (con.objFlag == 1) {
				travelDistance = newbenfit;
				for (Lsp u : d.lsps) {
					u.setTotalDistance(newbenfit);
					// d.parameterMatrix.get(u.id).getMaxSpanWeight()
					// engine update
					newbenfit += (totalSize - serveAll(route)) * con.unserved_price;
					// + u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight();
				}
			}
		}
		if (newbenfit < 0) {
			System.err.println("error");
			// newbenfit = (int) 1e10;
		}
		return newbenfit;
	}

	public double calcuNewBenfitPostProc(ArrayList<ArrayList<Order>> route) {
		// TODO Auto-generated method stub
		travelTime = 0;
		travelDistance = 0;

		double newbenfit = 0;
		postPone = new double[vehicles.size()];
		needMiddleAdd = new double[vehicles.size()][2];
		for (Lsp u : d.lsps) {
			u.setMaxUtilize(0);
			u.setMaxTime(0);
			u.setMinTime(10e3);
			u.setTripDisatance(0);
			u.setSimpleTripDistance(0);
		}

		for (int i = 0; i < route.size(); i++) {
			if (route.get(i) == null || route.get(i).size() <= 2)
				continue;
			if (route.get(i) != null) {
				newbenfit += calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			}
		}

		if (globalCnt >= maxCnt / 2) {
			if (con.objFlag == 0) {
				for (Lsp u : d.lsps) {
					u.setTotalDistance(newbenfit);
					// System.err.println(d.parameterMatrix.get(u.id).getMaxSpanWeight());

					// engine update
					newbenfit += // d.parameterMatrix.get(u.id).getMaxSpanWeight()
							(totalSize - serveAll(route)) * con.unserved_price
									+ u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight()
									+ route.size() * con.price;
					maxTripDis = u.getMaxTripDistance();
					// System.err.println(u.getMaxTime()+"\t"+d.parameterMatrix.get(u.id).getMaxSpanWeight()+"\t"+d.parameterMatrix.get(u.id).getWaitWeight());
				}
			}
			if (con.objFlag == 1) {
				travelDistance = newbenfit;
				for (Lsp u : d.lsps) {
					u.setTotalDistance(newbenfit);
					// d.parameterMatrix.get(u.id).getMaxSpanWeight()
					// engine update
					newbenfit += (totalSize - serveAll(route)) * con.unserved_price
							// + u.getMaxUtilize() * lsps.get(u.id).parameter.getLoadBalanceWeight()
							+ route.size() * con.price;
					// System.out.println("debug "+newbenfit+" "+(totalSize -
					// alns.serveAll(route))+" "+con.unserved_price+" "+route.size()+" "+con.price);

					/*
					 * newbenfit = lsps.get(u.id).parameter.getTravelWeight() * newbenfit /
					 * u.lspVehicles.size() + u.lspVehicles.size() * u.getMaxTripDistance()//
					 * d.parameterMatrix.get(u.id).getMaxSpanWeight() + (totalSize -
					 * alns.serveAll(route)) * con.price + lsps.get(u.id).parameter.getWaitWeight()
					 * * totalWaitTime + u.getMaxUtilize() *
					 * lsps.get(u.id).parameter.getLoadBalanceWeight();
					 */

					maxTripDis = u.getMaxTripDistance();
				}
			}
		}
		return newbenfit;
	}

	public double calcuNewBenfitDynamic(ArrayList<ArrayList<Order>> route) {
		// TODO Auto-generated method stub
		travelTime = 0;
		travelDistance = 0;
		double newbenfit = 0;
		postPone = new double[vehicles.size()];
		needMiddleAdd = new double[vehicles.size()][2];

		for (int i = 0; i < route.size(); i++) {
			if (route.get(i) == null || route.get(i).size() <= 2)
				continue;
			if (route.get(i) != null) {
				newbenfit += calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			}
		}

		int missingOrderSize = calculateMissOrder(route);
		// engine update
		newbenfit += missingOrderSize * con.unserved_price;// d.parameterMatrix.get(u.id).getMaxSpanWeight()

		if (newbenfit < 0) {
			newbenfit = (int) 1e10;
		}

		return newbenfit;
	}

	private int calculateMissOrder(ArrayList<ArrayList<Order>> route) {
		HashSet<Order> missOrder = new HashSet<Order>(cf.arraylistCloneObject(orders));

		for (int car = 0; car < route.size(); car++) {
			missOrder.removeAll(route.get(car));
		}

		Iterator itr = missOrder.iterator();
		while (itr.hasNext()) {
			Order x = (Order) itr.next();
			if (x == null || !con.initialMap.containsKey(x.id))
				itr.remove();
			else if (con.startEndNodes.contains(x))
				itr.remove();
		}
		return missOrder.size();
	}

	public ArrayList<ArrayList<Integer>> human(ArrayList<ArrayList<Order>> route) {
		ArrayList<ArrayList<Integer>> humanReadRoute = new ArrayList<ArrayList<Integer>>();
		for (ArrayList<Order> path : route) {
			ArrayList<Integer> newPath = new ArrayList<Integer>();

			for (Order node : path) {
				newPath.add(node.id);
			}

			humanReadRoute.add(newPath);
		}
		return humanReadRoute;
	}

	public ArrayList<ArrayList<Integer>> humanLoc(ArrayList<ArrayList<Order>> route) {
		ArrayList<ArrayList<Integer>> humanReadRouteLoc = new ArrayList<ArrayList<Integer>>();
		for (ArrayList<Order> path : route) {
			ArrayList<Integer> newPath = new ArrayList<Integer>();

			for (Order node : path) {
				newPath.add(node.orderLocation);
			}

			humanReadRouteLoc.add(newPath);
		}
		return humanReadRouteLoc;
	}

	private void updateTabuList() {
		for (ArrayList<Order> path : bestUsedRoute)
			tabuList.add(path.hashCode());
		for (ArrayList<Order> path : feasiusedRouteWhole)
			tabuList.add(path.hashCode());
		if (tabuList.size() > 200)
			tabuList.clear();
	}

	private ArrayList<ArrayList<Order>> addSolution(ArrayList<ArrayList<Order>> solution) {
		ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>();
		for (int i = 0; i < d.totalCar; i++) {
			ArrayList<Order> singPath = new ArrayList<Order>();
			int multiTripCnt = Math.max(lsps.get(0).multiTripCnt, 2);
			if (!vehicles.get(i).haveTrip) {
				if (d.lspMiddleDepotOrder.get(vehicles.get(i).originLSP).size() == 0) {
					while (multiTripCnt > 0) {
						singPath.add(d.vehicles.get(i).depot);
						multiTripCnt--;
					}
				} else {
					singPath.add(d.vehicles.get(i).endDepot);
					// singPath.add(d.vehicles.get(i).depot);
					// multiTripCnt -= 1;
					while (multiTripCnt > 0) {
						for (Order u : d.lspMiddleDepotOrder.get(vehicles.get(i).originLSP)) {
							singPath.add(u);
							multiTripCnt -= 1;
						}
					}
				}
				singPath.add(d.vehicles.get(i).endDepot);
			}
			route.add(singPath);
		}
		if (solution.size() < 1)
			return cf.cloneVector(route);
		for (int i = 0; i < route.size(); i++) {
			if (solution.size() - 1 < i)
				break;
			else if (solution.get(i).get(0).id != route.get(i).get(0).id)
				solution.add(i, cf.singleCloneOrder(route.get(i)));
			else if (solution.get(i).size() < 3)
				solution.set(i, cf.singleCloneOrder(route.get(i)));
		}

		// solution.add(cf.singleCloneOrder(route.get(i)));
		return cf.cloneVector(solution);
	}

	HashSet<Order> infeasibleOrder = new HashSet<Order>();
	boolean globalInsertInfeasible = false;

	private ArrayList<ArrayList<Order>> infeasibleInsert(ArrayList<ArrayList<Order>> solution)
			throws InterruptedException, ExecutionException {
		globalInsertInfeasible = true;
		System.out.println("initial 0\t" + human(solution) + "\t" + con.humanU(infeasibleOrder) + "\t" + check);

		double temp = calcuNewBenfit(solution);
		globalInsertInfeasible = true;
		System.out.println("initial 1\t" + human(solution) + "\t" + con.humanU(infeasibleOrder) + "\t" + temp);
		infeasibleOrder.removeAll(d.startEndNodes);
		for (ArrayList<Order> path : solution) {
			path.removeAll(infeasibleOrder);
		}
		System.out.println("before\t" + humanLoc(solution) + "\t" + con.humanU(infeasibleOrder));
		insertChoose = 1;
		insert(solution, cf.cloneToArray(infeasibleOrder));
		globalInsertInfeasible = false;
		infeasibleOrder.clear();
		temp = calcuNewBenfit(solution);
		System.out.println("after 1\t" + humanLoc(solution) + "\t" + con.humanU(infeasibleOrder) + "\t" + temp);
		infeasibleOrder.clear();
		return solution;
	}

	private HashSet<Order> missOrder(ArrayList<ArrayList<Order>> route) {
		HashSet<Order> howMany = new HashSet<Order>(cf.deepCopySet(d.orders));
		howMany.removeAll(d.startEndNodes);
		for (ArrayList<Order> path : route) {
			for (Order node : path) {
				if (orders.contains(node) && !d.startEndNodes.contains(node))
					howMany.remove(node);
			}
		}
		return howMany;

	}

	private void removeDuplicate(ArrayList<ArrayList<Order>> route) {
		Order pre = null;
		int cnt = 0;
		for (ArrayList<Order> path : route) {
			pre = null;
			Iterator<Order> i = path.iterator();
			cnt = 0;
			while (i.hasNext()) {
				Order s = i.next(); // must be called before you can call
				// i.remove()
				if (pre != null && s.id == pre.id)
					i.remove();
				else if (pre != null && s != null && d.dummyOrderMap != null && d.dummyOrderMap.values().contains(s)
						&& s.location == pre.location)
					i.remove();

				pre = s;
				cnt++;
			}

			if (path.size() > 3 && path.get(path.size() - 1).equals(path.get(path.size() - 2)))
				path.remove(path.size() - 1);
		}

		Iterator<ArrayList<Order>> iter = route.iterator();
		while (iter.hasNext()) {
			ArrayList<Order> path = iter.next();
			if (served(path) < 1)
				iter.remove();
		}
	}

	private int served(ArrayList<Order> path) {
		int sum = 0;
		for (Order r : path) {
			if (!con.startEndNodes.contains(r))
				sum += 1;
		}
		return sum;
	}

	private ArrayList<ArrayList<Order>> initilize(ArrayList<ArrayList<Order>> route) {
		postPone = new double[vehicles.size()];
		needMiddleAdd = new double[vehicles.size()][2];
		route.clear();
		int multiTripCnt = 0;
		int maxCnt = 0;
		for (int i = 0; i < d.totalCar; i++) {
			ArrayList<Order> singPath = new ArrayList<Order>();
			multiTripCnt = Math.max(lsps.get(0).multiTripCnt, 2);
			if (!vehicles.get(i).haveTrip) {
				if (d.lspMiddleDepotOrder.get(vehicles.get(i).originLSP).size() == 0) {
					while (multiTripCnt > 0) {
						singPath.add(d.vehicles.get(i).depot);
						multiTripCnt--;
					}
				} else {
					singPath.add(d.vehicles.get(i).depot);
					while (multiTripCnt > 0 && maxCnt < 2 * d.vehicles.size()) {
						for (Order u : d.lspMiddleDepotOrder.get(vehicles.get(i).originLSP)) {
							singPath.add(u);
							multiTripCnt -= 1;
						}
						maxCnt++;
					}
				}
				singPath.add(d.vehicles.get(i).endDepot);
			}
			route.add(singPath);
		}

		if (route.size() > 0) {
			HashSet<Order> removeSet = new HashSet<Order>(d.orders);
			removeSet.remove(null);
			removeSet.removeAll(d.startEndNodes);
			if (d.collectionDummyOrders != null)
				removeSet.removeAll(d.collectionDummyOrders);

			List<Order> data = new ArrayList<Order>();
			for (Order item : removeSet)
				data.add(item);

			// Sort order here,
			Collections.sort(data, new Comparator<Order>() {
				@Override
				public int compare(Order o1, Order o2) {
					Integer o1_uppertime = o1.upperTime;
					Integer o2_uppertime = o2.upperTime;
					return o1_uppertime.compareTo(o2_uppertime);
				}
			});

			if (orders.size() - 2 * vehicles.size() > 10) {
				KMedoids Kmedoids = new KMedoids(d.vehicles().size() * 2, 10000, con.disMatrix);

				List<ArrayList<Integer>> assignment = new ArrayList<ArrayList<Integer>>();
				Kmedoids.cluster(data);

				int insertPos = 0;
				double lowestCost = 1e11;
				double currentCost = 1e11;
				ArrayList<ArrayList<Order>> initialRoute = new ArrayList<ArrayList<Order>>();
				ArrayList<ArrayList<Order>> bestRoute = new ArrayList<ArrayList<Order>>();
				ArrayList<ArrayList<Order>> storedRoute = new ArrayList<ArrayList<Order>>();

				for (ArrayList<Integer> remove : assignment) {
					Collections.sort(remove, new Comparator<Integer>() {
						@Override
						public int compare(Integer o1, Integer o2) {
							return orders.get(o1).upperTime - orders.get(o2).upperTime;
						}
					});
				}

				for (ArrayList<Integer> remove : assignment) {
					initialRoute = cf.cloneVector(route);
					storedRoute = cf.cloneVector(route);
					currentCost = 1e11;
					lowestCost = 1e11;
					for (int cnt = 0; cnt < d.vehicles().size(); cnt++) {
						for (Integer chooseNode : remove) {
							insertPos = singleVehiclesInsertion(initialRoute, cnt, orders.get(chooseNode));
							if (insertPos > 0)
								initialRoute.get(cnt).add(insertPos, orders.get(chooseNode));
						}

						currentCost = calcuResult(new PathSolution(initialRoute.get(cnt), cnt), con.yita2, 0, 0, 0);
						if (currentCost < lowestCost) {
							bestRoute = cf.cloneVector(initialRoute);
							lowestCost = currentCost;
						}
						initialRoute = cf.cloneVector(storedRoute);
					}
					route = cf.cloneVector(bestRoute);
				}
			}
		}
		System.out.println("initial" + human(route));

		return route;
	}

	private ArrayList<Order> remove(ArrayList<ArrayList<Order>> route, int removeChoose) {
		// some nodes remove will be insert and same location, then, no need to
		// remove
		// out
		Random rand = new Random();
		HashSet<Order> removeSet = new HashSet<Order>(d.orders);
		removeSet.remove(null);

		int numberRemove = Math.max(2, (int) ((0.05 + rand.nextDouble() * 0.10) * (d.orders.size())));// -d.staticNodes.size())));
		// System.out.println("numberRemove\t" + numberRemove+"\t"+con.stops());
		int cnt = 0;
		List<Order> allServedNode = new ArrayList<Order>();
		ArrayList<ArrayList<Order>> splitPath = new ArrayList<ArrayList<Order>>();
		for (ArrayList<Order> path : route)
			removeSet.removeAll(path);
		int id = 0;
		switch (removeChoose) {
		case 0:
			allServedNode = cf.mergeAll(route);
			Collections.shuffle(allServedNode);
			if (allServedNode.size() > 1)
				removeSet.addAll(allServedNode.subList(0, Math.min(numberRemove, allServedNode.size())));
			break;
		case 1:
			// remove shopping mall
			int chooseLocation = 0;
			for (int i = 0; i < Math.max(2, (d.orders.size() - d.vehicles.size() * 2) / d.locations.length); i++) {
				chooseLocation = rand.nextInt(locations.length);
				for (ArrayList<Order> path : route) {
					for (Order o : path) {
						if (o.orderLocation == chooseLocation)
							removeSet.add(o);
					}
				}
			}
			break;
		case 2:
			// long trip one
			currentSolutionMultiTrips = new ArrayList<Trip>();
			for (int i = 0; i < route.size(); i++) {
				List<Integer> singlePath = new ArrayList<Integer>();
				for (int j = 1; j < route.get(i).size() - 1; j++) {

					if (!d.startEndNodes.contains(route.get(i).get(j)))
						singlePath.add(route.get(i).get(j).id);
					else {
						currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
						singlePath = new ArrayList<Integer>();
					}
				}
				currentSolutionMultiTrips.add(new Trip(i + 1, singlePath));
			}
			removeSet.addAll(splitAll(d.staticPath));
			break;
		case 3:
			// long route one
			int averageSize = calculateAverage(route);
			for (int i = 0; i < route.size(); i++) {
				if (route.get(i).size() > averageSize) {
					removeSet.addAll(
							route.get(i).subList((route.get(i).size() - averageSize) / 2, route.get(i).size() / 2));
				}
			}
			break;
		case 4:
			globalInsertInfeasible = true;
			calcuNewBenfit(route);
			infeasibleOrder.removeAll(d.startEndNodes);
			for (Order node : infeasibleOrder) {
				if (rand.nextBoolean())
					removeSet.add(node);
			}

			globalInsertInfeasible = false;
			infeasibleOrder.clear();
			break;
		case 5:
			int chooseGroup = rand.nextInt(d.timeWindowGroup.size());
			int size = d.timeWindowGroup.get(chooseGroup).size();
			for (int i = 0; i < Math.min(numberRemove, size / 3); i++) {
				id = rand.nextInt(size);
				for (ArrayList<Order> path : route) {
					for (Order o : path) {
						if (o.id == id)
							removeSet.add(o);
					}
				}
			}
			break;
		default:
			break;
		}
		cf.removeDepot(removeSet, d.startEndNodes);
		ArrayList<Order> newSet = cf.cloneToArray(removeSet);
		// cf.removeDepot(newSet, con.totalCar);
		return newSet;
	}

	private int calculateAverage(ArrayList<ArrayList<Order>> route) {
		int averageSize = 0;
		for (ArrayList<Order> path : route) {
			averageSize += path.size();
		}
		return (int) (averageSize / (1e-3 + route.size()));
	}

	private ArrayList<Order> splitAll(Set<List<Order>> staticPath) {
		ArrayList<Order> splitPath = new ArrayList<Order>();
		for (Integer u : d.trips.keySet()) {
			for (Trip t : d.trips.get(u)) {
				if (staticPath.contains(t.path) && rand.nextDouble() > 0.10)
					continue;
				if (rand.nextDouble() < 0.18) {
					splitPath.addAll(translate(t.path));
					splitPath.add(t.Start);
					splitPath.add(t.End);
				}
			}
		}

		for (Trip t : currentSolutionMultiTrips) {
			// if (t.path.size() <= 2)
			// continue;

			if (rand.nextDouble() < 0.5 && (t.path.size()) < 3) {
				splitPath.addAll(translate(t.path));
			}
			int chooseSpliPos = (int) (7 + Math.random() * 2);//
			if (t.path.size() > chooseSpliPos) {
				List<Order> newOrder = translate(t.path);
				Collections.shuffle(newOrder);

				splitPath.addAll(newOrder.subList(chooseSpliPos, newOrder.size()));
			}
		}

		return splitPath;
	}

	List<Order> translate(List<Integer> path) {
		List<Order> newPath = new ArrayList<Order>();
		for (Integer node : path)
			newPath.add(orders.get(node));
		return newPath;
	}

	public void setWeight() {
		removeWeigt = new double[removeSize];
		insertWeigt = new double[insertSize];
		double uv = 1.0 / (removeSize + 0.0D);
		double xy = 1.0 / (insertSize + 0.0D);
		for (int p = 0; p < removeSize; p++) {
			removeWeigt[p] = uv;
		}
		for (int p = 0; p < insertSize; p++) {
			insertWeigt[p] = xy;
		}
	}

	public double calcuTemp(double u, double newbenfit, double oldbenfit) {
		T0 *= u;
		double probability = Math.exp((oldbenfit - newbenfit) / T0);
		return probability;
	}

	private void insert(ArrayList<ArrayList<Order>> route, List<Order> removeSet)
			throws InterruptedException, ExecutionException {
		removeSet.removeAll(d.startEndNodes);

		// insertChoose = 0;
		Order chooseNode = null;
		int[] insertArray = new int[2];
		boolean wrong = false;

		// if (globalCnt > maxCnt / 2)
		switch (insertChoose) {
		case 0:
			for (ArrayList<Order> path : route)
				path.removeAll(removeSet);

			for (int index = 0; index < removeSet.size(); index++) {
				chooseNode = removeSet.get(index);
				wrong = false;
				for (ArrayList<Order> path : route) {
					if (path.contains(chooseNode))
						wrong = true;
				}
				if (!wrong) {
					insertArray = multiVehiclesInsertion(route, chooseNode);
					if (insertArray[1] > 0)
						route.get(insertArray[0]).add(insertArray[1], chooseNode);
				}
			}
			break;
		case 1:
			Collections.shuffle(removeSet);
			chooseNode = null;

			for (int index = 0; index < removeSet.size(); index++) {
				chooseNode = removeSet.get(index);
				ArrayList<Order> temp = new ArrayList<Order>();
				temp.add(chooseNode);
				for (ArrayList<Order> path : route)
					path.removeAll(temp);
				wrong = false;
				for (ArrayList<Order> path : route) {
					if (path.contains(chooseNode))
						wrong = true;
				}
				if (!wrong) {
					insertArray = multiVehiclesInsertion(route, chooseNode);
					if (insertArray[1] > 0)
						route.get(insertArray[0]).add(insertArray[1], chooseNode);
				}
			}
			break;
		case 2:
			// insert to the shortest max time one
			Collections.shuffle(removeSet);
			chooseNode = null;

			for (int index = 0; index < removeSet.size(); index++) {
				chooseNode = removeSet.get(index);
				ArrayList<Order> temp = new ArrayList<Order>();
				temp.add(chooseNode);
				for (ArrayList<Order> path : route)
					path.removeAll(temp);
				wrong = false;
				for (ArrayList<Order> path : route) {
					if (path.contains(chooseNode))
						wrong = true;
				}
				if (!wrong) {
					insertArray = multiVehicleMinSpanInsertion(route, chooseNode);
					if (insertArray[1] > 0)
						route.get(insertArray[0]).add(insertArray[1], chooseNode);
				}
			}
			break;
		// insert to simple calculation
		case 3:
			Collections.shuffle(removeSet);
			chooseNode = null;

			for (int index = 0; index < removeSet.size(); index++) {
				chooseNode = removeSet.get(index);
				ArrayList<Order> temp = new ArrayList<Order>();
				temp.add(chooseNode);
				for (ArrayList<Order> path : route)
					path.removeAll(temp);
				wrong = false;
				for (ArrayList<Order> path : route) {
					if (path.contains(chooseNode))
						wrong = true;
				}
				if (!wrong) {
					insertArray = multiVehicleInsertionNoLunchStartConsider(route, chooseNode);
					if (insertArray[1] > 0)
						route.get(insertArray[0]).add(insertArray[1], chooseNode);
				}
			}
			break;

		default:
			break;
		}
		// System.out.println("end of insert function: "+humanLoc(route));
	}

	private int[] multiVehicleInsertionNoLunchStartConsider(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		int[] minarray = new int[2];
		minarray[1] = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		needMiddleAdd = new double[vehicles.size()][2];
		postPone = new double[vehicles.size()];
		int start = 0;
		for (int i = 0; i < route.size(); i++) {
			if (chooseNode.specialVehicle > -1 && Math.abs(chooseNode.specialVehicle - i) > 1e-3)
				continue;
			// System.out.println("checking in veh "+i);
			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			if (increa_comp > 1e5 / 2)
				continue;
			globalCost = 0;
			start = 0;
			// useless depot service time equal to 1
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			for (int m = start; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;

				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}

				if (chooseNode.orderLocation() != route.get(i).get(m + 1).orderLocation()
						&& route.get(i).get(m + 1).upperTime - chooseNode.lowerTime < chooseNode.serviceTime()
								+ con.disMatrix[chooseNode.orderLocation()][route.get(i).get(m + 1).orderLocation()])
					continue;

				if (route.get(i).get(m).orderLocation() != chooseNode.orderLocation()
						&& chooseNode.upperTime - route.get(i).get(m).lowerTime < route.get(i).get(m).serviceTime()
								+ con.disMatrix[route.get(i).get(m).orderLocation()][chooseNode.orderLocation()])
					continue;

				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp + r.nextDouble() * 1;
				if (finalOptimizeFlag) {
					if (benefi < minBeneif && volumeFeasible) {
						minBeneif = benefi;
						minarray[0] = i;
						minarray[1] = m + 1;
					}
				} else {
					if (benefi < minBeneif) {
						minBeneif = benefi;
						minarray[0] = i;
						minarray[1] = m + 1;

					}
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;

	}

	private int singleVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, int choosedVehicle, Order chooseNode) {
		int minInsertPos = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e12;
		double globalCost = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);

		increa_comp = calcuResult(new PathSolution(route.get(choosedVehicle), choosedVehicle), con.yita2, 0, 0, 0);// +
		globalCost = 0;
		int start = 0;
		// useless depot service time equal to 1
		for (int j = 1; j < route.get(choosedVehicle).size() - 1; j++) {
			if (d.startEndNodes.contains(route.get(choosedVehicle).get(j))
					&& route.get(choosedVehicle).get(j).location.id != route.get(choosedVehicle).get(0).location.id)
				start++;
			else
				break;
		}
		// System.out.println(human(route));
		for (int m = start; m < route.get(choosedVehicle).size() - 1; m++) {
			indexNextDepot = 0;
			if (tripSize(route.get(choosedVehicle), m + 1) > vehicles.get(choosedVehicle).lsp.maxDrop - 1) {
				// if (indexNextDepot > m + 1 && indexNextDepot <
				// route.get(choosedVehicle).size() - 1)
				// m = indexNextDepot - 1;
				continue;
			}

			if (con.disMatrix[chooseNode.orderLocation()][route.get(choosedVehicle).get(m + 1).orderLocation()] > 1e-3
					&& route.get(choosedVehicle).get(m + 1).upperTime - chooseNode.lowerTime < chooseNode.serviceTime()
							+ con.disMatrix[chooseNode.orderLocation()][route.get(choosedVehicle).get(m + 1)
									.orderLocation()])
				continue;

			if (con.disMatrix[route.get(choosedVehicle).get(m).orderLocation()][chooseNode.orderLocation()] > 1e-3
					&& chooseNode.upperTime - route.get(choosedVehicle).get(m).lowerTime < route.get(choosedVehicle)
							.get(m).serviceTime()
							+ con.disMatrix[route.get(choosedVehicle).get(m).orderLocation()][chooseNode
									.orderLocation()])
				continue;

			route.get(choosedVehicle).add(m + 1, chooseNode);
			globalCost = calcuResult(new PathSolution(route.get(choosedVehicle), choosedVehicle), con.yita2, 0, 0, 0);
			benefi = globalCost - increa_comp + r.nextDouble() * 1;

			if (benefi < minBeneif) {
				minBeneif = benefi;
				minInsertPos = m + 1;
			}
			route.get(choosedVehicle).remove(m + 1);
		}

		return minInsertPos;
	}

	public int[] multiVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		int[] minarray = new int[2];
		minarray[1] = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e9;
		double globalCost = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		needMiddleAdd = new double[vehicles.size()][2];
		postPone = new double[vehicles.size()];
		int start = 0;
		for (int i = 0; i < route.size(); i++) {
			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			if (increa_comp > 1e5 / 2)
				continue;
			globalCost = 0;
			start = 0;
			// useless depot service time equal to 1
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			for (int m = start; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;

				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}

				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp + r.nextDouble() * 1;

				if (benefi < minBeneif) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	public int[] multiVehiclesInsertionDifferVehicle(ArrayList<ArrayList<Order>> originRoute,
			ArrayList<ArrayList<Order>> newRoute, Order chooseNode, int spcificK) {
		int[] minarray = new int[2];
		minarray[1] = -1;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		int initialSimilarity = 10000;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		needMiddleAdd = new double[vehicles.size()][2];
		postPone = new double[vehicles.size()];
		int start = 0;
		for (int i = 0; i < route.size(); i++) {
			if (Math.abs(spcificK - i) <= 1e-3)
				continue;
			if (chooseNode.specialVehicle > -1 && Math.abs(chooseNode.specialVehicle - i) > 1e-3)
				continue;

			initialSimilarity = calculateSimilarity(originRoute.get(spcificK), originRoute.get(i));
			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			globalCost = 0;
			// System.out.println(human(route));
			start = 0;
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			for (int m = start; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;

				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}
				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost - increa_comp + r.nextDouble() * 1;

				if ((needSmooth && benefi < minBeneif && globalCost < increa_comp * fitnessParaThreshold
						&& calcuAdjustTravelTime(route) < calcuAdjustTravelTime(orginRoute) + travelTimeParaThreshold
						&& calculateSimilarity(route.get(spcificK), route.get(i)) < initialSimilarity)) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	int calculateSimilarity(ArrayList<Order> path1, ArrayList<Order> path2) {
		// System.out.println(" similarity threshold "+similarityThreshold);
		int similarity = 0;
		for (Order nodeI : path1)
			for (Order nodeJ : path2) {
				if (d.startEndNodes.contains(nodeI) && d.startEndNodes.contains(nodeJ))
					continue;
				if (d.con.disMatrix[nodeI.orderLocation()][nodeJ.orderLocation()] <= similarityThreshold)
					similarity++;
			}
		return similarity;
	}

	double calcuAdjustTravelTime(ArrayList<ArrayList<Order>> feasiusedRouteWhole) {
		double sum = 0;
		if (needSmooth) {
			for (ArrayList<Order> path : feasiusedRouteWhole) {
				sum += calculateTime(path, 0, path.size() - 1);
			}
		}
		return sum;
	}

	public int[] multiVehiclesInsertion(ArrayList<ArrayList<Order>> newRoute, Order pickNode, Order dropNode) {
		int[] minarray = new int[3];
		minarray[1] = -1;
		double thisVehicleWaitMax = 0;
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		int start = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		needMiddleAdd = new double[vehicles.size()][2];
		postPone = new double[vehicles.size()];
		for (int i = 0; i < route.size(); i++) {
			if (dropNode.specialVehicle > -1 && Math.abs(dropNode.specialVehicle - i) > 1e-3)
				continue;
			increa_comp = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
			globalCost = 0;
			start = 0;
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			for (int m = start; m < route.get(i).size() - 1; m++)
				for (int n = m + 1; n < route.get(i).size(); n++) {
					indexNextDepot = m + 1;
					route.get(i).add(m + 1, pickNode);
					if (tripSize(route.get(i), n + 1) > vehicles.get(i).lsp.maxDrop - 1) {
						if (indexNextDepot > n + 1 && indexNextDepot < route.get(i).size() - 1)
							n = indexNextDepot - 1;
						route.get(i).remove(m + 1);
						continue;
					}

					route.get(i).add(n + 1, dropNode);
					globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
					benefi = globalCost - increa_comp;

					if (benefi < minBeneif) {
						minBeneif = benefi;
						minarray[0] = i;
						minarray[1] = m + 1;
						minarray[2] = n + 1;
					}
					route.get(i).remove(n + 1);
					route.get(i).remove(m + 1);
				}

		}
		return minarray;
	}

	private int[] multiVehicleSmallestInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		int[] minarray = new int[2];
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		int start = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		for (int i = 0; i < route.size(); i++) {
			if (notAllowedVehicle.contains(i) || !chooseNode.vehiclesIndex.contains(i + 1)) {// ||
																								// route.get(i).size()
																								// -
																								// 3.5
																								// >=
																								// 1.5
																								// *
																								// d.stops
																								// /
																								// d.totalCar
				continue;
			}
			start = 0;
			// useless depot service time equal to 1
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			// System.out.println(human(route));
			for (int m = start; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;
				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}
				route.get(i).add(m + 1, chooseNode);
				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				benefi = globalCost + r.nextDouble() * 1;

				if (benefi < minBeneif) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	private int[] multiVehicleMinSpanInsertion(ArrayList<ArrayList<Order>> newRoute, Order chooseNode) {
		// System.out.println("in mvi: "+chooseNode.orderLocation()+" "+globalCnt);
		// System.out.println("in mvi: "+humanLoc(newRoute));
		int[] minarray = new int[2];
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;
		int start = 0;
		ArrayList<ArrayList<Order>> route = cf.cloneVector(newRoute);
		for (int i = 0; i < route.size(); i++) {
			if (notAllowedVehicle.contains(i) || !chooseNode.vehiclesIndex.contains(i + 1)) {
				continue;
			}
			// System.out.println(human(route));
			start = 0;
			// useless depot service time equal to 1
			for (int j = 1; j < route.get(i).size() - 1; j++) {
				if (d.startEndNodes.contains(route.get(i).get(j))
						&& route.get(i).get(j).location.id != route.get(i).get(0).location.id)
					start++;
				else
					break;
			}
			for (int m = start; m < route.get(i).size() - 1; m++) {
				indexNextDepot = 0;
				if (tripSize(route.get(i), m + 1) > vehicles.get(i).lsp.maxDrop - 1) {
					if (indexNextDepot > m + 1 && indexNextDepot < route.get(i).size() - 1)
						m = indexNextDepot - 1;
					continue;
				}

				globalCost = calcuResult(new PathSolution(route.get(i), i), con.yita2, 0, 0, 0);
				if (globalCost > 1e5 / 2 || maxViolate > 360) {
					// if(chooseNode.orderLocation==16 && globalCnt <=10) {
					// System.out.println("here "+maxViolate+" "+globalCost);
					// }
					continue;
				}

				if (chooseNode.orderLocation() != route.get(i).get(m + 1).orderLocation()
						&& route.get(i).get(m + 1).upperTime - chooseNode.lowerTime < chooseNode.serviceTime()
								+ con.disMatrix[chooseNode.orderLocation()][route.get(i).get(m + 1).orderLocation()])
					continue;

				if (route.get(i).get(m).orderLocation() != chooseNode.orderLocation()
						&& chooseNode.upperTime - route.get(i).get(m).lowerTime < route.get(i).get(m).serviceTime()
								+ con.disMatrix[route.get(i).get(m).orderLocation()][chooseNode.orderLocation()])
					continue;

				route.get(i).add(m + 1, chooseNode);
				benefi = globalEndTime;

				if (benefi < minBeneif) {
					minBeneif = benefi;
					minarray[0] = i;
					minarray[1] = m + 1;
				}
				route.get(i).remove(m + 1);
			}
		}
		return minarray;
	}

	public int tripSize(ArrayList<Order> arrayList, int pos) {
		int size = arrayList.size();
		int cnt = 0;
		indexNextDepot = pos;
		int prelocation = arrayList.get(pos).orderLocation;

		if (!d.startEndNodes.contains(arrayList.get(pos))) {
			cnt = 1;
			for (int i = pos + 1; i < size - 1; i++) {
				if (d.startEndNodes.contains(arrayList.get(i))
						|| d.newOrdersCorresPickup.contains(arrayList.get(i).id)) {
					indexNextDepot = i;
					break;
				} else {
					if (arrayList.get(i).orderLocation != prelocation) {
						cnt++;
						prelocation = arrayList.get(i).orderLocation;
					}
				}
			}
		}
		int nextlocation = arrayList.get(pos).orderLocation;
		for (int i = pos - 1; i > 0; i--) {
			if (d.startEndNodes.contains(arrayList.get(i))) {
				break;
			} else {
				if (arrayList.get(i).orderLocation != nextlocation) {
					cnt++;
					nextlocation = arrayList.get(i).orderLocation;
				}
			}
		}
		return cnt;
	}

	private int tripSizeWhole(ArrayList<Order> arrayList, int k) {
		int size = arrayList.size();
		int cnt = 0;
		indexNextDepot = 0;
		int prelocation = arrayList.get(0).orderLocation;

		while (indexNextDepot < size - 1) {
			cnt = 0;
			for (int i = indexNextDepot + 1; i < size; i++) {
				if (d.startEndNodes.contains(arrayList.get(i))
						|| d.newOrdersCorresPickup.contains(arrayList.get(i).id)) {
					indexNextDepot = i;
					break;
				} else {
					if (arrayList.get(i).orderLocation != prelocation) {
						cnt++;
						prelocation = arrayList.get(i).orderLocation;
					}
				}
			}
			if (cnt > vehicles.get(k).lsp.maxDrop) {
				break;
			}
		}

		return Math.max(0, cnt - vehicles.get(k).lsp.maxDrop);
	}

	private void initial_2cnt() {
		cntRemove = new double[removeSize];
		scoreRemove = new double[removeSize];

		cntInsert = new double[insertSize];
		scoreInsert = new double[insertSize];
		for (int i = 0; i < removeSize; i++) {
			cntRemove[i] = 1.0D;
			scoreRemove[i] = 0.0D;
		}

		for (int i = 0; i < insertSize; i++) {
			cntInsert[i] = 1.0D;
			scoreInsert[i] = 0.0D;
		}
	}

	public void setRoutes(Vector<Integer> path) {
		startServTime = new int[con.stops() + con.totalCar() * 2 + 1];
	}

	public double calcuResultNoDistance(PathSolution pathSolution, double yita2) {
		boolean infeasible = false;
		int size = pathSolution.getPath().size();
		Order currentNode = pathSolution.getPath().get(0);
		Order nextNode = pathSolution.getPath().get(0);
		double totalTime = 0;
		double value = 0;
		double temp = 0;
		int k = pathSolution.getVehicleID();

		totalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime());

		for (int i = 1; i < size - 1; i++) {
			nextNode = pathSolution.getPath().get(i);
			if (orders.contains(nextNode) && con.nodeSetGroup.get(nextNode) != con.carSetGroup.get(k + 1)) {
				value = 1e10;
				break;
			}

			if (currentNode == nextNode) {
				continue;
			}
			temp = con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];

			totalTime += temp + currentNode.serviceTime();
			value += 10;

			currentNode = nextNode;

			if (totalTime < nextNode.lowerTime()) {
				totalTime = nextNode.lowerTime();
				value += nextNode.lowerTime() - totalTime;
			}

			if (totalTime > nextNode.upperTime()) {
				infeasible = true;
				if (totalTime > nextNode.upperTime()) {
					value += yita2 * (totalTime - nextNode.upperTime());
				} else
					value += lsps.get(vehicles.get(k).originLSP).parameter
							.getTimeVioCost(totalTime - nextNode.upperTime()) * (totalTime - nextNode.upperTime());
			}
		}
		return value;
	}

	public double calcuResult2(PathSolution pathSolution, double yita2, int pos, double totalTime, double value) {
		boolean feasible = true;
		int maxViolate = 0;
		if (!d.reSequenceFlagOn)
			feasible = calculateMultiDepotCapacityConstraint(pathSolution, pos);
		if (!feasible) {
			value = 1e9 + vioVolume * 100;
			volumeFeasible = false;
			// System.out.println("violation because of volume");
			return value;
		} else
			volumeFeasible = true;

		int size = pathSolution.getPath().size();
		Order currentNode = pathSolution.getPath().get(pos);
		Order nextNode = null, nodeK = null;
		double arrivalTime = 0;

		double temp = 0;
		int tempValue = 0;
		int k = pathSolution.getVehicleID();
		double totalDis = 0;
		double obj = 0;
		maxWait = 0;
		maxWaitPos = -1;
		totalTrip = 1;

		double tempTravelTime = 0;
		double initialTotalTime = 0;

		int tempNodeServiceTime = 0;
		int tempNextNodeServiceTime = 0;
		if (pos == 0) {
			initialTotalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime());
			totalTime = initialTotalTime;
		}

		arrivalTime = totalTime;
		boolean needWait = true;
		lunchFeasible = false;
		ArrayList<Integer> postponeTime = new ArrayList<Integer>();
		double waitTime = 0;
		double simpleTripDistance = 0;
		double tempDis = 0;
		needMiddleAdd[k] = new double[2];
		boolean CheckServiceTimeNoNeed = false;
		int start = 1;
		postPone[k] = 0;
		for (int j = 1; j < size - 1; j++) {
			if (d.startEndNodes.contains(pathSolution.getPath().get(j))
					&& pathSolution.getPath().get(j).location.id != pathSolution.getPath().get(0).location.id)
				start++;
			else
				break;
		}
		int upperWait = (int) 1e6;
		for (int i = pos + 1; i < size - 1; i++) {
			needWait = true;
			tempValue = 0;
			nextNode = pathSolution.getPath().get(i);
			nodeK = pathSolution.getPath().get(i + 1);

			tempNodeServiceTime = 0;
			tempNextNodeServiceTime = 0;
			temp = con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];

			if (i < size - 2) {
				tempDis = con.disMatrix[nextNode.orderLocation()][nodeK.orderLocation()];
				simpleTripDistance += tempDis;
			}

			tempTravelTime += temp;
			travelTime += temp;
			// (d.dynamicFlag &&
			CheckServiceTimeNoNeed = Math.abs(currentNode.orderLocation() - nextNode.orderLocation()) < 1e-3;
			CheckServiceTimeNoNeed = CheckServiceTimeNoNeed && d.dynamicFlag
					&& (d.startEndNodes.contains(nextNode) || d.newOrdersCorresPickup.contains(nextNode.id));
			if (i == 1 || (i > 1 && currentNode.orderLocation() != pathSolution.getPath().get(i - 2).orderLocation()))
				tempNodeServiceTime = currentNode.serviceTime();
			totalTime += temp + tempNodeServiceTime;
			initialTotalTime = totalTime;
			totalDis += con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
			travelDistance += con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
			arrivalTime = totalTime;
			if (nextNode.orderLocation != currentNode.orderLocation)
				tempNextNodeServiceTime = nextNode.serviceTime();
			if (d.dynamicFlag
					&& (d.startEndNodes.contains(nextNode) || d.newOrdersCorresPickup.contains(nextNode.id))) {
				// delete
			} else if (lsps.get(0).lunchLocationFlag && !d.startEndNodes.contains(nextNode)
					&& totalTime + tempNextNodeServiceTime >= locations[nextNode.orderLocation()].lunchStart
					&& totalTime <= locations[nextNode.orderLocation()].lunchEnd) {
				tempValue = (int) (locations[nextNode.orderLocation()].lunchEnd - totalTime);
				totalTime = locations[nextNode.orderLocation()].lunchEnd;
				if (tempValue >= 20) {
					lunchFeasible = true;
					lunchFeasibleCheckNeed = false;
				}
				// needWait = false;
			}

			if (d.startEndNodes.contains(nextNode)) {
				totalTrip += 1;
				tempDis = con.disMatrix[nextNode.orderLocation()][nodeK.orderLocation()];
				simpleTripDistance -= tempDis;
				tempDis = con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
				simpleTripDistance -= tempDis;

				if (vehicles.get(k).lsp.getMaxSimpleTripDistance() < simpleTripDistance)
					vehicles.get(k).lsp.setSimpleTripDistance(simpleTripDistance);

				simpleTripDistance = 0;
			}

			if (totalTime < nextNode.lowerTime()) {
				tempValue += (nextNode.lowerTime() - totalTime);
				totalTime = nextNode.lowerTime();
			}

			if (i == 1) {
				startTimeFirstOrder = (int) totalTime;
			}
			// if (con.slotsFalg) {
			// if (currentNode.orderLocation != nextNode.orderLocation &&
			// nextNode.location.type.equals("IMD")) {
			// tempValue = (int) Math.max(0,
			// con.splitSlot * Math.ceil(totalTime / (con.splitSlot + 0.0))
			// -
			// totalTime);
			// }
			// }

			if (totalTime > nextNode.upperTime()) {
				tempValue = 0;
				feasible = false;
				if (globalInsertInfeasible) {
					// if (!d.startEndNodes.contains(nextNode))
					infeasibleOrder.add(nextNode);
					System.out.println("adding infeasible order: " + nextNode.id);
					// else
					// infeasibleOrder.add(currentNode);
				}

				value += lsps.get(vehicles.get(k).originLSP).parameter.getTimeVioCost(totalTime - nextNode.upperTime());
				if (totalTime - nextNode.upperTime() > maxViolate)
					maxViolate = (int) (totalTime - nextNode.upperTime());
				totalDis += lsps.get(vehicles.get(k).originLSP).parameter
						.getTimeVioCost(totalTime - nextNode.upperTime());
			}

			if (i == start)
				postPone[k] = tempValue;
			if (nextNode.upperTime() - totalTime < upperWait)
				upperWait = (int) (nextNode.upperTime() - totalTime);

			if (needWait && tempValue > 0 && i > start) {
				postponeTime.add(Math.min(tempValue, upperWait));
			}

			if (maxWait < totalTime - initialTotalTime) {
				maxWait = tempValue;
				maxWaitPos = i;
				nextTripSize = size - 2 - i;
			}
			currentNode = nextNode;
			waitTime += tempValue;
		}

		if (postponeTime.size() > 0)
			postPone[k] = checkFirstThree(postPone[k], postponeTime, pathSolution, k, 2);

		int sizeTimeRecord = timeRecord.size();
		// if (!lunchFeasible && sizeTimeRecord > 0)
		// needMiddleAdd[k] = checkFeasibleLunchForward(pathSolution, 3, k);

		// if (!lunchFeasible) {
		// needMiddleAdd[k] = checkFeasibleLunchBackward(pathSolution, 30, k);//
		// start
		// 30 minutes earlier
		// }

		arrivalTime = totalTime;
		arrivalTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()]
				+ tempNextNodeServiceTime;
		tempTravelTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
				.orderLocation()];
		travelTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()];

		if (arrivalTime > vehicles.get(k).truckUpperTime()) {
			feasible = false;
			// System.out.println("debug "+arrivalTime+"
			// "+vehicles.get(k).truckUpperTime()+" "+k+" "+feasible);
			value += lsps.get(vehicles.get(k).originLSP).parameter
					.getTimeVioCost(arrivalTime - vehicles.get(k).truckUpperTime());
			totalDis += lsps.get(vehicles.get(k).originLSP).parameter
					.getTimeVioCost(arrivalTime - vehicles.get(k).truckUpperTime());
		}
		globalEndTime = arrivalTime;

		if (globalEndTime > vehicles.get(k).lsp.getMaxTime())
			vehicles.get(k).lsp.setMaxTime(globalEndTime);// timeSpan
		if (globalEndTime < vehicles.get(k).lsp.getMinTime())
			vehicles.get(k).lsp.setMinTime(globalEndTime);

		// System.err.println("k\t" + k + "\t" + vehicles.get(k).originLSP);
		value += lsps.get(vehicles.get(k).originLSP).parameter.getTravelWeight() * tempTravelTime;

		travelDistance += con.disMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
				.orderLocation()];
		totalDis += con.disMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()];
		if (con.objFlag == 0)
			obj = value;
		if (con.objFlag == 1)
			obj = totalDis;

		waitTime -= postPone[k] + needMiddleAdd[k][1];
		obj += 1e5 * tripSizeWhole(pathSolution.getPath(), k);
		// if (!feasible)
		// routesColumn.add(new Column(pathSolution.getPath(), value));
		return obj;
	}

	public double calcuResult(PathSolution pathSolution, double yita2, int pos, double totalTime, double value) {
		boolean feasible = true;
		double travelDis = travelDistance;
		double waitTime = 0;
		int k = pathSolution.getVehicleID();
		needMiddleAdd[k] = new double[2];
		postPone[k] = 0;
		ArrayList<Integer> postponeTime = new ArrayList<Integer>();
		feasible = calculateMultiDepotCapacityConstraint(pathSolution, pos);
		if (!feasible) {
			value = 1e9 + vioVolume * 100;
			volumeFeasible = false;
			check = false;
			// System.out.println("violation because of volume");
			return value;
		} else
			volumeFeasible = true;

		int size = pathSolution.getPath().size();
		Order currentNode = pathSolution.getPath().get(pos);
		Order nextNode = null, nodeK = null;
		// System.out.println("Current order location: "+currentNode.id);
		if (currentNode.orderLocation == 5) {
			System.out.println("Found mising order");
		}

		double temp = 0;

		double obj = 0;
		maxWait = 0;
		maxWaitPos = -1;
		totalTrip = 1;
		double initialTotalTime = 0;
		double tempValue = 0;
		int tempTravelTime = 0;
		int tempNodeServiceTime = 0;
		if (pos == 0) {
			initialTotalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime());
			totalTime = initialTotalTime;
		}
		int upperWait = (int) 1e6;

		int start = 1;

		for (int j = 1; j < size - 1; j++) {
			if (d.startEndNodes.contains(pathSolution.getPath().get(j))
					&& pathSolution.getPath().get(j).location.id != pathSolution.getPath().get(0).location.id)
				start++;
			else
				break;
		}

		for (int i = pos + 1; i < size - 1; i++) {
			tempValue = 0;
			nextNode = pathSolution.getPath().get(i);
			nodeK = pathSolution.getPath().get(i + 1);

			tempNodeServiceTime = 0;
			temp = con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];

			tempTravelTime += temp;
			travelTime += temp;
			travelDistance += con.disMatrix[currentNode.orderLocation()][nextNode.orderLocation()];
			if (i == 1 || (i > 1 && currentNode.orderLocation() != nextNode.orderLocation()))
				tempNodeServiceTime = currentNode.serviceTime();
			totalTime += temp + tempNodeServiceTime;

			if (pathSolution.getPath().get(0).serviceTime > 2 && !d.startEndNodes.contains(nextNode)
					&& totalTime + tempNodeServiceTime > locations[nextNode.orderLocation()].lunchStart
					&& totalTime < locations[nextNode.orderLocation()].lunchEnd) {
				// tempValue = (int) (locations[nextNode.orderLocation()].lunchEnd - totalTime);
				totalTime = locations[nextNode.orderLocation()].lunchEnd;
			}

			if (d.startEndNodes.contains(nextNode)) {
				totalTrip += 1;
			}

			if (totalTime < nextNode.lowerTime()) {
				tempValue = nextNode.lowerTime() - totalTime;
				totalTime = nextNode.lowerTime();
			}
			if (i == start && globalCnt > 14500) {
				postPone[k] = tempValue;
			}

			if (totalTime > nextNode.upperTime()) {
				if (totalTime > nextNode.upperTime() + 120)
					check = false;

				value += lsps.get(vehicles.get(k).originLSP).parameter.getTimeVioCost(totalTime - nextNode.upperTime());
				travelDistance += lsps.get(vehicles.get(k).originLSP).parameter
						.getTimeVioCost(totalTime - nextNode.upperTime());
				infeasibleOrder.add(nextNode);
				// System.out.println("Adding nextnode: "+nextNode.id);
			} else if (nextNode.upperTime() - totalTime < upperWait)
				upperWait = (int) (nextNode.upperTime() - totalTime);

			if (tempValue > 0 && i > start) {
				postponeTime.add((int) Math.min(tempValue, upperWait));
			}
			currentNode = nextNode;
			waitTime += tempValue;
		}

		totalTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()]
				+ currentNode.serviceTime;
		tempTravelTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
				.orderLocation()];
		travelTime += con.timeMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1).orderLocation()];

		if (totalTime > vehicles.get(k).truckUpperTime()) {
			if (totalTime > vehicles.get(k).truckUpperTime() + 120)
				check = false;
			value += lsps.get(vehicles.get(k).originLSP).parameter
					.getTimeVioCost(totalTime - vehicles.get(k).truckUpperTime());

			travelDistance += lsps.get(vehicles.get(k).originLSP).parameter
					.getTimeVioCost(totalTime - vehicles.get(k).truckUpperTime());
		}
		globalEndTime = totalTime;

		travelDistance += con.disMatrix[currentNode.orderLocation()][pathSolution.getPath().get(size - 1)
				.orderLocation()];
		value += lsps.get(vehicles.get(k).originLSP).parameter.getMaxSpanWeight()
				* (globalEndTime - vehicles.get(k).lsp.getMaxTime());

		if (d.con.objFlag == 0)
			obj = value + lsps.get(0).parameter.getTravelWeight() * tempTravelTime;
		if (d.con.objFlag == 1)
			obj = value + lsps.get(0).parameter.getTravelWeight() * (travelDistance - travelDis) * scalingFactor;
		obj += 1e5 * tripSizeWhole(pathSolution.getPath(), k);
		waitTime -= postPone[k];
		double waitPara = lsps.get(0).parameter.getWaitWeight();
		// if wait time large than 2 hours, increase the wait parameter
		if (waitTime >= 120)
			waitPara *= 20;
		else if (waitTime >= 60)
			waitPara *= 10;
		if (d.con.objFlag == 0) {
			obj += waitPara * waitTime;
		} else {
			obj += waitPara * waitTime;
		}
		if (!feasible)
			check = false;
		return obj;
	}

	private double[] checkFeasibleLunchBackward(PathSolution pathSolution, int lunchBreakAdd, int k) {
		double[] middleInsert = new double[2];
		int startAdd = 0;
		startAdd = (int) Math.max(-lunchBreakAdd, -postPone[k]);
		if (checkFeasible(startAdd, pathSolution.getPath(),
				Math.max(pathSolution.getPath().get(0).lowerTime, vehicles.get(k).truckLowerTime()) + postPone[k], 0, k,
				false) && lunchFeasible) {
			middleInsert[0] = 0;
			middleInsert[1] = startAdd;
		}
		return middleInsert;
	}

	private double[] checkFeasibleLunchForward(PathSolution pathSolution, int limit, int k) {
		HashMap<Integer, int[]> timeRecord = new HashMap<Integer, int[]>(this.timeRecord);
		limit = Math.min(limit, timeRecord.size());
		double postponeValue = 30;
		double[] middleInsert = new double[2];
		int cnt = 0;
		for (Integer startPos : timeRecord.keySet()) {
			if (cnt < limit && pathSolution.getPath().size() > startPos
					&& checkFeasible(postponeValue - timeRecord.get(startPos)[1], pathSolution.getPath(),
							timeRecord.get(startPos)[0], startPos, k, false)) {
				middleInsert[0] = startPos;
				middleInsert[1] = postponeValue - timeRecord.get(startPos)[1];
				lunchFeasible = true;
				break;
			}
			cnt++;
		}
		return middleInsert;
	}

	private double checkFirstThree(double postPone, ArrayList<Integer> postponeTime, PathSolution pathSolution, int k,
			int limit) {
		int postponeValue = (int) postPone;
		limit = Math.min(limit, postponeTime.size());
		for (int i = 0; i < limit; i++) {
			postponeValue += postponeTime.get(i);
			if (!checkFeasible(postponeValue, pathSolution.getPath(),
					Math.max(pathSolution.getPath().get(0).lowerTime, vehicles.get(k).truckLowerTime()), 0, k, true)) {
				// move backward
				postponeValue -= postponeTime.get(i);
				checkFeasible(postponeValue, pathSolution.getPath(),
						Math.max(pathSolution.getPath().get(0).lowerTime, vehicles.get(k).truckLowerTime()), 0, k,
						true);
				break;
			}
		}
		return postponeValue;
	}

	private boolean calculateMultiDepotCapacityConstraint(PathSolution pathSolution, int pos) {
		boolean feasible = true;
		int size = pathSolution.getPath().size();
		if (size < 2)
			return false;
		int k = pathSolution.getVehicleID();
		if (size == 3 && Math.abs(pathSolution.getPath().get(1).volume) > vehicles.get(k).capacity)
			return false;
		int contains = 0;
		int index = 0;
		int cnt = 0;

		// feasible = simpleCheck(pathSolution, pos);
		if (d.containsCollectionOrderFlag) {
			Set<Integer> newSet = con.cloneSet(d.collectionOrder.keySet());

			size = newSet.size();
			ArrayList<Integer> newPath = con.humanU(pathSolution.getPath());
			newSet.removeAll(newPath);
			index = 0;
			cnt = 0;

			contains = size - newSet.size();

			if (contains > 0) {
				for (Integer node : d.collectionOrder.keySet())
					orders.get(node).setVolume(d.collectionOrder.get(node)[0]);

				Set<Integer> nnSet = con.cloneSet(d.collectionOrder.keySet());
				nnSet.removeAll(newSet);
				ArrayList<Order> path = new ArrayList<Order>(con.cloneSingleVector(pathSolution.getPath()));
				ArrayList<Order> oldPath = new ArrayList<Order>(con.cloneSingleVector(pathSolution.getPath()));
				cnt = 1;
				for (Integer nodeIdx : nnSet) {
					index = newPath.indexOf(nodeIdx);
					path.add(index + cnt, d.dummyOrderMap.get(nodeIdx));
					cnt++;
				}
				pathSolution.setPath(path);

				feasible = deCoupleCheck(pathSolution, pos);
				pathSolution.setPath(con.cloneSingleVector(oldPath));

			}
		}
		if (contains < 1)
			feasible = deCoupleCheck(pathSolution, pos);
		for (Integer node : d.collectionOrder.keySet())
			orders.get(node).setVolume(d.collectionOrder.get(node)[0] + d.collectionOrder.get(node)[1]);
		return feasible;
	}

	private boolean deCoupleCheck(PathSolution pathSolution, int pos) {
		boolean feasible = true;
		int k = pathSolution.getVehicleID();
		vioVolume = 0;
		deductLoad(pathSolution, pos);
		double totalVolume = 0;
		int size = pathSolution.getPath().size();

		// add dummy order for collection order
		for (int i = pos; i < size - 1; i++) {
			totalVolume += forWardVolume.get(i);
			if (totalVolume > vehicles.get(k).capacity || totalVolume < -1e-3) {
				// System.out.println(totalVolume +"\t"+
				// vehicles.get(k).capacity+"\t"+forWardVolume.get(i));
				feasible = false;
				vioVolume += Math.max(0, (totalVolume - vehicles.get(k).capacity)) + Math.max(0, -totalVolume);
				// System.err.println(con.humanU(pathSolution.getPath())+"\t"+i+"\t"+totalVolume);
				break;
			}
		}
		return feasible;
	}

	private boolean simpleCheck(PathSolution pathSolution, int pos) {
		boolean feasible = true;
		ArrayList<Order> newPath = cf.arraylistCloneObject(pathSolution.getPath());
		int size = newPath.size();
		int k = pathSolution.getVehicleID();
		double totalVolume = 0;
		double posVolume = 0;
		double negVolume = 0;
		for (int i = 1; i < size - 1; i++) {
			if (d.collectionOrder.containsKey(newPath.get(i).id)) {
				posVolume += d.collectionOrder.get(newPath.get(i).id)[0];
				negVolume += d.collectionOrder.get(newPath.get(i).id)[1];
				// System.err.println(newPath.get(i).id+"\t"+
				// d.con.initialMap.get(newPath.get(i).id)+"\t"+newPath.get(i).volume()+"\t"+d.collectionOrder.get(newPath.get(i).id)[0]+"\t"+d.collectionOrder.get(newPath.get(i).id)[1]);
			}

			totalVolume += newPath.get(i).volume();
			if (d.startEndNodes.contains(newPath.get(i))) {
				totalVolume = 0;
				posVolume = 0;
				negVolume = 0;
			}
			if (Math.abs(totalVolume) > vehicles.get(k).capacity || posVolume > vehicles.get(k).capacity
					|| Math.abs(negVolume) > vehicles.get(k).capacity) {
				feasible = false;
				break;
			}

		}
		return feasible;
	}

	private void deductLoad(PathSolution pathSolution, int pos) {
		ArrayList<Order> newPath = cf.arraylistCloneObject(pathSolution.getPath());
		if (d.dynamicFlag) {
			// find the orders donot have a pickup, and record that pick up, put at
			// beginning of orders
			HashSet<Integer> unPicked = checkUnPicked(pathSolution.getPath());
			if (unPicked.size() > 0) {
				for (Integer node : unPicked) {
					for (Order nodeJ : d.startEndNodes) {
						if (Math.abs(nodeJ.orderLocation - node) < 1e-3) {
							newPath.add(0, nodeJ);
							break;
						}
					}
				}
			}
		}
		int size = newPath.size();
		Order currentNode = newPath.get(pos);
		Order nextNode = null;
		vioVolume = 0;
		forWardVolume = new HashMap<Integer, Double>();

		for (int i = pos; i < size - 1; i++) {
			forWardVolume.put(i, -newPath.get(i).volume());
		}

		for (int i = 0; i < size - 1; i++) {
			nextNode = newPath.get(i);
			if (d.startEndNodes.contains(nextNode)) {
				for (int j = i + 1; j < size - 1; j++) {
					currentNode = newPath.get(j);
					if (currentNode.newAddedOrderFlag)
						continue;
					if (d.startEndNodes.contains(currentNode)) {
						if (Math.abs(currentNode.orderLocation - nextNode.orderLocation) < 1e-3) {
							break;
						} else
							continue;
					} else if (currentNode.volume() > 0
							&& Math.abs(currentNode.pickupDepotLocation - nextNode.orderLocation) < 1e-3) {
						forWardVolume.put(i, forWardVolume.get(i) + currentNode.volume());
					}
				}
			}
		}
	}

	private HashSet<Integer> checkUnPicked(ArrayList<Order> path) {
		HashSet<Integer> storedDepot = new HashSet<Integer>();
		HashSet<Integer> unPicked = new HashSet<Integer>();
		Order nodeI = null;
		int i = 0;
		while (i < path.size() - 1) {
			nodeI = path.get(i);
			if (d.startEndNodes.contains(nodeI)) {
				storedDepot.add(nodeI.orderLocation);
			}
			for (int j = i + 1; j < path.size() - 1; j++) {
				// System.out.println("storedDepot\t"+storedDepot);
				if (d.startEndNodes.contains(path.get(j))) {
					i = j - 1;
					break;
				} else {
					if (!storedDepot.contains((Integer) path.get(j).pickupDepotLocation))
						unPicked.add(path.get(j).pickupDepotLocation);
				}
			}
			i++;
		}
		return unPicked;
	}

	public double calculateAfterPostPone(PathSolution pathSolution, double postPoneTime) {
		int size = pathSolution.getPath().size();
		Order currentNode = pathSolution.getPath().get(0);
		Order nextNode = null, nodeK = null;
		double temp = 0;
		double tempValue = 0;
		int k = pathSolution.getVehicleID();
		double initialTotalTime = 0;
		double thisVehicleWaitMax = 0;

		initialTotalTime = Math.max(currentNode.lowerTime(), vehicles.get(k).truckLowerTime()) + postPoneTime;
		totalTime = initialTotalTime;

		boolean needWait = true;
		int tempNodeServiceTime = 0;
		int tempNextNodeServiceTime = 0;
		for (int i = 1; i < size - 1; i++) {
			tempNodeServiceTime = 0;
			tempNextNodeServiceTime = 0;
			needWait = true;
			tempValue = 0;
			nextNode = pathSolution.getPath().get(i);
			nodeK = pathSolution.getPath().get(i + 1);
			temp = con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];

			if (i == 1 || (i > 1 && currentNode.orderLocation() != pathSolution.getPath().get(i - 2).orderLocation()))
				tempNodeServiceTime = currentNode.serviceTime();
			totalTime += temp + tempNodeServiceTime;
			initialTotalTime = totalTime;

			if (nextNode.orderLocation() != currentNode.orderLocation())
				tempNextNodeServiceTime = nextNode.serviceTime;

			if (lsps.get(0).lunchLocationFlag && !d.startEndNodes.contains(nextNode)
					&& totalTime + tempNextNodeServiceTime >= locations[nextNode.orderLocation()].lunchStart
					&& totalTime <= locations[nextNode.orderLocation()].lunchEnd) {
				tempValue = (locations[nextNode.orderLocation()].lunchEnd - totalTime);
				totalTime = locations[nextNode.orderLocation()].lunchEnd;
			}

			if (i == 1) {
				needWait = false;
			}

			if (totalTime < nextNode.lowerTime()) {
				tempValue += (nextNode.lowerTime() - totalTime);
				totalTime = nextNode.lowerTime();
			}

			if (needWait && tempValue > 0) {
				if (thisVehicleWaitMax < tempValue) {
					thisVehicleWaitMax = tempValue;
				}
			}
			currentNode = nextNode;
		}

		return thisVehicleWaitMax;
	}

	public boolean checkFeasible(double min, ArrayList<Order> pathSolution, double startTime, int startPos, int k,
			boolean needUpdate) {
		boolean feasible = true;

		int size = pathSolution.size();
		int tempNodeServiceTime = 0;
		Order currentNode = pathSolution.get(startPos);
		Order nextNode = null;

		double temp = 0;
		double totalTime = startTime + min;

		for (int i = startPos + 1; i < size - 1; i++) {
			nextNode = pathSolution.get(i);
			tempNodeServiceTime = 0;
			temp = con.timeMatrix[currentNode.orderLocation()][nextNode.orderLocation()];

			if (i == 1 || (i > 1 && currentNode.orderLocation() != nextNode.orderLocation()))
				tempNodeServiceTime = currentNode.serviceTime();
			totalTime += temp + tempNodeServiceTime;

			if (pathSolution.get(0).serviceTime > 2 && !d.startEndNodes.contains(nextNode)
					&& totalTime + tempNodeServiceTime > locations[nextNode.orderLocation()].lunchStart
					&& totalTime < locations[nextNode.orderLocation()].lunchEnd) {
				totalTime = locations[nextNode.orderLocation()].lunchEnd;
			}

			if (totalTime < nextNode.lowerTime()) {
				totalTime = nextNode.lowerTime();
			}
			currentNode = nextNode;

			if (totalTime > nextNode.upperTime()) {
				feasible = false;
				break;
			}

		}

		totalTime += currentNode.serviceTime
				+ con.timeMatrix[currentNode.orderLocation()][pathSolution.get(size - 1).orderLocation()];
		if (totalTime > vehicles.get(k).truckUpperTime())
			feasible = false;
		// System.out.println("Debug "+totalTime+ " "+vehicles.get(k).truckUpperTime()+"
		// "+k+" "+feasible);
		return feasible;
	}

	// may only useful for delivery plan optimization within same vehicle
	public ArrayList<Order> twoOptAlternate(ArrayList<Order> cities, int k) {
		ArrayList<Order> newTour;
		double bestDist = calcuResult(new PathSolution(cities, k), con.yita2, 0, 0, 0);
		double newDist;
		int swaps = 1;
		int improve = 0;
		long comparisons = 0;
		int[] depotIndex = new int[2];
		int start = 0;
		while (swaps != 0) { // loop until no improvements are made.
			swaps = 0;
			// initialise inner/outer loops avoiding adjacent calculations and
			// making use of
			// problem symmetry to half total comparisons.
			start = 1;
			for (int j = 1; j < cities.size() - 1; j++) {
				if (d.startEndNodes.contains(cities.get(j)))
					start++;
				else
					break;
			}
			for (int i = start; i < cities.size() - 2; i++) {
				for (int j = i + 1; j < cities.size() - 1; j++) {
					comparisons++;
					depotIndex = tripDepot(cf.listCloneObject(cities), i);
					if (j > depotIndex[1])
						continue;
					// check distance of line A,B + line C,D against A,C + B,D
					// if there is
					// improvement, call swap method.
					if (con.timeMatrix[cities.get(i).orderLocation()][cities.get(i + 1).orderLocation()]
							+ con.timeMatrix[cities.get(j + 1).orderLocation()][cities.get(j)
									.orderLocation()] >= (con.timeMatrix[cities.get(i).orderLocation()][cities
											.get(j + 1).orderLocation()]
											+ con.timeMatrix[cities.get(i - 1).orderLocation()][cities.get(j)
													.orderLocation()])) {
						newTour = swap(cities, i, j); // pass arraylist and 2
						// points to be swapped.

						newDist = calcuResult(new PathSolution(newTour, k), con.yita2, 0, 0, 0);

						if (newDist < bestDist) { // if the swap results in an
							// improved distance,
							// increment counters and
							// update distance/tour
							cities = cf.arraylistCloneObject(newTour);
							bestDist = newDist;
							swaps++;
							improve++;
						}
					}
				}
			}
		}
		return cities;
	}

	// @swap operator
	static ArrayList<Order> swap(ArrayList<Order> cities, int i, int j) {
		// conducts a 2 opt swap by inverting the order of the points between i
		// and j
		ArrayList<Order> newTour = new ArrayList<>();

		// take array up to first point i and add to newTour
		int size = cities.size();
		for (int c = 0; c <= i - 1; c++) {
			newTour.add(cities.get(c));
		}

		// invert order between 2 passed points i and j and add to newTour
		int dec = 0;
		for (int c = i; c <= j; c++) {
			newTour.add(cities.get(j - dec));
			dec++;
		}

		// append array from point j to end to newTour
		for (int c = j + 1; c < size; c++) {
			newTour.add(cities.get(c));
		}

		return newTour;
	}

	public void setGlobalCnt(double e) {
		this.globalCnt = (int) e;
	}

	public int onelspServeAll(ArrayList<ArrayList<Order>> route) {
		HashSet<Integer> howMany = new HashSet<Integer>();
		for (ArrayList<Order> path : route) {
			for (Order node : path) {
				if (orders.contains(node) && !d.startEndNodes.contains(node))
					howMany.add(node.id);
			}
		}
		return howMany.size();
	}

	// @not used currently
	public int[] multiVehiclesInsertionAviPos(ArrayList<Order> route, Order chooseNode, ArrayList<Integer> aviPos,
			int i) {
		int[] minarray = new int[2];
		double increa_comp = 0;
		double benefi = 0;
		double minBeneif = 1e10;
		double globalCost = 0;

		increa_comp = calcuResult(new PathSolution(route, i), con.yita2, 0, 0, 0);
		globalCost = 0;

		for (Integer m : aviPos) {
			route.add(m, chooseNode);
			globalCost = calcuResult(new PathSolution(route, i), con.yita2, 0, 0, 0);
			benefi = globalCost - increa_comp + r.nextDouble() * 1;

			if (benefi < minBeneif && globalCost < 1e6) {
				minBeneif = benefi;
				minarray[0] = i;
				minarray[1] = m;

			}
			route.remove(m);
		}

		return minarray;
	}
}
