package DynamicPreProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

public class DynamicRemainingTrips {
	//New and cancelled orders
	public int vehicleID;
	public int vehicleStartLoc;
	public ArrayList<Integer> orderIDs;
	public ArrayList<DynamicOrder> vehicleOrders;
	
	public HashMap<Integer, ArrayList<String>> remTrips = new HashMap<>();
	public HashMap<Integer, ArrayList<DynamicOrder>> tripOrderList = new HashMap<>();
	
	public DynamicRemainingTrips() {
		
	}
	
	public DynamicRemainingTrips(int vehicleID, ArrayList<Integer> orderIDs, int vehicleStartLoc) {
		this.vehicleID = vehicleID;
		this.orderIDs = orderIDs;
		this.vehicleStartLoc = vehicleStartLoc;
		//this.vehicleOrders = vehicleOrders;
		
	}
	
	public DynamicRemainingTrips(int vehicleID, ArrayList<Integer> orderIDs, ArrayList<DynamicOrder> vehicleOrders, int vehicleStartLoc) {
		this.vehicleID = vehicleID;
		this.orderIDs = orderIDs;
		this.vehicleStartLoc = vehicleStartLoc;
		this.vehicleOrders = vehicleOrders;
		
	}
	
	public void addRemainingTrip(int vehicleID, ArrayList<String> orderIDs, int vStartLoc) {
		this.remTrips.put(vehicleID, orderIDs);
		this.vehicleID = vehicleID;
		this.vehicleStartLoc = vStartLoc;
		
	}
	
	public HashMap<Integer, ArrayList<String>> getRemainingTrips() {
		return this.remTrips;
	}
	
	public void addRemainingTripOrders(int vehicleID, ArrayList<DynamicOrder> orders) {
		this.tripOrderList.put(vehicleID, orders);
		
	}
	
	public HashMap<Integer, ArrayList<DynamicOrder>> getRemainingTripOrders() {
		//System.out.println(tripOrderList.size());
		return this.tripOrderList;
	}
	
}