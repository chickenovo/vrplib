package DynamicPreProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import PreProcessor.LSP;
import PreProcessor.Order;

public class DynamicLSPList {
    public HashMap<Integer, DynamicLSP> LSPs = new HashMap<>();
    //public HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList = new HashMap<>();
    public int overallCount;
    public int lspCount;

    public DynamicLSPList(int overallCount, int lspCount){
        this.overallCount = overallCount;
        this.lspCount = lspCount;
    }

    public void addLSP(int id, DynamicLSP lsp){
    	//System.out.println("Adding LSP");
        this.LSPs.put(id, lsp);
    }

    public void updateOverallCount(HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList, HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList) {
        this.overallCount = 0;

        /*for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
            this.overallCount += entry.getValue().mergedOrders.size();
        }*/
        
        
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	
        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedTripList.entrySet()) {
            	//int vehicle = tripEntry.getKey();
            	ArrayList<DynamicOrder> orderList = tripEntry.getValue();
            	
            	for(DynamicOrder order : orderList) {
            		if(!order.isDummy) {
            			
            			this.overallCount += 1;
            		//outputMap.put(order.id, order.mergedOrdersId);
            		//order.id = id;
            		//outputMap.put(id, order.mergedOrdersId);
            		//id += 1;
            		}
            	}
            	
            } 
        	
        }
        
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	
        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedNewTripList.entrySet()) {
            	//int vehicle = tripEntry.getKey();
            	ArrayList<DynamicOrder> orderList = tripEntry.getValue();
            	
            	for(DynamicOrder order : orderList) {
            		if(!order.isDummy) {
            			
            			this.overallCount += 1;
            		//outputMap.put(order.id, order.mergedOrdersId);
            		//order.id = id;
            		//outputMap.put(id, order.mergedOrdersId);
            		//id += 1;
            		}
            	}
            	
            } 
        	
        }
        
    }

    public HashMap<Integer, ArrayList<DynamicOrder>> merge(DynamicRemainingTrips trips) throws Exception{
    	System.out.println("Start merging orders..");
    	HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList = new HashMap<>();
    	HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList = new HashMap<>();
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	mergedTripList = entry.getValue().mergeOrders(trips);
        }
        //System.out.println("Finished merging orders..");
       
        return mergedTripList;
        
    }
    
    public HashMap<Integer, ArrayList<DynamicOrder>> mergeNew() throws Exception{
    	//System.out.println("Start merging orders..");
    	HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList = new HashMap<>();
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	mergedNewTripList = entry.getValue().mergeNewOrders();
        }
        System.out.println("Finished merging orders..");
       
        return mergedNewTripList;
        
    }

    public HashMap<Integer, List<Integer>> updateId(HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList, 
    		HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList) {
    	//System.out.println("in update id fn "+mergedTripList.size());
    	int id = 1;
        HashMap<Integer, List<Integer>> outputMap= new HashMap<>();
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	
        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedTripList.entrySet()) {
            	int vehicle = tripEntry.getKey();
            	ArrayList<DynamicOrder> orderList = tripEntry.getValue();
            	
            	for(DynamicOrder order : orderList) {
            		if(!order.isDummy) {
            		//outputMap.put(order.id, order.mergedOrdersId);
            		order.id = id;
            		outputMap.put(id, order.mergedOrdersId);
            		id += 1;
            		}
            	}
            	
            } 
        	//System.out.println("count int "+id);
        }
        
        for (Map.Entry<Integer, DynamicLSP> entry : this.LSPs.entrySet()){
        	
        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedNewTripList.entrySet()) {
            	int vehicle = tripEntry.getKey();
            	ArrayList<DynamicOrder> orderList = tripEntry.getValue();
            	
            	for(DynamicOrder order : orderList) {
            		if(!order.isDummy) {
            		//outputMap.put(order.id, order.mergedOrdersId);
            		//System.out.println("print v id of new merged orders "+order.vID);	
            		order.id = id;
            		outputMap.put(id, order.mergedOrdersId);
            		id += 1;
            		}
            	}
            	
            } 
        	//System.out.println("count int "+id);
        }
        
         for(Map.Entry<Integer, List<Integer>> oentry : outputMap.entrySet()) {
        	int mergedID = oentry.getKey();
        	List<Integer> orderIDList = oentry.getValue();
        	
        	for (int i=0; i<orderIDList.size(); i++) {
        		//System.out.println("final hashmap output "+mergedID +" "+orderIDList.get(i));
        	}
        	
        	
        } 
         
         
         for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedTripList.entrySet()) {
         	int vehicle = tripEntry.getKey();
         	ArrayList<DynamicOrder> orderList = tripEntry.getValue();
         	
         	for(DynamicOrder order : orderList) {
         		if(!order.isDummy) {
         			//System.out.println("debug "+order.mergedOrdersId+" "+order.alreadyMeraged);
         		//outputMap.put(order.id, order.mergedOrdersId);
         		//order.id = id;
         		//outputMap.put(id, order.mergedOrdersId);
         		//id += 1;
         		}
         	}
         	
         }      		
        return outputMap;

    }
    
    
}
