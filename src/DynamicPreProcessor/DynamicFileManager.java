package DynamicPreProcessor;

import java.io.*;
import java.util.*;

import DynamicEngineRelated.RemainingTrips;

public class DynamicFileManager {
	
	DynamicRemainingTrips remTrips = new DynamicRemainingTrips();
	HashMap<Integer, ArrayList<DynamicOrder>> tripOrderList = new HashMap<>();
	DynamicLSPList lspList = new DynamicLSPList(0, 0);    //overall count, LSP count
	List<DynamicRemainingTrips> remainingTripsList = new ArrayList<DynamicRemainingTrips>();
	
	public DynamicLSPList getlspList() throws FileNotFoundException {
		return lspList;
		
	}
	
    public DynamicLSPList readData(String filename) throws FileNotFoundException {
        BufferedReader reader;
        ArrayList<DynamicVehicle> allVehicleList = new ArrayList<>();

        try {
            reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            
            while(line != null){
                String[] splits = line.split(",");

                if (splits[0].equals("overall")){
                    lspList = new DynamicLSPList(Integer.parseInt(splits[3]), Integer.parseInt(splits[1]));
                    
                    //Add the LSP
                    int LSP_ID = 0;
                    DynamicLSP lsp = new DynamicLSP(LSP_ID);
                    lspList.addLSP(LSP_ID, lsp); 
                }
                else if (splits[0].equals("vehicle")){
                    int id = Integer.parseInt(splits[1]);
                    int truckLowerTime = Integer.parseInt(splits[2]);
                    int truckUpperTime = Integer.parseInt(splits[3]);
                    double capacity = Double.parseDouble(splits[4]);   //volume capacity
                    int originLSP = Integer.parseInt(splits[6]);

                    String[] range = splits[8].substring(1, splits[8].length()-1).split(";");
                    //int lunchTimeStart = Integer.parseInt(range[0]);
                    //int lunchTimeEnd =  Integer.parseInt(range[1]);
                    int truckDepotStart = Integer.parseInt(range[0]);;
                    int truckDepotEnd = Integer.parseInt(range[1]);
                    int truckLoadTime = Integer.parseInt(splits[9]);
                    DynamicVehicle vehicle = new DynamicVehicle(id, truckLowerTime, truckUpperTime, 
                    		capacity, originLSP, truckDepotStart, truckDepotEnd, truckLoadTime );
                    allVehicleList.add(vehicle);
                }
                /* else if (splits[0].equals("lsp") & splits.length == 3){
                    int id = Integer.parseInt(splits[1]);

                    DynamicLSP lsp = new DynamicLSP(id);
                    lspList.addLSP(id, lsp);
                } */
                else if (splits[0].equals("order")){	
                    int id = Integer.parseInt(splits[1]);
                    int vID;
                    if(splits[2].equals("null"))
                    	vID = -1;
                    else
                    	vID = Integer.parseInt(splits[2]);
                    
                    int orderLocation = Integer.parseInt(splits[3]);
                    double weight = Double.parseDouble(splits[4]);
                    
                    String[] range = splits[5].substring(1, splits[5].length()-1).split(":");
                    
                    int lowerTime = Integer.parseInt(range[0]);
                    int upperTime = Integer.parseInt(range[1]);
                    
                    double volume = Double.parseDouble(splits[6]);
                    int serviceTime = Integer.parseInt(splits[7]);
                    int originLSP = Integer.parseInt(splits[8]);
                    int isDockerNeeded = Integer.parseInt(splits[9]);

                    String custumerId = "NOT GIVEN";
                    String foodType = "NOT GIVEN";
                    int pickUpLocationId = -1;

                    if (splits.length == 15) {
                        custumerId = splits[10];
                        foodType = splits[11];
                        pickUpLocationId = Integer.parseInt(splits[12]);
                    }
                    
                    String status = splits[13];
                    String isScheduled = splits[14];
                    
                    DynamicOrder order = new DynamicOrder(id, vID , orderLocation, weight, lowerTime, upperTime, volume, 
                    		serviceTime, originLSP, isDockerNeeded, custumerId, foodType, pickUpLocationId, status, isScheduled, false, 0);
                    
                    lspList.LSPs.get(originLSP).addOrder(order);
                }
                
                else if (splits[0].equals("RemainingTrips")){
                	int vehicleID = Integer.parseInt(splits[1]);
                	String listOfOrders = splits[2];
                	int vStartLoc = Integer.parseInt(splits[3]);
                	String orderList = (listOfOrders.replace('[',' ')).replace(']', ' ');
                	
                	String[] tokens = orderList.split(";");
                	String orderID = null;
                	ArrayList<String> orderIDs = new ArrayList<String>();
                	for (int i=0; i<tokens.length; i++) {
                		orderID = (tokens[i].replace('"', ' ').trim());
                		orderIDs.add(orderID);
                	}
                	
                	DynamicRemainingTrips t = new DynamicRemainingTrips();
                	
                	remTrips.addRemainingTrip(vehicleID, orderIDs, vStartLoc);
                	t.addRemainingTrip(vehicleID, orderIDs, vStartLoc);  
                	
                	//remainingTripsList.add(remTrips);
                	remainingTripsList.add(t);
                }
                
                line = reader.readLine();
            }

            for (DynamicVehicle v : allVehicleList){
                lspList.LSPs.get(v.originLSP).addVehicle(v);
            }
            
         
            reader.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return lspList;
    }
    
    public DynamicRemainingTrips matchTripOrders() {
  
    	HashMap<Integer, ArrayList<String>> trips = remTrips.getRemainingTrips();
    	Iterator itr = trips.entrySet().iterator();
        
    	while(itr.hasNext()) {
        	ArrayList<DynamicOrder> listOrders = new ArrayList<DynamicOrder>();
        	Map.Entry pair = (Map.Entry) itr.next();
        	String strOrderIDs = (pair.getValue()).toString();
        	String intOrderIDs = (strOrderIDs.replace('[',' ')).replace(']', ' ');
        	
        	String[] tokens = intOrderIDs.split(",");
        	String orderID = null;
        	for (int i=0; i<tokens.length; i++) {
        		
        		if(tokens[i].trim().length()<=2) {
        			DynamicOrder order = lspList.LSPs.get(0).getOrder(Integer.parseInt(tokens[i].toString().trim())-1);
        			order.setisDummy(false);
        			order.setlocNum(-5);
        			if(order.status.equals("Undelivered"))
        				listOrders.add(order);
        		} else {
        				//System.out.println(tokens[i].trim());
        				//System.out.println(tokens[i].trim().indexOf("(")+1);
            			DynamicOrder order = new DynamicOrder();				//Create a dummy order for depot
            			order.setisDummy(true);
            			order.setlocNum(Integer.parseInt(tokens[i].trim().substring(tokens[i].trim().indexOf("(")+1, tokens[i].trim().indexOf(")"))));
            			listOrders.add(order);
        		}
     
        	}
        	
        	remTrips.addRemainingTripOrders(Integer.parseInt(pair.getKey().toString()), listOrders);	
        }
        
    	return remTrips;
    }
    
    public void showOrders() {
    	for(int i=0; i<lspList.LSPs.get(0).orderList.size(); i++) {
    		DynamicOrder order = lspList.LSPs.get(0).getOrder(i);
    		//System.out.println("in show orders " +i+" "+order.getId());
    	}
    	   	
    	HashMap<Integer, ArrayList<DynamicOrder>> tripOrders = remTrips.getRemainingTripOrders();
    	Iterator itr = tripOrders.entrySet().iterator();
    	while (itr.hasNext()) {
    		Map.Entry pair = (Map.Entry) itr.next();
    		ArrayList<DynamicOrder> dm = tripOrders.get(pair.getKey());
    		for(DynamicOrder order: dm) {
    			//System.out.println("show orders "+pair.getKey()+ " "+order.getId()+" "+order.isDummy+" "+order.locNum+" "+order.status);
    		}
     		
    	}
	
    }

    public void writeData(String inputFilename, String outputFilename, DynamicLSPList lspList, HashMap<Integer,
    		ArrayList<DynamicOrder>> mergedTripList, HashMap<Integer,
    		ArrayList<DynamicOrder>> mergedNewTripList) throws FileNotFoundException {
       
    	BufferedWriter writer;
        BufferedReader reader;
        boolean isWrite = false;
        boolean isTripWrite = false;

        //System.out.println("writing order");
        try {
            writer = new BufferedWriter(new FileWriter(outputFilename));
            reader = new BufferedReader(new FileReader(inputFilename));

            String line = reader.readLine();

            while(line != null){
                String[] splits = line.split(",");

                if (splits[0].equals("overall")){
                    splits[3] = String.valueOf(lspList.overallCount);
                    String newLine = String.join(",", splits);
                    writer.write("\n");
                    writer.write(newLine);
                }
                else if (splits[0].equals("order")){

                    if (isWrite == false) {
                    	//System.out.println("length "+lspList.LSPs.entrySet().size());
                        for (Map.Entry<Integer, DynamicLSP> entry : lspList.LSPs.entrySet()) {
                        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedTripList.entrySet()) {
                        		int vehicleID  = tripEntry.getKey();
                        		ArrayList<DynamicOrder> orderList = tripEntry.getValue();
                        		for(DynamicOrder order : orderList) {
                        			if (!order.isDummy) {
                        			ArrayList<String> orderLine = new ArrayList<>();
                        			orderLine.add("order");
                        			orderLine.add(String.valueOf(order.id));
                        			//orderLine.add(String.valueOf(order.vID));
                        			orderLine.add(String.valueOf(vehicleID));
                        			orderLine.add(String.valueOf(order.orderLocation));
                        			orderLine.add(String.valueOf(order.weight));
                        			//orderLine.add("[");
                        			orderLine.add("["+String.valueOf(order.lowerTime)+":");
                        			//orderLine.add(":");
                        			orderLine.add(String.valueOf(order.upperTime)+"]");
                        			//orderLine.add("]");
                        			orderLine.add(String.valueOf(order.volume));
                        			orderLine.add(String.valueOf(order.serviceTime));
                        			orderLine.add(String.valueOf(order.originLSP)); // TODO Check
                        			orderLine.add(String.valueOf(order.isDockerNeeded));
                                

                        			if (splits.length == 15) {
                        				orderLine.add(order.custumerId);
                        				orderLine.add(order.foodType);
                        				orderLine.add(String.valueOf(order.pickUpLocationId));
                                }
                        			
                        			orderLine.add(String.valueOf(order.status));
                        			orderLine.add(String.valueOf(order.isScheduled));

                        			writer.newLine();

                        			line = "";
                        			for (String s : orderLine){
                        				//System.out.println(s);
                        				if (!s.endsWith("]")) 
                        					line = line + "," + s;
                        				else
                        					line = line + s;
                        				
                        			}
                        			writer.write(line.substring(1));
                        		}
                        		}
                        	}
                        	
                        	
                        	//Traverse through New Merged orders
                        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedNewTripList.entrySet()) {
                        		int vehicleID  = tripEntry.getKey();
                        		ArrayList<DynamicOrder> orderList = tripEntry.getValue();
                        		for(DynamicOrder order : orderList) {
                        			if (!order.isDummy) {
                        			ArrayList<String> orderLine = new ArrayList<>();
                        			orderLine.add("order");
                        			orderLine.add(String.valueOf(order.id));
                        			//orderLine.add(String.valueOf(order.vID));
                        			if(order.vID == -1) 
                        				orderLine.add("-1");
                        			else	
                        				orderLine.add(String.valueOf(order.vID));
                        			orderLine.add(String.valueOf(order.orderLocation));
                        			orderLine.add(String.valueOf(order.weight));
                        			//orderLine.add("[");
                        			orderLine.add("["+String.valueOf(order.lowerTime)+":");
                        			//orderLine.add(":");
                        			orderLine.add(String.valueOf(order.upperTime)+"]");
                        			//orderLine.add("]");
                        			orderLine.add(String.valueOf(order.volume));
                        			orderLine.add(String.valueOf(order.serviceTime));
                        			orderLine.add(String.valueOf(order.originLSP)); // TODO Check
                        			orderLine.add(String.valueOf(order.isDockerNeeded));
                                

                        			if (splits.length == 15) {
                        				orderLine.add(order.custumerId);
                        				orderLine.add(order.foodType);
                        				orderLine.add(String.valueOf(order.pickUpLocationId));
                                }
                        			
                        			//orderLine.add(String.valueOf(order.status));
                        			orderLine.add("New");
                        			orderLine.add(String.valueOf(order.isScheduled));

                        			writer.newLine();

                        			line = "";
                        			for (String s : orderLine){
                        				//System.out.println(s);
                        				if (!s.endsWith("]")) 
                        					line = line + "," + s;
                        				else
                        					line = line + s;
                        				
                        			}
                        			writer.write(line.substring(1));
                        		}
                        		}
                        	}
                        	
                        	//End traverse through New merged orders
                        	
                        }
                    }

                    isWrite = true;

                }

                else if (splits[0].equals("RemainingTrips")){

                    if (isTripWrite == false) {
                        for (Map.Entry<Integer, DynamicLSP> entry : lspList.LSPs.entrySet()) {
                        	for(Map.Entry<Integer, ArrayList<DynamicOrder>> tripEntry : mergedTripList.entrySet()) {
                        		int vehicleID  = tripEntry.getKey();
                        		ArrayList<String> remTripLine = new ArrayList<>();
                        		remTripLine.add("RemainingTrips");
                        		remTripLine.add(String.valueOf(vehicleID));
                        		ArrayList<DynamicOrder> orderList = tripEntry.getValue();
                        		ArrayList<String> orderListString = new ArrayList<>();
                        		
                        		for(DynamicOrder order : orderList) {
                        			if(order.id > 0) 
                        				orderListString.add(String.valueOf(order.id));
                        			else
                        				orderListString.add("depot("+order.locNum+")");
                        		}
                        		
                    			remTripLine.add(String.valueOf(orderListString).replace(",", ";").replaceAll("\\s+", ""));
                    			
                    			for (DynamicRemainingTrips rt : remainingTripsList) {
                    				if(vehicleID == rt.vehicleID) 
                    					remTripLine.add(String.valueOf(rt.vehicleStartLoc));
                    			
                    			}
                    			
                    			writer.newLine();
                    			line = "";
                    			for (String s : remTripLine){
                    				line = line + "," + s;
                    			}
                    			writer.write(line.substring(1));
                    			
                        	}
                        	
                        }
                    }
                   
                    isTripWrite = true;

                }
                
                else{
                    //System.out.println(line);
                    writer.newLine();
                    writer.write(line);
                }
                line = reader.readLine();


            }
            reader.close();
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        	
    }
}
