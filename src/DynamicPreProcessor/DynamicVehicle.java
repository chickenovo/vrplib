package DynamicPreProcessor;

import java.util.HashMap;
import java.util.Map;

public class DynamicVehicle {
    public int id;
    public double originalCapacity;
    public int truckLowerTime;
    public int truckUpperTime;
    public int originLSP;
    public int lunchTimeEnd;
    public int lunchTimeStart;
    public double capacity;
    public int truckDepotStart;
    public int truckDepotEnd;
    public int truckLoadTime;

    public Map<String, Double> fillCapacityForTimePeriod = new HashMap<>();
   // public int amSlotAquiredWeight = 0;
   // public int pmSlotAquired = 0;
  //  public int bothSlotOkey = 0;

    public DynamicVehicle(int id, int truckLowerTime, int truckUpperTime, double capacity, int originLSP,
                   int truckDepotStart, int truckDepotEnd, int truckLoadTime) {
        this.id = id;
        this.originalCapacity = capacity;
        this.capacity = originalCapacity;
        this.truckLowerTime = truckLowerTime;
        this.truckUpperTime = truckUpperTime;
        this.originLSP = originLSP;
        //this.lunchTimeEnd = lunchTimeEnd;
        //this.lunchTimeStart = lunchTimeStart;
        this.truckDepotStart = truckDepotStart;
        this.truckDepotEnd = truckDepotEnd;
        this.truckLoadTime = truckLoadTime;

        fillCapacityForTimePeriod.put("am", 0.0); //capacity for am
        fillCapacityForTimePeriod.put("pm", 0.0); // capacity for pm
        fillCapacityForTimePeriod.put("amOrpm", 0.0); //quantity suitable to both
    }


    public boolean checkVehicleHaveEnoughCapacity(double quantity, String timePeriod, boolean isSimpleMethod) {
        boolean vehicleHaveEnoughCapacity = false;

        if (isSimpleMethod) {
            vehicleHaveEnoughCapacity = quantity <= capacity;
        } else {
            switch (timePeriod) {
                case "am": {
                    if ((capacity - fillCapacityForTimePeriod.get("am")) > quantity) {
                        if ((capacity - (fillCapacityForTimePeriod.get("pm") + fillCapacityForTimePeriod.get("amOrpm")) > 0)) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("am", fillCapacityForTimePeriod.get("am") + quantity);
                        } else if ((capacity - (fillCapacityForTimePeriod.get("am") + fillCapacityForTimePeriod.get("amOrpm") + quantity)) > 0) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("am", fillCapacityForTimePeriod.get("am") + quantity);
                        } else {
                            vehicleHaveEnoughCapacity = false;
                        }
                    }

                    break;
                }
                case "pm": {
                    if ((capacity - fillCapacityForTimePeriod.get("pm")) > quantity) {
                        if ((capacity - (fillCapacityForTimePeriod.get("am") + fillCapacityForTimePeriod.get("amOrpm")) > 0)) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("pm", fillCapacityForTimePeriod.get("pm") + quantity);
                        } else if ((capacity - (fillCapacityForTimePeriod.get("pm") + fillCapacityForTimePeriod.get("amOrpm") + quantity)) > 0) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("am", fillCapacityForTimePeriod.get("am") + quantity);
                        } else {
                            vehicleHaveEnoughCapacity = false;
                        }
                    }

                    break;
                }

                case "amOrpm": {
                    boolean amCan = false;
                    boolean pmCan = false;
                    if ((capacity - fillCapacityForTimePeriod.get("am")) > quantity) {
                        if ((capacity - (fillCapacityForTimePeriod.get("pm") + fillCapacityForTimePeriod.get("amOrpm") + quantity) > 0)) {
                            amCan = true;
                        }
                        if ((capacity - (fillCapacityForTimePeriod.get("am") + fillCapacityForTimePeriod.get("amOrpm") + quantity)) > 0) {
                            pmCan = true;
                        }

                        if (!amCan && !pmCan) {
                            vehicleHaveEnoughCapacity = false;
                        } else if (amCan && pmCan) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("amOrpm", fillCapacityForTimePeriod.get("amOrpm") + quantity);
                        } else if (amCan) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("am", fillCapacityForTimePeriod.get("am") + quantity);
                        } else if (pmCan) {
                            vehicleHaveEnoughCapacity = true;
                            fillCapacityForTimePeriod.put("pm", fillCapacityForTimePeriod.get("pm") + quantity);
                        }
                    }
                    break;
                }
            }
        }

        // Todo check vehicle is have full capacity the set a flag vehicle is full
        // TODO amOrpm both use a list of capacity and check all possible solution.
        return vehicleHaveEnoughCapacity;
    }

}
