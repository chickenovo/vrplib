package DynamicPreProcessor;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class DynamicPreProcessorMainClass {
    /*public static void main(String[] args) throws InterruptedException, IOException,  ExecutionException {
        String inputFileName = args[0];
        String outputFileName = args[1];
        FileManager reader = new FileManager();
        LSPList lspList = reader.readData(inputFileName);

        lspList.merge();

        lspList.updateOverallCount();
        Map<Integer, List<Integer>> outputMap = lspList.updateId();

        reader.writeData(inputFileName, outputFileName, lspList);
    }*/

    public HashMap<Integer, List<Integer>> RunPreProcessor(String inputFile , String outputFile) throws Exception{
        String inputFileName = inputFile;
        String outputFileName = outputFile;
        DynamicFileManager reader = new DynamicFileManager();
        DynamicLSPList lspList = reader.readData(inputFileName);
        DynamicRemainingTrips trips = reader.matchTripOrders();

        //reader.showOrders();
        HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList = lspList.merge(trips);
        HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList = lspList.mergeNew();

        lspList.updateOverallCount(mergedTripList, mergedNewTripList);
        //lspList.updateId();
        HashMap<Integer, List<Integer>> outputMap = lspList.updateId(mergedTripList, mergedNewTripList);

        reader.writeData(inputFileName, outputFileName, lspList, mergedTripList, mergedNewTripList);

        return outputMap;
    }

}
