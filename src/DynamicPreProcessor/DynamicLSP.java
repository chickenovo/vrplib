package DynamicPreProcessor;

import PreProcessor.Order;
import PreProcessor.Vehicle;

import java.util.*;
import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class DynamicLSP {
    public int id;
    public ArrayList<DynamicOrder> orderList = new ArrayList<>();
    public ArrayList<DynamicOrder> neworderList = new ArrayList<>();
    public ArrayList<DynamicVehicle> vehicleList = new ArrayList<>();
   
    
    public HashMap<Integer, ArrayList<DynamicOrder>> originalTripList = new HashMap<Integer, ArrayList<DynamicOrder>>();
    public HashMap<Integer, ArrayList<DynamicOrder>> mergedTripList = new HashMap<Integer, ArrayList<DynamicOrder>>();
    public HashMap<Integer, ArrayList<DynamicOrder>> mergedNewTripList = new HashMap<Integer, ArrayList<DynamicOrder>>();
    
    public ArrayList<DynamicOrder> mergedOrders = new ArrayList<>();
    public int minOverLap = 120;

    public DynamicLSP(int id){
        this.id = id;
    }

    public void addOrder(DynamicOrder order){
        this.orderList.add(order);
    }
    
    public void addNewOrder(DynamicOrder order){
        this.neworderList.add(order);
    }

    public void addVehicle(DynamicVehicle vehicle){
        this.vehicleList.add(vehicle);
    }

    public DynamicOrder getOrder(int id){
        return this.orderList.get(id);
    }
    
    public DynamicOrder getNewOrder(int id){
        return this.neworderList.get(id);
    }

    public DynamicVehicle getVehicle(int id){
        return this.vehicleList.get(id);
    }



    public  HashMap<Integer, ArrayList<DynamicOrder>> mergeOrders(DynamicRemainingTrips t) {
        //t = new DynamicRemainingTrips();
        originalTripList = t.getRemainingTripOrders();
        HashMap<Integer, ArrayList<DynamicOrder>> ordersBasedOnLocation = new HashMap<>();
        //System.out.println("order list size " + orderList.size());
        //System.out.println("ogn list size " + originalTripList.size());
        for (Map.Entry<Integer, ArrayList<DynamicOrder>> entry : originalTripList.entrySet()) {

            ArrayList<DynamicOrder> tempList = entry.getValue();
            int count = 0;
            int vehicle = entry.getKey();
            ArrayList<DynamicOrder> tempMergedOrders = new ArrayList<>();
            Boolean isCurrentOrderMerged = false;

            //change this to a while loop till size is become zero
            for (int i = 0; i < tempList.size(); i++) {

                DynamicOrder currentOrder = tempList.get(i);
                //int count = i;
                //current order is not merged one

                if (i == 0 || currentOrder.isDummy) { // add deport condition also **********************
                    tempList.get(i).alreadyMeraged = 0;
                    DynamicOrder o = tempList.get(i);
                    o.mergedOrdersId.add(o.id);
                    o.alreadyMeraged = 1; //1 mean order is result of a after merge process, but actual it is not mergerd with a order
                    tempMergedOrders.add(o);
                } else {  //currentOrder.alreadyMeraged !=  0 have to be implemented

                    boolean orderIsMergedWithAnotherMergedOrder = false;
                    //check it can be merged with already merged one 1
                    DynamicOrder previousOrder = tempMergedOrders.get(tempMergedOrders.size() - 1);
                    count = tempMergedOrders.size() - 1;
                    if (previousOrder.isDummy) {
                        isCurrentOrderMerged = true;
                        tempList.get(i).alreadyMeraged = 0;
                        DynamicOrder o = tempList.get(i);
                        o.mergedOrdersId.add(o.id);
                        o.alreadyMeraged = 1; //1 mean order is result of a after merge process, but actual it is not mergerd with a order
                        tempMergedOrders.add(o);

                    } else if (previousOrder.alreadyMeraged == 1) { // previous order not merge with other orders

                       isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder, i, count, -2 );
                    } else if (previousOrder.alreadyMeraged == 2) {

                        isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder, i, count, -2 );
                    }


                    if ( !isCurrentOrderMerged) {  //tempList.get(i).alreadyMeraged == -1
                        tempList.get(i).alreadyMeraged = 0;
                        DynamicOrder o = tempList.get(i);
                        o.mergedOrdersId.add(o.id);
                        o.alreadyMeraged = 1; //1 mean order is result of a after merge process, but actual it is not mergerd with a order
                        tempMergedOrders.add(o);
                    }
                }
            }

            //mergedTripList.get(vehicle).addAll(tempMergedOrders);
            mergedTripList.put(vehicle, tempMergedOrders);
        }
        
        //System.out.println(mergedTripList.size());
        return mergedTripList;

    }
    

      public HashMap<Integer, ArrayList<DynamicOrder>> mergeNewOrders() throws Exception {
    	DynamicFileManager mgr = new DynamicFileManager();
    	DynamicLSPList lspList = mgr.getlspList();
    	
    	HashMap<Integer, ArrayList<DynamicOrder >> ordersBasedOnLocation = new HashMap<>();   // will have only new orders
    	
        //short orders by location
        for(DynamicOrder order : orderList) {
        	if(order.status.equals("New")) {
        		if (ordersBasedOnLocation.containsKey(order.orderLocation)) {
        			ordersBasedOnLocation.get(order.orderLocation).add(order);
        			//System.out.println(order.id);
        		} else {
        			ordersBasedOnLocation.put(order.orderLocation, new ArrayList<DynamicOrder>());
        			ordersBasedOnLocation.get(order.orderLocation).add(order);
        			//System.out.println(order.id);
        		}
        	}
        }

        //short orders by capacity
        //If it is better sort it according to customerID

        for (Map.Entry<Integer, ArrayList<DynamicOrder>> entry : ordersBasedOnLocation.entrySet()) {
            Collections.sort(entry.getValue(), new Comparator<DynamicOrder>() {
                @Override
                public int compare(DynamicOrder o1, DynamicOrder o2) {
                    if (o1.volume == o2.volume)
                        return 0;
                    return o1.volume < o2.volume ? -1 : 1;
                }

            });
        }

        //sort vehicle according to the capacity
        Collections.sort(vehicleList, (Comparator<DynamicVehicle>) (v1, v2) -> {
            if (v1.capacity == v2.capacity)
                return 0;
            return v1.capacity < v2.capacity ? -1 : 1;
        });
        

        for (Map.Entry<Integer, ArrayList<DynamicOrder>> entry : ordersBasedOnLocation.entrySet()) {

            ArrayList<DynamicOrder> tempList = entry.getValue();
            int count = 0;
            int location = entry.getKey();

            ArrayList<DynamicOrder> tempMergedOrders = new ArrayList<>();
           
            //change this to a while loop till size is become zero
            
            
            for (int i = 0; i < tempList.size(); i++) {
            	//System.out.println("size " +  tempMergedOrders.size());
            	Boolean isCurrentOrderMerged = false;
                DynamicOrder currentOrder = tempList.get(i);
                //current order is not merged one

                if (i == 0) { // add deport condition also **********************
                    tempList.get(i).alreadyMeraged = 0;
                    DynamicOrder o = tempList.get(i);
                    o.mergedOrdersId.add(o.id);
                    o.alreadyMeraged = 1; //1 mean order is result of a after merge process, but actual it is not merged with a order
                    tempMergedOrders.add(o);
                    
                } else {  //currentOrder.alreadyMeraged !=  0 have to be implemented

                    boolean orderIsMergedWithAnotherMergedOrder = false;
                    //check it can be merged with already merged one 1
                    for(int j = tempMergedOrders.size() - 1; j >= 0; j--) {
              	
                    	DynamicOrder previousOrder = tempMergedOrders.get(j);
                    	count = j;
                    	// previous order not merge with other orders
                    	if (previousOrder.getvID() == currentOrder.getvID() || previousOrder.getvID() == -1 || currentOrder.getvID() == -1)	{
                    	   if (previousOrder.getvID() == currentOrder.getvID()) {
                    		   isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder, i, count, currentOrder.getvID());
                    		   //System.out.print("Condition1 "+previousOrder.id+ " " +currentOrder.id+"\n");
                    	   }
                    	   else if (currentOrder.getvID() > 0) {
                    		   isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder, i, count, currentOrder.getvID());
                    		   System.out.print("Condition 2"+previousOrder.id+ " " +currentOrder.id);
                    	   }
                    	   else if (previousOrder.getvID() > 0) {                  		 
                    		   isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder, i, count, previousOrder.getvID());
                    		   System.out.print("Condition 3"+previousOrder.id+ " " +currentOrder.id);
                    	   }
                    	} 
               
                    	
                    	if (isCurrentOrderMerged)
                    		break;
                    }
                    
                    if ( !isCurrentOrderMerged) {  //tempList.get(i).alreadyMeraged == -1
                        tempList.get(i).alreadyMeraged = 0;
                        DynamicOrder o = tempList.get(i);
                        o.mergedOrdersId.add(o.id);
                        o.alreadyMeraged = 1; //1 mean order is result of a after merge process, but actual it is not mergerd with a order
                        tempMergedOrders.add(o);
                        //System.out.println("result of merged but not merged "+o.getId()+" "+tempMergedOrders.get(0).id+" "+tempMergedOrders.get(1).id);
                    }
                
                }
                
            }

            mergedNewTripList.put(location, tempMergedOrders);
        }
         
        return mergedNewTripList;

    }

    public boolean mergeTwoOrders(ArrayList<DynamicOrder> tempMergedOrders, ArrayList<DynamicOrder> tempList, DynamicOrder currentOrder, 
    		DynamicOrder previousOrder, int i , int count, int vehicleID){

    	
        boolean isCurrentOrderMerged = false;
        
        int L1 = currentOrder.lowerTime;
        int L2 = previousOrder.lowerTime;
        int U1 = currentOrder.upperTime;
        int U2 = previousOrder.upperTime;

        int maxL = max(L1, L2);
        int minU = min(U1, U2);
        boolean isTWOverLap = maxL < minU;
        int TWOverLap = minU - maxL;
        double totalCap = currentOrder.volume + previousOrder.volume;
        int st = currentOrder.serviceTime + previousOrder.serviceTime;
        boolean isSamePickUpLocation = (currentOrder.pickUpLocationId == previousOrder.pickUpLocationId ? true : false); //change according to the requirement
        boolean isSameOrderLocation = (currentOrder.orderLocation == previousOrder.orderLocation ? true : false);
        boolean isSameFoodType = (currentOrder.foodType).equalsIgnoreCase(previousOrder.foodType);  //change according to the requirement
        boolean isvehicleCanAllocate = false;
        
        if (vehicleID > 0) {
        	for (DynamicVehicle v : vehicleList) {
                //isvehicleCanAllocate = v.checkVehicleHaveEnoughCapacity(totalCap, whatTimeCanAllocate, true);
        		if (v.id == vehicleID && totalCap <= v.capacity) {
        			//System.out.println("v capacity "+v.capacity);
        			isvehicleCanAllocate = true;
        		}
                if (isvehicleCanAllocate) {
                    break;
                }
            }
        }
        
        else if (vehicleID == -1) {
        	for (DynamicVehicle v : vehicleList) {
                //isvehicleCanAllocate = v.checkVehicleHaveEnoughCapacity(totalCap, whatTimeCanAllocate, true);
        		if (totalCap <= v.capacity) {
        			isvehicleCanAllocate = true;
        		}
                if (isvehicleCanAllocate) {
                    break;
                }
            }
        	
        }
        
        
        else if (vehicleID == -2) {
        	isvehicleCanAllocate = true;
        }
        
        if (isTWOverLap && TWOverLap >= minOverLap && (st + maxL) < (minU + st / 2) && isSameFoodType && isSamePickUpLocation && isSameOrderLocation) {
        	System.out.println("Merging orders "+currentOrder.id+" "+previousOrder.id);
            //boolean isvehicleCanAllocate = true;
            
            

            //  String whatTimeCanAllocate = "no"; // 'am' , 'pm' ot 'amOrpm'
            //check vehicle if needed #####################

            if (isvehicleCanAllocate) {
                // create new order

                isCurrentOrderMerged = true;
                int minID = min(currentOrder.id, previousOrder.id);

                double weight = currentOrder.weight + previousOrder.weight;
                int lowerTime = maxL;
                int upperTime = minU;
                double volume = currentOrder.volume + previousOrder.volume;
                // int serviceTime = nextOrder.serviceTime+ currentOrder.serviceTime;
                int serviceTime = 0;
                String customerId = "NOT GIVEN";
                String foodType = currentOrder.foodType;
                int pickupLocationId = currentOrder.pickUpLocationId;

                //previous merged orders have two different customer id. so resultant order customerID changes to "MergedIDs"
                if (!previousOrder.custumerId.equalsIgnoreCase("MergedIDs")) {
                    //"NOT GIVEN" is used if customer id is not provided, so we cann't reduce the service time
                    if (!currentOrder.custumerId.equalsIgnoreCase("NOT GIVEN") && !previousOrder.custumerId.equalsIgnoreCase("NOT GIVEN")) {
                        if (currentOrder.custumerId.equalsIgnoreCase(previousOrder.custumerId)) {
                            // serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime; //no need to divide current merged order time window
                            //previous commented logic is used to dived service time by two if customerID is same
                            serviceTime = currentOrder.serviceTime < previousOrder.serviceTime ? previousOrder.serviceTime : currentOrder.serviceTime; //get the maximum customer ID of two orders
                            customerId = currentOrder.custumerId;
                        } else {
                            serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
                            customerId = "MergedIDs";
                        }
                    } else {
                        serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
                        customerId = "MergedIDs";
                    }
                } else {
                    ArrayList<Integer> serveiceTimeOfSameCustomerID = new ArrayList<>();
                    Integer maxServiceTime = 0;

                    for (int z = 0; z < previousOrder.getMergedOrderList().size(); z++) {
                        DynamicOrder temp = previousOrder.getMergedOrderList().get(z);
                        if (!currentOrder.custumerId.equalsIgnoreCase("NOT GIVEN") && !temp.custumerId.equalsIgnoreCase("NOT GIVEN")) {
                            if (currentOrder.custumerId.equalsIgnoreCase(temp.custumerId)) {
                                serveiceTimeOfSameCustomerID.add(temp.serviceTime);
                                maxServiceTime = maxServiceTime < temp.serviceTime ? temp.serviceTime : maxServiceTime;// add extra logic to filter the max service time of batch
                            }
                        }

                    }
//                                                if (serveiceTimeOfSameCustomerID.size() == 0 ) {
//                                                    serviceTime = currentOrder.serviceTime + nextOrder.serviceTime;
//                                                    customerId = "MergedIDs";
//                                                } else if (serveiceTimeOfSameCustomerID.size() == 1) {
//                                                    serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime - serveiceTimeOfSameCustomerID.get(0)/2;
//                                                    customerId = "MergedIDs";
//                                                } else {
//                                                    serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime; //no need to divide current merged order time window
//                                                    customerId = "MergedIDs";
//                                                }

                    // previous commented part is used for if service ime is divided by two.

                    if (serveiceTimeOfSameCustomerID.size() == 0) {
                        serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
                        customerId = "MergedIDs";
                    } else {
                        serviceTime = (maxServiceTime < currentOrder.serviceTime ? currentOrder.serviceTime : maxServiceTime) + previousOrder.serviceTime - maxServiceTime;
                        customerId = "MergedIDs";
                    }

                }

                // int serviceTime = currentOrder.serviceTime + nextOrder.serviceTime;
                int originLSP = 0; //check change
                // int empty = 0; //TODO
                int isDockerNeeded = currentOrder.isDockerNeeded; //check
                int location = currentOrder.orderLocation;
                
                int vehicle;
                if (vehicleID == -2) {
                	vehicle = currentOrder.vID;
                	//System.out.println("-2 "+vehicle+" "+currentOrder.id+" "+previousOrder.id);
                }
                else {
                	vehicle = vehicleID;
                	//System.out.println("Else "+vehicle+" "+currentOrder.id+" "+previousOrder.id);
                }
                ArrayList<Integer> tempMergedOrdersId = previousOrder.mergedOrdersId;
                ArrayList<DynamicOrder> tempMergedOrderslist = previousOrder.mergedOrderList;
                tempMergedOrdersId.add(currentOrder.id);
                DynamicOrder newMergedOrder = new DynamicOrder( minID, vehicle, location, weight, lowerTime, upperTime, volume,
                        serviceTime, originLSP, isDockerNeeded, customerId, foodType, pickupLocationId, "Undelivered","No", false, -1);
                //add current order to orderList of mergeOrders
                tempMergedOrderslist.add(currentOrder);
                newMergedOrder.mergedOrderList = tempMergedOrderslist;
                newMergedOrder.alreadyMeraged = 2;  //merge so value is 1
                newMergedOrder.tempMergedOrderId = minID;
                newMergedOrder.mergedOrdersId = tempMergedOrdersId;
                tempMergedOrders.set(count, newMergedOrder);
                tempList.get(i).alreadyMeraged = 0;
            }

        }



        return isCurrentOrderMerged;
    }

}