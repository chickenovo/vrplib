package DynamicPreProcessor;

import java.util.ArrayList;
import java.util.List;

public class DynamicOrder {
	public int vID;
    public int id;
    public int orderLocation;
    public double weight;
    public int lowerTime;
    public int upperTime;
    public double volume = 0;
    public int serviceTime;
    public int originLSP;
    //public int empty; //TODO
    public int isDockerNeeded;
    public ArrayList<Integer> mergedOrdersId = new ArrayList<>();
    public ArrayList<DynamicOrder> mergedOrderList = new ArrayList<>();
    public int alreadyMeraged = -1;
    public int tempMergedOrderId = -1;
    String custumerId = "NOT GIVEN";
    String foodType = "NOT GIVEN";
    int pickUpLocationId = -1;
    String status;
    String isScheduled;
    boolean isDummy;
    int locNum;
    
    public DynamicOrder() {
    	
    }
    
    public DynamicOrder(int id, int vID, int orderLocation, double weight, int lowerTime, int upperTime,
                 double volume, int serviceTime, int originLSP, int isDockerNeeded, String custumerId, 
                 String foodType, int pickUpLocationId, String status, String isScheduled, boolean isDummy, int locNum) {
        this.vID = vID;
    	this.id = id;
        this.weight = weight;
        this.isDockerNeeded = isDockerNeeded;
        this.volume = volume;
        this.upperTime = upperTime;
        this.lowerTime = lowerTime;
        this.serviceTime = serviceTime;
        this.originLSP = originLSP;
        this.orderLocation = orderLocation;
        // this.mergedOrdersId.add(id);
        tempMergedOrderId = id;
        this.custumerId = custumerId;
        this.foodType = foodType;
        this.pickUpLocationId = pickUpLocationId;
        this.status = status;
        this.isScheduled = isScheduled;
        this.isDummy = isDummy;
        this.locNum = locNum;
    }
    
    public int getvID() {
        return vID;
    }
    
    public int getOrderLocation() {
        return orderLocation;
    }

    public double getWeight() {
        return weight;
    }

    public int getLowerTime() {
        return lowerTime;
    }

    public int getUpperTime() {
        return upperTime;
    }

    public double getVolume() {
        return volume;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getOriginLSP() {
        return originLSP;
    }

    public int getIsDockerNeeded() {
        return isDockerNeeded;
    }

    public ArrayList<Integer> getMergedOrdersId() {
        return mergedOrdersId;
    }

    public int getAlreadyMeraged() {
        return alreadyMeraged;
    }

    public int getTempMergedOrderId() {
        return tempMergedOrderId;
    }

    public int getId() {
        return id;
    }
    
    public String getStatus() {
    	return status;
    }
    
    public String isScheduled() {
    	return isScheduled;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setvID(int vID) {
        this.vID = vID;
    }

    public void setOrderLocation(int orderLocation) {
        this.orderLocation = orderLocation;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setLowerTime(int lowerTime) {
        this.lowerTime = lowerTime;
    }

    public void setUpperTime(int upperTime) {
        this.upperTime = upperTime;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public void setOriginLSP(int originLSP) {
        this.originLSP = originLSP;
    }

    public void setIsDockerNeeded(int isDockerNeeded) {
        this.isDockerNeeded = isDockerNeeded;
    }

    public void setMergedOrdersId(ArrayList<Integer> mergedOrdersId) {
        this.mergedOrdersId = mergedOrdersId;
    }

    public void setAlreadyMeraged(int alreadyMeraged) {
        this.alreadyMeraged = alreadyMeraged;
    }

    public void setTempMergedOrderId(int tempMergedOrderId) {
        this.tempMergedOrderId = tempMergedOrderId;
    }
    
    public int getLowerTime(DynamicOrder order) {
        return  order.lowerTime;
    }


    public String getCustumerId() {
        return custumerId;
    }

    public String getFoodType() {
        return foodType;
    }

    public int getPickUpLocationId() {
        return pickUpLocationId;
    }

    public void setCustumerId(String custumerId) {
        this.custumerId = custumerId;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public void setPickUpLocationId(int pickUpLocationId) {
        this.pickUpLocationId = pickUpLocationId;
    }

    public ArrayList<DynamicOrder> getMergedOrderList() {
        return mergedOrderList;
    }

    public void setMergedOrderList(ArrayList<DynamicOrder> mergedOrders) {
        this.mergedOrderList = mergedOrders;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public void setisScheduled(String isScheduled) {
        this.isScheduled = isScheduled;
    }
    
    public void setisDummy(boolean b) {
    	this.isDummy = b;
    }
    
    public boolean getisDummy() {
    	return this.isDummy;
    }
    
    public void setlocNum(int locNum) {
    	this.locNum = locNum;
    }
    
    public int getlocNum() {
    	return this.locNum;
    }
}

