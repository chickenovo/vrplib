package PreProcessor;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;

public class FileManager {
    public LSPList readData(String filename) throws FileNotFoundException {
        BufferedReader reader;
        LSPList lspList = new LSPList(0, 0);
        ArrayList<Vehicle> allVehicleList = new ArrayList<>();

        try {
            reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();

            while(line != null){
                String[] splits = line.split(",");

                if (splits[0].equals("overall")){
                    lspList = new LSPList(Integer.parseInt(splits[3]), Integer.parseInt(splits[1]));
                }
                else if (splits[0].equals("vehicle")){
                    int id = Integer.parseInt(splits[1]);
                    int truckLowerTime = Integer.parseInt(splits[2]);
                    int truckUpperTime = Integer.parseInt(splits[3]);
                    double volumeCapacity = Double.parseDouble(splits[4]);
                    double weightCapacity = Double.parseDouble(splits[5]);
                    int originLSP = Integer.parseInt(splits[6]);
                    int truckType = Integer.parseInt(splits[7]);
                    int loadTime = Integer.parseInt(splits[10]);
                    
                    String[] range = splits[9].substring(1, splits[9].length()-1).split(";");
                    int lunchTimeStart = Integer.parseInt(range[0]);
                    int lunchTimeEnd =  Integer.parseInt(range[1]);
                    
                    range = splits[8].substring(1, splits[8].length()-1).split(";");
                    int truckDepotLow = Integer.parseInt(range[0]);
                    int truckDepotHigh = -1;
                    if (!range[1].equalsIgnoreCase("null")) {
                    	truckDepotHigh= Integer.parseInt(range[1]);
                    }
                    
                    Vehicle vehicle = new Vehicle(id, truckLowerTime, truckUpperTime, volumeCapacity, weightCapacity, originLSP, truckType, truckDepotLow,
                    		truckDepotHigh, lunchTimeStart, lunchTimeEnd, loadTime);
                    allVehicleList.add(vehicle);
                }
                else if (splits[0].equals("lsp") & splits.length == 3){
                    int id = Integer.parseInt(splits[1]);

                    LSP lsp = new LSP(id);
                    lspList.addLSP(id, lsp);
                }
                else if (splits[0].equals("order")){
                    int id = Integer.parseInt(splits[1]);
                    int orderLocation = Integer.parseInt(splits[2]);
                    double weight = Double.parseDouble(splits[3]);
                    int lowerTime = Integer.parseInt(splits[4]);
                    int upperTime = Integer.parseInt(splits[5]);
                    double volume = Double.parseDouble(splits[6]);
                    int serviceTime = Integer.parseInt(splits[7]);
                    int originLSP = Integer.parseInt(splits[8]);
                    int isDockerNeeded = Integer.parseInt(splits[9]);

                    String custumerId = "NOT GIVEN";
                    String foodType = "NOT GIVEN";
                    int pickUpLocationId = -1;
                    int pickupTimeFrom = 420;
                    int pickupTimeTo = 1200;

                    if (splits.length >= 11) {
                        custumerId = splits[10];
                    }
                    if (splits.length >= 12) {
                        foodType = splits[11];
                    }
                    if (splits.length >= 13) {
                        pickUpLocationId = Integer.parseInt(splits[12]);
                    }
                    if (splits.length >= 14) {
                        if (!splits[13].isEmpty() && !splits[13].equals("-1") && !splits[13].equals(null)) // if value is not assigned, send the default value to the engine
                            pickupTimeFrom = Integer.parseInt(splits[13]);
                    }
                    if (splits.length >= 15) {
                        if (!splits[14].isEmpty() && !splits[14].equals("-1") && !splits[13].equals(null)) // if value is not assigned, send the default value to the engine
                            pickupTimeTo = Integer.parseInt(splits[14]);
                    }

                    Order order = new Order(id, orderLocation, weight, lowerTime, upperTime, volume, serviceTime, originLSP, isDockerNeeded, custumerId, foodType, pickUpLocationId, pickupTimeFrom, pickupTimeTo);

                    lspList.LSPs.get(originLSP).addOrder(order);
                }
                else if (splits.length == 6 && splits[0].matches("[0-9]+")) {
                	int averageLoadPerVehicle = Integer.parseInt(splits[5]);
                	// Check if averageLoadPerVehicle is different from -1 (-1 means no average load per vehicles mentioned from LSP)
                	if (averageLoadPerVehicle != -1 && averageLoadPerVehicle > 0) {
                		lspList.LSPs.get(Integer.parseInt(splits[0])).AVERAGE_LOAD_PER_VEHICLE = averageLoadPerVehicle;
                	}
                	
                }
                line = reader.readLine();
            }

            for (Vehicle v : allVehicleList){
                lspList.LSPs.get(v.originLSP).addVehicle(v);
            }
            reader.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return lspList;
    }

    public void writeData(String inputFilename, String outputFilename, LSPList lspList) throws FileNotFoundException {
        BufferedWriter writer;
        BufferedReader reader;
        boolean isWrite = false;
        boolean isWrite2 = false; // This flag is for vehicle

        try {
            writer = new BufferedWriter(new FileWriter(outputFilename));
            reader = new BufferedReader(new FileReader(inputFilename));

            String line = reader.readLine();

            while(line != null){
                String[] splits = line.split(",");

                if (splits[0].equals("overall")){
                    splits[3] = String.valueOf(lspList.overallCount);
                    int totalVehicles = 0;
                    for (Map.Entry<Integer, LSP> entry : lspList.LSPs.entrySet()) {
                    	totalVehicles += entry.getValue().vehicleList.size();
                    }
                    splits[2] = String.valueOf(totalVehicles);
                    String newLine = String.join(",", splits);
                    writer.write(newLine);
                }
                else if (splits[0].equals("order")){

                    if (isWrite == false) {

                        for (Map.Entry<Integer, LSP> entry : lspList.LSPs.entrySet()) {
                            for (Order order:entry.getValue().mergedOrders){
                                ArrayList<String> orderLine = new ArrayList<>();

                                orderLine.add("order");
                                orderLine.add(String.valueOf(order.id));
                                orderLine.add(String.valueOf(order.orderLocation));
                                orderLine.add(String.valueOf(order.weight));
                                orderLine.add(String.valueOf(order.lowerTime));
                                orderLine.add(String.valueOf(order.upperTime));
                                orderLine.add(String.valueOf(order.volume));
                                orderLine.add(String.valueOf(order.serviceTime));
                                orderLine.add(String.valueOf(order.originLSP)); // TODO Check
                                orderLine.add(String.valueOf(order.isDockerNeeded));

                                if (splits.length >= 11) {
                                    orderLine.add(order.custumerId);
                                }
                                if (splits.length >= 12) {
                                    orderLine.add(order.foodType);
                                }
                                if (splits.length >= 13) {
                                    orderLine.add(String.valueOf(order.pickUpLocationId));
                                }

                                if (splits.length >= 14) {
                                    orderLine.add(String.valueOf(order.pickupTimeFrom));
                                }
                                if (splits.length >= 15) {
                                    orderLine.add(String.valueOf(order.pickupTimeTo));
                                }

                                writer.newLine();

                                line = "";
                                for (String s : orderLine){
                                    line = line + "," + s;
                                }
                                writer.write(line.substring(1));
                            }
                        }
                    }

                    isWrite = true;

                }
                else if (splits[0].equals("vehicle")) {
                	
                	if (isWrite2 == false) {
                		
                		for (Map.Entry<Integer, LSP> entry : lspList.LSPs.entrySet()) {
                			for (Vehicle vehicle:entry.getValue().vehicleList) {
                				ArrayList<String> vehicleLine = new ArrayList<>();
                				
                				vehicleLine.add("vehicle");
                				vehicleLine.add(String.valueOf(vehicle.id));
                				vehicleLine.add(String.valueOf(vehicle.truckLowerTime));
                				vehicleLine.add(String.valueOf(vehicle.truckUpperTime));
                				vehicleLine.add(String.valueOf(vehicle.capacity));
                				vehicleLine.add(String.valueOf(vehicle.weightCapacity));
                				vehicleLine.add(String.valueOf(vehicle.originLSP));
                				vehicleLine.add(String.valueOf(vehicle.truckType));
                				vehicleLine.add('[' + String.valueOf(vehicle.truckDepotLow) + ';' + (vehicle.truckDepotHigh != -1? String.valueOf(vehicle.truckDepotHigh): "null") + ']');
                				vehicleLine.add('[' + String.valueOf(vehicle.lunchTimeStart) + ';' + String.valueOf(vehicle.lunchTimeEnd) + ']');
                				vehicleLine.add(String.valueOf(vehicle.loadTime));
                				
                                writer.newLine();

                                line = "";
                                for (String s : vehicleLine){
                                    line = line + "," + s;
                                }
                                writer.write(line.substring(1));
                			}
                		}
                		
                		isWrite2 = true;
                	}
                	
                }
                else{
                    //System.out.println(line);
                    writer.newLine();
                    writer.write(line);
                }
                line = reader.readLine();


            }
            reader.close();
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
