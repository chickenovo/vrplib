package PreProcessor;

import java.util.ArrayList;
import java.util.List;

public class Order {
    public int id;
    public int orderLocation;
    public double weight;
    public int lowerTime;
    public int upperTime;
    public double volume = 0;
    public int serviceTime;
    public int originLSP;
    //public int empty; //TODO
    public int isDockerNeeded;
    public ArrayList<Integer> mergedOrdersId = new ArrayList<>();
    public ArrayList<Order> mergedOrderList = new ArrayList<>();
    public ArrayList<Integer> sortedMergedOrdersId = new ArrayList<>();
	public ArrayList<Order> sortedMergedOrderList = new ArrayList<>();
    public int alreadyMeraged = -1;
    public int tempMergedOrderId = -1;
    String custumerId = "NOT GIVEN";
    String foodType = "NOT GIVEN";
    int pickUpLocationId = -1;
    int pickupTimeFrom;
    int pickupTimeTo;
    
    public double posVolume = 0;
	public double negVolume = 0;   
    public boolean isCollectionOrder = false;

    public Order(int id, int orderLocation, double weight, int lowerTime, int upperTime,
                 double volume, int serviceTime, int originLSP, int isDockerNeeded, String custumerId, String foodType,
                 int pickUpLocationId, int pickupTimeFrom, int pickupTimeTo) {
        this.id = id;
        this.weight = weight;
        this.isDockerNeeded = isDockerNeeded;
        this.volume = volume;
        this.upperTime = upperTime;
        this.lowerTime = lowerTime;
        this.serviceTime = serviceTime;
        this.originLSP = originLSP;
        this.orderLocation = orderLocation;
        // this.mergedOrdersId.add(id);
        tempMergedOrderId = id;
        this.custumerId = custumerId;
        this.foodType = foodType;
        this.pickUpLocationId = pickUpLocationId;
        this.pickupTimeTo = pickupTimeTo;
        this.pickupTimeFrom = pickupTimeFrom;
    }
    
    public void setPosNegVolume(double posVolume, double negVolume) {
		this.posVolume = posVolume;
		this.negVolume = negVolume;
	}

    public int getOrderLocation() {
        return orderLocation;
    }

    public double getWeight() {
        return weight;
    }

    public int getLowerTime() {
        return lowerTime;
    }

    public int getUpperTime() {
        return upperTime;
    }

    public double getVolume() {
        return volume;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getOriginLSP() {
        return originLSP;
    }

    public int getIsDockerNeeded() {
        return isDockerNeeded;
    }

    public ArrayList<Integer> getMergedOrdersId() {
        return mergedOrdersId;
    }

    public int getAlreadyMeraged() {
        return alreadyMeraged;
    }

    public int getTempMergedOrderId() {
        return tempMergedOrderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrderLocation(int orderLocation) {
        this.orderLocation = orderLocation;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setLowerTime(int lowerTime) {
        this.lowerTime = lowerTime;
    }

    public void setUpperTime(int upperTime) {
        this.upperTime = upperTime;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public void setOriginLSP(int originLSP) {
        this.originLSP = originLSP;
    }

    public int getPickupTimeFrom() {
        return pickupTimeFrom;
    }

    public int getPickupTimeTo() {
        return pickupTimeTo;
    }

    public void setPickupTimeFrom(int pickupTimeFrom) {
        this.pickupTimeFrom = pickupTimeFrom;
    }

    public void setPickupTimeTo(int pickupTimeTo) {
        this.pickupTimeTo = pickupTimeTo;
    }

    public void setIsDockerNeeded(int isDockerNeeded) {
        this.isDockerNeeded = isDockerNeeded;
    }

    public void setMergedOrdersId(ArrayList<Integer> mergedOrdersId) {
        this.mergedOrdersId = mergedOrdersId;
    }

    public void setAlreadyMeraged(int alreadyMeraged) {
        this.alreadyMeraged = alreadyMeraged;
    }

    public void setTempMergedOrderId(int tempMergedOrderId) {
        this.tempMergedOrderId = tempMergedOrderId;
    }
    public int getLowerTime(Order order) {
        return  order.lowerTime;
    }


    public String getCustumerId() {
        return custumerId;
    }

    public String getFoodType() {
        return foodType;
    }

    public int getPickUpLocationId() {
        return pickUpLocationId;
    }

    public void setCustumerId(String custumerId) {
        this.custumerId = custumerId;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public void setPickUpLocationId(int pickUpLocationId) {
        this.pickUpLocationId = pickUpLocationId;
    }

    public ArrayList<Order> getMergedOrderList() {
        return mergedOrderList;
    }

    public void setMergedOrderList(ArrayList<Order> mergedOrders) {
        this.mergedOrderList = mergedOrders;
    }

}
