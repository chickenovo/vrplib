package PreProcessor;

import java.util.*;

import PreProcessor.Order;
import Data.PathSolution;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class LSP {
	public ArrayList<ArrayList<Order>> route = new ArrayList<ArrayList<Order>>();
	public int timeVio = 0;

	public int id;
	public ArrayList<Order> orderList = new ArrayList<>();
	public ArrayList<Vehicle> vehicleList = new ArrayList<>();
	public ArrayList<Order> mergedOrders = new ArrayList<>();

	public int pickupMinOverLap = 20;

	public HashMap<Integer, Integer> minOverlapTimeForEachOrderDestination = new HashMap<Integer, Integer>();

	public int minOverLap = 20;

	public int AVERAGE_LOAD_PER_VEHICLE = 10;
	Random rand = new Random();;

	public LSP(int id) {
		this.id = id;
	}

	public void addOrder(Order order) {
		this.orderList.add(order);
	}

	public void addVehicle(Vehicle vehicle) {
		this.vehicleList.add(vehicle);
	}

	public Order getOrder(int id) {
		return this.orderList.get(id);
	}

	public Vehicle getVehicle(int id) {
		return this.vehicleList.get(id);
	}
	
    class HandleVehicle {
    	double capacity;
    	double currentCapacity = 0;
    	boolean [] time;
    	Vehicle vehicle;
    }
    
    public void findK() {
    	ArrayList<HandleVehicle> hv = new ArrayList<HandleVehicle>(this.vehicleList.size());
    	
    	int LOADING_TIME = 60;
    	
    	// Find average service time
    	int serviceTime = 0;
    	for (Order order : this.mergedOrders) 
    		serviceTime += order.serviceTime;
    	double avgServiceTime = serviceTime / this.mergedOrders.size();
    	int HARD_TIME = 30 + (int) Math.round(avgServiceTime); // 30 is catered for wait + move time
    	
    	//Sort vehicles by capacity and by available time
		Collections.sort(this.vehicleList, new Comparator<Vehicle>() {
			@Override
			public int compare(Vehicle v1, Vehicle v2) {
				Double v2_cap = v2.capacity;
				Double v1_cap = v1.capacity;
				int sComp = v2_cap.compareTo(v1_cap);
				if (sComp != 0) {
					return sComp;
				}
				
				Integer v2_avail = v2.truckUpperTime - v2.truckLowerTime;
				Integer v1_avail = v1.truckUpperTime - v1.truckLowerTime;
				
				return v2_avail.compareTo(v1_avail);
			}
		});
		
    	for (Vehicle v : this.vehicleList) {
    		int availableTime = v.truckUpperTime - v.truckLowerTime - 1*LOADING_TIME;
    		int numberOfTimeSectors = (int) Math.floor((double) availableTime / HARD_TIME);
    		HandleVehicle cc = new HandleVehicle();
    		cc.capacity = v.capacity;
    		cc.time = new boolean[numberOfTimeSectors];
    		cc.vehicle = v;
    		hv.add(cc);
    	}
    	
    	
    	// Short merged order by time windows
		Collections.sort(this.mergedOrders, new Comparator<Order>() {
			@Override
			public int compare(Order v1, Order v2) {
				return Integer.compare((v1.upperTime - v1.lowerTime) ,(v2.upperTime - v2.lowerTime));
			}
		});
		
		// Loop through orders and fill in the empty vehicles
		
		for (Order order : this.mergedOrders) {
			int lowerTime = order.lowerTime;
			int upperTime = order.upperTime;
			int counter = 0;
			for (Vehicle vehicle : this.vehicleList) {
				if (((vehicle.truckLowerTime <= lowerTime) && (vehicle.truckUpperTime >= upperTime)) || (vehicle.truckLowerTime <= upperTime)) {
					HandleVehicle cc = hv.get(counter);
					if ((cc.currentCapacity + order.volume) > cc.capacity) {
						counter += 1;
						continue;
					}
					int firstSector = (int) Math.round((lowerTime - vehicle.truckLowerTime - LOADING_TIME) / (double) HARD_TIME);
					firstSector = Math.min(Math.max(0, firstSector), cc.time.length-1); // to prevent firstSector < 0
					int lastSector = ((int) Math.round((upperTime - vehicle.truckLowerTime - LOADING_TIME) / (double) HARD_TIME)) -1;
					lastSector = Math.min(cc.time.length-1, Math.max(Math.max(lastSector, 1), firstSector)); // max inside to prevent lastSector < 1 and min outside to prevent lastSector > time.length which could be happen if order's time windows are near the end of the veh's time window
					int middleUp = Math.min((int) Math.ceil((firstSector + lastSector) / 2), lastSector);
					int middleDown = Math.max(middleUp, firstSector);
					boolean isFit = false;
					//System.out.println(lowerTime);
					//System.out.println(upperTime);
					//System.out.println(firstSector);
					//System.out.println(lastSector);
					if ((firstSector == 9 ) && (lastSector == 6 )) {
						System.out.println("ok");
					}
					while (true && (middleDown >= firstSector) && (middleUp <= lastSector)) {
						if (cc.time[middleUp] == false) {
							cc.time[middleUp] = true;
							isFit = true;
							cc.currentCapacity += order.volume;
							break;
						}
						if (cc.time[middleDown] == false) {
							cc.time[middleDown] = true;
							isFit = true;
							cc.currentCapacity += order.volume;
							break;
						}
						
						if (middleUp == lastSector && middleDown == firstSector) {
							isFit = false;
							break;
						}
						
						if (middleUp < lastSector)
							middleUp += 1;
						if (middleDown > firstSector)
							middleDown -= 1;
					}
					
					if (isFit == false) {
						counter += 1;
					} else {
						break;
					}
				}
			}
		}
		
		ArrayList<Vehicle> finalVehicles = new ArrayList<Vehicle>();
		for (HandleVehicle cc : hv) {
			for (boolean b : cc.time) {
				if (b == true) {
					finalVehicles.add(cc.vehicle);
					break;
				}
			}
		}
		this.vehicleList = finalVehicles;
		//this.vehicleList = new ArrayList<>(this.vehicleList.subList(0, 9));
		System.out.println("Size of vehicle needs in pre-processing: " + this.vehicleList.size());
    }
    

	// According to paper A Two-stage hybrid local search for the vehicle routing
	// problem with time windows
	public void minNumberVehicles() {
		int k = (int) Math.ceil((mergedOrders.size() / (double) AVERAGE_LOAD_PER_VEHICLE));
		int o = 0;
		double temperature = 2000;
		double coolFactor = 0.95;
		ArrayList<Vehicle> vehicleListTemp = new ArrayList<>(this.vehicleList);
		for (int i = 0; i < 5000; i++) {
			o = rand.nextInt(3);
			// dominate, if same number of vehicle, higher r^2 win, if different number of
			// vehicles, larger win
			temperature *= coolFactor;

		}
		this.vehicleList = new ArrayList<>(vehicleListTemp.subList(0, k));

	}	
	
	public void pickupPreProcess() {	
    	//Pickup location is depotID for normal delivery orders
    	//orderLocation is depotID for pickup orders
    	for(Order order: orderList) {
    		if(order.volume < 0) {						
    			System.out.println("pickup order id: "+order.id);		
    			//Swap pickup location and delivery location for such orders
    			//so that mergeOrders() is able to handle merging of pickup orders
    			int originalOrderLocationID = order.orderLocation;
    			order.orderLocation = order.pickUpLocationId;
    			order.pickUpLocationId = originalOrderLocationID;
    			order.isCollectionOrder = true;
    			order.negVolume = - order.volume;
  
    		} else {
    			order.isCollectionOrder = false;
    			order.posVolume = order.volume;
    		
    		}
    	}
    	
    }
	
	
	public void mergeOrders() {

		HashMap<Integer, ArrayList<Order>> ordersBasedOnLocation = new HashMap<>();

		// sort orders by location
		for (Order order : orderList) {
			if (ordersBasedOnLocation.containsKey(order.orderLocation)) {
				ordersBasedOnLocation.get(order.orderLocation).add(order);
			} else {
				ordersBasedOnLocation.put(order.orderLocation, new ArrayList<Order>());
				ordersBasedOnLocation.get(order.orderLocation).add(order);
			}
		}
		
		//added new code to sort orders by customerId and commented out below code here
		for (Map.Entry<Integer, ArrayList<Order>> entry : ordersBasedOnLocation.entrySet()) {
			Collections.sort(entry.getValue(), new Comparator<Order>() {
				@Override
				public int compare(Order o1, Order o2) {
					if (o1.custumerId.equalsIgnoreCase(o2.custumerId))
						return 0;
					return o1.volume < o2.volume ? -1 : 1;
				}

			});
		} 
		// end of newly added code

		/* sort orders by capacity
		// If it is better sort it according to customerID
		for (Map.Entry<Integer, ArrayList<Order>> entry : ordersBasedOnLocation.entrySet()) {
			Collections.sort(entry.getValue(), new Comparator<Order>() {
				@Override
				public int compare(Order o1, Order o2) {
					if (o1.volume == o2.volume)
						return 0;
					return o1.volume < o2.volume ? -1 : 1;
				}

			});
		} */

		// sort vehicle according to the capacity
		Collections.sort(vehicleList, (Comparator<Vehicle>) (v1, v2) -> {
			if (v1.capacity == v2.capacity)
				return 0;
			return v1.capacity < v2.capacity ? -1 : 1;
		});

		for (Map.Entry<Integer, ArrayList<Order>> entry : ordersBasedOnLocation.entrySet()) {
			ArrayList<Order> tempList = entry.getValue();
			int count = 0;
			ArrayList<Order> tempMergedOrders = new ArrayList<>();

			for (int i = 0; i < tempList.size(); i++) {
				Boolean isCurrentOrderMerged = false;
				Order currentOrder = tempList.get(i);

				if (i == 0) { // add deport condition also **********************
					tempList.get(i).alreadyMeraged = 0;
					Order o = tempList.get(i);
					o.mergedOrdersId.add(o.id);
					o.alreadyMeraged = 1; // 1 mean order is result of a after merge process, but actual it is not
											// merged with a order
					tempMergedOrders.add(o);

				} else { // else need to be iterate trough merged order list and have to find a suitable
							// place

					for (int j = tempMergedOrders.size() - 1; j >= 0; j--) {
						Order previousOrder = tempMergedOrders.get(j);
						count = j;
						isCurrentOrderMerged = mergeTwoOrders(tempMergedOrders, tempList, currentOrder, previousOrder,
								i, count, currentOrder.originLSP);
						if (isCurrentOrderMerged)
							break;
					}

					if (!isCurrentOrderMerged) { // tempList.get(i).alreadyMeraged == -1
						tempList.get(i).alreadyMeraged = 0;
						Order o = tempList.get(i);
						o.mergedOrdersId.add(o.id);
						o.alreadyMeraged = 1; // 1 mean order is result of a after merge process, but actual it is not
												// mergerd with a order
						tempMergedOrders.add(o);
					}
				}
			}
			mergedOrders.addAll(tempMergedOrders);
		}
		
		// Update cumulative negative and positive volumes of merged orders
		for (Order order : mergedOrders) {
	        double positiveVolume = 0;
	        double negativeVolume = 0;
	        
	        for(Order o:orderList) {
	        	if(order.mergedOrdersId.get(0) == o.id) {
	        		if(o.isCollectionOrder) {
	        			negativeVolume = o.negVolume;
	        		} else {
	        			positiveVolume = o.posVolume;
	        		}
	        	}
	        }
	        
	        for(Order innerOrder: order.mergedOrderList) {
	        	//System.out.println(order.id);
	        	if(innerOrder.isCollectionOrder) {
	        		negativeVolume += innerOrder.negVolume;
	        	} else {
	        		positiveVolume += innerOrder.posVolume;
	        	}   	
	        }
	        
	        order.setPosNegVolume(positiveVolume, negativeVolume);   
	        
	        /*
	        if(order.volume < 0) {
	        	//if the constituent order are all collection orders swap
	        	//pickup location and delivery location for such orders back to original
	        	    	
	        	boolean allCollection = false;
	        	for(int constituentOrderID: order.mergedOrdersId ) {
	        		for(Order o:orderList) {
	        			if(o.id == constituentOrderID) {
	        				if(o.volume < 0) {
	        					allCollection = true;
	        				} else {
	        					allCollection = false;
	        				}
	        			}
	        		}
	        		
	        	}
	        	
	        	if(allCollection) {
	        		int originalPickUpLocationID = order.pickUpLocationId;
		        	order.pickUpLocationId = order.orderLocation;
		        	order.orderLocation = originalPickUpLocationID;
	        		
	        	}
	        	
	        } */
	    }
		
		// Sort merged orders based on upper time window
		for (Order mergedOrder : mergedOrders) {
			for(Integer oid: mergedOrder.mergedOrdersId) {
				for(Order o1:orderList) {
					if(oid == o1.id) {
						mergedOrder.sortedMergedOrderList.add(o1);
					}
				}
			}
			Collections.sort(mergedOrder.sortedMergedOrderList, (Comparator<Order>) (o1, o2) -> {
				if (o1.upperTime == o2.upperTime)
					return 0;
				return o1.upperTime < o2.upperTime ? -1 : 1;
			});	
						
			for(Order io: mergedOrder.sortedMergedOrderList) {
				mergedOrder.sortedMergedOrdersId.add(io.id);
				//System.out.println("in merge function "+mergedOrder.id+" "+io.id+" "+io.upperTime);
			}
		}		
	}

	public boolean mergeTwoOrders(ArrayList<Order> tempMergedOrders, ArrayList<Order> tempList, Order currentOrder,
			Order previousOrder, int indexOfCurrentOrder, int indexOfPreviousOrder, int originLSP) {
		boolean isCurrentOrderMerged = false;

		int L1 = currentOrder.lowerTime;
		int L2 = previousOrder.lowerTime;
		int U1 = currentOrder.upperTime;
		int U2 = previousOrder.upperTime;
		int maxL = max(L1, L2);
		int minU = min(U1, U2);
		boolean isTWOverLap = maxL < minU;
		int TWOverLap = minU - maxL;

		int PL1 = currentOrder.pickupTimeFrom;
		int PL2 = previousOrder.pickupTimeFrom;
		int PU1 = currentOrder.pickupTimeTo;
		int PU2 = previousOrder.pickupTimeTo;
		int pmaxL = max(PL1, PL2);
		int pminU = min(PU1, PU2);
		boolean isPTWOverLap = pmaxL < pminU;
		int PTWOverLap = pminU - pmaxL;

		double totalCap = currentOrder.volume + previousOrder.volume;
		int st = currentOrder.serviceTime + previousOrder.serviceTime;
		boolean isSamePickUpLocation = (currentOrder.pickUpLocationId == previousOrder.pickUpLocationId ? true : false); // change
																															// according
																															// to
																															// the
																															// requirement
		boolean isSameOrderLocation = (currentOrder.orderLocation == previousOrder.orderLocation ? true : false);
		
		
		boolean isSameFoodType = (currentOrder.foodType).equalsIgnoreCase(previousOrder.foodType); // change according
																									// to the
																								// requirement
		
		//boolean isCollection = (((L1==PL2) &&  (U1==PU2) && isSameFoodType && isSamePickUpLocation && isSameOrderLocation) ? true : false);
		boolean TWExactMatch = ((L1==L2 && U1==U2) ? true : false);
		//for merging collection and delivery orders
		boolean isOverlap = (max(PL2, L1) < min(PU2, U1));
		int overlap = min(PU2, U1) - max(PL2, L1);
		
		boolean dropOff_pickup_exactMatch = (((L1==PL2 && U1==PU2) || (PL1==L2 && PU1==U2)) ? true : false);
		boolean both_pickup_exactMatch =  ((PL1==PL2 && PU1==PU2) ? true : false);
		
		boolean isCollection = ((((overlap >= minOverLap && overlap >=max(previousOrder.serviceTime, currentOrder.serviceTime))  
										|| (dropOff_pickup_exactMatch) || (both_pickup_exactMatch))
				&& isSameFoodType && isSamePickUpLocation && isSameOrderLocation 
				&& (previousOrder.isCollectionOrder || currentOrder.isCollectionOrder)) ? true : false);
		
		//System.out.println("isCollection "+isCollection+" "+previousOrder.id+" "+currentOrder.id+" "+
		//		dropOff_pickup_exactMatch+" "+overlap+" "+previousOrder.volume+" "+currentOrder.volume +" "+
		//		previousOrder.isCollectionOrder+" "+currentOrder.isCollectionOrder);
		
		boolean isvehicleCanAllocate = false;

		for (Vehicle v : vehicleList) {
			// isvehicleCanAllocate = v.checkVehicleHaveEnoughCapacity(totalCap,
			// whatTimeCanAllocate, true);
			if (totalCap <= v.capacity)
				isvehicleCanAllocate = true;

			if (isvehicleCanAllocate)
				break;
		}
		
		//System.out.println("TWExactMatch "+ TWExactMatch+" "+previousOrder.id+" "+currentOrder.id+" "+previousOrder.volume+" "+currentOrder.volume);
		//System.out.println(isPTWOverLap+" "+PTWOverLap+" "+pickupMinOverLap+" "+max(previousOrder.serviceTime, currentOrder.serviceTime)+" "+PL1+" "+PU1+" "+PL2+" "+PU2 );
		
		if ((((isTWOverLap && TWOverLap >= minOverLap && TWOverLap >=max(previousOrder.serviceTime, currentOrder.serviceTime)) || (TWExactMatch) ) 
				//&& isPTWOverLap && PTWOverLap >= pickupMinOverLap &&  PTWOverLap >= max(previousOrder.serviceTime, currentOrder.serviceTime) 
				&& isPTWOverLap && PTWOverLap >= pickupMinOverLap 
				//&& (st + maxL) < (minU + st / 2) && isSameFoodType && isSamePickUpLocation && isSameOrderLocation) || isCollection) {
				&& isSameFoodType && isSamePickUpLocation && isSameOrderLocation) || isCollection) {
			if (isvehicleCanAllocate) {
				// create new order

				isCurrentOrderMerged = true;
				int minID = min(currentOrder.id, previousOrder.id);

				double weight = currentOrder.weight + previousOrder.weight;
				int lowerTime = maxL;
				int upperTime = minU;
				int pickupLowerTime = pmaxL;
				int pickupUpperTime = pminU;
				double volume = currentOrder.volume + previousOrder.volume;
				// int serviceTime = nextOrder.serviceTime+ currentOrder.serviceTime;
				int serviceTime = 0;
				String customerId = "NOT GIVEN";
				String foodType = currentOrder.foodType;
				int pickupLocationId = currentOrder.pickUpLocationId;

				// previous merged orders have two different customer id. so resultant order
				// customerID changes to "MergedIDs"
				if (!previousOrder.custumerId.equalsIgnoreCase("MergedIDs")) {
					// "NOT GIVEN" is used if customer id is not provided, so we cann't reduce the
					// service time
					if (!currentOrder.custumerId.equalsIgnoreCase("NOT GIVEN")
							&& !previousOrder.custumerId.equalsIgnoreCase("NOT GIVEN")) {
						if (currentOrder.custumerId.equalsIgnoreCase(previousOrder.custumerId)) {
						
							// serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime; //no need
							// to divide current merged order time window
							// previous commented logic is used to dived service time by two if customerID
							// is same
							serviceTime = currentOrder.serviceTime < previousOrder.serviceTime
									? previousOrder.serviceTime
									: currentOrder.serviceTime; // get the maximum customer ID of two orders
							customerId = currentOrder.custumerId;
						} else {
							serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
							customerId = "MergedIDs";
						}
					} else {
						serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
						customerId = "MergedIDs";
					}
				} else {
					ArrayList<Integer> serveiceTimeOfSameCustomerID = new ArrayList<>();
					Integer maxServiceTime = 0;

					for (int z = 0; z < previousOrder.getMergedOrderList().size(); z++) {
						Order temp = previousOrder.getMergedOrderList().get(z);
						if (!currentOrder.custumerId.equalsIgnoreCase("NOT GIVEN")
								&& !temp.custumerId.equalsIgnoreCase("NOT GIVEN")) {
							if (currentOrder.custumerId.equalsIgnoreCase(temp.custumerId)) {
								serveiceTimeOfSameCustomerID.add(temp.serviceTime);
								maxServiceTime = maxServiceTime < temp.serviceTime ? temp.serviceTime : maxServiceTime;// add
																														// extra
																														// logic
																														// to
																														// filter
																														// the
																														// max
																														// service
																														// time
																														// of
																														// batch
							}
						}

					}

					// if (serveiceTimeOfSameCustomerID.size() == 0 ) {
					// serviceTime = currentOrder.serviceTime + nextOrder.serviceTime;
					// customerId = "MergedIDs";
					// } else if (serveiceTimeOfSameCustomerID.size() == 1) {
					// serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime -
					// serveiceTimeOfSameCustomerID.get(0)/2;
					// customerId = "MergedIDs";
					// } else {
					// serviceTime = currentOrder.serviceTime/2 + nextOrder.serviceTime; //no need
					// to divide current merged order time window
					// customerId = "MergedIDs";
					// }

					// previous commented part is used for if service time is divided by two.

					if (serveiceTimeOfSameCustomerID.size() == 0) {
						serviceTime = currentOrder.serviceTime + previousOrder.serviceTime;
						customerId = "MergedIDs";
					} else {
						serviceTime = (maxServiceTime < currentOrder.serviceTime ? currentOrder.serviceTime
								: maxServiceTime) + previousOrder.serviceTime - maxServiceTime;
						customerId = "MergedIDs";
					}

				}

				int isDockerNeeded = currentOrder.isDockerNeeded; // check
				int location = currentOrder.orderLocation;

				ArrayList<Integer> tempMergedOrdersId = previousOrder.mergedOrdersId;
				ArrayList<Order> tempMergedOrderslist = previousOrder.mergedOrderList;
				tempMergedOrdersId.add(currentOrder.id);
				Order newMergedOrder = new Order(minID, location, weight, lowerTime, upperTime, volume, serviceTime,
						originLSP, isDockerNeeded, customerId, foodType, pickupLocationId, pickupLowerTime,
						pickupUpperTime);
				// add current order to orderList of mergeOrders
				tempMergedOrderslist.add(currentOrder);
				//System.out.println(currentOrder.id);
				newMergedOrder.mergedOrderList = tempMergedOrderslist;
				newMergedOrder.alreadyMeraged = 2; // merge so value is 1
				newMergedOrder.tempMergedOrderId = minID;
				newMergedOrder.mergedOrdersId = tempMergedOrdersId;
				tempMergedOrders.set(indexOfPreviousOrder, newMergedOrder);
				tempList.get(indexOfCurrentOrder).alreadyMeraged = 0;
				
				if(previousOrder.isCollectionOrder && currentOrder.isCollectionOrder) {
					newMergedOrder.isCollectionOrder = true;
				}
				
				
			}

		}

		return isCurrentOrderMerged;
	}
}