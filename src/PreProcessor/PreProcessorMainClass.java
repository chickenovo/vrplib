package PreProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class PreProcessorMainClass {
	HashMap<Integer, Integer> vehicleIdMap;
	HashMap<Integer, List<Integer>> orderIdMap;
	HashMap<Integer, double[]> volumeIdMap;

    public void RunPreProcessor(String inputFile , String outputFile) throws IOException{
        String inputFileName = inputFile;
        String outputFileName = outputFile;
        FileManager reader = new FileManager();
        LSPList lspList = reader.readData(inputFileName);

        lspList.merge();
        
        // k-logic in pre-processing
        lspList.calulateVehicles();               
        
        lspList.updateOverallCount();
        this.orderIdMap = lspList.updateId();
        this.vehicleIdMap = lspList.updateVehicleId();
        this.volumeIdMap = lspList.updateVolume();

        reader.writeData(inputFileName, outputFileName, lspList);
    }
    

    public HashMap<Integer, Integer> getVehicleIdMap() {
    	return this.vehicleIdMap;
    }

    public HashMap<Integer, List<Integer>> getOrderIdMap() {
        return this.orderIdMap;
    }
    
	public HashMap<Integer, double[]> getCollectionOrders() {
		HashMap<Integer, double[]> collectionOrders = new HashMap<Integer, double[]>();
		for(Map.Entry<Integer, double[]> entry: volumeIdMap.entrySet()) {
			if(entry.getValue()[1] > 0) {
				double[] value = new double[2];
				value[0] = entry.getValue()[0];
				value[1] = - entry.getValue()[1];
				collectionOrders.put(entry.getKey(), value);
			}
		}
		
		return collectionOrders;
	}

}
