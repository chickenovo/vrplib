package PreProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LSPList {
    public HashMap<Integer, LSP> LSPs = new HashMap<>();
    public int overallCount;
    public int lspCount;

    public LSPList(int overallCount, int lspCount){
        this.overallCount = overallCount;
        this.lspCount = lspCount;
    }

    public void addLSP(int id, LSP lsp){
        this.LSPs.put(id, lsp);
    }

    public void updateOverallCount(){
        this.overallCount = 0;

        for (Map.Entry<Integer, LSP> entry : this.LSPs.entrySet()){
            this.overallCount += entry.getValue().mergedOrders.size();
        }
    }

    public void merge(){
        for (Map.Entry<Integer, LSP> entry : this.LSPs.entrySet()){
        	entry.getValue().pickupPreProcess();
            entry.getValue().mergeOrders();
            //entry.getValue().pickupPostProcess();
        }
    }
    
    /**
     * Calculating the required vehicles for each LSP
     */
	
    public void calulateVehicles() {
    	for (Map.Entry<Integer, LSP> entry : this.LSPs.entrySet()) {
    		entry.getValue().findK();
    	}
    }
    
    /**
     * This function returns a hashmap which maps between the old id of vehicle and the new id after being sorted by k-most-capacity
     * @return outputMap: a Map contains mapping between the new sorted id and old id of vehicles
     */
    
    public HashMap<Integer, Integer> updateVehicleId() {
    	int id = 1;
    	HashMap<Integer, Integer> outputMap = new HashMap<>();
        
    	for (Map.Entry<Integer, LSP> entry: this.LSPs.entrySet()) {
    		for (Vehicle vehicle : entry.getValue().vehicleList) {

    			outputMap.put(id, vehicle.id);
                vehicle.id = id;
    			id += 1;
    		}
    	}
    	
    	return outputMap;
    }

    public HashMap<Integer, List<Integer>> updateId(){
        int id = 1;
        HashMap<Integer, List<Integer>> outputMap= new HashMap<>();

        for (Map.Entry<Integer, LSP> entry : this.LSPs.entrySet()){
            for (Order order : entry.getValue().mergedOrders) {
                order.id = id;
                //outputMap.put(id, order.mergedOrdersId);
                outputMap.put(id, order.sortedMergedOrdersId);
                id += 1;
            }
        }

        return outputMap;

    }
    
    //updateVolume() updates the positive and negative volumes of merged orders/
    public HashMap<Integer, double[]> updateVolume() {
    	HashMap<Integer, double[]> volumeMap = new HashMap<>();
    	for (Map.Entry<Integer, LSP> entry : this.LSPs.entrySet()){
            for (Order order : entry.getValue().mergedOrders) {
            	double [] posnegVolume = new double[2];
            	posnegVolume[0] = order.posVolume;
            	posnegVolume[1] = order.negVolume;
            	//System.out.println(order.negVolume);
            	volumeMap.put(order.id, posnegVolume);
            }       
        }  	
    	return volumeMap;
    }
}
