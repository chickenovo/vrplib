package Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

public class Lsp {
	public int id;
	public int rank;
	public HashSet<Order> lspRealOrders;
	public HashMap<Integer,Integer> lspVechiclesHiddenMap;
	public Vector<Integer> lspVehicles;
	public List<Vehicle> lspVehiclesObject;
	double maxUtilize;
	double maxTime;
	double minTime;
	double maxTripDistanceDepotExclude;
	double maxSimpleTripDistance;
	public boolean increasedDrops = false;
	public double totalDistance;
	double lunchMinVioCost = 10;

	public int totalCar;
	public int totalSize;
	public HashMap<Integer, Integer> vehicleMap;
	public HashSet<Integer> locations;
	public ArrayList<Integer> realLocations;
	public int maxDrop = 1000;
	double minLunch = 0;
	double maxEndTime = 11100;
	public double timeLimit;

	public HashMap<Integer, Boolean> locationCheckCP;

	public int multiTripCnt = 2;
	public HashSet<Order> multiDepotOrders;
	public Parameter parameter;
	public Boolean lunchLocationFlag = true;
	public boolean needCheckMaxDrop = true;

	public Lsp(HashSet<Order> lspRealOrders, Vector<Integer> lspVehicles) {
		this.lspRealOrders = lspRealOrders;
		this.lspVehicles = lspVehicles;
		this.locations = new HashSet<Integer>();
		this.vehicleMap = new HashMap<Integer, Integer>();
		this.locationCheckCP = new HashMap<Integer, Boolean>();
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public void setExtra(int maxDrop, double maxEndTime, double minLunch) {
		if (maxDrop < 0) {
			this.maxDrop = 1000;
			needCheckMaxDrop = false;
		} else {
			this.maxDrop = maxDrop;
		}

		if (minLunch <= 0) {
			this.lunchLocationFlag = false;
			this.minLunch = -1;
		} else {
			this.minLunch = minLunch;
		}
		
		if (maxEndTime < 0)
			this.maxEndTime = 10000;
		else {
			this.maxEndTime = maxEndTime;
		}
	}

	public void setTotalSize() {
		this.totalSize = lspRealOrders.size();
		this.totalCar = lspVehicles.size();
	}

	public void setTimeLimit(double timeLimit) {
		this.timeLimit = timeLimit;
	}

	public void setRealLocations() {
		realLocations = new ArrayList<Integer>();
		for (Integer u : locations)
			realLocations.add(u);
	}

	public void setVechicleMap(ArrayList<Vehicle> vehicles) {
		int cnt = 0;
		this.lspVehiclesObject = new ArrayList<Vehicle>();
		for (Integer vehicleId : this.lspVehicles) {
			this.vehicleMap.put(cnt, vehicleId - 1);
			//
			 
			cnt++;
			this.lspVehiclesObject.add(vehicles.get(vehicleId - 1));
		}
	}

	public void addVechicle(int vehicle) {
		this.lspVehicles.add(vehicle);
	}

	public void addRealOrders(Order order) {
		this.lspRealOrders.add(order);
	}

	public Vector<Integer> lspVehicles() {
		// TODO Auto-generated method stub
		return this.lspVehicles;
	}

	public void addRank(Integer rank) {
		this.rank = rank;
	}

	public void setMaxUtilize(double maxUtilize) {
		this.maxUtilize = maxUtilize;
	}

	public void setMaxTime(double maxTime) {
		this.maxTime = maxTime;
	}

	public double getMaxUtilize() {
		return maxUtilize;
	}

	public double getMaxTime() {
		return maxTime;
	}

	public double getMinTime() {
		return minTime;
	}

	public void setMinTime(double minTime) {
		// TODO Auto-generated method stub
		this.minTime = minTime;
	}
	
	public int getMaxDrop() {
		return maxDrop;
	}

	public double getMaxTripDistance() {
		// TODO Auto-generated method stub
		return maxTripDistanceDepotExclude;
	}

	public void setTripDisatance(double tripDistance) {
		this.maxTripDistanceDepotExclude = tripDistance;
	}

	public double getMaxSimpleTripDistance() {
		// TODO Auto-generated method stub
		return maxSimpleTripDistance;
	}

	public void setSimpleTripDistance(double simpleTripDistance) {
		this.maxSimpleTripDistance = simpleTripDistance;

	}

	public double getMinLunch() {
		// TODO Auto-generated method stub
		return this.minLunch;
	}

	public double getMaxEndTime() {
		// TODO Auto-generated method stub
		return this.maxEndTime;
	}

	public void addLocations(int orderLocation) {
		this.locations.add(orderLocation);
	}

	public void setSingleLocationCheckCP(Integer locationId, boolean b) {
		this.locationCheckCP.put(locationId, b);

	}

	public void setMultiTripCnt(double ceil) {
		this.multiTripCnt = (int) ceil;
	}

	public void setId(int cnt) {
		this.id = cnt;
	}

	public void setMultiNiddleDepot(HashSet<Order> multiDepotOrders) {
		this.multiDepotOrders = multiDepotOrders;

	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}

	public void setMaxDrop(int maxDrop) {
		this.maxDrop = maxDrop;
		
	}

	public void setIncreasedDrops(boolean increasedDrops) {
		this.increasedDrops = increasedDrops;
		
	}

}
