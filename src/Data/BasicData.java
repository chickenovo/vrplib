package Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PrimitiveIterator;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import MasterModel.CommonFunction;

//stops = Integer.parseInt(tokens[3]);
//sumAllOrder = stops + totalCar * 2 + 1;

public class BasicData {
	public HashMap<Integer, int[]> lspDocks = new HashMap<Integer, int[]>();
	public ArrayList<Order> noNeedFifteen = new ArrayList<Order>();
	public Set<Order> startEndNodes = new HashSet<Order>();
	// public List<Order> staticNodes;
	public boolean dynamicFlag = false;
	public ArrayList<Vehicle> vehicles = new ArrayList<>();
	public ArrayList<Lsp> lsps = new ArrayList<Lsp>();
	public ArrayList<Order> orders = new ArrayList<Order>();
	public Location[] locations;
	public Constants con = new Constants();
	/**************************************************************/
	public File file;
	public String fileName;
	public Scanner st;
	public boolean globalInitialCheckCP = false;
	/**************************************************************/
	public int totalCar = 0;
	int setSumAllOrder = 0;
	
	//// order upper time window beyond 12 midnight paramMidnightTime = 1439
	public int paramMidnightTime = 1439;
	
	// make the closing time of this location 7am (36-hour concept) = 1860 
	public int paramMallClosingTime = 1860 ;
	
	// set a 3-hour buffer window for order time window
	public int midnightBuffer = 180;
	
	public int minOrderTime = (int) 10e8;
	public int stops = 0;
	int points = 0;
	int imdTotalGroup = 0;
	public int[] Ucapacity;

	public int timeLimit = 20;
	public int iterations = 25000;
	int objFlag = 0;
	double alpha = 0;
	public ArrayList<Order> impossibleServeOrder;
	double timeVioCost = 500;
	double travelWeight = 10;// 10
	double waitWeight = 0;// 1
	double maxSpanWeight = 20;// 20
	double loadBalanceWeight = 0;// 10
	List<AvilDock> avaiDocking = new ArrayList<>();
	public List<DummyOrder> dummyOrders = new ArrayList<>();
	HashSet<Integer> fairPriceOrders = new HashSet<>();
	public HashSet<Integer> startNodes = new HashSet<>();
	public HashSet<Integer> endNodes = new HashSet<>();

	HashMap<Integer, Integer> normalVehicle;
	HashMap<Integer, Integer> rank = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> carSetGroup = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> nodeSetGroup = new HashMap<Integer, Integer>();
	List<Integer> depotSet;
	public List<Integer> shuffledCarSet;
	ArrayList<Integer> allNodes;
	int totalGroup = 0;
	int deleteDepotCnt = 0;
	boolean finalCheck;
	boolean needAddExtra = false;
	private int waitTime;
	HashMap<Integer, Integer> depotTimeMap = new HashMap<Integer, Integer>();
	int[][] oriMatrix = null;
	double[][] disMatrix = null;
	public ArrayList<Integer> sameGroup = new ArrayList<Integer>();
	ArrayList<Integer> imdSameGroup = new ArrayList<Integer>();
	// HashMap<Integer, ArrayList<Integer>> orderInGroup = new HashMap<Integer,
	// ArrayList<Integer>>();
	public HashMap<Integer, ArrayList<Integer>> imdOrderInGroup;
	ArrayList<Integer> imdOrders = new ArrayList<Integer>();
	ArrayList<Integer> corrCar = new ArrayList<Integer>();
	HashMap<Integer, Boolean> nonIMDMap = new HashMap<Integer, Boolean>();
	HashSet<Integer> mallSet = new HashSet<Integer>();
	ArrayList<ArrayList<Double>> finalTimeHash = new ArrayList<ArrayList<Double>>();
	ArrayList<int[]> dockingSlotsList = new ArrayList<int[]>();
	boolean dockingVehicelType;
	int sumAllSameGroupOrder = 0;

	int posCnt = 0;
	int negCnt = 0;
	int cnt = 0;
	int posAveWeight = 0;
	int negAveWeight = 0;
	int aveWeight = 0;
	int numLSP = 0;
	public int tripsSize = 0;
	public int maxTime = 0;
	public int maxOrderUpperTime = 0;
	public ConcurrentHashMap<Integer, List<Trip>> trips;
	public Set<List<Order>> staticPath = new HashSet<List<Order>>();
	public List<Integer> vehiclesOrderSize;
	HashMap<Integer, List<Integer>> initialMap;
	HashMap<Integer, Integer> initialVehicleMap;
	int maxId = 0;
	String outPut;
	// HashMap<Integer, Integer> initialVehicleMap;
	public boolean allTrips;
	public List<ArrayList<Order>> timeWindowGroup;
	public double totalTimeSpan;
	public CommonFunction cf;
	public int maxServiceTime = 0;
	public ArrayList<Order> staticNodes;
	// public ArrayList<Order> newOrders;

	public ArrayList<HashSet<Integer>> allDepots = new ArrayList<HashSet<Integer>>();
	public ArrayList<HashSet<Integer>> vehicleDepots = new ArrayList<HashSet<Integer>>();
	public ArrayList<HashSet<Order>> lspMiddleDepotOrder = new ArrayList<HashSet<Order>>();
	int globalTemp = 0;
	public boolean reSequenceFlagOn = false;
	public List<Integer> newOrdersId = new ArrayList<Integer>();
	public ArrayList<Order> newOrders = new ArrayList<Order>();
	public List<Integer> newOrdersCorresPickup = new ArrayList<Integer>();
	public boolean containsCollectionOrderFlag;
	public HashMap<Integer, double[]> collectionOrder;
	public HashMap<Integer, Order> dummyOrderMap;
	public HashSet<Order> collectionDummyOrders;
	public boolean withMixLSPCPOptimize = true;
	
	public BasicData(String fileName, String outPut, HashMap<Integer, List<Integer>> initialMap,
			HashMap<Integer, Integer> initialVehicleMap, HashMap<Integer, double[]> collectionOrder)
			throws IOException {
		this.outPut = outPut;
		file = new File(fileName);
		this.initialMap = initialMap;
		this.initialVehicleMap = initialVehicleMap;
		this.containsCollectionOrderFlag = false;
		this.collectionOrder = collectionOrder;

		readFile(file);
		updateLocationTimings();
		
		
		if (collectionOrder.size() > 0) {
			this.containsCollectionOrderFlag = true;
			setColectionOrders();
		}
	}

	public BasicData() {
		// TODO Auto-generated constructor stub
	}
	
	public void updateLocationTimings() {
		for(Location l: locations) {
			for(Order o: orders) {
				if(o != null) {
					if (o.upperTime >= paramMidnightTime) {					// order upper time window beyond 12 midnight paramMidnightTime = 1439
						if(o.orderLocation() == l.id || o.pickupLocation() == l.id) {
							// make the closing time of this location 7am (36-hr concept) 1860   // paramMallClosingTime
							//System.out.println("Midnight order "+ o.id +" found. Setting location "+ l.id +" closing time");
							//l.close = paramMallClosingTime;	
							l.close = o.upperTime + midnightBuffer;
						}
					}
				}
			}
			
			for(Vehicle v: vehicles) {
				if(v.truckUpperTime >= paramMidnightTime) {
					if(v.startLocation == l.id || v.endLocation == l.id) {
						//System.out.println("Vehicle Depot found for midnight order. Setting location id "+ l.id +" closing time");
						//l.close = paramMallClosingTime;	
						l.close = v.truckUpperTime + midnightBuffer;
					}
				}
			}
		}
		
	}
	
	private void setColectionOrders() {
		this.dummyOrderMap = new HashMap<Integer, Order>();
		collectionDummyOrders = new HashSet<Order>();
		// set collection orders weight to pos
		// set collection orders dummy order weight to neg
		for (Integer id : collectionOrder.keySet()) {
			orders.get(id).setVolume(collectionOrder.get(id)[0]+collectionOrder.get(id)[1]);
			Order od = new Order();
			od.set(orders.size() + 1, 0, 0, 0, 0, 0, 0, 0);
			od.setVolume(collectionOrder.get(id)[1]);
			orders.add(od);
			collectionDummyOrders.add(od);
			dummyOrderMap.put(id, orders.get(orders.size() - 1));
		}
		orders.removeAll(dummyOrderMap.values());
	}

	public void revertVolume() {
		// TODO Auto-generated method stub
		for (Integer id : collectionOrder.keySet()) {
			orders.get(id).setVolume(collectionOrder.get(id)[0] + collectionOrder.get(id)[1]);
		}

	}

	private void readFile(File file) throws IOException {
		int[] preDock = new int[2];
		int[] currentDock = new int[2];
		CommonFunction cf = new CommonFunction();
		cnt = 0;
		allTrips = false;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		int orderCnt = 0;
		int vehicleCnt = 0;
		int numberTripsCnt = 0;
		HashSet<Integer> tripVehicles = new HashSet<Integer>();
		int locationsCnt = 0;
		int consolatedCnt = 0;
		int travelTimeCnt = 0;
		ArrayList<Double> totalOrderLoad = new ArrayList<Double>();
		double vehicleTotalCapacity = 0;

		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// String[] tokens =
				// line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				String[] tokens = line.split(",", -1);
				if (tokens.length == 6 && tokens[0].matches("overall")) {
					for (int i = 0; i < Integer.parseInt(tokens[1]); i++)
						lsps.add(new Lsp(new HashSet<Order>(), new Vector<Integer>()));
					totalGroup = Integer.parseInt(tokens[4]);
					totalCar = Integer.parseInt(tokens[2]);
					stops = Integer.parseInt(tokens[3]);
					points = Integer.parseInt(tokens[4]);
					//timeVioCost = 8;// Integer.parseInt(tokens[5]);
					timeVioCost = 8;// Integer.parseInt(tokens[5]);
					imdTotalGroup = totalGroup;
					orders = new ArrayList<Order>();
					locations = new Location[points];
					oriMatrix = new int[points][points];
					disMatrix = new double[points][points];
					sameGroup = new ArrayList<Integer>();
					imdSameGroup = new ArrayList<Integer>();
					for (int i = 0; i < stops + 1; i++) {
						sameGroup.add(-1);
						imdSameGroup.add(-1);
						orders.add(null);
					}

					for (int i = 0; i < totalCar; i++)
						vehicles.add(null);

					trips = new ConcurrentHashMap<Integer, List<Trip>>();
					Ucapacity = new int[points];

					for (int i = 0; i < Integer.parseInt(tokens[1]); i++) {
						vehicleDepots.add(new HashSet<Integer>());
						allDepots.add(new HashSet<Integer>());
						lspMiddleDepotOrder.add(new HashSet<Order>());
					}
				}

				if (tokens.length == 3 && tokens[0].matches("lsp") && tokens[1].matches("[0-9]+")) {
					int cur = Integer.parseInt(tokens[1]);
					rank.put(cur, Integer.parseInt(tokens[2]));
				}

				if (tokens.length >= 5 && tokens[0].matches("Location") && tokens[1].matches("[0-9]+")) {
					int cur = Integer.parseInt(tokens[1]);
					Ucapacity[cur] = Integer.parseInt(tokens[2]);
					Location temp = new Location(true, Ucapacity[cur]);
					locations[cur] = temp;
					if (Ucapacity[cur] < 300)
						mallSet.add(cur);
					else if (Ucapacity[cur] < 1)
						Ucapacity[cur] = 300;
					locations[cur].setStart(Integer.parseInt(tokens[3]));
					locations[cur].setEnd(Integer.parseInt(tokens[4]));
					locations[cur].setId(cur);
					// System.err.println(cur+"\t"+locations[cur].open+"\t"+locations[cur].close);
					if (tokens.length == 7) {
						locations[cur].setType(tokens[5]);
					} else if (tokens.length == 8) {
						String[] parts = tokens[5].split(":");
						String[] newPart = parts[0].split("\\[");
						locations[cur].setLunchBreakStartTime(Integer.parseInt(newPart[1]));
						newPart = parts[(parts.length - 1)].split("\\]");
						locations[cur].setLunchBreakEndTime(Integer.parseInt(newPart[0]));
						locations[cur].setType(tokens[6]);
					}
					locationsCnt++;
				}

				if (tokens.length >= 8 && tokens[0].matches("vehicle") && tokens[1].matches("[0-9]+")) {
					int cur = Integer.parseInt(tokens[1]);
					int truckType = Integer.parseInt(tokens[7]);
					Vehicle newVec = new Vehicle(Double.parseDouble(tokens[4]), (int) Double.parseDouble(tokens[2]),
							(int) Double.parseDouble(tokens[3]), truckType);

					if (tokens.length > 8) {
						String[] parts = tokens[8].split(";");
						String[] newPart = parts[0].split("\\[");
						if (newPart[1].matches("^-?\\d+$") && !newPart[1].equals("null")) {
							newVec.setStartLocation(Integer.parseInt(newPart[1]));
							locations[Integer.parseInt(newPart[1])].setType("IMD");
						} else {
							newVec.setStartLocation(locations.length - 1);
							Location temp = new Location(false, 1000);
							temp.setStart(0);
							temp.setEnd(1440);
							locations[locations.length - 1] = temp;
						}
						newPart = parts[1].split("\\]");
						if (!newPart[0].equals("null")) {
							newVec.setEndLocation(Integer.parseInt(newPart[0]));
						} else {
							newVec.setEndLocation(locations.length - 1);
							Location temp = new Location(false, 1000);
							temp.setStart(0);
							temp.setEnd(1440);
							locations[locations.length - 1] = temp;
						}

						parts = tokens[9].split(";");
						newPart = parts[0].split("\\[");
						newVec.setLunchBreakStartTime(Integer.parseInt(newPart[1]));
						newPart = parts[(parts.length - 1)].split("\\]");
						newVec.setLunchBreakEndTime(Integer.parseInt(newPart[0]));
						if (tokens.length > 9)
							newVec.setDepotLoadTime(Integer.parseInt(tokens[10]));
					}
					newVec.setOriginLSP(Integer.parseInt(tokens[6]));
					newVec.setVolume(Double.parseDouble(tokens[4]));
					vehicles.set(cur - 1, newVec);

					carSetGroup.put(cur, Integer.parseInt(tokens[6]));
					if (numLSP < Integer.parseInt(tokens[6]))
						numLSP = Integer.parseInt(tokens[6]);

					int temp = Integer.parseInt(tokens[6]);

					vehicleDepots.get(temp).add(newVec.startLocation);
					vehicleDepots.get(temp).add(newVec.endLocation);
					globalTemp = newVec.startLocation;
					vehicleCnt++;
				}

				if (tokens.length >= 10 && tokens[0].matches("order") && tokens[1].matches("[0-9]+")) {
					// System.out.println(tokens[1]);
					int cur = Integer.parseInt(tokens[1]);
					double posWeight = 0;
					double negWeight = 0;
					double weight = Double.parseDouble(tokens[3]);
					int temp = 0;
					// all non imd use -1
					if (weight > 0)
						posWeight = weight;
					if (weight < 0)
						negWeight = weight;

					temp = Integer.parseInt(tokens[2]);
					sameGroup.set(cur, temp);

					if (orders.get(cur) == null)
						orders.set(cur, new Order());
					orders.get(cur).set(cur, weight, posWeight, negWeight, (int) Double.parseDouble(tokens[4]),
							(int) Double.parseDouble(tokens[5]), (int) Double.parseDouble(tokens[7]),
							Integer.parseInt(tokens[2]));
					
					orders.get(cur).setCustomerId(tokens[10]);
					orders.get(cur).setCargoType(tokens[11]);

					orders.get(cur).setDepotPickup(Integer.parseInt(tokens[12]));
					// set warehouse to IMD

					orders.get(cur).setPickUpOpen(Integer.parseInt(tokens[13]));
					orders.get(cur).setPickUpClose(Integer.parseInt(tokens[14]));
					// orders.get(cur).setDepotPickup(globalTemp);
					if (minOrderTime > (int) Double.parseDouble(tokens[4]))
						minOrderTime = (int) Double.parseDouble(tokens[4]);

					if (maxOrderUpperTime < (int) Double.parseDouble(tokens[5]))
						maxOrderUpperTime = (int) Double.parseDouble(tokens[5]);
					if (maxServiceTime < (int) Double.parseDouble(tokens[7]))
						maxServiceTime = (int) Double.parseDouble(tokens[7]);

					orders.get(cur).setVolume(Double.parseDouble(tokens[6]));
					imdSameGroup.set(cur, Integer.parseInt(tokens[2]));
					if (!nodeSetGroup.containsKey((Integer) cur))
						nodeSetGroup.put(cur, Integer.parseInt(tokens[8]));

					if (Integer.parseInt(tokens[9]) > 0) {
						fairPriceOrders.add(cur);
					}

					orders.get(cur).setOriginLSP(Integer.parseInt(tokens[8]));
					allDepots.get(Integer.parseInt(tokens[8])).add(Integer.parseInt(tokens[12]));
					// allDepots.get(Integer.parseInt(tokens[8])).add(globalTemp);
					orderCnt++;
		
				}

				if (tokens.length == 2 && tokens[0].matches("TimeLimit")) {
					timeLimit = Integer.parseInt(tokens[1]);
				}

				if (tokens.length == 2 && tokens[0].matches("Iterations")) {
					iterations = Integer.parseInt(tokens[1]);//
				}

				if (tokens.length == 2 && tokens[0].matches("alpha")) {
					alpha = Double.parseDouble(tokens[1]);
				}

				if (tokens.length == 2 && tokens[0].matches("objFlag")) {
					objFlag = Integer.parseInt(tokens[1]);
				}

				int timeLimit = 15;
				int iterations = 25000;
				double alpha = 0;

				int temp = 0;
				int locationStart = 0;
				int locationEnd = 0;
				if ((tokens.length == 6) && (tokens[0].matches("[0-9]+"))) {
					String[] parts = tokens[1].split(";");
					String[] newPart = parts[0].split("\\[");

					if (newPart[1].matches("^-?\\d+$")) {
						travelWeight = Double.parseDouble(newPart[1]);
					}
					waitWeight = Double.parseDouble(parts[1]);// 1
					timeVioCost = Double.parseDouble(parts[2]);

					maxSpanWeight = Double.parseDouble(parts[3]);// 20
					newPart = parts[4].split("\\]");
					loadBalanceWeight = Double.parseDouble(newPart[0]);// 10

					if (travelWeight / waitWeight >= 9)
						waitWeight *= 4;// because the parallel takes waiting time weight as same as travel time, which
										// makes engine may never got good solution, so no choice but increase the
										// waiting time wait
					if (travelWeight > 9)
						lsps.get(Integer.parseInt(tokens[0])).setParameter(new Parameter(travelWeight / 10.0,
								waitWeight / 10.0, timeVioCost / 10.0, maxSpanWeight / 10.0, loadBalanceWeight / 10.0));
					else
						lsps.get(Integer.parseInt(tokens[0])).setParameter(
								new Parameter(travelWeight, waitWeight, timeVioCost, maxSpanWeight, loadBalanceWeight));

					lsps.get(Integer.parseInt(tokens[0])).setExtra(Integer.parseInt(tokens[2]),
							Double.parseDouble(tokens[3]), Double.parseDouble(tokens[4]));

				}
				if ((tokens.length == 5) && (tokens[0].matches("trip"))) {
					String[] parts = tokens[2].split("-");
					String[] newPart = parts[0].split("\\[");

					temp = Integer.parseInt(tokens[1]);
					locationStart = Integer.parseInt(tokens[3]);
					locationEnd = Integer.parseInt(tokens[4]);
					tripVehicles.add(temp);
					List<Integer> singlePath = new ArrayList<Integer>();
					if (newPart[1].matches("^-?\\d+$")) {
						nodeSetGroup.put(Integer.parseInt(newPart[1]), stops + temp);
						if (orders.get(Integer.parseInt(newPart[1])) == null)
							orders.set(Integer.parseInt(newPart[1]), new Order());
						orders.get(Integer.parseInt(newPart[1])).setVehiclesIndex(temp);
						singlePath.add(Integer.parseInt(newPart[1]));
						consolatedCnt++;
					}
					for (int i = 1; i < parts.length - 1; i++) {
						nodeSetGroup.put(Integer.parseInt(parts[i]), stops + temp);
						if (orders.get(Integer.parseInt(parts[i])) == null)
							orders.set(Integer.parseInt(parts[i]), new Order());

						orders.get(Integer.parseInt(parts[i])).setVehiclesIndex(temp);
						singlePath.add(Integer.parseInt(parts[i]));
						consolatedCnt++;

					}
					if (parts.length > 1) {
						newPart = parts[(parts.length - 1)].split("\\]");
						nodeSetGroup.put(Integer.parseInt(newPart[0]), stops + temp);

						if (orders.get(Integer.parseInt(newPart[0])) == null)
							orders.set(Integer.parseInt(newPart[0]), new Order());

						orders.get(Integer.parseInt(newPart[0])).setVehiclesIndex(temp);
						singlePath.add(Integer.parseInt(newPart[0]));
						consolatedCnt++;
					} else {
						String[] cPart = newPart[1].split("\\]");
						nodeSetGroup.put((Integer.parseInt(cPart[0])), stops + temp);
						if (orders.get(Integer.parseInt(cPart[0])) == null)
							orders.set(Integer.parseInt(cPart[0]), new Order());
						orders.get(Integer.parseInt(cPart[0])).setVehiclesIndex(temp);
						singlePath.add(Integer.parseInt(cPart[0]));
						consolatedCnt++;
					}
					carSetGroup.remove(temp);
					// first put, then, rearrange, or no need in fact
					carSetGroup.put(temp, stops + temp);
					Trip tempTrip = new Trip(temp, singlePath);
					Order Start = new Order();
					Order End = new Order();
					Start.setLocationId(locationStart);
					End.setLocationId(locationEnd);

					tempTrip.setLocationStartEnd(Start, End);

					if (trips.containsKey(temp))
						trips.get(temp).add(tempTrip);
					else {
						List<Trip> preConsolated = new ArrayList<Trip>();
						preConsolated.add(tempTrip);
						trips.put(temp, preConsolated);
					}
					cnt++;
				}

				if (tokens.length == 4 && isInteger(tokens[0])) {
					oriMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = Integer.parseInt(tokens[2]);
					if (Integer.parseInt(tokens[0]) == Integer.parseInt(tokens[1]))
						oriMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = 0;
					if (tokens.length > 3) {
						disMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = Double
								.parseDouble(tokens[3]);
						if (Integer.parseInt(tokens[0]) == Integer.parseInt(tokens[1]))
							disMatrix[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])] = 0;
					}

					// oriMatrix[Integer.parseInt(tokens[1])][Integer.parseInt(tokens[0])]
					// = Integer.parseInt(tokens[2]);
					if (Integer.parseInt(tokens[2]) > maxTime)
						maxTime = Integer.parseInt(tokens[2]);
					travelTimeCnt++;
				}

				if (tokens.length == 1 && isInteger(tokens[0])) {
					timeLimit = Integer.parseInt(tokens[0]);
				}
			}

			int tempVal = 0;
			for (Integer vehicleId : trips.keySet()) {
				tempVal = vehicleId;
				vehicles.get(tempVal - 1).setDepot(null);
				vehicles.get(tempVal - 1).setTrip(true);
			}
			int lowerTime = 0;
			cnt = 0;

			for (Vehicle vehicle : vehicles) {
				vehicleTotalCapacity += vehicle.volume();
				if (!vehicle.getTripBool()) {
					// System.err.println(vehicles.get(cnt).truckLowerTime);
					lowerTime = Math.max(vehicles.get(cnt).truckLowerTime, locations[vehicle.startLocation].open);
					// vehicles.get(cnt).setTruckLowerTime(lowerTime);

					Order o = new Order();
					o.set(orders.size(), 0, 0, 0, vehicle.truckLowerTime, vehicle.truckUpperTime, 0,
							vehicle.startLocation);
					o.setServiceTime(vehicle.depotLoadTime);
					o.setOriginLSP(vehicle.originLSP);
					orders.add(o);
					startEndNodes.add(o);
					vehicle.setDepot(o);
					imdSameGroup.add(o.orderLocation);
					if (vehicle.startLocation != vehicle.endLocation) {
						Order o1 = new Order();
						o1.set(orders.size(), 0, 0, 0, vehicle.truckLowerTime, vehicle.truckUpperTime, 0,
								vehicle.endLocation);
						o1.setServiceTime(0);
						o1.setOriginLSP(vehicle.originLSP);
						orders.add(o1);
						startEndNodes.add(o1);
						vehicle.setEndDepot(o1);
						vehicle.setStartEndDepotDiff(2);
					} else {
						vehicle.setEndDepot(o);
						vehicle.setStartEndDepotDiff(1);
					}

					vehicle.setMiddleDepot(o);
					vehicle.setTrip(false);
					if (maxId < orders.size())
						maxId = orders.size();

				}
				cnt++;
			}
			int cnt = 0;

			for (int i = 0; i < locations.length - 1; i++)
				locations[i].setUcapacity(Ucapacity[i]);
			int small = (int) 1e10;
			for (AvilDock u : avaiDocking) {
				small = (int) 1e10;
				if (u.aviDock > Ucapacity[u.mallId]) {
					Ucapacity[u.mallId] = u.aviDock;
					locations[u.mallId].setUcapacity(u.aviDock);
					if (u.aviDock < small)
						small = u.aviDock;

				}
				locations[u.mallId].setMinCapacity(small);
			}
			addDummy();
			imdOrderInGroup = new HashMap<>();
			for (int i = 0; i < imdTotalGroup; i++)
				imdOrderInGroup.put(i, new ArrayList<Integer>());

			cnt = 0;
			for (Integer orderLocation : sameGroup) {
				if (orderLocation > -1 && mallSet.contains(orderLocation)) {
					nonIMDMap.put(cnt, false);
				} else
					nonIMDMap.put(cnt, true);
				cnt++;
			}

			cnt = 0;

			for (Integer location : imdSameGroup) {
				if (orders.get(cnt) != null && locations[orders.get(cnt).orderLocation].type == "IMD") {
					imdOrderInGroup.get(location).add(cnt);
					imdOrders.add(cnt);
				}
				cnt++;
			}

			Iterator<Entry<Integer, ArrayList<Integer>>> iter = imdOrderInGroup.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<Integer, ArrayList<Integer>> entry = iter.next();
				if (entry.getValue() == null || entry.getValue().size() == 0) {
					iter.remove();
				}
			}

			for (Integer o : imdOrderInGroup.keySet()) {
				sumAllSameGroupOrder += imdOrderInGroup.get(o).size();
			}

			if (trips.keySet().size() == totalCar)
				allTrips = true;
			// recheck again
			iter = imdOrderInGroup.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<Integer, ArrayList<Integer>> entry = iter.next();
				if (entry.getValue() == null || entry.getValue().size() == 0) {
					iter.remove();
				}
			}

			allNodes = new ArrayList<Integer>();
			for (int i = 1; i < orders.size(); i++) {
				if (orders.get(i).weight() > 0) {
					posAveWeight += orders.get(i).weight();
					posCnt++;
				}
				if (orders.get(i).weight() < 0) {
					negAveWeight += -orders.get(i).weight();
					negCnt++;
				}
				aveWeight += orders.get(i).weight();
			}
			aveWeight /= orders.size();

			if (posCnt > 0)
				posAveWeight /= posCnt;
			if (negCnt > 0)
				negAveWeight /= negCnt;

		} finally {
			LineIterator.closeQuietly(it);
		}

		if (orderCnt < stops) {
			System.err.println("number of orders is smaller than " + (stops));
			return;
		}

		if (vehicleCnt < totalCar) {
			System.err.println("number of vehicles is smaller than " + totalCar);
			return;
		}
		if (locationsCnt < points) {
			System.err.println("number of vehicles is smaller than " + totalCar);
			return;
		}
		if (travelTimeCnt < points * (points - 1)) {
			System.err.println("No or only partial travel time between locations been provided");
		}

		HashSet<ArrayList<Integer>> timeWindowIden = new HashSet<ArrayList<Integer>>();
		impossibleServeOrder = new ArrayList<Order>();
		for (Order od : orders) {
			if (od == null)
				continue;
			if (startEndNodes.contains(od))
				continue;
			ArrayList<Integer> timeWindowList = new ArrayList<Integer>();
			timeWindowList.add(od.lowerTime);
			timeWindowList.add(od.upperTime);
			timeWindowList.add(od.originLSP);
			timeWindowIden.add(timeWindowList);

		}
		ArrayList<Integer> timeWindowList = new ArrayList<Integer>();
		ArrayList<Order> timeWindowSingleGroup = new ArrayList<Order>();
		timeWindowGroup = new ArrayList<ArrayList<Order>>();
		cnt = 0;
		for (ArrayList<Integer> timewindow : timeWindowIden) {
			timeWindowSingleGroup.clear();
			for (Order od : orders) {
				if (od == null)
					continue;
				if (startEndNodes.contains(od))
					continue;

				if (Math.abs(od.originLSP - timewindow.get(2)) < 1e-3
						&& (Math.abs(od.upperTime - timewindow.get(1)) <= 60)
						&& (Math.abs(od.lowerTime - timewindow.get(0)) <= 60))
					timeWindowSingleGroup.add(od);
			}
			timeWindowGroup.add(cf.singleCloneOrder(timeWindowSingleGroup));
			cnt++;
		}

		// updateTripsWithMoreOrders();
		// stops and sumAllOrder change, put in between

		depotSet = new ArrayList<Integer>();

		shuffledCarSet = new ArrayList<Integer>();
		for (int i = 1; i <= totalCar; i++) {
			depotSet.add(i);
			shuffledCarSet.add(i - 1);
		}

		con.setInitial(timeLimit, sumAllSameGroupOrder, imdTotalGroup, totalCar, stops, points, oriMatrix, disMatrix,
				totalGroup, deleteDepotCnt, shuffledCarSet, collectionOrder);

		con.setAveWeight(aveWeight);
		con.setPosAveWeight(posAveWeight);
		con.setNegAveWeight(negAveWeight);
		con.setAllStartEnd(startEndNodes);
		con.setAllSet(depotSet, imdOrders, imdOrderInGroup, allNodes, sameGroup, nodeSetGroup, imdSameGroup, nonIMDMap,
				carSetGroup, lsps, dummyOrders, initialMap, objFlag);
		con.setAlpha(alpha, outPut);
		con.setMaxId(maxId);
		con.initializeX();
		update(nodeSetGroup, carSetGroup);
		// pass the vehicleIntialMap same way as the intialMap
		con.setInitialVehicleMap(initialVehicleMap);

		HashSet<Order> doubleCheckOrder = new HashSet<Order>();
		for (Order o : orders)
			doubleCheckOrder.add(o);
		orders.clear();
		for (Order o : doubleCheckOrder)
			orders.add(o);

		totalTimeSpan = 0;// sumTimeSpan()*0.5;
		totalOrderLoad = new ArrayList<Double>();
		for (Lsp u : lsps)
			totalOrderLoad.add(0.0);
		int locationId = 0;
		maxOrderUpperTime = 0;
		
		for (int i = 1; i < orders.size(); i++) {
			totalOrderLoad.set(orders.get(i).originLSP,
					totalOrderLoad.get(orders.get(i).originLSP) + orders.get(i).volume);
			locationId = orders.get(i).orderLocation;
			orders.get(i).setLocation(locations[locationId]);
			if (con.slotsFalg && !locations[locationId].type.equals("IMD"))
				noNeedFifteen.add(orders.get(i));

			if (orders.get(i).lowerTime < locations[locationId].open)
				orders.get(i).setLowerTime(locations[locationId].open);
			if (orders.get(i).upperTime > locations[locationId].close) {}
				//orders.get(i).setUpperTime(locations[locationId].close);
			if (maxOrderUpperTime < orders.get(i).upperTime())
				maxOrderUpperTime = orders.get(i).upperTime();
		}
		con.setNoNeedFifteen(noNeedFifteen);
		// System.err.println(totalOrderLoad + "\t" + 2 * vehicleTotalCapacity);
		for (Vehicle k : vehicles) {
			k.setlSP(lsps.get(k.originLSP));
			// k.setTruckLowerTime(Math.max(k.truckLowerTime, k.depot.location.open));
			// k.setTruckUpperTime(Math.min(k.truckUpperTime, k.depot.location.close));
			k.setInitialUpper(k.truckUpperTime);
			k.setInitialLower(k.truckLowerTime);
			// if (k.truckUpperTime - maxTime - maxServiceTime > maxOrderUpperTime)
			// k.setTruckUpperTime((int) (maxOrderUpperTime + maxServiceTime + 0.9 *
			// maxTime));// !!!!

		}

		for (int i = 0; i < lsps.size(); i++) {
			lsps.get(i)
					.setMultiTripCnt(Math.max(Math.ceil(totalOrderLoad.get(i) / vehicleTotalCapacity),
							Math.ceil((lsps.get(i).lspRealOrders.size() - lsps.get(i).lspVehicles.size() + 0.0)
									/ (0.0 + lsps.get(i).totalCar * lsps.get(i).maxDrop))));

			if (totalOrderLoad.get(i) > 2 * vehicleTotalCapacity * 0.8) {
				for (Integer k : lsps.get(i).lspVehicles) {
					vehicles.get(k - 1).updateVolume(0.1);
				}
			}
		}
		cnt = 0;
		for (Lsp u : lsps) {
			u.setId(cnt);
			cnt++;
		}

		if (allDepots.size() > 0) {
			cnt = 0;
			for (HashSet<Integer> singleLspDepot : allDepots) {
				if (vehicleDepots.get(cnt).size() == 1)
					singleLspDepot.removeAll(vehicleDepots.get(cnt));

				// System.err.println("singleLspDepot\t"+singleLspDepot);

				if (singleLspDepot.size() > 0) {
					for (Integer locId : singleLspDepot) {
						Order o1 = new Order();
						o1.set(orders.size(), 0, 0, 0, locations[locId].open, locations[locId].close, 0, locId);
						//o1.setServiceTime(45);// !!!must change, hard code
						o1.setServiceTime(vehicles.get(0).depotLoadTime);
						// System.out.println("new depot index\t"+(orders.size() - 1));
						orders.add(orders.size(), o1);
						o1.setLocation(locations[locId]);
						lspMiddleDepotOrder.get(cnt).add(o1);
						startEndNodes.add(o1);
					}
				}
				cnt++;
			}
		}

		cnt = 0;
		for (HashSet<Order> multiDepotOrders : lspMiddleDepotOrder) {
			lsps.get(cnt).setMultiNiddleDepot(multiDepotOrders);
			cnt++;
		}

		double maxCapacity = 0;
		for (Vehicle k : vehicles) {
			k.setvehicleStartEndNodes();
			if (k.capacity > maxCapacity)
				maxCapacity = k.capacity;
		}

		impossibleServeOrder = new ArrayList<Order>();
		for (Order od : orders) {
			if (od == null)
				continue;
			if (startEndNodes.contains(od))
				continue;

			if (od.volume > maxCapacity)
				impossibleServeOrder.add(od);
		}
		orders.removeAll(impossibleServeOrder);

		// lspDocks, key is location, value is number of orders
		// 2. if some locations been visited by several lsps, then, split them according
		// to number of orders each lsp order ever visit this location
		lspDocks = new HashMap<Integer, int[]>();
		for (Integer location : imdOrderInGroup.keySet())
			for (Integer orderId : imdOrderInGroup.get(location)) {
				if (lspDocks.get(location) != null) {
					lspDocks.get(location)[orders.get(orderId).originLSP] += 1;
				} else {
					lspDocks.put(location, new int[lsps.size()]);
					lspDocks.get(location)[orders.get(orderId).originLSP] += 1;
				}
			}
		for (Map.Entry<Integer, int[]> entry : lspDocks.entrySet()) {
			int[] dockSplitValue = new int[entry.getValue().length];
			int sum = Arrays.stream(entry.getValue()).sum();
			int capacity = locations[entry.getKey()].Ucapacity - dockSplitValue.length;
			for (int i = 0; i < dockSplitValue.length; i++) {
				dockSplitValue[i] = (int) (1 + (capacity * entry.getValue()[i] + 0.0) / (sum + 0.0));
			}
			capacity -= Arrays.stream(dockSplitValue).sum();
			ArrayIndexComparator comparator = new ArrayIndexComparator(dockSplitValue);
			Integer[] indexes = comparator.createIndexArray();
			Arrays.sort(indexes, comparator);
			// sort array, the big one add one more
			for (int i = 0; i < indexes.length && capacity > 0; i++) {
				dockSplitValue[indexes[i]] += 1;
				capacity--;
			}
			lspDocks.put(entry.getKey(), dockSplitValue);
		}
		// for(Lsp lspCurrent: lsps)
	}

	public class ArrayIndexComparator implements Comparator<Integer> {
		private final int[] array;

		public ArrayIndexComparator(int[] array) {
			this.array = array;
		}

		public Integer[] createIndexArray() {
			Integer[] indexes = new Integer[array.length];
			for (int i = 0; i < array.length; i++) {
				indexes[i] = i; // Autoboxing
			}
			return indexes;
		}

		@Override
		public int compare(Integer index1, Integer index2) {
			// descending
			if (array[index1] < array[index2])
				return 1;
			else if (array[index1] > array[index2])
				return -1;
			return 0;
		}
	}

	private int calculateTime(String string) {
		int finalTime = 0;
		char[] digits1 = string.toCharArray();

		if (digits1.length == 4) {
			finalTime += 600 * Character.getNumericValue(digits1[0]) + 60 * Character.getNumericValue(digits1[1])
					+ 10 * Character.getNumericValue(digits1[2]) + Character.getNumericValue(digits1[3]);
		} else if (digits1.length == 3) {
			finalTime += 60 * Character.getNumericValue(digits1[0]) + 10 * Character.getNumericValue(digits1[1])
					+ Character.getNumericValue(digits1[2]);
		}
		return finalTime;
	}



	private double sumTimeSpan() {
		double sum = 0;
		for (int k = 0; k < totalCar; k++)
			sum += vehicles.get(k).truckUpperTime() - vehicles.get(k).truckLowerTime();
		return sum;
	}

	private void updateTripsWithMoreOrders() {
		cnt = 1;
		int orderCnt = 0;
		vehiclesOrderSize = new ArrayList<Integer>();
		int tempVal = orders.size() - 1;
		for (int i = 0; i < totalCar; i++)
			vehiclesOrderSize.add(0);

		for (Integer vehicleId : trips.keySet()) {
			orderCnt = 0;

			tripsSize += trips.get(vehicleId).size();
			for (Trip t : trips.get(vehicleId)) {
				t.Start.setLowerTime(Math.max(minOrderTime - maxTime,
						Math.max(vehicles.get(vehicleId - 1).truckLowerTime, locations[t.Start.orderLocation].open)));
				// vehicles.get(vehicleId - 1).setTruckLowerTime(t.Start.lowerTime);
				t.Start.setUpperTime(
						Math.min(vehicles.get(vehicleId - 1).truckUpperTime, locations[t.Start.orderLocation].close));
				t.Start.setServiceTime(0);

				List<Integer> vehiclesIndex = new ArrayList<Integer>();
				vehiclesIndex.add(vehicleId);
				t.Start.setLeft(vehicles.get(vehicleId - 1).rankLsp, t.path, vehiclesIndex, vehicleId);

				t.End.setLowerTime(
						Math.max(vehicles.get(vehicleId - 1).truckLowerTime, locations[t.End.orderLocation].open));
				t.End.setUpperTime(
						Math.min(vehicles.get(vehicleId - 1).truckUpperTime, locations[t.End.orderLocation].close));
				t.End.setServiceTime(0);

				List<Integer> vehiclesIndexN = new ArrayList<Integer>();
				vehiclesIndexN.add(vehicleId);
				t.End.setLeft(vehicles.get(vehicleId - 1).rankLsp, t.path, vehiclesIndexN, vehicleId);

				t.Start.setId(tempVal + cnt);
				t.End.setId(tempVal + cnt + 1);
				if (maxId < tempVal + cnt + 1)
					maxId = tempVal + cnt + 1;
				startEndNodes.add(t.Start);
				startEndNodes.add(t.End);
				cnt += 2;
				orderCnt += t.path.size();
			}
			vehiclesOrderSize.set(vehicleId - 1, orderCnt);
		}
	}

	private void addDummy() {
		for (AvilDock u : avaiDocking) {
			if (u.aviDock < Ucapacity[u.mallId]) {
				DummyOrder dummyOrder = new DummyOrder(u.mallId, u.start, u.end, Ucapacity[u.mallId] - u.aviDock);
				dummyOrders.add(dummyOrder);
			}
		}

	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	private void update(HashMap<Integer, Integer> nodeSetGroup, HashMap<Integer, Integer> carSetGroup) {
		int lspId = 0;

		for (Integer key : carSetGroup.keySet()) {
			lspId = carSetGroup.get(key);
			lsps.get(lspId).addVechicle(key);
		}
		for (Integer lspID : carSetGroup.values()) {
			for (Integer key : nodeSetGroup.keySet()) {
				lspId = nodeSetGroup.get(key);
				if (Math.abs(lspID - lspId) < 1e-3) {
					lsps.get(lspId).addRealOrders(orders.get(key));
					lsps.get(lspId).addLocations(orders.get(key).orderLocation);

				}
			}
		}

		for (Lsp u : lsps) {
			for (Integer truckID : trips.keySet())
				u.lspVehicles.remove(truckID);
		}
		int minTime = (int) 1e10;
		int maxTime = 0;
		int cnt = 0;
		for (Order order : orders) {
			if (order == null)
				continue;

			if (order.vehiclesIndex == null || order.vehiclesIndex.size() < 1) {
				order.setVehicles(lsps.get(order.originLSP).lspVehicles);
			}

			if (cnt > totalCar && cnt <= stops + totalCar) {
				if (order.lowerTime < minTime)
					minTime = order.lowerTime;
				if (order.upperTime > maxTime)
					maxTime = order.upperTime;
			}
			cnt++;
		}

		for (Vehicle vehicle : vehicles) {
			for (Integer id : rank.keySet()) {
				if (vehicle.originLSP == id)
					vehicle.setAddRank(rank.get(id));
			}
		}

		for (Order order : orders) {
			if (order == null)
				continue;

			if (locations[order.orderLocation].type != null && locations[order.orderLocation].type.equals("IMD"))
				order.setIsMall();
		}

		updateLspRelated();

	}

	private void updateLspRelated() {
		ArrayList<Double> lspOrders = new ArrayList<Double>();
		HashMap<Integer, Integer> simpleCheckPossibleVisit = new HashMap<Integer, Integer>();
		HashMap<ArrayList<Integer>, Integer> totalIMDMallPossibleVisit = new HashMap<ArrayList<Integer>, Integer>();
		HashMap<ArrayList<Integer>, Integer> seperateLspIMDMallPossibleVisit = new HashMap<ArrayList<Integer>, Integer>();
		double sum = 0;
		for (Lsp u : lsps) {
			u.setTotalSize();
			u.setVechicleMap(vehicles);
			u.setRealLocations();
			lspOrders.add((double) u.totalSize);
			sum += u.totalSize;
		}
		int cnt = 0;
		for (Lsp u : lsps) {
			u.setTimeLimit(timeLimit * lspOrders.get(cnt) / sum);
			for (Order o : u.lspRealOrders) {
				if (!o.isMall)
					continue;
				if (o != null && simpleCheckPossibleVisit.containsKey(o.orderLocation)) {
					simpleCheckPossibleVisit.put(o.orderLocation, simpleCheckPossibleVisit.get(o.orderLocation) + 1);
				} else
					simpleCheckPossibleVisit.put(o.orderLocation, 1);

				ArrayList<Integer> orderLocationAndTime = new ArrayList<Integer>();
				orderLocationAndTime.add(o.orderLocation);
				orderLocationAndTime.add(o.lowerTime);
				orderLocationAndTime.add(o.upperTime);
				ArrayList<Integer> seperateLspOrderLocationAndTime = new ArrayList<Integer>();
				seperateLspOrderLocationAndTime.addAll(orderLocationAndTime);
				seperateLspOrderLocationAndTime.add(0, o.originLSP);
				if (o != null && o.isMall && totalIMDMallPossibleVisit.containsKey(orderLocationAndTime)) {
					totalIMDMallPossibleVisit.put(orderLocationAndTime,
							totalIMDMallPossibleVisit.get(orderLocationAndTime) + 1);
				} else
					totalIMDMallPossibleVisit.put(orderLocationAndTime, 1);

				if (o != null && o.isMall
						&& seperateLspIMDMallPossibleVisit.containsKey(seperateLspOrderLocationAndTime)) {
					seperateLspIMDMallPossibleVisit.put(seperateLspOrderLocationAndTime,
							seperateLspIMDMallPossibleVisit.get(seperateLspOrderLocationAndTime) + 1);
				} else
					seperateLspIMDMallPossibleVisit.put(seperateLspOrderLocationAndTime, 1);
			}
			// u.setDockingSlots();
			cnt++;
		}
		// this is how many visit for each IMD-mall: location, count, if smaller than
		// min docking slots of whole day, no need check this location
		// location.noNeedCheck, if all no need check, globalNoNeed false

		for (Integer locationId : simpleCheckPossibleVisit.keySet()) {
			if (simpleCheckPossibleVisit.get(locationId) > Math.max(totalCar, locations[locationId].minCapacity)) {
				globalInitialCheckCP = true;
			} else {
				for (Lsp u : lsps) {
					u.setSingleLocationCheckCP(locationId, false);
				}
			}
		}
		boolean allFalse = true;
		if (globalInitialCheckCP) {
			System.err.println();// this is which location, time from, time to, count, if smaller
			// than min docking slots during that time period, no need check

			ArrayList<Node> G = new ArrayList<Node>(totalIMDMallPossibleVisit.keySet().size());
			cnt = 0;
			for (ArrayList<Integer> locationTimeFromToCount : totalIMDMallPossibleVisit.keySet()) {
				Node n = new Node();
				n.setLabel(cnt);
				n.setLocationId(locationTimeFromToCount.get(0));
				G.add(n);
				cnt++;
			}
			int cntA = -1;
			for (ArrayList<Integer> locationTimeFromToThis : totalIMDMallPossibleVisit.keySet()) {
				int cntB = -1;
				cntA++;
				for (ArrayList<Integer> locationTimeFromToThat : totalIMDMallPossibleVisit.keySet()) {
					cntB++;
					if (cntB <= cntA)
						continue;
					if (G.get(cntB).locationId == G.get(cntA).locationId
							&& (locationTimeFromToThis.get(2) - locationTimeFromToThat.get(1))
									* (locationTimeFromToThat.get(2) - locationTimeFromToThis.get(1)) > 0) {
						Edge e = new Edge();
						e.setDestination(G.get(cntB));
						e.setWeight(totalIMDMallPossibleVisit.get(locationTimeFromToThis)
								+ totalIMDMallPossibleVisit.get(locationTimeFromToThat));
						G.get(cntA).addEdge(e);
					}

				}
			}

			for (Node n : G) {
				n.calculateTotalWeight();
				if (n.sumWeight > Math.max(totalCar, locations[n.locationId].minCapacity)) {
					allFalse = false;
				} else {
					for (Lsp u : lsps) {
						u.setSingleLocationCheckCP(n.locationId, false);
					}
				}
			}
			if (allFalse)
				globalInitialCheckCP = false;

			if (!allFalse) {
				ArrayList<NodeLsp> GLsp = new ArrayList<NodeLsp>(seperateLspIMDMallPossibleVisit.keySet().size());

				cnt = 0;
				for (ArrayList<Integer> locationTimeFromToCount : seperateLspIMDMallPossibleVisit.keySet()) {
					if (lsps.get(locationTimeFromToCount.get(0)).locationCheckCP.get(locationTimeFromToCount.get(1))) {
						NodeLsp n = new NodeLsp();
						n.setLabel(cnt);
						n.setlspID(locationTimeFromToCount.get(0));
						n.setLocationId(locationTimeFromToCount.get(1));
						G.add(n);
						cnt++;
					}
				}

				cntA = -1;
				// for (ArrayList<Integer> locationTimeFromToThis : GLsp) {
				// int cntB = -1;
				// cntA++;
				// for (ArrayList<Integer> locationTimeFromToThat : GLsp) {
				// cntB++;
				// if (cntB <= cntA)
				// continue;
				// if (G.get(cntB).locationId == G.get(cntA).locationId
				// && (locationTimeFromToThis.get(2) - locationTimeFromToThat.get(1))
				// * (locationTimeFromToThat.get(2) - locationTimeFromToThis.get(1)) > 0) {
				// Edge e = new Edge();
				// e.setDestination(G.get(cntB));
				// e.setWeight(totalIMDMallPossibleVisit.get(locationTimeFromToThis)
				// + totalIMDMallPossibleVisit.get(locationTimeFromToThat));
				// G.get(cntA).addEdge(e);
				// }
				//
				// }
				// }
				// dummyGroup start from begin
			}

			// locations[n.locationId].CapacityThatTimePeriod
			// for (Lsp u : lsps)
			// {
			// u.setlspCurrentDummyGroup();
			// u.setUcapacity(Ucapacity[n.locationId]*/n.sumWeight);
			// }
			// } else {
			// for (Lsp u : lsps)
			// u.setSingleLocationCheckCPFalseThatTimePeriod(locations[n.locationId]);

		}

		System.err.println(seperateLspIMDMallPossibleVisit);// this is which lsp, location, time from, time to, count,
															// split based on this time period, based on middle value of
															// largest-smallest, medium or dedian

	}

	public Vector<Integer> convertListToVector(List<Integer> path) {
		Vector<Integer> vec = new Vector<Integer>();
		for (Integer node : path)
			vec.add(node);
		return vec;
	}

	public boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	public ArrayList<Vehicle> vehicles() {
		return this.vehicles;
	}

	public ArrayList<Order> orders() {
		return this.orders;
	}

	public Location[] locations() {
		// TODO Auto-generated method stub
		return this.locations;
	}

	public ArrayList<Lsp> lsps() {
		return this.lsps;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

	public int totalSize(ArrayList<ArrayList<Order>> allRoute) {
		int size = 0;
		for (ArrayList route : allRoute) {
			size = route.size() - 2;
		}
		return size;
	}

	public void initilize(Set<Order> startEndNodes, ArrayList<Vehicle> vehicles, ArrayList<Lsp> lsps,
			ArrayList<Order> orders, Location[] locations, Constants con, int totalCar, int stops, int[] ucapacity,
			HashSet<Integer> fairPriceOrders) {
		this.startEndNodes = startEndNodes;
		this.vehicles = vehicles;
		this.lsps = lsps;
		this.orders = orders;
		this.locations = locations;
		this.con = con;
		this.totalCar = totalCar;
		this.stops = stops;
		this.Ucapacity = ucapacity;
		this.fairPriceOrders = fairPriceOrders;
	}

	public void setDynamicFlag(boolean dynamicFlag) {
		this.dynamicFlag = dynamicFlag;
	}

	public void setReSequenceFlagOn(boolean reSequenceFlagOn) {
		this.reSequenceFlagOn = reSequenceFlagOn;

	}

	public void setNewOrders(List<Integer> newOrdersId, ArrayList<Order> newOrders,
			List<Integer> newOrdersCorresPickup) {

		this.newOrdersId = newOrdersId;
		this.newOrders = newOrders;
		this.newOrdersCorresPickup = newOrdersCorresPickup;
	}

}
