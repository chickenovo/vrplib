package Data;

import java.util.ArrayList;

public class SimplifiedOrder {
	public int lowerTime;
	public int upperTime;
	public int locationId;
	public int fixedOrder;
	public int lSPId;
	public double volume;
	public int tripCnt = 0;
	public SimplifiedOrder(int lowerTime, int upperTime, int locationId, double volume, int fixedOrder) {
		this.lowerTime = lowerTime;
		this.upperTime = upperTime;
		this.locationId = locationId;
		this.volume = volume;
		this.fixedOrder = fixedOrder;
	}
	public void setLSPId(int lSPId) {
		this.lSPId= lSPId;	
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    SimplifiedOrder other = (SimplifiedOrder) obj;
	    if (lowerTime != other.lowerTime)
	        return false;
	    if (upperTime != other.upperTime)
	        return false;
	    if (locationId != other.locationId)
	        return false;
	    if (fixedOrder != other.fixedOrder)
	        return false;
	    if (tripCnt != other.tripCnt)
	        return false;
	    return true;
	}

	@Override
	public int hashCode() {
		ArrayList<Integer> newHash = new ArrayList<Integer>();
		newHash.add(this.lowerTime);
		newHash.add(this.upperTime);
		newHash.add(this.locationId);
		newHash.add(this.fixedOrder);
		newHash.add(this.tripCnt);
		return newHash.hashCode();
	}
	public void setTripId(int tripCnt) {
		this.tripCnt = tripCnt;
		
	}


}
