package Data;

import java.util.ArrayList;

public class DummyOrder {
	int mallId;
	int start;
	int end;
	public int usedDock;

	public DummyOrder(int mallId, int open, int close, int usedDock) {
		this.mallId = mallId;
		this.start = open;
		this.end = close;
		this.usedDock = usedDock;
		
	}

	public int getMin() {
		// TODO Auto-generated method stub
		return this.start;
	}

	public int getMax() {
		return this.end;
	}

	public int serviceTime() {
		// TODO Auto-generated method stub
		return this.end-this.start;
	}

	public int getMallId() {
		// TODO Auto-generated method stub
		return this.mallId;
	}
}
