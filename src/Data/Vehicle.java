package Data;

import java.util.ArrayList;
import java.util.HashSet;

public class Vehicle {
	public double capacity;
	public int truckLowerTime;
	public int truckUpperTime;
	public int truckDockType;
	public int rankLsp;
	public int originLSP;
	public double volume;
	public int startLocation;
	public int endLocation;
	public boolean haveTrip;
	public Order depot;
	public Order endDepot = null;
	public ArrayList<Order> middleDepots;
	public int depotLoadTime;
	public int lunchTimeEnd;
	public int lunchTimeStart;
	public Lsp lsp;
	public int initialUpper;
	public int initialLower;
	public HashSet<Integer> vehicleStartMiddleNodes;
	public HashSet<Order> vehicleStartMiddleOrders;

	public HashSet<Integer> vehicleStartMiddleLocations;
	public HashSet<Order> noOrderRelatedDepots;
	public int stratEndDepotDiff;

	public Vehicle(double capacity, int truckLowerTime, int truckUpperTime, int truckDockType) {
		this.capacity = capacity;
		this.truckLowerTime = truckLowerTime;
		this.truckUpperTime = truckUpperTime;
		this.initialUpper = truckUpperTime;
		this.truckDockType = truckDockType;
	}

	public void setStartLocation(int startLocation) {
		this.startLocation = startLocation;
	}

	public void setNoRelatedOrderDepots(HashSet<Order> noOrderRelatedDepots) {
			this.noOrderRelatedDepots = noOrderRelatedDepots;
	}

	public void setvehicleStartEndNodes() {
		vehicleStartMiddleNodes = new HashSet<Integer>();
		vehicleStartMiddleNodes.add(startLocation);
		// vehicleStartEndNodes.add(endLocation);
		for (Order u : lsp.multiDepotOrders) {
			vehicleStartMiddleNodes.add(u.orderLocation);
		}
		setvehicleStartEndOrders();
		//setNoRelatedOrderDepots();
	}

	private void setvehicleStartEndOrders() {
		vehicleStartMiddleOrders = new HashSet<Order>();
		vehicleStartMiddleOrders.add(depot);
		vehicleStartMiddleLocations = new HashSet<Integer>();
		// vehicleStartEndNodes.add(endLocation);
		for (Order u : lsp.multiDepotOrders) {
			vehicleStartMiddleOrders.add(u);
			vehicleStartMiddleLocations.add(u.orderLocation);
		}
		// System.out.println("vehicleStartMiddleLocations\t"+vehicleStartMiddleLocations);
	}

	public void setEndLocation(int endLocation) {
		this.endLocation = endLocation;
	}

	public double capacity() {
		// TODO Auto-generated method stub
		return capacity;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public void updateVolume(double para) {
		this.volume *= (1 + para);
	}

	public void setAddRank(int rankLsp) {
		this.rankLsp = 6 - rankLsp;
	}

	public int truckLowerTime() {
		// TODO Auto-generated method stub
		return this.truckLowerTime;
	}

	public int truckUpperTime() {
		// TODO Auto-generated method stub
		return this.truckUpperTime;
	}

	public int truckDockType() {
		// TODO Auto-generated method stub
		return truckDockType;
	}

//	public void setTruckLowerTime(int totalTime) {
//		this.truckLowerTime = totalTime;
//	}
//
//	public void setTruckUpperTime(int totalTime) {
//		this.truckUpperTime = totalTime;
//	}

	public void setOriginLSP(int originLSP) {
		this.originLSP = originLSP;
	}

	public void setTrip(boolean haveTrip) {
		this.haveTrip = haveTrip;
	}

	public void setDepot(Order depot) {
		this.depot = depot;
	}

	public boolean getTripBool() {
		// TODO Auto-generated method stub
		return this.haveTrip;
	}

	public double volume() {
		// TODO Auto-generated method stub
		return this.volume;
	}

	public void setDepotLoadTime(int depotLoadTime) {
		this.depotLoadTime = depotLoadTime;

	}

	public void setLunchBreakStartTime(int lunchTimeStart) {
		// TODO Auto-generated method stub
		this.lunchTimeStart = lunchTimeStart;
	}

	public void setLunchBreakEndTime(int lunchTimeEnd) {
		// TODO Auto-generated method stub
		this.lunchTimeEnd = lunchTimeEnd;
	}

	public void setEndDepot(Order o1) {
		this.endDepot = o1;

	}

	public void setMiddleDepot(Order o1) {
		middleDepots = new ArrayList<Order>();
		middleDepots.add(o1);
	}

	public void setlSP(Lsp lsp) {
		this.lsp = lsp;

	}

	public void setInitialUpper(int truckUpperTime) {
		this.initialUpper = truckUpperTime;
	}

	public void setInitialLower(int truckLowerTime) {
		this.initialLower = truckLowerTime;
	}

	public void setStartEndDepotDiff(int nn) {
		this.stratEndDepotDiff = nn;
		//System.err.println("this.stratEndDepotDiff\t"+this.stratEndDepotDiff);
		
	}

}
