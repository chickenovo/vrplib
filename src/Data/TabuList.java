package Data;

import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.collections15.buffer.CircularFifoBuffer;

public class  TabuList {
	
	private HashSet<Integer> tabuList;
	
	public TabuList(Integer size) {
		this.tabuList = new HashSet<Integer>();
	}


	public Boolean contains(Integer hashcode) {
		return tabuList.contains(hashcode);
	}
	

	public void add(Integer hashcode) {
		tabuList.add(hashcode);
		
	}



}