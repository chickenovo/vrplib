package Data;

import java.util.List;
import java.util.Vector;

/**
 * Represents a solution or partial solution to the vehicle routing problem.
 */
public class Solution {
	// values of -1 point to the depot
	private Vector<Vector<Integer>> routes;
	private double toursCost = -1;

	public Solution(Vector<Vector<Integer>> vector) {
		this.setRoutes(vector);
	}

	public Solution(Vector<Vector<Integer>> routes, double toursCost) {
		this(routes);
		this.toursCost = toursCost;
	}

	public Vector<Vector<Integer>> getRoutes() {
		return routes;
	}

	private void setRoutes(Vector<Vector<Integer>> routes) {
		this.routes = routes;
	}

}
