package Data;
import java.util.ArrayList;
import java.util.List;


public class Trip {
	public double cost = 1e8;
	public int car;
	public List<Integer> path;
	public int startTime;
	public double endTime;

	public Order Start;
	public Order End;
	public int startLocationId;
	public Trip(int car, List<Integer> singlePath) {
		this.car = car;
		this.path = singlePath;	
	}

	public void setPathU(List<Order> path) {
		List<Integer> newPath = new ArrayList<Integer>();
		for (Order node : path)
			newPath.add(node.id);
		this.path = newPath;
		
	}
	public void setcost(double c) {
		this.cost = c;
	}

	public double getcost() {
		return this.cost;
	}

	public int compareTo(Trip compareFruit) {	
		int compareTime = (int) ((Trip) compareFruit).startTime; 
		//ascending order
		return (int) (this.startTime - compareTime);
		//descending order
		//return compareQuantity - this.quantity;
	}	
	
	public List<Integer> getpath() {
		return this.path;
	}

	public int startTime() {
		return startTime;
	}

	public void setLocationStartEnd(Order Start, Order End) {
		this.Start = Start;
		this.End = End;
		
	}
	
	public void setLocationStartId(int startLocationId) {
		this.startLocationId = startLocationId;		
	}
}
