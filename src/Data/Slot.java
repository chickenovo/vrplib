package Data;

import java.util.Vector;

public class Slot {
	public int id;
	public Vector<int[]> unused;
	public Vector<int[]> used;

	public Slot(int divide, int smallest, int largest) {
		used = new Vector<>();
		unused = new Vector<>();
		for (int i = smallest; i < largest; i++) {
			int[] single = new int[2];
			single[0] = i;
			single[1] = i + divide;
			unused.add(single);
		}
	}

}
