package Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

public class Order {
	public int id;
	int pickUpOpen = 0;
	int pickUpClose = 10000;
	public double weight = 0;
	public double posWeight = 0;
	public double negWeight = 0;
	public double volume = 0;
	public double posVolume = 0;
	public double negVolume = 0;

	public int upperTime;
	public int lowerTime;
	public int waitTime;
	public int serviceTime;
	public int rankLsp;
	public int originLSP;
	public boolean newAddedOrderFlag = false;
	public int orderLocation = -1;
	protected HashSet<Integer> forbiddenNextOrder;
	public List<Integer> vehiclesIndex;
	public List<Integer> path;
	public int depotVechicle;
	public int specialVehicle = -1;

	public boolean isMall;
	public boolean isDepot;
	public Order tripStart;
	public Location location;
	public String deliveryStatus;
	public String customerId;
	public String cargoType;
	public int pickupDepotLocation;
	public String schedualFlag;

	public Order() {
		this.vehiclesIndex = new ArrayList<Integer>();
	}

	public void set(int cur, double weight, double posWeight, double negWeight, int lowerTime, int upperTime,
			int serviceTime, int orderLocation) {
		this.id = cur;
		this.weight = weight;
		this.posWeight = posWeight;
		this.negWeight = negWeight;
		this.lowerTime = lowerTime;
		this.upperTime = upperTime;
		this.serviceTime = serviceTime;
		this.orderLocation = orderLocation;
		this.isMall = false;
	}

	public void setVehiclesIndex(int i) {
		this.vehiclesIndex = new ArrayList<Integer>();
		this.vehiclesIndex.add(i);

	}

	public void setAddRank(int rankLsp) {
		this.rankLsp = 6 - rankLsp;
	}

	public void setIsMall() {
		this.isMall = true;
	}

	public void setWaitTime(int curWaitTime) {
		this.waitTime = curWaitTime;
	}

	public void forbiddenNextOrder(HashSet<Integer> forbiddenNextOrder) {
		this.forbiddenNextOrder = forbiddenNextOrder;
	}

	public double weight() {
		return this.weight;
	}

	public double posWeight() {
		// TODO Auto-generated method stub
		return this.posWeight;
	}

	public double negWeight() {
		// TODO Auto-generated method stub
		return this.negWeight;
	}

	public int upperTime() {
		// TODO Auto-generated method stub
		return this.upperTime;
	}

	public int lowerTime() {
		// TODO Auto-generated method stub
		return this.lowerTime;
	}

	public int serviceTime() {
		// TODO Auto-generated method stub
		return this.serviceTime;
	}

	public int orderLocation() {
		return this.orderLocation;
	}

	public void setLowerTime(int lowerTime) {
		this.lowerTime = lowerTime;
	}

	public void setUpperTime(int upperTime) {
		this.upperTime = upperTime;
	}

	public int waitTime() {
		// TODO Auto-generated method stub
		return this.waitTime;
	}

	public int rankLsp() {
		// TODO Auto-generated method stub
		return this.rankLsp;
	}

	public void setVolume(double volume) {

		this.volume = volume;

		if (volume >= 0)
			posVolume = volume;
		else
			negVolume = volume;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	public void setVehicles(Vector<Integer> lspVehicles) {
		this.vehiclesIndex = lspVehicles;
	}

	public List<Integer> getVehiclesIndex() {
		// TODO Auto-generated method stub
		return this.vehiclesIndex;
	}

	public List<Integer> path() {
		return this.path;
	}

	public void setPath(List<Integer> path2) {
		this.path = new Vector<Integer>();
		for (Integer node : path2)
			this.path.add(node);
	}

	public void setTripStart(Order tipStart) {
		this.tripStart = tipStart;
	}

	public void setLocationId(int orderLocationId) {
		this.orderLocation = orderLocationId;
	}

	public void setLocation(Location orderLocation) {
		this.location = orderLocation;
	}

	public void setLeft(int rankLsp, List<Integer> path2, List<Integer> vehiclesIndex, Integer vehicleId) {
		this.rankLsp = rankLsp;
		this.path = path2;
		this.vehiclesIndex = vehiclesIndex;
		this.depotVechicle = vehicleId;
		this.isDepot = true;
	}

	public void setId(int id) {
		this.id = id;

	}

	public void setOriginLSP(int originLSP) {
		this.originLSP = originLSP;

	}

	public double volume() {
		// TODO Auto-generated method stub
		return this.volume;
	}

	@Override
	public boolean equals(Object v) {
		boolean retVal = false;

		if (v instanceof Order) {
			Order ptr = (Order) v;
			if (ptr.id == this.id) // && ptr.path == this.path && ptr.orderLocation == this.orderLocation)
				retVal = true;
		}

		return retVal;
	}

	@Override
	public int hashCode() {
		int hash = 0;// 7
		//
		// if(this.path!=null)
		// hash = 17 * this.path.hashCode() + this.id;
		// else
		hash = this.id;
		return hash;
	}

	public void setStatus(String string) {
		// TODO Auto-generated method stub
		this.deliveryStatus = string;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;

	}

	public void setCargoType(String cargoType) {
		this.cargoType = cargoType;
	}

	public void setDepotPickup(int pickupDepotLocation) {
		this.pickupDepotLocation = pickupDepotLocation;
	}

	//not use now, previously used for predefined vehicle order assignment
	public void setSchedualFlag(String flag) {
		this.schedualFlag = flag;

	}

	public void setPreDefVehicle(int parseInt) {
		this.specialVehicle = parseInt;
	}

	public Integer pickupLocation() {
		// TODO Auto-generated method stub
		return this.pickupDepotLocation;
	}

	public void setNewOrderFlag(boolean newAddedOrderFlag) {
		this.newAddedOrderFlag = newAddedOrderFlag;
		
	}

	public void setPickUpOpen(int parseInt) {
		this.pickUpOpen = parseInt;	
	}

	public void setPickUpClose(int parseInt) {
		this.pickUpClose = parseInt;		
	}
	
}
