package Data;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private int label;
	private List<Edge> edges = new ArrayList<Edge>();
	int locationId;
	int sumWeight;
	int from;
	int to;

	public void setLabel(int cnt) {
		this.label = cnt;
	}

	public void addEdge(Edge e) {
		getEdges().add(e);
//		from=;
//		to=
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void calculateTotalWeight() {
		sumWeight = 0;
		for(Edge e: edges)
			sumWeight +=e.getWeight();
	}
}
