package Data;

import edu.princeton.cs.algs4.StdOut;

public class Interval {
	public final double min;
	public final double max;

	public Interval(double min, double max) {
		if (Double.isInfinite(min) || Double.isInfinite(max))
			throw new IllegalArgumentException("Endpoints must be finite");
		if (Double.isNaN(min) || Double.isNaN(max))
			throw new IllegalArgumentException("Endpoints cannot be NaN");

		// convert -0.0 to +0.0
		if (min == 0.0)
			min = 0.0;
		if (max == 0.0)
			max = 0.0;

		if (min <= max) {
			this.min = min;
			this.max = max;
		} else
			throw new IllegalArgumentException("Illegal interval");
	}

	public double min() {
		return min;
	}

	public double max() {
		return max;
	}

	public boolean intersects(Interval that) {
		if (this.max < that.min)
			return false;
		if (that.max < this.min)
			return false;
		return true;
	}

	public boolean contains(double x) {
		return (min <= x) && (x <= max);
	}

	/**
	 * Returns the length of this interval.
	 *
	 * @return the length of this interval (max - min)
	 */
	public double length() {
		return max - min;
	}

	public String toString() {
		return "[" + min + ", " + max + "]";
	}

	public boolean equals(Object other) {
		if (other == this)
			return true;
		if (other == null)
			return false;
		if (other.getClass() != this.getClass())
			return false;
		Interval that = (Interval) other;
		return this.min == that.min && this.max == that.max;
	}

	public int hashCode() {
		int hash1 = ((Double) min).hashCode();
		int hash2 = ((Double) max).hashCode();
		return 31 * hash1 + hash2;
	}

}
