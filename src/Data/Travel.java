package Data;

import java.util.ArrayList;

public class Travel {
	int to;
	int[] timeDependent;
	double distance;
	int time;
	public Travel(int to, int[] timeDependent) {
		this.to = to;
		this.timeDependent = timeDependent;
	}
	
	public void TravelDis(int to, double distance) {
		this.to = to;
		this.distance = distance;
	}
	
	public void TravelTime(int to, int time) {
		this.to = to;
		this.time = time;
	}
}
