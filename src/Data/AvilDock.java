package Data;

public class AvilDock {
	public int mallId;
	public int start;
	public int end;
	public int aviDock;

	public AvilDock(int mallId, int aviDock, int start, int end) {
		this.mallId = mallId;
		this.aviDock = aviDock;
		this.start = start;
		this.end = end;	
	}

	public int getMin() {
		// TODO Auto-generated method stub
		return this.start;
	}

	public int getMax() {
		return this.end;
	}

	public int serviceTime() {
		// TODO Auto-generated method stub
		return this.end-this.start;
	}
}
