package Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

/**
 * Represents a solution or partial solution to the vehicle routing problem.
 */
public class PathSolution {
	// values of -1 point to the depot
	public ArrayList<Order> path;
	private int vehicleID = 0;
	private HashSet<Integer> uniqueLTimeWindow;
	private HashSet<Integer> uniqueUTimeWindow;
	private HashSet<Integer> uniqueLocation;
	public double value = 10e8;

	public PathSolution(ArrayList<Order> route, int k) {
		this.setRoutes(route);
		this.setVehicleID(k);
	}

	public PathSolution(List<Order> route) {
		this.path = (ArrayList<Order>) route;
		for (Order o : route) {
			this.uniqueLTimeWindow.add(o.lowerTime);
			this.uniqueUTimeWindow.add(o.upperTime);
			this.uniqueLocation.add(o.orderLocation);
		}
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getValue(double value) {
		return this.value;
	}

	public ArrayList<Order> getPath() {
		return path;
	}

	private void setRoutes(ArrayList<Order> route) {
		this.path = route;
	}

	public int getVehicleID() {
		return vehicleID;
	}

	public void setVehicleID(int vehicleID) {
		this.vehicleID = vehicleID;
	}
	//
	// @Override
	// public boolean equals(Object v) {
	// boolean retVal = false;
	//
	// if (v instanceof PathSolution) {
	// PathSolution ptr = (PathSolution) v;
	// if (ptr.uniqueLTimeWindow.hashCode() == this.uniqueLTimeWindow.hashCode()
	// && ptr.uniqueUTimeWindow.hashCode() == this.uniqueUTimeWindow.hashCode()
	// && ptr.uniqueLocation.hashCode() == this.uniqueLocation.hashCode())
	// retVal = true;
	// }
	//
	// return retVal;
	// }

	@Override
	public int hashCode() {
		int hash = 0;// 7
		//
		// if(this.path!=null)
		// hash =
		// else
		hash = 17 * this.uniqueLTimeWindow.hashCode() + 7 * uniqueUTimeWindow.hashCode()
				+ this.uniqueLocation.hashCode();
		return hash;
	}

	public void setPath(ArrayList<Order> oldPath) {
		this.path = oldPath;
	}

}
