package Data;

public class CriticalFeature {
	public double totalTime = 0;
	public double value = 0;
	public double weight[] = new double[3];
	public double volume[] = new double[3];
	
	public CriticalFeature(double totalTime, double value, double[] weight, double[] volume) {
		this.totalTime = totalTime;	
		this.value = value; 
		this.weight = weight;
		this.volume = volume;	
	}
}
