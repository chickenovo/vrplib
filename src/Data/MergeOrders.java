package Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import MasterModel.CommonFunction;

//stops = Integer.parseInt(tokens[3]);
//sumAllOrder = stops + totalCar * 2 + 1;

public class MergeOrders {
	int totalGroup;
	int totalCar;
	int stops;
	int points;
	int timeVioCost;
	List<List<Integer>> tripsInteger = new ArrayList<List<Integer>>();
	List<ArrayList<SimplifiedOrder>> trips = new ArrayList<ArrayList<SimplifiedOrder>>();
	public HashMap<Integer, List<Integer>> initialMap = new HashMap<Integer, List<Integer>>();
	public HashMap<Integer, Integer> initialVehicleMap = new HashMap<Integer, Integer>();
	public HashMap<Integer, Integer> verseMap = new HashMap<Integer, Integer>();
	public ArrayList<Integer> initialVehicle = new ArrayList<Integer>();
	int[] serviceTime;
	SimplifiedOrder[] orders;
	SimplifiedOrder[] newOrders;
	SimplifiedOrder[] chekU = new SimplifiedOrder[2];
	HashSet<SimplifiedOrder> chekUSet = new HashSet<>();

	int newOrderSize = 0;
	int sumTrip = 0;
	ArrayList<Integer> newServiceTime = new ArrayList<Integer>();
	String fileName;

	public MergeOrders(String fileName) throws IOException {
		this.fileName = fileName;
		File file = new File(fileName);

		int cnt = 0;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		int temp = 0;
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// String[] tokens =
				// line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				String[] tokens = line.split(",", -1);
				if (tokens.length == 6 && tokens[0].matches("overall")) {
					totalGroup = Integer.parseInt(tokens[4]);
					totalCar = Integer.parseInt(tokens[2]);
					stops = Integer.parseInt(tokens[3]);
					points = Integer.parseInt(tokens[4]);
					timeVioCost = Integer.parseInt(tokens[5]);
					trips = new ArrayList<ArrayList<SimplifiedOrder>>();
					orders = new SimplifiedOrder[stops + 1];
					serviceTime = new int[stops + 1];
				}

				if (tokens.length == 10 && tokens[0].matches("order") && tokens[1].matches("[0-9]+")) {
					int cur = Integer.parseInt(tokens[1]);
					double weight = Double.parseDouble(tokens[3]);
					temp = 0;
					temp = Integer.parseInt(tokens[2]);

					SimplifiedOrder oder = new SimplifiedOrder((int) Double.parseDouble(tokens[4]),
							(int) Double.parseDouble(tokens[5]), (int) Double.parseDouble(tokens[2]), Double.parseDouble(tokens[6]),
							(int) Double.parseDouble(tokens[9]));

					oder.setLSPId(Integer.parseInt(tokens[8]));

					orders[cur] = oder;
					serviceTime[cur] = Integer.parseInt(tokens[7]);
				}
				if ((tokens.length == 5) && (tokens[0].matches("trip"))) {
					sumTrip++;
					String[] parts = tokens[2].split("-");
					String[] newPart = parts[0].split("\\[");
					temp = Integer.parseInt(tokens[1]);
					List<Integer> singlePath = new ArrayList<Integer>();
					if (newPart[1].matches("^-?\\d+$")) {
						singlePath.add(Integer.parseInt(newPart[1]));
					}
					for (int i = 1; i < parts.length - 1; i++) {
						singlePath.add(Integer.parseInt(parts[i]));
					}
					if (parts.length > 1) {
						newPart = parts[(parts.length - 1)].split("\\]");
						singlePath.add(Integer.parseInt(newPart[0]));
					} else {
						String[] cPart = newPart[1].split("\\]");
						singlePath.add(Integer.parseInt(cPart[0]));
					}
					tripsInteger.add(singlePath);
					if (!initialVehicle.contains(temp))
						initialVehicle.add(temp);
					cnt++;
				}
			}

		} finally {
			LineIterator.closeQuietly(it);
		}
		
		if (sumTrip == 0) {
			
			String[] extensionRemoved = this.fileName.split("\\.");
			String newFile = extensionRemoved[0] + "new." + extensionRemoved[1];
			PrintWriter out = new PrintWriter(newFile);
			cnt = 0;
			int newCnt = 1;
			int size = 0;
			int vehicleCnt = 1;
			it = FileUtils.lineIterator(file, "UTF-8");
			try {
				while (it.hasNext()) {
					String line = it.nextLine();
					String[] tokens = line.split(",", -1);
					if (tokens[0].matches("vehicle")) {
						initialVehicleMap.put(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[1]));
					}
					if (tokens[0].matches("order")) {
						List<Integer> ord = new ArrayList<Integer>();
						ord.add(Integer.parseInt(tokens[1]));
						initialMap.put(Integer.parseInt(tokens[1]), ord);
					}
					out.println(line);
				}

			} finally {
				LineIterator.closeQuietly(it);
			}
			out.close();
			return;
		}
		cnt = 0;

		for (int i = 0; i < initialVehicle.size(); i++) {
			initialVehicleMap.put(i + 1, initialVehicle.get(i));
			verseMap.put(initialVehicle.get(i), i + 1);
		}
		int initialSize = 0;
		int index = 0;
		int tripCnt = 0;
		for (List<Integer> tripInt : tripsInteger) {
			for (Integer node : tripInt) {
				orders[node].setTripId(tripCnt);
			}
			tripCnt++;
		}

		ArrayList<Integer> singlePathMap = new ArrayList<Integer>();
		for (List<Integer> tripInt : tripsInteger) {
			ArrayList<SimplifiedOrder> singlePath = new ArrayList<SimplifiedOrder>();

			for (Integer node : tripInt) {
				initialSize = singlePath.size();
				
				if (!singlePathMap.contains(orders[node].hashCode())) {
					singlePath.add(orders[node]);
					singlePathMap.add(orders[node].hashCode());
					index = singlePathMap.size() - 1;
					initialMap.put(index + 1, new ArrayList<Integer>());
					initialMap.get(index + 1).add(node);
					newServiceTime.add(serviceTime[node]);
				} else {
					index = singlePathMap.indexOf(orders[node].hashCode());
					initialMap.get(index + 1).add(node);
					newServiceTime.set(index, newServiceTime.get(index) + serviceTime[node]);
				}

			}
			trips.add(singlePath);

		}
		//System.out.println("initialMap\t" + initialMap);

		// if(initialMap==null||initialMap.size()<1)
		// initialMap

		newOrderSize = 0;
		for (ArrayList<SimplifiedOrder> singlePath : trips) {
			newOrderSize += singlePath.size();
		}
		outPut(file);
	}

	public void outPut(File file) throws IOException {
		String[] extensionRemoved = this.fileName.split("\\.");
		String newFile = extensionRemoved[0] + "new." + extensionRemoved[1];
		PrintWriter out = new PrintWriter(newFile);
		int cnt = 0;
		int newCnt = 1;
		int size = 0;
		int vehicleCnt = 1;
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				// String[] tokens =
				// line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
				String[] tokens = line.split(",", -1);
				if (tokens.length == 6 && tokens[0].matches("overall")) {
					out.println("overall," + tokens[1] + "," + verseMap.keySet().size() + "," + newOrderSize + ","
							+ tokens[4] + "," + tokens[5]);
				} else if ((tokens.length == 5) && (tokens[0].matches("trip"))) {
					String singTrip = "";
					size = trips.get(cnt).size();
					for (int i = 1; i < size; i++) {
						singTrip += newCnt + "-";
						newCnt++;
					}
					singTrip += newCnt;
					newCnt++;
					cnt++;
					out.println("trip," + verseMap.get(Integer.parseInt(tokens[1])) + ",[" + singTrip + "]," + tokens[3]
							+ "," + tokens[4]);

				} else if (tokens.length >= 8 && tokens[0].matches("vehicle") && tokens[1].matches("[0-9]+")) {
					if (initialVehicle.contains(Integer.parseInt(tokens[1]))) {

						if (tokens.length > 8)
							out.println("vehicle," + verseMap.get(Integer.parseInt(tokens[1])) + "," + tokens[2] + ","
									+ tokens[3] + "," + tokens[4] + "," + tokens[5] + "," + tokens[6] + "," + tokens[7]
									+ "," + tokens[8]);
						else
							out.println("vehicle," + verseMap.get(Integer.parseInt(tokens[1])) + "," + tokens[2] + ","
									+ tokens[3] + "," + tokens[4] + "," + tokens[5] + "," + tokens[6] + ","
									+ tokens[7]);
						vehicleCnt++;
					}
				} else if ((!tokens[0].matches("trip")) && (!tokens[0].matches("order"))
						&& (!tokens[0].matches("vehicle"))) {
					out.println(line);
				}

			}

		} finally {
			LineIterator.closeQuietly(it);
		}
		cnt = 0;
		for (Integer node : initialMap.keySet()) {
			cnt++;
			
			out.println("order," + cnt + "," + orders[initialMap.get(node).get(0)].locationId + "," + 0 + ","
					+ orders[initialMap.get(node).get(0)].lowerTime + ","
					+ orders[initialMap.get(node).get(0)].upperTime + "," + 0 + "," + newServiceTime.get(cnt - 1) + ","
					+ orders[initialMap.get(node).get(0)].lSPId + ","
					+ +orders[initialMap.get(node).get(0)].fixedOrder);
		}

		out.close();

	}
}
