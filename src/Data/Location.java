package Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class Location {
	boolean imd;
	public int id;
	public int Ucapacity;
	public int open;
	public int close;
	public ArrayList<ArrayList<int[]>> blocked;
	public int minCapacity;
	public HashMap<Integer, int[]> travelTo;
	public int lunchStart = 720;
	public int lunchEnd = 720;
	public String type;

	public Location(boolean imd, int Ucapacity) {
		this.imd = imd;
		this.Ucapacity = Ucapacity;

		this.blocked = new ArrayList<ArrayList<int[]>>();
		for (int i = 0; i < Ucapacity; i++)
			this.blocked.add(new ArrayList<int[]>());
	}

	public void setUcapacity(int aviDock) {
		this.Ucapacity = aviDock;
	}

	public int Ucapacity() {
		return this.Ucapacity;
	}

	public void addTravel(Travel t) {
		if (this.travelTo == null)
			this.travelTo = new HashMap<Integer, int[]>();
		this.travelTo.put(t.to, t.timeDependent);
	}

	public ArrayList<ArrayList<int[]>> getBlocked() {
		return this.blocked;
	}

	public void setBlocked(int whichSLot, int whichOrder, int blockStart, int blockEnd) {
		int[] newBlock = new int[3];
		newBlock[0] = whichOrder;
		newBlock[1] = blockStart;
		newBlock[2] = blockEnd;

		int size = this.blocked.get(whichSLot).size();
		int index = 0;
		for (index = 0; index < size - 1; index++) {
			if (blockStart >= this.blocked.get(whichSLot).get(index)[2]
					&& blockEnd <= this.blocked.get(whichSLot).get(index + 1)[1])
				break;
			if (blockStart >= 0 && blockEnd <= this.blocked.get(whichSLot).get(index)[1])
				break;
		}

		this.blocked.get(whichSLot).add(index, newBlock);

	}

	public void removeSlots(Vector<Integer> vector) {
		// TODO Auto-generated method stub
		for (ArrayList<int[]> slots : this.getBlocked()) {
			for (Integer order : vector)
				slots.removeIf(m -> (m[0] == order));
		}

	}

	public int slotCheck(int from, int to) {
		int slotID = 0;
		int returnID = -1;
		if (this.blocked.size() < 1)
			returnID = 0;
		for (ArrayList<int[]> slots : this.getBlocked()) {
			if (slots.size() < 1)
				returnID = 0;
			if (returnID > -1)
				break;
			for (int[] interval : slots) {
				if (interval[1] <= from || interval[0] >= to) {
					returnID = slotID;
					break;
				}
			}
			slotID++;
		}

		return returnID;
	}

	public void clearSlots() {
		this.blocked = new ArrayList<ArrayList<int[]>>();
		for (int i = 0; i < Ucapacity; i++)
			this.blocked.add(new ArrayList<int[]>());
	}

	public void setMinCapacity(int small) {
		this.minCapacity = small;

	}

	public void setStart(int start) {
		this.open = start;
	}

	public void setEnd(int close) {
		this.close = close;
	}

	public void setLunchBreakStartTime(int lunchStart) {
		this.lunchStart = lunchStart;

	}

	public void setLunchBreakEndTime(int lunchEnd) {
		this.lunchEnd = lunchEnd;
	}

	public void setType(String string) {
		this.type = string;
	}

	public void setId(int cur) {
		this.id = cur;		
	}

}
