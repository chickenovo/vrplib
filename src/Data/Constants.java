package Data;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

// All of them cannot changed or can only update the value in a given class that may use them
public class Constants {
	public String outPut;
//	public double timeVioCost = 50;
//	public double initialTimeVioWeight = 50;
//	public double travelWeight = 1;//10
//	public double waitWeight = 0;//1
//	public double maxSpanWeight = 2;//20
//	public double loadBalanceWeight = 0;//10
	public ArrayList<Order> noNeedFifteen = new ArrayList<Order>();
	public double[] x;
	public int callCPTimes = 5;
	public int paramALNSItr = 5;
	public int splitSlot = 15;
	public double yita1 = 1;
	public double yita2 = 1;
	public double yita3 = 1;
	public double rho = 0.01;
	public double alpha1 = 1;
	public double alpha2 = 1;
	public int price = 10000;
	public int unserved_price = 10000;
	public int rankInterval = 20;

	public double waitingTimePara = 0.1;
	public double capacityVioCost = 2888;
	public int posAveWeight;
	public int negAveWeight;
	public int aveWeight;
	//public int maxCapacity;
	public int timeLimit;
	public int sumAllOrder;
	public int sumAllSameGroupOrder;
	public int imdTotalGroup;
	public int totalCar;
	public int stops;
	public int points;
	public boolean vioCheck1 = true;
	public boolean vioCheck2 = true;
	public boolean vioCheck3 = true;
	public List<Integer> allOrders = new ArrayList<Integer>();
	public HashMap<Integer, Integer> normalVehicle;
	public int[][] timeMatrix = null;
	public double[][] disMatrix = null;
	public ArrayList<Lsp> lsps;
	// public Vector<Vector<Integer>> nodeSet = new Vector<Vector<Integer>>();
	public List<Integer> depotSet;
	public Set<Order> startEndNodes;
	public HashMap<Integer, ArrayList<DummyOrder>> dummyGroup = new HashMap<Integer, ArrayList<DummyOrder>>();
	public HashMap<Integer, ArrayList<Integer>> imdOrderInGroup = new HashMap<Integer, ArrayList<Integer>>();

	public ArrayList<Integer> imdOrders = new ArrayList<Integer>();
	public ArrayList<Integer> corrCar = new ArrayList<Integer>();
	public ArrayList<Integer> allNodes;
	public List<Integer> shuffledCarSet;
	public int[] waitTime;
	public HashMap<Integer, Integer> depotTimeMap = new HashMap<Integer, Integer>();
	public ArrayList<Integer> sameGroup = new ArrayList<Integer>();
	public ArrayList<Integer> imdSameGroup = new ArrayList<Integer>();
	public HashMap<Integer, Boolean> nonIMDMap = new HashMap<Integer, Boolean>();
	public HashMap<Integer, Integer> carSetGroup = new HashMap<Integer, Integer>();
	public List<DummyOrder> dummyOrders;
	public int totalGroup = 0;
	public int deleteDepotCnt = 0;
	/**************************************************************************/
	public HashMap<Integer, List<Integer>> initialMap;

	public HashMap<Integer, Integer> initialVehicleMap;
	public boolean finalCheck;
	public boolean needAddExtra = false;
	public ArrayList<ArrayList<Double>> finalTimeHash = new ArrayList<ArrayList<Double>>();
	public boolean dockingVehicelType;
	public double alpha = 0.1;//weight for wait time
	public HashMap<Integer, Integer> nodeSetGroup;
	public int maxId;
	public int objFlag;
	public boolean slotsFalg = false;
	
	public Constants() {

	}

	public void initializeX() {
		x = new double[8];
		x[1] = 0.9;
		x[2] = 0.5;
		x[3] = 0.5;
		x[4] = 0.9999;
		x[5] = 0.5;
		x[6] = 0.5;
		x[7] = 0.5;
	}

	public void resetyita(boolean vioCheck1, boolean vioCheck2) {
		if (!vioCheck1)
			yita1 = yita1 * (1 + rho);
		if (!vioCheck2)
			yita2 = yita2 * (1 + rho);
		if (vioCheck1)
			yita1 = yita1 / (1 + rho);
		if (vioCheck2)
			yita2 = yita2 / (1 + rho);
	}

	public void initial_yita() {
		yita1 = 1;
		yita2 = 1;
	}

	public void resetyita_novio() {
		yita1 = yita1 / (1 + rho);
		yita2 = yita2 / (1 + rho);

	}


	public void setPosAveWeight(int posAveWeight) {
		this.posAveWeight = posAveWeight;
	}

	public void setNegAveWeight(int negAveWeight) {
		this.negAveWeight = negAveWeight;
	}

	public void setAveWeight(int aveWeight) {
		this.aveWeight = aveWeight;
	}

	public void setSumAllOrder(int sumAllOrder) {
		this.sumAllOrder = sumAllOrder;
	}

	public void setSumAllSameGroupOrder(int sumAllSameGroupOrder) {
		this.sumAllSameGroupOrder = sumAllSameGroupOrder;
	}

	public void setImdTotalGroup(int imdTotalGroup) {
		this.imdTotalGroup = imdTotalGroup;
	}

	public void setTotalCar(int totalCar) {
		this.totalCar = totalCar;
	}
	public void setMaxId(int maxId) {
		this.maxId = maxId;
	}
	public void setStops(int stops) {
		this.stops = stops;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	//add vehicleId map to map vehicle originalID to vehicleCurrentID
	public HashMap<Integer, Integer> getInitialVehicleMap() {
		return initialVehicleMap;
	}

	public void setInitialVehicleMap(HashMap<Integer, Integer> initialVehicleMap ) {
		this.initialVehicleMap = initialVehicleMap;
	}

	public int stops() {
		// TODO Auto-generated method stub
		return stops;
	}

	public int totalCar() {
		// TODO Auto-generated method stub
		return totalCar;
	}

	public double[] getX() {
		return x;
	}

	public void setX(double[] x) {
		this.x = x;
	}

	public double yita1() {
		return this.yita1;
	}

	public double yita2() {
		return this.yita2;
	}

	public double alpha1() {
		return this.alpha1;
	}

	public double alpha2() {
		return this.alpha2;
	}

	public void setInitialYita1(double yita1) {
		this.yita1 = yita1;
	}

	public void setInitialYita2(double yita2) {
		this.yita2 = yita2;

	}

	public void setInitialYita3(double yita3) {
		this.yita3 = yita3;

	}

	public double[] x() {
		// TODO Auto-generated method stub
		return this.x;
	}

	public void setVioCheck3(boolean b) {
		this.vioCheck3 = false;

	}

	public void increaseYita2() {
		this.yita2 *= (1 + rho);
	}

	public void decreaseYita2() {
		this.yita2 /= (1 + rho);
	}

	public void increaseYita3() {
		this.yita3 *= (1 + rho);
	}

	public void decreaseYita3() {
		this.yita3 /= (1 + rho);
	}

	public int sumAllOrder() {
		// TODO Auto-generated method stub
		return sumAllOrder;
	}

	public double aveWeight() {
		// TODO Auto-generated method stub
		return this.aveWeight;
	}

	public double posAveWeight() {
		// TODO Auto-generated method stub
		return this.posAveWeight;
	}

	public double negAveWeight() {
		// TODO Auto-generated method stub
		return this.negAveWeight;
	}
	public ArrayList<Integer> humanU(Collection<Order> collection) {
		ArrayList<Integer> remove = new ArrayList<Integer>();
		for (Order node : collection)
			remove.add(node.id);
		return remove;

	}
	public HashMap<Integer, double[]> collectionOrder;
	public HashMap<Integer, Integer> lspVechiclesHiddenMap = new HashMap<Integer, Integer> ();

	public void setInitial(int timeLimit, int sumAllSameGroupOrder, 
			int imdTotalGroup, int totalCar, int stops, int points, int[][] timeMatrix,
			double[][] disMatrix, int totalGroup, int deleteDepotCnt, List<Integer> shuffledCarSet, HashMap<Integer, double[]> collectionOrder) {
		this.timeLimit = timeLimit;
		this.sumAllSameGroupOrder = sumAllSameGroupOrder;
		this.imdTotalGroup = imdTotalGroup;
		this.totalCar = totalCar;
		this.stops = stops;
		this.points = points;
		this.timeMatrix = timeMatrix;
		this.disMatrix = disMatrix;
		this.totalGroup = totalGroup;
		this.deleteDepotCnt = deleteDepotCnt;
		this.shuffledCarSet = shuffledCarSet;
		this.collectionOrder = collectionOrder;
	}
	public void setVioCheck1(boolean b) {
		this.vioCheck1 = b;
	}

	public void setVioCheck2(boolean b) {
		this.vioCheck2 = b;
	}

	public boolean vioCheck1() {
		return vioCheck1;
	}

	public boolean vioCheck2() {
		return vioCheck2;
	}

	public void setAllOrders(List<Integer> allOrders) {
		this.allOrders = allOrders;
	}

	public List<Integer> allOrders() {
		return allOrders;
	}

	public int count(Vector<Vector<Integer>> tempRoute) {
		int totalCnt = 0;
		corrCar = new ArrayList<Integer>();
		deleteDepotCnt = 0;
		int cnt = 0;
		int corrCarCnt = 0;
		int car = 0;
		for (Vector<Integer> path : tempRoute) {
			cnt = 0;
			totalCnt += path.size() - 1;
			deleteDepotCnt += path.size() - 2;
			for (Integer node : path) {
				if (cnt > 0 && node <= totalCar)
					deleteDepotCnt--;
				cnt++;
			}

			for (int i = 0; i < path.size() - 1; i++)
				corrCar.add(car);
			car++;
		}
		return totalCnt;
	}

	public void setFinalCheck(boolean b) {
		finalCheck = b;
	}

	public int calcuDepot(Vector<Vector<Integer>> route) {
		int totalCnt = 0;
		for (Vector<Integer> path : route) {
			for (Integer node : path) {
				if (node <= totalCar) {
					totalCnt++;
				}

			}
			totalCnt--;
		}
		return totalCnt;
	}

	public int calcuServe(Vector<Vector<Integer>> route) {
		int totalCnt = 0;
		int carCnt = 0;
		for (Vector<Integer> path : route) {
			for (Integer node : path) {
				if (node > totalCar && node <= stops + totalCar) {
					totalCnt++;
				}
			}
			carCnt++;
		}
		return totalCnt;
	}

	public Vector<Vector<Integer>> reSequence(Vector<Vector<Integer>> allRoute) {
		Vector<Vector<Integer>> route = new Vector<Vector<Integer>>();
		for (int i = 1; i <= this.totalCar; i++)
			for (Vector<Integer> path : allRoute) {
				if (path.get(0) == i) {
					route.add(path);
				}
			}
		return route;
	}

	public int[][] getTimeMatrix() {
		return timeMatrix;
	}

	public ArrayList<Integer> getSameGroup() {
		return sameGroup;
	}

	public HashMap<Integer, Integer> depotTimeMap() {
		// TODO Auto-generated method stub
		return depotTimeMap;
	}

	public HashMap<Integer, ArrayList<Integer>> imdOrderInGroup() {
		// TODO Auto-generated method stub
		return imdOrderInGroup;
	}

	public void setTimeMatrix(int i, int j, int u) {
		timeMatrix[i][j] = u;
	}

	public int waitTime(int currentNode) {
		// TODO Auto-generated method stub
		return waitTime[currentNode];
	}

	public HashMap<Integer, Boolean> nonIMDMap() {
		// TODO Auto-generated method stub
		return nonIMDMap;
	}

	public ArrayList<Integer> corrCar() {
		// TODO Auto-generated method stub
		return corrCar;
	}
	public void setTerminal(int rankInterval) {
		// TODO Auto-generated method stub
		this.rankInterval = rankInterval;
	}
	public List<Integer> depotSet() {
		// TODO Auto-generated method stub
		return depotSet;
	}

	// public void setWaitTime(int[] waitTime) {
	// this.waitTime = waitTime;
	//
	// }
	//
	// public void setDepotTimeMap(HashMap<Integer, Integer> depotTimeMap) {
	// this.depotTimeMap = depotTimeMap;
	//
	// }

	// public void setNodeSet(Vector<Vector<Integer>> nodeSet) {
	// this.nodeSet = nodeSet;
	// }
	//
	// public Vector<Vector<Integer>> nodeSet() {
	// // TODO Auto-generated method stub
	// return nodeSet;
	// }

	public void setAllSet(List<Integer> depotSet, ArrayList<Integer> imdOrders,
			 HashMap<Integer, ArrayList<Integer>> imdOrderInGroup,
			ArrayList<Integer> allNodes, ArrayList<Integer> sameGroup, HashMap<Integer, Integer> nodeSetGroup,
			ArrayList<Integer> imdSameGroup, HashMap<Integer, Boolean> nonIMDMap, HashMap<Integer, Integer> carSetGroup,
			ArrayList<Lsp> lsps, List<DummyOrder> dummyOrders,  HashMap<Integer, List<Integer>>initialMap, int objFlag) {
		this.depotSet = depotSet;
		this.imdOrders = imdOrders;
		this.imdOrderInGroup = imdOrderInGroup;
		this.allNodes = allNodes;
		this.sameGroup = sameGroup;
		this.nodeSetGroup = nodeSetGroup;
		this.imdSameGroup = imdSameGroup;
		this.nonIMDMap = nonIMDMap;
		this.waitTime = new int[this.stops + this.totalCar * 2 + 1];
		this.carSetGroup = carSetGroup;
		this.lsps = lsps;
		this.dummyOrders = dummyOrders;
		dummyGroup = new HashMap<Integer, ArrayList<DummyOrder>>();
		this.initialMap = initialMap;
		for (DummyOrder od : dummyOrders) {
			if (!dummyGroup.keySet().contains(od.mallId)) {
				dummyGroup.put(od.mallId, new ArrayList<DummyOrder>());
			}
		}

		for (DummyOrder od : dummyOrders) {
			dummyGroup.get(od.mallId).add(od);
		}
		this.objFlag = objFlag;
	}

	public double globalPrice() {
		// TODO Auto-generated method stub
		return 500;
	}

	public void setAlpha(double alpha, String outPut) {
		this.alpha = alpha;
        this.outPut = outPut;
	}
	public ArrayList<Order> cloneSingleVector(ArrayList<Order> path) {
		ArrayList<Order> route = new ArrayList<Order>();
		for (Order p : path) {
			route.add(p);
		}
		return route;
	}
	// public void doubleInterval() {
	// this.rankInterval*=1;
	// }

	public void setSlotsInterval(int newNumber) {
		this.splitSlot = newNumber;
	}

	public void setNoNeedFifteen(ArrayList<Order> noNeedFifteen) {
		this.noNeedFifteen = noNeedFifteen;
	}

	public void setTimeVioCost() {
		//this.timeVioCost = initialTimeVioWeight;
	}

	public void setParameters(double timeVioCost, double travelWeight, double waitWeight, double maxSpanWeight, double loadBalanceWeight) {
//		this.timeVioCost = timeVioCost;
//		this.initialTimeVioWeight = timeVioCost;
//		this.travelWeight = travelWeight;
//		this.waitWeight = waitWeight;
//		this.maxSpanWeight = maxSpanWeight;
//		this.loadBalanceWeight = loadBalanceWeight;
	}



	public void setObjFlag(int objFlag) {
		this.objFlag = objFlag;
	}

	public ArrayList<Integer> humanU(HashSet<Order> initial) {
		ArrayList<Integer> remove = new ArrayList<Integer>();
		for (Order node : initial)
			remove.add(node.id);
		return remove;
	}

	public void setAllStartEnd(Set<Order> startEndNodes) {
		this.startEndNodes = startEndNodes;
	}

	public ArrayList<Integer> humanU(ArrayList<Order> initial) {
		ArrayList<Integer> remove = new ArrayList<Integer>();
		for (Order node : initial)
			remove.add(node.id);
		return remove;
	}

	public Set<Integer> cloneSet(Set<Integer> keySet) {
		Set<Integer> newKeySet = new HashSet<Integer>();
		for (Integer p : keySet) {
			newKeySet.add(p);
		}
		return newKeySet;
	}
	

//	public void setOrdersVehicles(ArrayList<Order> orders, ArrayList<Vehicle> vehicles) {
//		this.allOrders = orders;
//		this.setOrdersVehicles= vehicles;
//		
//	}

}
