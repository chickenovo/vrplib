package Data;

public class Edge {
	private Node destination;
	private double weight;

	public void setDestination(Node node) {
		this.destination = node;

	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
}
