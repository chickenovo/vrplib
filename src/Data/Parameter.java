package Data;

public class Parameter {
	double travelWeight = 1;
	double waitWeight = 0.2;
	public double timeVioCost = 10;
	double maxSpanWeight = 2;
	double loadBalanceWeight = 0;

	public Parameter(double travelWeight, double waitWeight, double timeVioCost, double maxSpanWeight,
			double loadBalanceWeight) {
		this.travelWeight = travelWeight;
		this.waitWeight = waitWeight;
		this.setTimeVioCost(timeVioCost);
		this.maxSpanWeight = maxSpanWeight;
		this.loadBalanceWeight = loadBalanceWeight;
	}

	public double getTimeVioCost(double violate) {
		//System.out.println("timeVioCost "+timeVioCost);
		if(violate>60&&violate<=120)
			return timeVioCost*10*violate;
		if(violate>120)
			return timeVioCost*20*violate;
		return timeVioCost*violate;
	}

	public void setTimeVioCost(double timeVioCost) {
		this.timeVioCost = timeVioCost;
	}

	public double getTravelWeight() {
		return travelWeight;
	}

	public double getWaitWeight() {
		// TODO Auto-generated method stub
		return waitWeight;
	}

	public double getMaxSpanWeight() {
		// TODO Auto-generated method stub
		return maxSpanWeight;
	}

	public double getLoadBalanceWeight() {
		// TODO Auto-generated method stub
		return loadBalanceWeight;
	}

	public void setWaitWeight(double d) {
		this.waitWeight = d;
	}

}
