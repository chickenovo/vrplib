package Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class KMedoids {
	int numberOfClusters = 0;
	int maxIterations = 100;
	double[][] distance;
	Random rg;
	double sumAllDistance = 0;
	List<ArrayList<Integer>> assignment = new ArrayList<ArrayList<Integer>> ();
	public KMedoids(int numberOfClusters, int maxIterations, double[][] distance) {
		this.numberOfClusters = numberOfClusters;
		this.maxIterations = maxIterations;
		this.distance = distance;
		rg = new Random(System.currentTimeMillis());
	}

	public List<ArrayList<Integer>> cluster(List<Order> data) {
		
		//System.out.println("in cluster");
		HashSet<Order> oldMedoids = new HashSet<Order>();
		int random = 0;
		int maxCnt = 0;
		//System.out.println(oldMedoids.size()+" "+numberOfClusters);
		while (oldMedoids.size() < numberOfClusters&&maxCnt<20) {
			random = rg.nextInt(data.size());
			oldMedoids.add(data.get(random));
			maxCnt++;
		}
		//System.err.println(oldMedoids.size());
		List<Order> medoids = new ArrayList<Order>();
		for (Order item : oldMedoids)
			medoids.add(item);
		data.removeAll(medoids);

		boolean changed = true;
		int count = 0;

		assignment = assign(medoids, data);
//		data.addAll(medoids);
		while (count < maxIterations) {
			count++;
			recalculateMedoids(medoids, data);
		}
		for(int i=0;i<assignment.size();i++)
			assignment.get(i).add(medoids.get(i).id);
		//System.err.println(assignment);
		return assignment;

	}

	private List<ArrayList<Integer>> assign(List<Order> medoids, List<Order> data) {
		List<ArrayList<Integer>> out = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < medoids.size(); i++)
			out.add(new ArrayList<Integer>());
		sumAllDistance = 0;
		for (int i = 0; i < data.size(); i++) {
			double bestDistance = distance[data.get(i).orderLocation()][medoids.get(0).orderLocation()];
			int bestIndex = 0;
			for (int j = 1; j < medoids.size(); j++) {
				double tmpDistance = distance[data.get(i).orderLocation()][medoids.get(j).orderLocation()];
				if (tmpDistance < bestDistance) {
					bestDistance = tmpDistance;
					bestIndex = j;
				}
			}
			out.get(bestIndex).add(data.get(i).id);
			sumAllDistance += bestDistance;
		}
		return out;

	}

	private boolean recalculateMedoids(List<Order> medoids, List<Order> data) {
		boolean changed = false;
		int newChosenMedoidsIndex = 0;
		Order newChosenMedoids;
		int random = 0;
		double bestDistance = 0;
		Order storedValueDataOld;

		Integer storedIndexMedoidsOld;
		Order storedValueMedoidsOld;
		List<ArrayList<Integer>> newAssignment = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < numberOfClusters; i++) {
			if(data.size()<2)
				continue;
			random = rg.nextInt(data.size());
			newChosenMedoids = data.get(random);
			data.remove(random);
			storedValueDataOld = newChosenMedoids;

			newChosenMedoidsIndex = rg.nextInt(medoids.size());
			bestDistance = sumAllDistance;
			data.add(medoids.get(newChosenMedoidsIndex));
			storedIndexMedoidsOld = newChosenMedoidsIndex;
			storedValueMedoidsOld = medoids.get(newChosenMedoidsIndex);

			medoids.set(newChosenMedoidsIndex, newChosenMedoids);
			newAssignment = assign(medoids, data);
			
			if (sumAllDistance > bestDistance) {
				data.remove(data.size() - 1);
				data.add(storedValueDataOld);
				medoids.set(storedIndexMedoidsOld, storedValueMedoidsOld);
			} else
				assignment = clone(newAssignment);
		}
		return changed;
	}

	public List<ArrayList<Integer>> clone(List<ArrayList<Integer>> newAssignment) {
		List<ArrayList<Integer>> tempSet = new ArrayList<ArrayList<Integer>>();
		int node = 0;
		for (ArrayList<Integer> path : newAssignment) {
			Iterator it = path.iterator();
			ArrayList<Integer> newSet = new ArrayList<Integer>();
			while (it.hasNext()) {// tabuInsert(route, node);
				node = (Integer) it.next();
				newSet.add(node);
			}
			tempSet.add(newSet);
		}
		return tempSet;
	}

}
