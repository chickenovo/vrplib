from __future__ import print_function
# import sys
# !{sys.executable} -m pip install ortools
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import numpy as np
import pandas as pd
import json
import math
from threading import Thread


# Methods
def DistanceInKm(lat1, lon1, lat2, lon2):
    R = 6373.0
    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat2)
    lon2 = math.radians(lon2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    # print("Result:", R * c, "km")
    return R * c


def DistanceMatrixByHaversine(places):
    D = [[0 for x in range(len(places.index))] for y in range(len(places.index))]
    total = 0
    count = 0
    minD = 100
    for i in range(len(places.index)):
        for j in range(len(places.index)):
            D[i][j] = DistanceInKm(places.Latitude[i], places.Longitude[i],
                                   places.Latitude[j], places.Longitude[j])
            if places.Id[i] == 0 or places.Id[j] == 0:
                D[i][j] = 0
            count += 1
            total += D[i][j]
            if minD > D[i][j] > 0:
                minD = D[i][j]
    print('Average distance: ', total / count)
    print('Min distance: ', minD)
    return D


def print_solution(store, data, manager, routing, assignment):
    """Prints assignment on console."""
    time_dimension = routing.GetDimensionOrDie('Time')
    total_time = 0
    total_load = 0
    solution = {}
    for vehicle_id in range(data['num_vehicles']):
        solution[vehicle_id] = list()
        index = routing.Start(vehicle_id)
        time_load = 0
        route_load = 0
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            node_index = manager.IndexToNode(index)
            route_load += data['volume'][node_index]
            plan_output += '{0} Time({1},{2}) -> '.format(
                manager.IndexToNode(index), assignment.Min(time_var),
                assignment.Max(time_var))
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            time_load += routing.GetArcCostForVehicle(previous_index, index, vehicle_id)
            solution[vehicle_id].append(data['name'][node_index])

        time_var = time_dimension.CumulVar(index)
        plan_output += '{0} Time({1},{2})\n'.format(
            manager.IndexToNode(index), assignment.Min(time_var),
            assignment.Max(time_var))
        plan_output += 'Time of the route: {}min\n'.format(
            time_load)
        plan_output += 'Load of the route: {}l\n'.format(route_load)
        print(plan_output)
        total_time += assignment.Min(time_var)
        total_load += route_load

    print('Total time of all routes: {}min'.format(total_time))
    print('Total load of all routes: {}'.format(total_load))
    with open(store + '_solution.json', 'w') as fp:
        json.dump(solution, fp)


def main(store, numVehicles):
    # Preparing data
    places = pd.read_csv(store + ".csv")
    DOrg = DistanceMatrixByHaversine(places)
    timeCode = -1
    timeToCodeDict = {}
    codeToTimeDict = {}
    for i in range(12, 24):
        for j in range(0, 60):
            time = i * 100 + j
            timeCode += 1
            timeToCodeDict.update({str(time): timeCode})
            codeToTimeDict.update({str(timeCode): time})

    data = {}
    # Assuming driver's average speed to be 25km per hour
    data['name'] = places.Name
    data['time_matrix'] = np.ceil(np.dot(60 / 25, DOrg))
    data['time_windows'] = list()
    for i in range(len(places.index)):
        startTime = timeToCodeDict[places.DeliveryWindowStart[i].replace(':', '')]
        endTime = timeToCodeDict[places.DeliveryWindowEnd[i].replace(':', '')]
        data['time_windows'].append((startTime, endTime))

    data['num_vehicles'] = numVehicles
    data['volume'] = np.around(places.Volume.values)
    data['capacity'] = [100] * data['num_vehicles']
    data['depot'] = 0

    print("Solving.........")
    manager = pywrapcp.RoutingIndexManager(
        len(data['time_matrix']), data['num_vehicles'], data['depot'])
    # Create Routing Model
    routing = pywrapcp.RoutingModel(manager)

    # Add transit callback
    def time_callback(from_index, to_index):
        """Returns the travel time between the two nodes."""
        # Convert from routing variable Index to time matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['time_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(time_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)
    time = 'Time'
    routing.AddDimension(
        transit_callback_index,
        60,  # allow waiting time
        540,  # maximum time per vehicle (9hrs per day)
        False,  # Don't force start cumul to zero.
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    # time_dimension.SetGlobalSpanCostCoefficient(100)

    # Add time window constraints for each location except depot & stores.
    for location_idx, time_window in enumerate(data['time_windows']):
        if location_idx == 0:
            continue
        index = manager.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
        
    # Add time window constraints for each vehicle start node.
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        time_dimension.CumulVar(index).SetRange(data['time_windows'][0][0],
                                                data['time_windows'][0][1])
    for i in range(data['num_vehicles']):
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.Start(i)))
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.End(i)))

    # # Add Capacity constraints
    def volume_callback(from_index):
        # Convert from routing variable Index to Volume at NodeIndex
        from_node = manager.IndexToNode(from_index)
        return data['volume'][from_node]

    volume_callback_index = routing.RegisterUnaryTransitCallback(volume_callback)

    routing.AddDimensionWithVehicleCapacity(
        volume_callback_index,
        0,
        data['capacity'],
        True,
        'Capacity'
    )
    volume_dimension = routing.GetDimensionOrDie('Capacity')
    for node_index in [1, 2, 3, 4]:
        index = manager.NodeToIndex(node_index)
        volume_dimension.SlackVar(index).SetRange(0, 100)
        routing.AddDisjunction([node_index], 0)

    # Add fixed cost
    routing.SetFixedCostOfAllVehicles(100)

    # Setting 1st solution heuristic
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.time_limit.seconds = 300
    search_parameters.first_solution_strategy = (routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
    search_parameters.log_search = True
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    # Solve it
    result = True
    assignment = routing.SolveWithParameters(search_parameters)
    if assignment:
        print_solution(store, data, manager, routing, assignment)
    else:
        print("No Solution")
        result = False
    return result


if __name__ == '__main__':
    # numVehicles = 4
    # store = 'storeA'
    # print('Solving for ' + store)
    # while not main(store, numVehicles):
    #     numVehicles += 1

    # numVehicles = 3
    # store = 'storeB'
    # print('Solving for ' + store)
    # while not main(store, numVehicles):
    #     numVehicles += 1

    numVehicles = 3
    store = 'storeC'
    print('Solving for ' + store)
    while not main(store, numVehicles):
        numVehicles += 1


